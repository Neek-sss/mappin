from django.urls import path, re_path
from django.conf import settings

from . import views

urlpatterns = [
    re_path(r'^resources/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}
            ),
    path('', views.index, name='index'),
]

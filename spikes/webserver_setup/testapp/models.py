from django.db import models


class Test1(models.Model):
    name = models.CharField(max_length=128, primary_key=True, null=False)

    def __str__(self):
        return self.name


class Test2(models.Model):
    name = models.CharField(max_length=128, primary_key=True, null=False)
    data = models.ForeignKey(to=Test1, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

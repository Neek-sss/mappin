from django.test import TestCase
from .models import Test1, Test2


class BaseTestCase(TestCase):
    def test_assert(self):
        self.assertEqual(1, 1)

    def test_test1_data(self):
        x = Test1(name="Test Name")
        x.save()
        ret = Test1.objects.get(name="Test Name")
        self.assertEqual(ret.name, x.name)

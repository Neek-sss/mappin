from django.http import HttpResponse
from django.template import loader
from .models import Test1


def index(request):
    test_list = Test1.objects.order_by('name')
    template = loader.get_template('testapp/index.html')
    context = {
        'test_list': test_list,
    }
    return HttpResponse(template.render(context, request))

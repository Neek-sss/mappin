from django.apps import AppConfig


class FullstackTestConfig(AppConfig):
    name = 'fullstack_test'

from django.shortcuts import HttpResponse
from yattag import Doc, indent
from cssutils import css


def index(_):

    # Create the CSS Sheet
    sheet = css.CSSStyleSheet()
    style = css.CSSStyleDeclaration()
    style['color'] = 'red'
    rule = css.CSSStyleRule(selectorText=u'body', style=style)
    sheet.add(rule)

    # Create Brython
    brython = '''
    from browser import document, alert

    def echo(event):
        alert(document["zone"].value)

    document["mybutton"].bind("click", echo)
    '''

    # Create the HTML Document,
    # inserting the CSS and Brython
    doc, tag, text = Doc().tagtext()

    # Create the HTML header
    doc.asis('<!DOCTYPE html>')

    with tag('html', lang='en'):
        with tag('head'):
            with tag('meta', charset='UTF-8'):
                pass

            # Include CSS Stylesheet
            with tag('style'):
                text(str(sheet.cssText, 'UTF-8'))

            # Include Brython dependencies
            with tag('script', type='text/javascript',
                     src="/static/brython/brython.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/brython/brython_stdlib.js"):
                pass

        # Start Byrthon from the beginning
        with tag('body', onload='brython()'):

            # Run the Brython script
            with tag('script', type='text/python'):
                doc.asis(brython)

            # Other stuff
            with tag('h1'):
                text('Hello world!')

            # A button that the script interacts with
            with tag('input', id="zone"):
                with tag('button', id="mybutton"):
                    text('Click!')

    # Return the final HTML Document
    return HttpResponse(indent(doc.getvalue()), content_type="text/html")

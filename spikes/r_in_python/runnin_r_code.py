import rpy2.robjects as robjects

# r Variables

pi = robjects.r('pi')
print(pi[0])

# Functions

two_pi_r = robjects.r('''
        function(r, verbose=FALSE) {
            if (verbose) {
                cat("I am calling f().\n")
            }
            2 * pi * r
        }
        ''')

print(two_pi_r(3)[0])

# Interop r and Python

letters = robjects.r('letters')
rcode = 'paste(%s, collapse="-")' % (letters.r_repr())
print(robjects.r(rcode)[0])

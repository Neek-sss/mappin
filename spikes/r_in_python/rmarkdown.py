import os

import rpy2.robjects as robjects

cwd = os.getcwd()

robjects.r("setwd('%s')" % cwd)
robjects.r("rmarkdown::render('example.rmd',  output_format = c('html_document', 'pdf_document', 'word_document'))")

import rpy2.robjects.lib.ggplot2 as ggplot2
from rpy2.robjects.lib import grid
from rpy2.robjects.packages import data
from rpy2.robjects.packages import importr

grid.activate()

base = importr('base')
datasets = importr('datasets')

mtcars = data(datasets).fetch('mtcars')['mtcars']

pp = ggplot2.ggplot(mtcars) + \
     ggplot2.aes_string(x='wt', y='mpg', col='factor(cyl)') + \
     ggplot2.geom_point() + \
     ggplot2.geom_smooth(ggplot2.aes_string(group='cyl'),
                         method='lm')
pp.plot()

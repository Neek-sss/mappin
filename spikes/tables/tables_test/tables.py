import django_tables2 as tables
from .models import Person


class PersonTable(tables.Table):
    change = tables.TemplateColumn('''
            <a href="/schedule/update_schedule/{{ record.id }}">
                Update
            </a>
            <a href="/schedule/delete_schedule/{{ record.id }}"
                onclick="return confirm("Are you sure you want to delete this?")">
                Delete
            </a>''',
                                   verbose_name=u'Change')

    class Meta:
        model = Person
        template_name = 'django_tables2/bootstrap.html'

from django.apps import AppConfig


class TablesTestConfig(AppConfig):
    name = 'tables_test'

from browser import window, document, alert, timer

Math = window.Math

Map = window.ol.Map
View = window.ol.View
get_center = window.ol.extent.getCenter
ImageLayer = window.ol.layer.Image
Projection = window.ol.proj.Projection
ImageStatic = window.ol.source.ImageStatic

# Size of the view of the map
extent = [
    0,  # Left
    0,  # Bottom
    1920,  # Top
    1080,  # Right
]

# The view of the map
projection = Projection.new({
    'code': 'test-image',
    'units': 'pixels',
    'extent': extent
})

map = Map.new({
    'layers': [
        ImageLayer.new({
            'source': ImageStatic.new({
                'url': '/static/test.png',
                'projection': projection,
                'imageExtent': extent
            })
        })
    ],
    'target': 'map',
    'view': View.new({
        'projection': projection,
        'center': get_center(extent),
        'zoom': 2,
        'maxZoom': 8
    })
})

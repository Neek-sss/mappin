from django.shortcuts import HttpResponse
from yattag import Doc, indent
from cssutils import css
from os import path


def index(_):
    # Load Brython
    map_script = open(path.join(path.dirname(__file__), 'frontend/map.py'), mode='r').read()

    # Create the HTML Document,
    # inserting the CSS and Brython
    doc, tag, text = Doc().tagtext()

    # Create the HTML header
    doc.asis('<!DOCTYPE html>')

    with tag('html', lang='en'):
        with tag('head'):
            with tag('meta', charset='UTF-8'):
                pass

            # Include OpenLayers CSS
            with tag('link', rel='stylesheet', href='/static/OpenLayers v5.3.0/ol.css'):
                pass

            # Include Brython dependencies
            with tag('script', type='text/javascript',
                     src="/static/brython/brython.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/brython/brython_stdlib.js"):
                pass

            # Include OpenLayers
            with tag('script', type='text/javascript',
                     src="/static/OpenLayers v5.3.0/ol.js"):
                pass

        # Start Byrthon from the beginning
        with tag('body', onload='brython(1)'):
            with tag('h1'):
                text('Hello world!')

            with tag('div', id='map', klass='map'):
                pass

            # Run the Brython script
            with tag('script', type='text/python'):
                doc.asis(map_script)

    # Return the final HTML Document
    return HttpResponse(indent(doc.getvalue()), content_type="text/html")

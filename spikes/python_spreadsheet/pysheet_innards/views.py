from django.shortcuts import HttpResponse
from yattag import Doc, indent
from cssutils import css
from os import path


def index(_):
    # Create the CSS Sheet
    sheet = css.CSSStyleSheet()
    # style = css.CSSStyleDeclaration()
    # style['color'] = 'red'
    # rule = css.CSSStyleRule(selectorText=u'body', style=style)
    # sheet.add(rule)
    #
    # # Load Brython
    dialog_script = open(path.join(path.dirname(__file__), 'frontend/dialog.py'), mode='r').read()
    menu_script = open(path.join(path.dirname(__file__), 'frontend/menu.py'), mode='r').read()
    spreadsheet_script = open(path.join(path.dirname(__file__), 'frontend/spreadsheet.py'), mode='r').read()

    # Create the HTML Document,
    # inserting the CSS and Brython
    doc, tag, text = Doc().tagtext()

    # Create the HTML header
    doc.asis('<!DOCTYPE html>')

    with tag('html', lang='en'):
        with tag('head'):
            with tag('meta', charset='UTF-8'):
                pass

            # Include SlickGrid CSS
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/css/smoothness/jquery-ui-1.11.3.custom.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/slick.grid.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/slick-default-theme.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/examples.css'):
                pass

            # Include custom CSS Stylesheet
            with tag('style'):
                text(str(sheet.cssText, 'UTF-8'))

            # Include SlickGrid dependencies
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/lib/jquery-ui-1.11.3.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/lib/jquery.event.drag-2.3.0.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/lib/jquery.event.drop-2.3.0.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/slick.core.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/slick.grid.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/slick.editors.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/slick.formatters.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/slick.dataview.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/plugins/slick.cellrangedecorator.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/plugins/slick.cellrangeselector.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/SlickGrid-2.4.3/plugins/slick.cellselectionmodel.js"):
                pass

            # Include Brython dependencies
            with tag('script', type='text/javascript',
                     src="/static/brython/brython.js"):
                pass
            with tag('script', type='text/javascript',
                     src="/static/brython/brython_stdlib.js"):
                pass

        # Start Byrthon from the beginning
        with tag('body', onload='brython(1)'):
            with tag('h1'):
                text('Hello world!')

            with tag('div', id="zone"):
                pass

            # Run the Brython script
            with tag('script', type='text/python'):
                doc.asis(dialog_script)
                doc.asis(menu_script)
                doc.asis(spreadsheet_script)

    # Return the final HTML Document
    return HttpResponse(indent(doc.getvalue()), content_type="text/html")

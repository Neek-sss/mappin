from django.apps import AppConfig


class PysheetInnardsConfig(AppConfig):
    name = 'pysheet_innards'

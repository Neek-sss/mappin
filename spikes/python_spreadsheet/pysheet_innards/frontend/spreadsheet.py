from browser import document, html, console, window
import random


def table_grid():
    table_div = html.DIV(id='tables', style={'width':'600px','height': '500px'})
    return table_div


def open_table():
    number_of_tables = len(document.select_one('#tables').children)
    table_name = 'table_' + str(number_of_tables)
    add_grid(table_name, number_of_tables)


def add_row(grid, row):
    data_view = grid.getData()
    data_view.beginUpdate()
    grid.invalidateRow(data_view.getLength())
    data_view.addItem(row)
    data_view.endUpdate()
    grid.updateRowCount()
    grid.render()


def add_grid(table_name, number_of_tables):
    Slick = window.Slick
    # TODO: Don't just append a DIV; actually do it right
    document.select_one('#tables') <= html.DIV(id=table_name)

    columns = [
        {
            'id': 'id',
            'name': 'ID',
            'field': 'id',
            'width': 80,
            'editor': Slick.Editors.Text,
        },
        {
            'id': 'title',
            'name': 'Title',
            'field': 'title',
            'width': 120,
            'cssClass': 'cell-title',
            'editor': Slick.Editors.Text,
        },
        {
            'id': 'desc',
            'name': 'Description',
            'field': 'description',
            'width': 100,
            'editor': Slick.Editors.LongText
        },
        {
            'id': 'duration',
            'name': 'Duration',
            'field': 'duration',
            'editor': Slick.Editors.Text
        },
        {
            'id': '%',
            'name': '% Complete',
            'field': 'percentComplete',
            'width': 80,
            'resizable': False,
            'formatter': Slick.Formatters.PercentCompleteBar,
            'editor': Slick.Editors.PercentComplete
        },
        {
            'id': 'start',
            'name': 'Start',
            'field': 'start',
            'minWidth': 60,
            'editor': Slick.Editors.Date
        },
        {
            'id': 'finish',
            'name': 'Finish',
            'field': 'finish',
            'minWidth': 60,
            'editor': Slick.Editors.Date
        },
        {
            'id': 'effort-driven',
            'name': 'Effort Driven',
            'width': 80,
            'minWidth': 20,
            'maxWidth': 80,
            'cssClass': 'cell-effort-driven',
            'field': 'effortDriven',
            'formatter': Slick.Formatters.Checkmark,
            'editor': Slick.Editors.Checkbox
        }
    ]

    options = {
        'editable': True,
        'autoEdit': True,
        'enableAddRow': True,
        'leaveSpaceForNewRows': True,
        'asyncEditorLoading': False,
        'enableCellNavigation': True,
        'enableCellRangeSelection': True,
        'multiSelect': True,
        'rerenderOnResize': True,
    }

    data = []

    for i in range(10):
        d = {
            'id': "id_" + str(i),
            'title': "Task " + str(i),
            'description': "This is a sample task description.\n  It can be multiline",
            'duration': "5 days",
            'percentComplete': round(random.random() * 100),
            'start': "01/01/2009",
            'finish': "01/05/2009",
            'effortDriven': (i % 5 == 0),
        }
        data.append(d)

    data_view = Slick.Data.DataView.new()
    data_view.beginUpdate()
    data_view.setItems(data)
    data_view.endUpdate()

    grid = Slick.Grid.new('#tables', data_view, columns, options)
    grid.setSelectionModel(Slick.CellSelectionModel.new())
    grid.onAddNewRow.subscribe(lambda e, args: add_row(grid, args.item))


def new_table_button():
    return html.INPUT(id='new_table_button', type='button', value='+')


document['zone'] <= table_grid()
document['zone'] <= new_table_button()
open_table()

from browser import document, html, console


def menu():
    menu = html.DIV(id='menu')
    menu.append(html.INPUT('workbook_name', type='text', value='Workbook 1'))
    menu.append(html.INPUT('save', type='button', value='Save'))

    open_button = html.INPUT('open', type='button', value='Open')
    open_button.bind('click', activate_open_dialog)
    menu.append(open_button)

    menu <= html.INPUT('new', type='button', value='New')
    return menu


document['zone'] <= menu()

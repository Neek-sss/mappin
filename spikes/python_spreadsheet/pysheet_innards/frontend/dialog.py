from browser import document, html, console


def activate_open_dialog(event):
    open_dialog_element = document.select_one('#open_dialog')
    del open_dialog_element.attrs['close']
    open_dialog_element.attrs['open'] = None


def cancel_open_dialog(_):
    open_dialog_element = document.select('#open_dialog')[0]
    del open_dialog_element.attrs['open']
    open_dialog_element.attrs['close'] = None


def ok_open_dialog(_):
    pass


def open_dialog():
    form = html.FORM()
    form <= html.SELECT(id='workbook_list', name='workbook_list')

    cancel_button = html.BUTTON('Cancel')
    cancel_button.bind('click', cancel_open_dialog)

    ok_button = html.BUTTON('Ok')
    ok_button.bind('click', ok_open_dialog)

    dialog = html.DIALOG(id='open_dialog', autoOpen=False, close=None)
    dialog <= html.P('Select an archive')
    dialog <= form
    dialog <= ok_button
    dialog <= cancel_button

    return dialog


document['zone'] <= open_dialog()

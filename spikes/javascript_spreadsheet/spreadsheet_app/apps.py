from django.apps import AppConfig


class SpreadsheetAppConfig(AppConfig):
    name = 'spreadsheet_app'

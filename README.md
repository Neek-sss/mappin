# Mappin: An Interactive Inventory Software

A web-based inventory software that allows asset management from both spreadsheets and maps.

Documentation can be found in the *docs* folder.

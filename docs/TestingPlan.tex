% Testing Plan
\documentclass{article}
\title{\Huge Testing Plan: \\* \huge Inventory Management Software}
\author{Brandi Kennedy  \&  Nicholas Yahr}
\date{April 2019}

\usepackage{multicol}
\usepackage[margin=1in]{geometry}
\usepackage{mdframed}
\usepackage{xcolor}


\begin{document}

\maketitle

\begin{center}
\line(1,0){450}
\end{center}
\begin{abstract}
\noindent This testing plan will cover our project and its functionality, as well as our specific plans for testing our
software. Our inventory management software could be useful for many companies, but will be utilized at
Otsego Public Schools. It will organize data including cost, model and manufacturer and will display
them on a customized map. Reports will be exportable. Our plan shows step by step how we intend
to implement tests for our software. Tests will include unit testing, system testing, accessibility testing,
usability testing and black box testing. When all testing is complete, our software should function how
it is intended to.
\end{abstract}
\begin{center}
\line(1,0){450}
\end{center}
\newpage

\tableofcontents
\newpage

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Introduction}
\end{mdframed}
Testing is a very important part of software development and there are many things to consider whilst
planning your testing and implementing it. Testing will easily show you the errors and bugs within your
system and will make it much easier to catch flaws. It will allow you to make sure your software is meeting
all of the customers expectations and you are producing a quality product. Testing can save you a lot of
time and money because it prevents your software from breaking and finds problems before the software
is live. This can even prevent future bugs found by users. In this paper we will discuss how testing will
be utilized in Mappin, our inventory management system. We will be planning for various types of testing
including unit, system, accessibility, usability and black box. We address all parts of testing including what
each test focuses on, its objective, our planned technique, how we know its completed and considerations we
may need to take into account or address.

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Background}
\end{mdframed}
Many, if not most, companies have a need to keep track of inventory for financial and logistical reasons,
but existing inventory software is mostly based around a list of items with associated data. While data
such as cost, model, and manufacturer is important for reporting purposes, the inventory is less useful for
internal reference where the physical location of the item is needed. That is the problem that Mappin strives
to solve. Mappin integrates a spreadsheet-based data view that holds the bulk of the inventory data with
a user-defined map for quick reference and organization. The recorded data can also be used to generate
custom reports using provided reporting modules.

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Unit Testing}
\end{mdframed}
Unit testing will focus on finding errors in the following areas:
\begin{itemize}
\item Individual units/components of the software
\end{itemize}
Unit testing is the testing of individual software components. It is performed by the software developers and
is the first level of software testing. It helps maintain confidence when changing/writing code.

	\subsection{Test Objective}
Validate that all units of the software perform as expected.

	\subsection{Technique}
\begin{itemize}
\item Choose unit test framework - Python's built in ``unittest'' should suffice
\item Write tests before beginning the software code and be sure to test every component
\item Keep tests up to date and well documented
\end{itemize}

	\subsection{Completion Criteria}
\begin{itemize}
\item Components pass all tests
\item Failed tests are addressed and retested until they pass
\end{itemize}

	\subsection{Special Considerations}
\begin{itemize}
\item Unit testing is time consuming and should be completed before you begin your code
\item Can be modified if stories change or modifications are made while developing
\end{itemize}

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{System Testing}
\end{mdframed}
System testing will focus on finding errors in the following areas:
\begin{itemize}
\item Item access
\item Editing
\item Addition
\item Removal
\end{itemize}
System testing will focus on the overall functionality of the website as it is accessed by the user. Item access,
editing, addition, and removal will be tested in both the spreadsheet and map interface. A team independent
of the development team will perform this testing.

	\subsection{Test Objective}
Test for single-building inventories as well as multiple-building inventories. Report functionality will also be
tested and the returned reports verified. This functionality will be tested for multiple browsers and both
Windows-based and Linux-based systems. Finally, security of the website will be tested to ensure that access is
properly restricted on a per-user basis.

	\subsection{Technique}
\begin{itemize}
\item Selenium full testing scripts will be created and ran continually
\item Use stories to guide testing and behavior of the system as a whole
\end{itemize}

	\subsection{Completion Criteria}
\begin{itemize}
\item The client's experience is what they expected
\item Correct software if it does not function properly
\item All stories can be completed and are user friendly
\end{itemize}

	\subsection{Special Considerations}
\begin{itemize}
\item Should not cause any severe drops in the speed of the process because of the ease in prototyping a
Python-based web server
\end{itemize}

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Accessibility Testing}
\end{mdframed}
Accessibility testing will focus on finding errors in the following areas:
\begin{itemize}
\item Speech recognition
\item Screen reader
\item Screen magnification
\item Special keyboard
\end{itemize}
Accessibility testing ensures that the tested application/web interface is usable for people with disabilities
including deafness, color blindness, blindness, old age and other disabilities. Accessibility testing can be
automated and done manually. Most automated tests do not catch everything a person might, so it is good
to do both types of testing.

	\subsection{Test Objective}
Ensure application supports people with vision, physical, cognitive, literacy and hearing disabilities.

	\subsection{Technique}
\begin{itemize}
\item Web Accessibility Evaluation tool (WAVE) will be utilized to validate the web page manually for various types of accessibility
\item Manually inspect accessibility tools to ensure they are performing properly
\end{itemize}

	\subsection{Completion Criteria}
\begin{itemize}
\item All tests are executed
\item  All errors displayed by WAVE are addressed and corrected
\item All applications are manually inspected and perform properly
\end{itemize}

	\subsection{Special Considerations}
\begin{itemize}
\item Testing will require WAVE, a browser extension that evaluates accessibility
\item The process must be invoked manually and could lead to human error
\end{itemize}

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Usability Testing}
\end{mdframed}
Usability testing will focus on finding errors in the following areas:
\begin{itemize}
\item Ease of task performance for a user
\item User experience is exactly what the client desires
\end{itemize}
Usability testing will include pre-determined tasks based on tasks that the client expects to perform on the finished product. With no additional information or guidance the users will attempt to perform these tasks and will report back any difficulties that they had in performing them.

	\subsection{Test Objective}
To ensure that the users experience with the program is positive and that all tasks can be completed by the
user without additional guidance.

	\subsection{Technique}
\begin{itemize}
\item Working alpha and beta versions of the product will be provided to select users along with a series of
tasks they must perform
\item User is observed by developer/the team
\item User offers feedback on their experience and thought process while performing the given tasks 
\end{itemize}

	\subsection{Completion Criteria}
\begin{itemize}
\item Information collected is used to improve the user experience and fine-tune the placement of interactive
elements
\item Potential issues are addressed and retested
\end{itemize}

	\subsection{Special Considerations}
\begin{itemize}
\item Testing is not 100\% representative to a real life scenario
\item The feedback is only from a few chosen users 
\end{itemize}

\begin{mdframed}[backgroundcolor=black!10,rightline=false,leftline=false]
\section{Black Box Testing}
\end{mdframed}
Black box testing will focus on finding errors in the following areas:
\begin{itemize}
\item Missing/incorrect functions
\item Interface errors
\item Behavior/performance
\item Initialization errors
\end{itemize}
Black box testing tests the functions of a website without the tester having knowledge of the internal structure
of the system. The tester is usually a developer who is not on the projects developing team.

	\subsection{Test Objective}
Uncover behavioral or performance discrepancies in the specifications from a users point of view. Focus on
the inputs and outputs of the software based on the software requirements and specifications.

	\subsection{Technique}
\begin{itemize}
\item Examine requirements and specifications of the system
\item Develop test cases and execute
\end{itemize}

	\subsection{Completion Criteria}
\begin{itemize}
\item All identified discrepancies have been addressed
\item System is re-tested after bugs are fixed
\end{itemize}

	\subsection{Special Considerations}
\begin{itemize}
\item Chances of ignoring possible conditions of the scenario to be tested
\item Complete Test Coverage is not possible for large and complex projects
\end{itemize}



\end{document}
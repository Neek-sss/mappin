# Managing the Backend Code

The backend code is organized in a fashion based on [Django](https://www.djangoproject.com/)'s
recommended structure within the two directories *server* and *mappin*.
*server* holds the basic files required in order to run a [Django](https://www.djangoproject.com/) server,
while *mappin* contains all of the code specific to running the `Mappin`
site.  The following two sections cover these directories.

### Server

Within this directory are four unique files that work together to run
a server session: *dev-settings.py*, *serve-settings.py*, *urls.py*, and
*wsgi.py*.  Following is an explanation of each file:

* *dev-settings.py* - The settings that are used when running [Django](https://www.djangoproject.com/)'s
default development server.  This dev server is an insecure, low-setup
server that only allows basic features but is useful to quickly test
code as it is being developed.  This should rarely if ever need to be
changed, but instructions on this file can be found at
[Django's settings file page](https://docs.djangoproject.com/en/2.1/ref/settings/).

* *serve-settings.py* - The settings used when running the *real*
production server.  It comes pre-configured with [Django](https://www.djangoproject.com/)'s security
settings and the settings required to run `Mappin`, however some
configuration is required in order to set up the serve in order to run
correctly and securely.  These steps can be found in
[Serving the Site](./serving.md).

* *urls.py* - This declares all urls that can be accessed through this
server.  Upon opening this file, one will notice that it is quite empty.
That is because it points all requests towards either [Django](https://www.djangoproject.com/)'s admin
url or `Mappin`'s specific urls, listed in the *mappin* directory.

* *wsgi.py* - This is a bit of code glue that allows servers to run
[Django](https://www.djangoproject.com/) websites by an internal WSGI application link (used by
[cherrypy](https://cherrypy.org/)).

### Mappin

This directory is a little more involved, as this is where all the
"interesting" `Mappin`-side code happens.  It contains files and
sub directories which will be covered in that order.

In terms of files, this directory contains the following:

* *admin.py* - Defines all of the database objects that are accessible
through [Django]](https://www.djangoproject.com/)'s Admin portal

* *app.py* - A code-glue file that defines the *mappin* folder as an
"app" that [Django]](https://www.djangoproject.com/) can use.

* *models.py* - This file contains all of the database object
definitions used by `Mappin`, explained in more depth in
[Database Objects](./database.md).

* *urls.py* - This file is where the definition of all urls used by
*Mappin* reside.  Each of these urls are accessible through the pattern
`<site>/mappin/<url>`.  NOTE: Urls ending in a `/` can only be accessed
if the `/` is included!

The folders are covered in the following sub sections.

##### Frontend

There are a number of files within this directory that are split into a
number of categories.  Each of these categories are explained below.
It is left to the developer to figure out the exact details of each
file, but there isn't much difference between them, just specific
labels.

* *\*.xcf* - These are source files for the icons used within `Mappin`,
provided for simple editing in case someone wants to change them.  They
were created with [GIMP](https://www.gimp.org/)

* *load\*.js* - These are the JavaScript (JS) files for loading the *WASM*
code generated for the frontend.  Eventually these *may* not be needed
if the *WASM* files can be loaded directly, but for now the files are
loaded using JS and then run through these files.

* *report.js* - This specific file manages the reporting functionality
provided in the report panel.  May be transitioned to WASM in another
life.


##### Migrations (generated)

This is an automatically generated directory created when the command

```
python3 src/manage.py makemigrations
```

is run.  These files dictate how a database is to be created, and more
importantly, how data should be transferred from one version of the
database to another.  More information on this process can be found at
[Django's site](https://docs.djangoproject.com/en/2.2/topics/migrations/).

##### Static

The *static* directory contains all of the external dependencies
required in order to run the client.  This data is requested from the
server from `<site>/static/<resource>`.  The *images* subdirectory
contains all of the internally created icons.  Also included are the
[SlickGrid](https://github.com/6pac/SlickGrid) and [OpenLayers](https://openlayers.org/) libraries, and two internally created
CSS stylesheets.

##### Tests

This directory holds all tests that are provided to run on the server.
These tests are split between unit tests (that are very quick, simple
tests on specific components of the program) and integration tests (that
test the server as a whole, creating a test server and database and
using [Selenium](https://www.seleniumhq.org/) to automate the frontend interaction).  Also, a
*helpers.py* file contains a few functions that make writing these tests
a little easier.

##### Views

In here lies all of the code that actually runs the server.  Each of
these *views* is attached to a url so that the code within each view is
run only when the url is accessed by a client.  Following is a *general*
idea of what each of these source files contain.

* *api.py* - This file contains the views associated with adding,
updating, and deleting accepted database object types.  For some types
histories are kept in order to provide real-time updating to the client.

* *helpers.py* - This provides some simple helper functions and views
that don't do a lot of work in-and-of themselves, but make code a little
cleaner when they are separated away.

* *interface.py* - The sole purpose of this file is to provide the
interface view.

* *loaders.py* - This file provides loaders for database objects and
history.

* *login.py* - This file provides the login view as well as the views
to perform login/logout requests.

* *report.py* - An as-of-yet empty file that will provide any necessary
server-side processing for reporting.

* *settings.py* - Provider of the settings page to the client.

* *uploaders.py* - Provides the ability to upload images to the server,
accessed by the `<site>/media/<image key>` url.

* *user_management.py* - Provides the views for secure user management.
Many of these views could probably be subsets of the *api* file, but
would be more difficult to manage security in so it seemed better to
handle them in a case-by-case basis.

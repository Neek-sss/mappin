# Project Definition

**Title**: Inventory Management Software (Mappin)

**Date**: 2/6/2019

**Team**: Brandi Kennedy and Nick Yahr

**Client**: Otsego Public Schools Technology Department

**Abstract**: A web-based inventory and asset management software
that provides both a spreadsheet and map input system.

**Description**: In order to make the user experience as usable
as possible, both a graphic map-based and spreadsheet user
interface should be provided.  Integration with Google Sheets
would a nice feature.  Items should be able to be added, removed,
and edited from specific buildings.  Each department should only
be able to see the items that they are responsible for.  The
information that can be attached to each item should include
an Asset ID, Serial Number, Manufacturer, Make, Model, and 
urchase Date.  Information regarding warranty and repairs would
be additionally helpful.  All of this data should be generated
into reports that can be customized with provided report "modules."
Being able to use cameras from mobile devices to scan some
data/barcodes would be a nice feature for mobile devices. Finally,
the software should have logins for each user and keep track of
the amount of data that each user adds for a later "gamification"
feature including points for data added and a leaderboard.

**Legal Requirements**: 508 and FERPA Compliance

**System Requirements**: Windows-compatible server, PC- and
mobile-compatible web-based client

**Licensing**: Open-Source, MIT License

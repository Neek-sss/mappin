# Backups

Backups must be scheduled by the system administrator using cron or
Event Scheduler, depending on the system.  If automatic backups are not
scheduled, then manually run backups can be run but must be run regularly
to prevent data loss.  Backups can be run and data restored as detailed
below

### Database

In order to back up the database, run the following command:

```
python3 src/manage.py dbbackup
```

This will create a backup file in the backup folder named in the form
`<HOSTNAME>-<DATETIME>.dump`.

In order to restored the data to the server,
first delete/move/rename the current database file.  Then run the command:

```
python3 src/manage.py dbrestore
```

twice.  There will be some "errors" as the command runs, but these can
be safely ignored.  Once this completes, the database data has been
restored!


### Media Directory

In addition to database backups, the server can also backup the media
directory, saving all of the item/building images.  This backup can be
performed by running the command:

```
python3 src/manage.py mediabackup
```

This will create a file of the form `<HOSTNAME>-<DATETIME>.tar`.

To restore the media directory to the server, run:

```
python3 src/manage.py mediarestore
```

once.  The images should appear in the directory automatically.

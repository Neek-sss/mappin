# Testing

Testing was performed in a number of different areas focusing on unit,
integration, accessibility, and usability.  Unit and integration testing
is automated and can be run using the following commands.  Accessibility
testing was performed using the Firefox extension [WAVE](https://wave.webaim.org/).  Usability
testing was performed with live testers.

### Server

* Ensure that geckodriver is installed
* Run:
```
python3 src/manage.py test mappin.tests.unit
python3 src/manage.py test mappin.tests.integration
```

### Frontend

* Perform the setup options in [Managing the Frontend Code](./frontend.md)
* Run:
```
cargo test
cargo make test
```

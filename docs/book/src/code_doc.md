# Code Documentation

### Python Documentation

All of the python code is documented using the [Doxygen](http://www.doxygen.nl/) format.  In
addition inner code comments explain what is happening within the code
and functions and variables are named in order to be as readable as
possible.

### Rust Documentation

The Rust code is documented in the standard Rust documentation format
(very similar to [Doxygen](http://www.doxygen.nl/)). The code is also similarly documented and
named to the python in order to be as easy to use as possible. Code
documentation can be automatically generated with just a couple of
commands.

Run the following in each of the [Rust](https://www.rust-lang.org/) modules to
generate and open module documentation in the browser.

```
cargo doc --no-deps --open
```

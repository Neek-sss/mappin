# Testing Script

* Login to the `Mappin` site with the provided credentials
* Create the following three buildings with their items
* Give each building a map
* Put all of the items on the map in an appropriate location
* Give all of the items an image
* Archive all of the *Optiplex 380* computers and replace them with
the listed *Optiplex 7010*'s.
* Bring back one of the *Optiplex 380*'s to *Branch Office 1* and give
it the name *103-INTERN*
* Go to *Branch Office 2* and switch *101-PRESENTER* and *103-OFFICE*,
renaming the computers so that they match their new locations

### Central Office

| Name          | Manufacturer | Make     | Model | Year | Serial Number | Price | Obtained   |
| ------------- | ------------ | -------- | ----- | ---- | ------------- | ----- | ---------- |
| FRONT-DESK    | Dell         | Optiplex | 380   | 2007 | JKLDF44       | 300   | 01-05-2007 |
| 101-PRESENTER | Dell         | Optiplex | 380   | 2007 | DJKFSJL       | 300   | 01-05-2007 |
| 102-PRESENTER | Dell         | Optiplex | 380   | 2007 | 45JFKK6       | 300   | 11-06-2008 |
| 103-OFFICE    | Dell         | Optiplex | 390   | 2008 | FJ553KE       | 300   | 11-06-2008 |
| 105-OFFICE    | Dell         | Optiplex | 390   | 2008 | FEJ5434       | 300   | 11-06-2008 |
| 101-PROJECTOR | Epson        |          |       |      | DSFJ44JJFTTTF |       |            |
| 102-PROJECTOR | Epson        |          |       |      | EWRWJ22RCJVGH |       |            |

### Branch Office 1

| Name          | Manufacturer | Make     | Model | Year | Serial Number | Price | Obtained   |
| ------------- | ------------ | -------- | ----- | ---- | ------------- | ----- | ---------- |
| FRONT-DESK    | Dell         | Optiplex | 390   | 2008 | 4JGJY66       | 300   | 11-06-2008 |
| 101-PRESENTER | Dell         | Optiplex | 390   | 2008 | CXJRJEW       | 300   | 11-06-2008 |
| 103-OFFICE    | Dell         | Optiplex | 390   | 2008 | FEWJC06       | 300   | 11-06-2008 |
| 101-PROJECTOR | Epson        |          |       |      | REJDKC66KGKGK |       |            |

### Branch Office 2

| Name          | Manufacturer | Make     | Model | Year | Serial Number | Price | Obtained   |
| ------------- | ------------ | -------- | ----- | ---- | ------------- | ----- | ---------- |
| FRONT-DESK    | Dell         | Optiplex | 7010  | 2010 | SDJEKEK       | 300   | 03-22-2010 |
| 101-PRESENTER | Dell         | Optiplex | 7010  | 2010 | DCXJT55       | 300   | 03-22-2010 |
| 103-OFFICE    | Dell         | Optiplex | 7010  | 2010 | CXJEJ44       | 300   | 03-22-2010 |
| 101-PROJECTOR | Epson        |          |       |      | FDSCJXKEKEKRF |       |            |

### Replacement Optiplex 7010's

| Name          | Manufacturer | Make     | Model | Year | Serial Number | Price | Obtained   |
| ------------- | ------------ | -------- | ----- | ---- | ------------- | ----- | ---------- |
| FRONT-DESK    | Dell         | Optiplex | 7010  | 2010 | KSJFEWE       | 300   | 04-30-2012 |
| 101-PRESENTER | Dell         | Optiplex | 7010  | 2010 | HTOUYTP       | 300   | 04-30-2012 |
| 102-PRESENTER | Dell         | Optiplex | 7010  | 2010 | 43JGRTY       | 300   | 04-30-2012 |

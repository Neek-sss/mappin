# Database Objects

The following is a basic definition of the objects stored within the
database (each in their own table) as well as the fields within those
objects.  For more information on database in [Django](https://www.djangoproject.com/) go to
[Django's Site](https://docs.djangoproject.com/en/2.2/ref/databases/).

NOTE: The database used by `Mappin` includes a number of tables that
[Django](https://www.djangoproject.com/) and site middleware which are not documented here.

NOTE: Some of these objects are nearly identical, but are differentiated
as it makes it easier to access the specific types of data without ID
collisions (and code is simpler).

### InventoryBuilding

* name - The display name of the building as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  Does not
have to be unique so that archived buildings can have the same name as
unarchived buildings.
* created - The [DateField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#datefield) representing when this building was created
* archive - A [BoolField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#boolfield) stating whether or not the building has been
archived.
* image - An [InventoryBuildingImage](./database.md#inventorybuildingimage)
representing the building on the map

### InventoryBuildingHistory

* history_object - The [InventoryBuilding](./database.md#inventorybuilding)
that this history is in reference to.
* time_of_change - The server time at which this change occurred as a
[FloatField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#floatfield).
* type - The api type of the change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield) (can be "add",
"update", or "delete").
* data - The data used to make this change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  This
varies for each operation and database object, so it's easier as
character data.

### InventoryBuildingImage

* path - A [URLField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#urlfield) that gives the unique media url for this image of
the form `/media/<image key>`.
* width - [IntegerField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#integerfield) width of the image in pixels
* height - [IntegerField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#integerfield) height of the image in pixels

### InventoryBuildingImageHistory

* history_object - The [InventoryBuildingImage](./database.md#inventorybuildingimage)
that this history is in reference to.
* time_of_change - The server time at which this change occurred as a
[FloatField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#floatfield).
* type - The api type of the change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield) (can be "add",
"update", or "delete").
* data - The data used to make this change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  This
varies for each operation and database object, so it's easier as
character data.

### InventoryItem

* name - The display name of the item as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  Does not have
to be unique so that archived items can have the same name as unarchived
items.
* manufacturer - The company that manufactured the device; [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield)
* make - The make of the device [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield)
* model - The model number of the device [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield)
* year - The year of the device's creation [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield)
* serial_number - The unique serial number of the device; [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).
NOTE: uniqueness is not enforced in case there are identical serial
numbers between manufacturers.
* price - The price of the device at purchase; [DecimalField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#decimalfield)
* obtained - The [DateField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#datefield) when the device was obtained
* on_map - A [BoolField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#boolfield) indicating whether or not the device should be
displayed on the map.
* x_coord - The left/right coordinate of the device on the map;
[DecimalField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#decimalfield)
* y_coord - The down/up coordinate of the device on the map;
[DecimalField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#decimalfield)
* archive - A [BoolField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#boolfield) stating whether or not the item has been
archived.
* building - The [InventoryBuilding](./database.md#inventorybuilding)
that this belongs to.
* image - An [InventoryItemImage](./database.md#inventoryitemimage)
representing the item on the map.

### InventoryItemHistory

* history_object - The [InventoryItem](./database.md#inventoryitem) that this history is in
reference to.
* time_of_change - The server time at which this change occurred as a
[FloatField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#floatfield).
* type - The api type of the change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield) (can be "add",
"update", or "delete").
* data - The data used to make this change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  This
varies for each operation and database object, so it's easier as
character data.

### InventoryItemImage

* path - A [URLField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#urlfield) that gives the unique media url for this image of
the form `/media/<image key>`.
* width - [IntegerField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#integerfield) width of the image in pixels
* height - [IntegerField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#integerfield) height of the image in pixels

### InventoryItemImageHistory

* history_object - The [InventoryItemImage](./database.md#inventoryitemimage)
that this history is in reference to.
* time_of_change - The server time at which this change occurred as a
[FloatField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#floatfield).
* type - The api type of the change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield) (can be "add",
"update", or "delete").
* data - The data used to make this change as a [CharField](https://docs.djangoproject.com/en/2.2/ref/models/fields/#charfield).  This
varies for each operation and database object, so it's easier as
character data.

# Stories

* Project: Mappin
* Class: CS 4910
* Client: Otsego Public Schools
* Team: Nick Yahr & Brandi Kennedy

### Description

The Mappin interactive inventory software is a web-based program to
keep track of "items," which are stored in "buildings."  These "items"
are any data that the user wishes to keep track of, and is recorded
within the system.  The user interacts with this data via spreadsheet,
map, and archive and can create reports from the recorded data. 

### Core

| Story                                                                                                                                                                                                             | Time Estimate | Risk | Time Spent | Completion Amount (%) |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | ---- | ---------- | --------------------- |
| Interactive spreadsheet interface (similar to Google Docs or Excel).  Allows editing of inventory data, adding new data, and removing existing data.                                                              | 100 hrs.      | 1    | 100        | 100%                  |
| Map interface that allows positioning of inventory objects on a user-defined map.  This data and the data in the interactive spreadsheet are connected, so data added/edited/deleted in one is also in the other. | 100 hrs.      | 1    | 100        | 100%                  |
| Archive that allows users to view and manage items that no longer are in service.  These items can also be added to existing buildings, if desired.                                                               | 100 hrs.      | 1    | 100        | 100%                  |
| Reporting system that allows custom reports to be created from pre-defined reporting modules that interact with the inventory data.  These reports are provided as CSV data dumps or PDF presentation reports.    | 30 hrs.       | 7    | 3          | 10%                   |
| User management system that allows creating, editing, and deleting users and passwords.                                                                                                                           | 20 hrs.       | 7    | 5          | 25%                   |
| User management has permission-based access to settings.  For instance, only certain groups can manage users or access restricted buildings.                                                                      | 20 hrs.       | 7    | 20         | 100%                  |
| A settings page that allows for resetting the user's password, access to user management (if allowed), and access to Django's built-in admin page.                                                                | 20 hrs.       | 7    | 20         | 100%                  |


### R1

| Story                                                                                                                                                                                                          | Time Estimate | Risk | Time Spent | Completion Amount (%) |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | ---- | ---------- | --------------------- |
| Authentication that integrates with already existent Active Directory authentication (LDAP).                                                                                                                   | 4 hrs.        | 4    | 0          | 0%                    |


### R2

| Story                                                                                                                                                                          | Time Estimate | Risk | Time Spent | Completion Amount (%) |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------- | ---- | ---------- | --------------------- |
| Google integration that allows data to be modified from Google Drive-based documents.  These documents should be editable from Google Docs, preferrably through Google Sheets. | 10 hrs.       | 10   | 0          | 0%                    |
| Allow the user to define their own reporting modules.                                                                                                                          | 10 hrs.       | 10   | 0          | 0%                    |

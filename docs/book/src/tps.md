# TPS

* Project: Mappin
* Class: CS 4910
* Client: Otsego Public Schools
* Team: Nick Yahr & Brandi Kennedy

### 11/25/2019
| Task           | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| -------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Reporting      | Brandi | 20 hr.        | 1           | 2 hr.      | 20%        |        |

### 11/18/2019
| Task           | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| -------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Reporting      | Brandi | 30 hr.        | 1           | 30 hr.     | 20%        |        |
| Reporting      | Nick   | 30 hr.        | 1           | 30 hr.     | 20%        |        |

### 11/11/2019  
| Task        | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ----------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Bugfixing   | Nick   | 10 hr.        | 1           | 5 hr.      | 100%       |        |
| Reports     | Brandi | 5 hr.         | 1           | 5 hr.      | 100%       |        |

### 11/4/2019
| Task           | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| -------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Permissions    | Nick   | 5 hr.         | 1           | 5 hr.      | 100%       |        |
| Bugfixing      | Nick   | 3 hr.         | 1           | 3 hr.      | 100%       |        |

### 10/28/2019
| Task           | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| -------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Testing        | Nick   | 3 hr.         | 1           | 1 hr.      | 100%       |        |
| Bugfixing      | Nick   | 3 hr.         | 1           | 3 hr.      | 100%       |        |
| Final Features | Nick   | 10 hr.        | 4           | 3 hr.      | 33%        |        |
| Reporting      | Brandi | 20 hr.        | 7           | 2 hr.      | 0%         |        |

### 10/21/2019  
| Task      | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Testing   | Nick   | 3 hr.         | 1           | 1 hr.      | 66%        |        |
| Reporting | Brandi | 20 hr.        | 7           | 2 hr.      | 0%         |        |

### 10/14/2019
| Task          | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Testing       | Nick   | 3 hr.         | 1           | 3 hr.      | 33%        |        |

### 10/07/2019
| Task          | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Slides        | Nick   | 5 hr.         | 1           | 5 hr.      | 100%       |        |
| Slides        | Brandi | 5 hr.         | 1           | 5 hr.      | 100%       |        |

### 9/30/2019
| Task              | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ----------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| GUI Presentation  | Nick   | 10 hr.        | 1           | 10 hr.     | 80%        |        |
| Report for SD     | Brandi | 3 hr.         | 1           | 3 hr.      | 70%        |        |

### 9/23/2019
| Task          | Who    | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------- | ------ | ------------- | ----------- | ---------- | ---------- | ------ |
| Odds and ends | Nick   | 2 hr.         | 1           | 2 hr.      | 30%        |        |
| Report for SD | Brandi | 2 hr.         | 1           | 2 hr.      | 100%       |        |


### 9/16/2019
| Task            | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Rewrite in WASM | Nick | 60 hr.        | 1           | 30 hr.     | 100%       |        |

### 9/9/2019
| Task            | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Rewrite in WASM | Nick | 60 hr.        | 7           | 30 hr.     | 50%       |        |

### 9/2/2019
| Task                        | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Replace ServerAPI with WASM | Nick | 30 hr.        | 5           | 30 hr.     | 100%       |        |

### 8/26/2019
| Task                        | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Replace ServerAPI with WASM | Nick | 30 hr.        | 5           | 15 hr.     | 50%        |        |


### 8/19/2019
| Task                    | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ----------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Abstracting data access | Nick | 20 hr.        | 1           | 20 hr.     | 100%       |        |

### 8/12/2019
| Task         | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------ | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Create Tests | Nick | 80 hr.        | 7           | 20 hr.     | 25%        |        |

### 8/5/2019
| Task                  | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Refactoring           | Nick | 20 hr.        | 1           | 20 hr.     | 100%       |        |
| Image uploading       | Nick | 15 hr.        | 1           | 15 hr.     | 100%       |        |
| Image management      | Nick | 20 hr.        | 1           | 20 hr.     | 100%       |        |
| Documentation         | Nick | 10 hr.        | 1           | 20 hr.     | 100%       |        |

### 7/22/2019
| Task                  | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Refactoring           | Nick | 20 hr.        | 1           | 10 hr.     | 50%        |        |
| Image uploading       | Nick | 20 hr.        | 7           | 5 hr.      | 25%        |        |

### 7/15/2019
| Task                  | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| --------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Archive               | Nick | 6 hr.         | 1           | 4 hr.      | 67%        |        |
| Item/Building History | Nick | 3 hr.         | 1           | 3 hr.      | 100%       |        |
| Client/Server Syncing | Nick | 5 hr.         | 1           | 5 hr.      | 100%       |        |

### 7/8/2019
| Task         | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------ | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Timestamping | Nick | 3 hr.         | 1           | 3 hr.      | 100%       |        |

### 7/1/2019
| Task        | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ----------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| CSRF Tokens | Nick | 5 hr.         | 1           | 5 hr.      | 100%       |        |
| User Login  | Nick | 2 hr.         | 1           | 2 hr.      | 100%       |        |
| Building UI | Nick | 20 hr.        | 1           | 20 hr.     | 100%       |        |

### 6/24/2019
| Task                      | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Building specific entries | Nick | 8 hr.         | 1           | 8 hr.      | 100%       |        |
| Building UI               | Nick | 16 hr.        | 1           | 12 hr.     | 75%        |        |

### 6/17/2019
| Task                      | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Building specific entries | Nick | 8 hr.         | 1           | 5 hr.      | 70%        |        |
| Building UI               | Nick | 16 hr.        | 1           | 8 hr.      | 50%        |        |

### 6/10/2019
| Task                                 | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ------------------------------------ | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Panels are selectable                | Nick | 3 hr.         | 1           | 3 hr.      | 100%       |        |
| UI Updates (working with flex boxes) | Nick | 2 hr.         | 1           | 2 hr.      | 100%       |        |
| Refactoring                          | Nick | 5 hr.         | 1           | 5 hr.      | 100%       |        |

### 6/3/2019
| Task                    | Who  | Time Estimate  | Risk (1-10) | Time Spent | Completion | Review |
| ----------------------- | ---- | -------------- | ----------- | ---------- | ---------- | ------ |
| Refactor client scripts | Nick | 5 hr.          | 1           | 5 hr.      | 100%       |        |
| Dynamic panels          | Nick | 10 hr.         | 1           | 5 hr.      | 50%        |        |

### 5/27/2019
| Task                               | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ---------------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Refactor/Simplify client-side data | Nick | 2 hr.        | 1           | 2 hr.      | 100%       |        |
| Squashed the Delete Bug            | Nick | 2 hr.        | 1           | 2 hr.      | 100%       |        |
| Implemented zoom filter            | Nick | 6 hr.        | 1           | 6 hr.      | 100%       |        |

### 4/15/2019

| Task                                         | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| -------------------------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Basic spreadsheet implementation             | Nick | 10 hr.        | 1           | 5 hr.      | 50%        |        |
| Spreadsheet editing                          | Nick | 3 hr.         | 1           | 1 hr.      | 33 %       |        |
| Server-side changes when editing spreadsheet | Nick | 10 hr.        | 3           | 5 hr.      | 50%        |        |

### 4/8/2019

| Task                         | Who  | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ---------------------------- | ---- | ------------- | ----------- | ---------- | ---------- | ------ |
| Spreadsheet Library Research | Nick | 10 hr.        | 5           | 5 hr.      | 50%        |        |
| Map Library Research         | Nick | 10 hr.        | 5           | 5 hr.      | 50%        |        |

### Pre - 4/8/2019

| Task                    | Who           | Time Estimate | Risk (1-10) | Time Spent | Completion | Review |
| ----------------------- | ------------- | ------------- | ----------- | ---------- | ---------- | ------ |
| Choose a Language       | Nick & Brandi | 1 hr.         | 3           | 1 hr.      | 100%       | N/A    |
| Choose a Server Package | Nick & Brandi | 1 hr.         | 3           | 1 hr.      | 100%       | N/A    |
| Set up Git              | Nick          | 1 hr.         | 1           | 1 hr.      | 100%       |        |

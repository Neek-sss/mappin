# Serving the Site

In order to the site, a couple of changes must be made to
the project as the default setup allows for serving the site
only over Django's built-in development server, not over a
secure HTTPS server.  This part of the guide covers the steps
that must be taken.

### Generate A Keypair

First a private/public key pair must be created for your
unique server.  This ensures that all data is sent securely
between your server and your clients, and ensures the identity
of your server.  If you have an existing key pair that you
would like to use, then you do not have to generate a new
pair.  Instead copy/paste that key pair in place of this step.

To generate a key pair, install [OpenSSL](https://www.openssl.org/).  For Linux systems,
this is probably already installed or is readily available via
the system's package manager.  Windows systems can install it
by going to [OpenSSL](https://www.openssl.org/)'s site and running the appropriate
installer.

Once completing the install steps, open a command prompt and move to
the project's root directory (usually *mappin*) and run:

```
openssl req -newkey rsa:2048 -new -nodes -x509 -keyout key.pem -out cert.pem
```

Filling the prompts with the appropriate information.

Then change the `SECRET_KEY` entry within the `serve-settings.py`
to a random string; it must be random in order for it to be secure.

Also, set `STATIC_ROOT` to the directory that all the static server
files should be saved in to then provide to the client, and
`MEDIA_ROOT` to the directory that will store all client-uploaded
media.

Further configuration settings can be found at [Django's site](https://docs.djangoproject.com/en/2.2/topics/settings/).

The static data files must be moved to their proper locations
in order for them to be properly provided to the client.  This is done
with the following command:

```
python3 src/manage.py collectstatic
```

Finally, the `HOST` variable in `src/serve.py` should be set to
the Full Qualified Domain Name (FQDN) or IP Address of the server,
whichever clients will use to connect to the server. 

Now the the server can be run via the command:

```
python3 src/serve.py
```

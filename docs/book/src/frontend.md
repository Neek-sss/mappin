# Managing the Frontend Code

The frontend is split into three different Rust modules, and a
collective "lib" module containing code that all three use.  This
allows the pages to be loaded and updated separately while still
preventing repeat code.  This guide covers how to setup the tools
necessary to compile these modules into code usable by the
[Django](https://www.djangoproject.com/)/[cherrypy](https://cherrypy.org/)
server.

The lib module (named *mappin-lib*) is held in the folder of the
same name, and is not meant to be compiled directly, but instead
is used as a dependency of the other three modules.

Additionally a library providing bindings to the JavaScript libraries
that `Mappin` uses is contained in the *mappin-lib-js* directory.

The three page modules are as follows:

* **mappin-login** - Provides a simple login page which allows the
user to access the rest of the site
* **mappin-interface** - The main program page containing panels
that makeup the spreadsheets functionality
* **mappin-settings** - A page containing panels allowing and
user-specific settings site-wide (if the correct permissions
exist for the user) to be managed

The code is organized into a *lib.rs* containing a function named
*wasm_main* that is the start of the program.  The *interface* and
*settings* modules then create a header to the page and a series of
panels containing the page content. The rest of the source within each
provide those content pieces.  The *login* code is slightly different in
that it does not contain a header or panels, but merely the login form.

The code within each of the pages is pre-compiled into the "pkg"
folders within each sub-folder, but it may be valuable to re-compile
them if updates to the compiler, libraries, or custom edits to
Mappin's code occur.

In order to compile the code, follow these steps:

* Install [rustup](https://rustup.rs/)
* Run:
```
cargo install wasm-pack
cargo install cargo-make
```
* To build the code run:
```
cargo make build
```

Once this is done the *pkg* folder should appear in each directory,
which holds the *WASM* and *JS* files, as well as a *target* directory
in the root of *mappin* that holds the build artifacts.

# TODO

* [X] Documents
    * [X] [Version control](./standard.md#git)
    * [X] [Automation](./testing.md)
    * [X] [Coding](./standard.md#style)
    * [X] [Legal](./LICENSE.md)
* [X] Testing
    * [X] [Unit](./testing.md)
    * [X] [System](./testing.md)
    * [X] [Usability](./testing.md)
    * [X] [Accessibility](./testing.md)
    * [X] Acceptance
* [ ] Testing Plan/Report (6-7 pages)
* [X] [Security](./security.md)
* [ ] Maintenance
    * [X] [Install/Deploy](./starting_from_scratch.md)
    * [ ] Operation
    * [ ] Maintenance
    * [X] [Frontend Development](./frontend.md)
    * [X] [Backend Development](./backend.md)
* [X] [Stories](./stories.md)
* [X] [TPS](./tps.md)
* [X] [Frontend Source code organization](./frontend.md)
* [X] [Backend Source code organization](./backend.md)
* [X] Spikes
* [ ] Demonstration
* [X] Status 

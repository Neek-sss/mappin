# Security

Security was provided following the practices recommended by [Django](https://www.djangoproject.com/).
This documentation can be found at
[Security in Django](https://docs.djangoproject.com/en/2.2/topics/security/).

### HTTPS with HSTS

The site is served over an HTTPS server in order to encrypt the data as
it is passed between the client and server.  Data such as
usernames/passwords must be securely passed, so they should be encrypted
as they are passed.  This is enforced for every url that the site
provides.  In addition clients are forced to access the site through
HTTPS as the HSTS protocol redirects clients from HTTP to HTTPS if they
attempt an HTTP connection.

### Logins

Logins are required for every server request accept for loading the
login page.  If a request is attempted from a client that is not logged
in, that client is redirected to the login screen.  This prevents users
from making anonymous changes as well as preventing access to data from
unauthorized sources.

### Sessions

All access to the server requires a client session.  This is maintained
through a token passed between the client and server.  Clients without a
token are redirected to the login page and provided a new token.  This
token can then be used to securely maintain credentials without the
client needing to worry about securely storing important information
such as usernames/passwords.

### Cross Site Scripting (XSS)

Cross site scripting is an attempt by an outside source to run malicious
code by injecting it into the client in places such as the HTML or
cookies.  This is prevented by sanitizing inputs (done automatically
through the client libraries).

### Cross Site Request Forgery (CSRF)

The session token is constantly changed for each client/server
interaction, updating the cookie set in the client and the matching
value stored in the server.  If the token passed to the server does not
match the stored token, or the client failed to pass a token, then the
client must get a new token and re-authenticate.  This prevents
malicious code from stealing a login session, as the token will change
as soon as the request is completed and the malicious actor will not
have that new token.

### SQL Injection

Similar to XSS, SQL injections attempt to run malicious code but instead
of on the client side this code is attempted directly on the server.
The SQL injection can be prevented by sanitizing the SQL code and this
is provided by `Django`'s query parameterization (bindings to the
database that generate code from programmatic statements).

### Clickjacking

Clickjacking occurs when a malicious source attempts to render the page
within another page containing its own malicious outer code.  To the
user the page would seem to behave normally, but any code can be added
to the page circumventing any security set up by the application.
`Django` prevents this with its `X-Frame-Options` middleware.

### Permissions

Users may only interact with data that they have permission to access.
This means that users may not change data unless they have access, and
cannot even read data that they should not have access to (for instance,
user account names).  User management is provided to change permissions
as well as create and delete accounts.

### Password Handling

Passwords are not stored on the client for any period of time.  They are
sent to the server to verify and process.  This prevents a user from
skipping past security checks and accessing the server directly with
unauthorized requests.

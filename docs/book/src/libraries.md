# Libraries

## Python

*These must be installed in order to run the server.  Steps will
vary depending on the system, but in general Linux users should
use their built-in package manager and Windows users should use
pip.*

* [Django](https://www.djangoproject.com/) - MVC (Model/View/Connector) framework
* [Yattag](https://www.yattag.org/) - HTML
* [CSSUtils](https://pypi.org/project/cssutils/) - CSS
* [Pillow](https://pillow.readthedocs.io/en/stable/) - Image handling
* [Django-Import-Export](https://django-import-export.readthedocs.io/en/latest/) - Import/Export of data to a number of
formats
* [Django-Backup](https://django-dbbackup.readthedocs.io/en/master/index.html) - Manages
the backup/restore of both the database and the media directory
* [Codebraid](https://github.com/gpoore/codebraid) - Generating reports to provide to the server

## Reporting
* [Pandoc](https://pandoc.org/) - The backend used to generate the reports

## Serving

*These must be installed in order for the server to be provided securely
to clients.  They follow the same steps as other Python dependencies.*

* [cherrypy](https://cherrypy.org/) - Server program

## Javascript

*These are already served in the frontend/static folder, but are
noted here for reference*

* [SlickGrid](https://github.com/6pac/SlickGrid) - Spreadsheet
* [OpenLayers](https://openlayers.org/) - Map

## Rust

*These libraries are retrieved automatically through the cargo
program packaged with rustup.  It is only included here for note.*

* [wasm-bindgen](https://crates.io/crates/wasm-bindgen) - Used to create bindings from Rust to JavaScript
* [serde-wasm-bindgen](https://crates.io/crates/serde-wasm-bindgen) - Greatly simplifies the process of turning
Rust structs into JavaScript objects (as simple as adding a macro tag).
* [web-sys](https://crates.io/crates/web-sys) - Provides bindings to most basic JavaScript objects
provided by web browsers.
* [js-sys](https://crates.io/crates/js-sys) - Allows access to the JavaScript engine of the web browser
and methods for reading/writing that data.
* [serde](https://crates.io/crates/serde) - A very general library for serializing/deserializing data
in Rust code.
* [serde_json](https://crates.io/crates/serde_json) - Allows serializing/deserializing JSON (JavaScript
Object Notation) for structs tagged with as *serde*.
* [maplit](https://crates.io/crates/maplit) - A set of macros that simplify creation of things like
hashmaps.
* [lazy_static](https://crates.io/crates/lazy_static) - Simplifies the process of creating static variables
with complex initialization processes.

## Code Testing

* Requirements detailed in [Managing the Frontend Code](./frontend.md)
* [Geckodriver](https://github.com/mozilla/geckodriver/releases) - Allows running a program-controlled *Firefox* web
browser (a program that must be installed; Google is your best friend)
* [Selenium](https://www.seleniumhq.org/) - Allows automated testing with browsers (A python package)

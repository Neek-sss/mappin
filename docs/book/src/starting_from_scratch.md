# Starting From Scratch

* Install [Python3](https://www.python.org/) and the used libraries
* Clone the repository
* Start a command line process
* Go to the mappin repository
* Run:
```
python3 src/manage.py makemigrations mappin
python3 src/manage.py migrate
python3 src/manage.py createsuperuser
python3 src/manage.py runserver
```
* Open an internet browser to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

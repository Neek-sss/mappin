# Standards

### Style

Coding style standards follow those declared in the Django Coding
Standard section of the Django Website:
[Django | Coding style](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/)

In brief these include:

* 4 space indentation (python code)
* 2 space indentation (html code)
* Hanging indents (not vertical alignment)
* InitialCaps for classes, underscores for anything else
* Don't abbreviate names, except when necessary (use ```test_variable```,
not ```t_variable```; use ```ret```, not ```return```)
* In general, code should be formatted to make it as readable as possible

### Git

Code versioning is managed by Git.  The following should always apply:

* Test changes before committing them
* Commit changes locally
* Fetch and merge upstream data
* Only then commit

Git tutorials can be found at:
[Git | Gittutorial](https://git-scm.com/docs/gittutorial#)

Specifically instructions for collaboration:
[Git | Gittutorial](https://git-scm.com/docs/gittutorial#_using_git_for_collaboration)

Commit messages should follow the guidelines addressed
in Conventional Commits:
[Conventional Commits](https://www.conventionalcommits.org)

In brief:

* Header with: ```<type\>[scope]: <description\>```
* A body describing the change
* Type is fix, feature, docs, style, refactor, test, or something similar
* Scope is a shorthand for the area of the code (ex. Database, Map, Static)
* Description is no more than a few words that gives a general feel of the changes

### Testing

All parts of the code should have corresponding unit tests.  All
unit tests must pass at all times unless there is a *very* good
reason for them not too.  Any tests that do not pass should be
addressed immediately.  Tests will include, but not be excluded to:

* Unit
* System
* Usability
* Accessibility
* Black Box

Unit test examples can be found at the Django documentation:
[Django | Writing and Running Tests](https://docs.djangoproject.com/en/2.2/topics/testing/overview/)

### Documentation

All functions and objects shall be documented for future use.  At
minimum a general description, parameters, and the return of each
function should be documented.  For classes, the class itself, all
fields, and the class functions should be documented similarly.
Documentation tutorials can be found at:
[Real Python | Documenting Python Code: A Complete Guide](https://realpython.com/documenting-python-code/)

from django.test import Client, TestCase
from mappin.models import *

from ..helpers import *


class EmptyLoadingTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def check_number_of_response_items(self, url, number):
        login(self)
        response = post(self, url)
        self.assertEqual(number, len(response))

    def test_loading_item(self):
        self.check_number_of_response_items('/mappin/load/item/', 0)

    def test_loading_image_item(self):
        self.check_number_of_response_items('/mappin/load/image_item/', 0)

    def test_loading_building(self):
        self.check_number_of_response_items('/mappin/load/building/', 1)

    def test_loading_image_building(self):
        self.check_number_of_response_items('/mappin/load/image_building/', 0)


class FullLoadingTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def check_number_of_response_items(self, url, number):
        login(self)
        response = post(self, url)
        self.assertEqual(number, len(response))

    def test_loading_item(self):
        building = InventoryBuilding(name="FirstBuilding")
        building.save()

        InventoryItem(name="First", building=building).save()
        InventoryItem(name="Second", building=building).save()
        InventoryItem(name="Third", building=building).save()

        self.check_number_of_response_items('/mappin/load/item/', 3)

    def test_loading_image_item(self):
        InventoryItemImage(path="/first/path", height=1, width=1).save()
        InventoryItemImage(path="/second/path", height=1, width=1).save()
        InventoryItemImage(path="/third/path", height=1, width=1).save()

        self.check_number_of_response_items('/mappin/load/image_item/', 3)

    def test_loading_building(self):
        self.check_number_of_response_items('/mappin/load/building/', 1)

        InventoryBuilding(name="FirstBuilding").save()
        InventoryBuilding(name="SecondBuilding").save()
        InventoryBuilding(name="ThirdBuilding").save()

        self.check_number_of_response_items('/mappin/load/building/', 4)

    def test_loading_image_building(self):
        InventoryBuildingImage(path='/first/path', height=1, width=1).save()
        InventoryBuildingImage(path='/second/path', height=1, width=1).save()
        InventoryBuildingImage(path='/third/path', height=1, width=1).save()

        self.check_number_of_response_items('/mappin/load/image_building/', 3)

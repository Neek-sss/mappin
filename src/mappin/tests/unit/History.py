from django.test import Client, TestCase
from mappin.models import *

from ..helpers import *


class EmptyHistoryTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def check_number_of_response_items(self, url, number, data):
        login(self)
        response = post(self, url, data)
        self.assertEqual(number, len(response))

    def test_item_history(self):
        self.check_number_of_response_items('/mappin/history/item/', 0, data={'time': 0})

    def test_image_item_history(self):
        self.check_number_of_response_items('/mappin/history/image_item/', 0, data={'time': 0})

    def test_building_history(self):
        self.check_number_of_response_items('/mappin/history/building/', 0, data={'time': 0})

    def test_image_building_history(self):
        self.check_number_of_response_items('/mappin/history/image_building/', 0, data={'time': 0})


class FullHistoryTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def check_number_of_response_items(self, url, number, data):
        login(self)
        response = post(self, url, data)
        self.assertEqual(number, len(response))

    def test_item_history(self):
        building = InventoryBuilding(name="FirstBuilding")
        building.save()

        item1 = InventoryItem(name="First", building=building)
        item1.save()
        item2 = InventoryItem(name="Second", building=building)
        item2.save()

        InventoryItemHistory(history_object=item1, building=building, time_of_change=1, type='add', data='').save()
        InventoryItemHistory(history_object=item1, building=building, time_of_change=2, type='update', data='').save()
        InventoryItemHistory(history_object=item1, building=building, time_of_change=10, type='update', data='').save()
        InventoryItemHistory(history_object=item2, building=building, time_of_change=11, type='add', data='').save()
        InventoryItemHistory(history_object=item2, building=building, time_of_change=12, type='update', data='').save()

        self.check_number_of_response_items('/mappin/history/item/', 3, data={'time': 5})

    def test_image_item_history(self):
        item_image1 = InventoryItemImage(path="/first/path", height=1, width=1)
        item_image1.save()
        item_image2 = InventoryItemImage(path="/second/path", height=1, width=1)
        item_image2.save()

        InventoryItemImageHistory(history_object=item_image1, time_of_change=1, type='add', data='').save()
        InventoryItemImageHistory(history_object=item_image1, time_of_change=2, type='update', data='').save()
        InventoryItemImageHistory(history_object=item_image2, time_of_change=3, type='add', data='').save()
        InventoryItemImageHistory(history_object=item_image2, time_of_change=11, type='update', data='').save()
        InventoryItemImageHistory(history_object=item_image2, time_of_change=12, type='update', data='').save()

        self.check_number_of_response_items('/mappin/history/image_item/', 2, data={'time': 9})

    def test_building_history(self):
        building1 = InventoryBuilding(name="FirstBuilding")
        building1.save()
        building2 = InventoryBuilding(name="SecondBuilding")
        building2.save()
        building3 = InventoryBuilding(name="ThirdBuilding")
        building3.save()

        InventoryBuildingHistory(history_object=building1, time_of_change=1, type='add', data='').save()
        InventoryBuildingHistory(history_object=building1, time_of_change=2, type='update', data='').save()
        InventoryBuildingHistory(history_object=building2, time_of_change=4, type='add', data='').save()
        InventoryBuildingHistory(history_object=building2, time_of_change=8, type='update', data='').save()
        InventoryBuildingHistory(history_object=building2, time_of_change=16, type='update', data='').save()
        InventoryBuildingHistory(history_object=building3, time_of_change=32, type='add', data='').save()

        self.check_number_of_response_items('/mappin/history/building/', 3, data={'time': 5})

    def test_image_building_history(self):
        building_image1 = InventoryBuildingImage(path="/first/path", height=1, width=1)
        building_image1.save()
        building_image2 = InventoryBuildingImage(path="/second/path", height=1, width=1)
        building_image2.save()

        InventoryBuildingImageHistory(history_object=building_image1, time_of_change=1, type='add', data='').save()
        InventoryBuildingImageHistory(history_object=building_image1, time_of_change=2, type='update', data='').save()
        InventoryBuildingImageHistory(history_object=building_image2, time_of_change=3, type='add', data='').save()
        InventoryBuildingImageHistory(history_object=building_image2, time_of_change=4, type='update', data='').save()

        self.check_number_of_response_items('/mappin/history/image_building/', 3, data={'time': 2})

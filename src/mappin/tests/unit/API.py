from django.test import Client, TestCase
from mappin.models import *

from ..helpers import *


class AddTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def test_add_building(self):
        login(self)

        post(self, '/mappin/load/building/')
        response = post(self, '/mappin/api/building/', data={'id': 50, 'type': 'add', 'name': 'TestBuilding'})[0]

        self.assertEqual(response['fields']['name'], 'TestBuilding')
        self.assertEqual(response['pk'], 2)

    def test_add_item(self):
        login(self)

        building = InventoryBuilding(name="Test")
        building.save()
        building_dict = model_to_dict(building)

        response = post(self, '/mappin/api/item/', data={'id': 50, 'type': 'add', 'name': 'TestItem', 'building': building_dict['pk']})[0]

        self.assertEqual(response['fields']['name'], 'TestItem')
        self.assertEqual(response['pk'], 1)


class UpdateTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        create_user(cls)

    def test_update_building(self):
        login(self)

        building = InventoryBuilding(name="Test")
        building.save()
        building_dict = model_to_dict(building)

        post(self, '/mappin/api/building/', data={'id': building_dict['pk'], 'type': 'update', 'column': 'name', 'data': 'NewName'})

        self.assertEqual(InventoryBuilding.objects.filter(id=1).first().name, 'NewName')

    def test_update_item(self):
        login(self)

        building = InventoryBuilding(name="Test")
        building.save()
        building_dict = model_to_dict(building)

        item = InventoryItem(name="Test", building=building)
        item.save()
        item_dict = model_to_dict(item)

        post(self, '/mappin/api/item/', data={'id': item_dict['pk'], 'building': building_dict['pk'],'type': 'update', 'column': 'name', 'data': 'NewName'})

        self.assertEqual(InventoryItem.objects.filter(id=1).first().name, 'NewName')

import json

from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from selenium.webdriver.common.by import By


def create_user(self, username='neek-sss', password='password', email='neek-sss@mail.com'):
    self.user = {'username': username, 'email': email, 'password': password}
    User.objects.create_superuser(self.user['username'], self.user['username'] + '@email.com', self.user['password'])


def login(self, username='neek-sss', password='password'):
    self.assertTrue(self.client.login(username=username, password=password))


def to_form_url(data):
    ret = ''
    for key, value in data.items():
        ret += str(key) + '=' + str(value) + '&'
    return ret


def post(self, url, data=None, status=200):
    if data is None:
        data = {}

    response = self.client.post(url, data=to_form_url(data), content_type='application/x-www-form-urlencoded')
    self.assertEqual(response.status_code, status)
    if response.content is None or response.content is b'':
        return []
    else:
        return json.loads(response.content)


def model_to_dict(model):
    return json.loads(serializers.serialize('json', [model]))[0]


def no_csrf_setup():
    settings.MIDDLEWARE = settings.MIDDLEWARE + ['mappin.tests.integration.disable_csrf.DisableCSRF']


def spreadsheet_cell(driver, row: int, column: int):
    return driver.find_element(By.XPATH,
                               "//div[@id=\'Spreadsheet_content\']/div[4]/div[3]/div[1]/div[" + str(
                                   row) + "]/div[" + str(
                                   column) + "]")


def spreadsheet_cell_selector(driver, row: int):
    return driver.find_element(By.XPATH,
                               "//div[@id=\'Spreadsheet_content\']/div[4]/div[3]/div[1]/div[" + str(
                                   row) + "]/div[1]/input")


def archive_cell_selector(driver, row: int):
    return driver.find_element(By.XPATH,
                               "//div[@id=\'Archive_content\']/div[4]/div[3]/div[1]/div[" + str(
                                   row) + "]/div[1]/input")

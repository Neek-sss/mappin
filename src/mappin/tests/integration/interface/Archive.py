import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from mappin.models import *
from ..helpers import *


# @skip("A long test")
class ArchiveAddRemoveTest(StaticLiveServerTestCase):
    driver = None
    user = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        create_user(cls)
        options = webdriver.FirefoxOptions()
        options.headless = True
        cls.driver = webdriver.Firefox(options=options)
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def test_archiving_restoring(self):
        building = InventoryBuilding(name="Building1")
        building.save()

        InventoryItem(name="ItemA", building=building, archive=False).save()
        InventoryItem(name="ItemB", building=building, archive=True).save()
        InventoryItem(name="ItemC", building=building, archive=True).save()
        InventoryItem(name="ItemD", building=building, archive=False).save()

        self.driver.set_window_size(1366, 768)
        self.driver.get('%s%s' % (self.live_server_url, '/'))

        self.driver.find_element(By.ID, "username_input").click()
        self.driver.find_element(By.ID, "username_input").send_keys(self.user['username'])
        self.driver.find_element(By.ID, "password_input").send_keys(self.user['password'])
        self.driver.find_element(By.ID, "submit_button").click()

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "Map_panel_icon")).perform()
        self.driver.find_element(By.ID, "Map_to_Archive").click()

        archive_cell_selector(self.driver, 2).click()
        self.driver.find_element(By.ID, "restore_item").click()

        spreadsheet_cell_selector(self.driver, 3).click()
        self.driver.find_element(By.ID, "archive_item").click()

        time.sleep(10)

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "user_hover")).perform()
        self.driver.find_element(By.ID, "logout_button").click()
        self.driver.close()

        item_a = InventoryItem.objects.filter(name='ItemA').first()
        self.assertFalse(item_a.archive)
        item_b = InventoryItem.objects.filter(name='ItemB').first()
        self.assertTrue(item_b.archive)
        item_c = InventoryItem.objects.filter(name='ItemC').first()
        self.assertFalse(item_c.archive)
        item_d = InventoryItem.objects.filter(name='ItemD').first()
        self.assertTrue(item_d.archive)

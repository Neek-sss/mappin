import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from mappin.models import *
from ..helpers import *


# @skip("A long test")
class SpreadsheetAddEditTest(StaticLiveServerTestCase):
    driver = None
    user = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        create_user(cls)
        options = webdriver.FirefoxOptions()
        options.headless = True
        cls.driver = webdriver.Firefox(options=options)
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def test_adding_editing(self):
        self.driver.set_window_size(1366, 768)
        self.driver.get('%s%s' % (self.live_server_url, '/'))

        self.driver.find_element(By.ID, "username_input").click()
        self.driver.find_element(By.ID, "username_input").send_keys(self.user['username'])
        self.driver.find_element(By.ID, "password_input").send_keys(self.user['password'])
        self.driver.find_element(By.ID, "submit_button").click()

        self.driver.find_element(By.CSS_SELECTOR, ".new-row > .l1").click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("COMP1")
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys(Keys.ENTER)
        self.driver.find_element(By.CSS_SELECTOR, ".new-row > .l1").click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("COMP2")
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys(Keys.ENTER)
        self.driver.find_element(By.CSS_SELECTOR, ".new-row > .l1").click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("COMP3")
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys(Keys.ENTER)

        spreadsheet_cell(self.driver, 1, 3).click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("CompanyA")
        spreadsheet_cell(self.driver, 2, 5).click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("Model1")
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys(Keys.ENTER)

        time.sleep(10)

        spreadsheet_cell(self.driver, 3, 6).click()
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys("2000")
        self.driver.find_element(By.CSS_SELECTOR, ".editor-text").send_keys(Keys.ENTER)

        time.sleep(20)

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "user_hover")).perform()
        self.driver.find_element(By.ID, "logout_button").click()
        self.driver.close()

        first = InventoryItem.objects.filter(name='COMP1')
        self.assertEqual(1, len(first))
        first_item = first.first()
        self.assertEqual('CompanyA', first_item.manufacturer)
        self.assertEqual('', first_item.model)
        self.assertEqual('', first_item.year)

        second = InventoryItem.objects.filter(name='COMP2')
        self.assertEqual(1, len(second))
        second_item = second.first()
        self.assertEqual('', second_item.manufacturer)
        self.assertEqual('Model1', second_item.model)
        self.assertEqual('', second_item.year)

        third = InventoryItem.objects.filter(name='COMP3')
        self.assertEqual(1, len(third))
        third_item = third.first()
        self.assertEqual('', third_item.manufacturer)
        self.assertEqual('', third_item.model)
        self.assertEqual('2000', third_item.year)

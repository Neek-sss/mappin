import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from mappin.models import *
from ..helpers import *


# @skip("A long test")
class BuildingAddEdit(StaticLiveServerTestCase):
    driver = None
    user = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        create_user(cls)
        options = webdriver.FirefoxOptions()
        options.headless = True
        cls.driver = webdriver.Firefox(options=options)
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def test_adding_editing(self):
        self.driver.set_window_size(1366, 768)
        self.driver.get('%s%s' % (self.live_server_url, '/'))

        self.driver.find_element(By.ID, "username_input").click()
        self.driver.find_element(By.ID, "username_input").send_keys(self.user['username'])
        self.driver.find_element(By.ID, "password_input").send_keys(self.user['password'])
        self.driver.find_element(By.ID, "submit_button").click()

        self.driver.find_element(By.ID, "building_name").click()
        ActionChains(self.driver).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).perform()
        self.driver.find_element(By.ID, "building_name_input").send_keys("FirstBuildingName")
        ActionChains(self.driver).key_down(Keys.ENTER).key_up(Keys.ENTER).perform()

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "building_name")).perform()
        self.driver.find_element(By.ID, "new_building").click()

        self.driver.find_element(By.ID, "building_name").click()
        ActionChains(self.driver).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).perform()
        self.driver.find_element(By.ID, "building_name_input").send_keys("NewBuildingName")
        ActionChains(self.driver).key_down(Keys.ENTER).key_up(Keys.ENTER).perform()

        time.sleep(10)

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "user_hover")).perform()
        self.driver.find_element(By.ID, "logout_button").click()
        self.driver.close()

        self.assertEqual(1, len(InventoryBuilding.objects.filter(name="FirstBuildingName")))
        self.assertEqual(1, len(InventoryBuilding.objects.filter(name="NewBuildingName")))


# @skip("Not sure what it's issue is, because running it individually works...")
class BuildingArchiveRestore(StaticLiveServerTestCase):
    driver = None
    user = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        create_user(cls)
        options = webdriver.FirefoxOptions()
        options.headless = False
        cls.driver = webdriver.Firefox(options=options)
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def test_archive_restore(self):
        archived_building = InventoryBuilding(name="ArchivedBuilding", archive=True)
        archived_building.save()
        archived_building_id = archived_building.id

        active_building = InventoryBuilding(name="ActiveBuilding")
        active_building.save()
        active_building_id = active_building.id

        unused_building = InventoryBuilding(name="UnusedBuilding")
        unused_building.save()
        unused_building_id = unused_building.id

        self.driver.set_window_size(1366, 768)
        self.driver.get('%s%s' % (self.live_server_url, '/'))

        self.driver.find_element(By.ID, "username_input").click()
        self.driver.find_element(By.ID, "username_input").send_keys(self.user['username'])
        self.driver.find_element(By.ID, "password_input").send_keys(self.user['password'])
        self.driver.find_element(By.ID, "submit_button").click()

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "building_name")).perform()
        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "change_building")).perform()
        self.driver.find_element(By.ID, "change_to_building_" + str(unused_building_id)).click()

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "building_name")).perform()
        self.driver.find_element(By.ID, "archive_building").click()

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "building_name")).perform()
        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "restore_building")).perform()
        self.driver.find_element(By.ID, "restore_to_building_" + str(archived_building_id)).click()

        time.sleep(10)

        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "user_hover")).perform()
        self.driver.find_element(By.ID, "logout_button").click()
        self.driver.close()

        self.assertEqual(1, len(InventoryBuilding.objects.filter(name="ArchivedBuilding")))
        archived_building = InventoryBuilding.objects.filter(name="ArchivedBuilding").first()
        self.assertEqual(False, archived_building.archive)

        self.assertEqual(1, len(InventoryBuilding.objects.filter(name="ActiveBuilding")))
        active_building = InventoryBuilding.objects.filter(name="ActiveBuilding").first()
        self.assertEqual(False, active_building.archive)

        self.assertEqual(1, len(InventoryBuilding.objects.filter(name="UnusedBuilding")))
        unused_building = InventoryBuilding.objects.filter(name="UnusedBuilding").first()
        self.assertEqual(True, unused_building.archive)

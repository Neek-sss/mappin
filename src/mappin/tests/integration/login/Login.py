from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from ..helpers import *


# @skip("A long test")
class LoginTest(StaticLiveServerTestCase):
    driver = None
    user = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        create_user(cls)
        options = webdriver.FirefoxOptions()
        options.headless = True
        cls.driver = webdriver.Firefox(options=options)
        cls.driver.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def test_loginLogout(self):
        self.driver.set_window_size(1366, 768)
        self.driver.get('%s%s' % (self.live_server_url, '/'))
        self.driver.find_element(By.ID, "username_input").click()
        self.driver.find_element(By.ID, "username_input").send_keys(self.user['username'])
        self.driver.find_element(By.ID, "password_input").send_keys(self.user['password'])
        self.driver.find_element(By.ID, "submit_button").click()
        ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID, "user_hover")).perform()
        self.driver.find_element(By.ID, "logout_button").click()
        self.driver.close()

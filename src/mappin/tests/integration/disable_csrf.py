class DisableCSRF(object):

    def __init__(self, get_response):
        '''
        This class disables all CSRF checks, enabling automated Selenium
        integration testing through Pytest.

        Args:
            get_response (obj): A callable object for use elsewhere
        '''

        self.get_response = get_response

    def __call__(self, request):
        '''
        The default interaction with middleware.

        Args:
            request (HttpRequest): The request from the client
        '''

        # What to do before the request is processed
        setattr(request, '_dont_enforce_csrf_checks', True)

        # Process the request
        response = self.get_response(request)

        # What to do after the request is processed

        # Return the response
        return response

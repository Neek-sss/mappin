from django.db import models


class InventoryBuildingImage(models.Model):
    """
    The data allowing the client to connect with user uploaded building images.
    """

    path = models.URLField()
    """str: The URL to the image"""

    width = models.IntegerField()
    """int: The width of the image"""

    height = models.IntegerField()
    """int: The height of the image"""


class InventoryBuildingImageHistory(models.Model):
    """
    A collection of all changes made to a building image since its creation.
    """

    history_object = models.ForeignKey(
        InventoryBuildingImage,
        on_delete=models.CASCADE
    )
    """InventoryBuildingImage: The building image being tracked"""

    time_of_change = models.FloatField()
    """float: The timestamp of the change"""

    type = models.CharField(max_length=128, default='')
    """str: The identifying type of the chage"""

    data = models.CharField(max_length=128, default='')
    """str: A JSON string containing the data describing the change"""


class InventoryBuilding(models.Model):
    """
    The physical location that houses a collection of inventory items.
    """

    name = models.CharField(max_length=128, default='')
    """str: The display name of the building"""

    created = models.DateField(auto_now=True)
    """date: The creation date of the building"""

    archive = models.BooleanField(default=False)
    """bool: Whether or not the building has been archived"""

    image = models.ForeignKey(
        InventoryBuildingImage,
        on_delete=models.SET_NULL,
        null=True
    )
    """InventoryBuildingImage: The optional image represention on the map"""


class InventoryBuildingHistory(models.Model):
    """
    A collection of all changes made to a building since its creation.
    """

    history_object = models.ForeignKey(
        InventoryBuilding,
        on_delete=models.CASCADE
    )
    """InventoryBuilding: The building being tracked"""

    time_of_change = models.FloatField()
    """float: The timestamp of the change"""

    type = models.CharField(max_length=128, default='')
    """str: The identifying type of the chage"""

    data = models.CharField(max_length=128, default='')
    """str: A JSON string containing the data describing the change"""


class InventoryItemImage(models.Model):
    """
    The data allowing the client to connect with user uploaded item images.
    """

    path = models.URLField()
    """str: The URL to the image"""

    width = models.IntegerField()
    """int: The width of the image"""

    height = models.IntegerField()
    """int: The height of the image"""


class InventoryItemImageHistory(models.Model):
    """
    A collection of all changes made to an item image since its creation.
    """

    history_object = models.ForeignKey(
        InventoryItemImage,
        on_delete=models.CASCADE
    )
    """InventoryItemImage: The item image being tracked"""

    time_of_change = models.FloatField()
    """float: The timestamp of the change"""

    type = models.CharField(max_length=128, default='')
    """str: The identifying type of the chage"""

    data = models.CharField(max_length=128, default='')
    """str: A JSON string containing the data describing the change"""


class InventoryItem(models.Model):
    """
    An item that is being recorded within the inventory.  All associated data is
    connected to that particular item.
    """

    name = models.CharField(max_length=128, default='')
    """str: The display name of the item"""

    manufacturer = models.CharField(max_length=128, default='')
    """str: The entity that created the item"""

    make = models.CharField(max_length=128, default='')
    """str: The make of the item"""

    model = models.CharField(max_length=128, default='')
    """str: The model of the item"""

    year = models.CharField(max_length=128, default='')
    """str: The manufacturing year of the item"""

    serial_number = models.CharField(max_length=128, default='')
    """str: The unique serial number of the item"""

    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    """decimal: How much the item cost"""

    obtained = models.DateField(auto_now=True)
    """date: When the item was obtained by the owning entity"""

    on_map = models.BooleanField(default=False)
    """bool: Whether or not the item is on the map"""

    x_coord = models.DecimalField(max_digits=10, decimal_places=4, default=0.0)
    """decimal: Left/Right coordinates on the map"""

    y_coord = models.DecimalField(max_digits=10, decimal_places=4, default=0.0)
    """decimal: Down/Up coodinates on the map"""

    archive = models.BooleanField(default=False)
    """bool: Whether or not the item has been archived"""

    building = models.ForeignKey(InventoryBuilding, on_delete=models.CASCADE)
    """InventoryBuilding: The building containing the item"""

    image = models.ForeignKey(
        InventoryItemImage,
        on_delete=models.SET_NULL,
        null=True
    )
    """InventoryItemImage: The optional image represention the map"""


class InventoryItemHistory(models.Model):
    """
    A collection of all changes made to an item since its creation.
    """

    building = models.ForeignKey(InventoryBuilding, on_delete=models.CASCADE)
    """InventoryBuilding: The building containing the item being tracked"""

    history_object = models.ForeignKey(InventoryItem, on_delete=models.CASCADE)
    """InventoryItem: The item being tracked"""

    time_of_change = models.FloatField()
    """float: The timestamp of the change"""

    type = models.CharField(max_length=128, default='')
    """str: The identifying type of the chage"""

    data = models.CharField(max_length=128, default='')
    """str: A JSON string containing the data describing the change"""

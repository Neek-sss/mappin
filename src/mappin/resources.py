from import_export import resources

from .models import InventoryBuilding, InventoryItem


class InventoryItemResource(resources.ModelResource):
    class meta:
        model = InventoryItem


class InventoryBuildingResource(resources.ModelResource):
    class meta:
        model = InventoryBuilding

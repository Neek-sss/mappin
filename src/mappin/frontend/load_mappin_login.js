// Setup the WASM in JS's window
async function run() {
    window.mappin_wasm = await import('/mappin_login.js')
    await window.mappin_wasm.default()
}
run();

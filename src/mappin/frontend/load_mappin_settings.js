// Setup the WASM in JS's window
async function run() {
    window.mappin_wasm = await import('/mappin_settings.js')
    await window.mappin_wasm.default()
}
run();

//report form for user to filter data and run

function Report() {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        }
    });

    $.ajax({
        type:'post',
        url: '/mappin/load/item/',
        models: '',
        async: false
    }) .done(function(msg){
            window.data = msg
            })

    var db = openDatabase("model", "1.0", "", 2 * 1024 *1024)

    var checkboxFilter


    var Report_header = document.getElementById("report_header")
    var Report_content = document.getElementById("report_content")

    var x1 = document.createElement("P")
        var t1 = document.createTextNode("Use the filters below to create a report.")
            x1.appendChild(t1)
            report_header.appendChild(x1)

    var searchBar = function createBar (searchString) {


        var search = document.createElement("div")
        var input = document.createElement("input")
        let searchButton = document.createElement("button")
        searchButton.setAttribute("id","searchid")

//        document.getElementById("searchid").onclick = function(){
//            alert('hi')
//        }


        input.type="text"

        searchButton.innerText="Search"
        search.className = "search"

        search.appendChild(input)
        search.appendChild(searchButton)

    return search }

    console.log('Adding search bar')
    report_content.appendChild(searchBar())

    var x3 = document.createElement("P")
    var t3 = document.createTextNode("Select variables and run report.")
        x3.appendChild(t3)
        report_content.appendChild(x3)

    var reportButton = document.createElement("BUTTON")
        reportButton.innerHTML = "Run Report"
        report_content.appendChild(reportButton)
    reportButton.setAttribute("id","runreport")

    var exportButton = document.createElement("BUTTON")
        exportButton.innerHTML = "Export CSV"
        report_content.appendChild(exportButton)
    exportButton.setAttribute("id","exportcsv")


    document.getElementById("searchid").onclick = function(){
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM  foo',[], function(tx,results){
                var len = results.rows.length,i;
                for(i=0;i<len;i++){
                    alert(results.row.item(i).text);
                }
                            console.log('sent value')
            })
            console.log('post log')
        })
    }

    document.getElementById("runreport").onclick = function(){
         alert('running')
     }

    document.getElementById("exportcsv").onclick = function(){

              var data;
              console.log(checkboxFilter);
              if(checkboxFilter == 1){

                data = [
                ['yolo','1'],
                ['','']
                ];
              }
              else if(checkboxFilter == 0 || checkboxFilter == null){
                data = [
                ['yolo','0'],
                ['','']
                ];
              }
              var csv = 'Name,Title\n';
                 data.forEach(function(row) {
                         csv += row.join(',');
                         csv += "\n";
                 });

                 console.log(csv);
                 var hiddenElement = document.createElement('a');
                 hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                 hiddenElement.target = '_blank';
                 hiddenElement.download = 'test.csv';
                 hiddenElement.click();
             }


 }

function run(){
    window.report = Report
}
run()

from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from yattag import Doc, indent


@csrf_protect
@ensure_csrf_cookie
@login_required(login_url='/mappin')
def settings_interface(request):
    """
    The client settings page.  The page allows users to change their own settings,
    or the settings of the server, if they have permission

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    # Get the document's constructors
    doc, tag, text = Doc().tagtext()

    # Create an HTML document
    doc.asis('<!DOCTYPE html>')
    with tag('html', lang='en'):

        # Create the HTML header
        with tag('head'):
            with tag('meta', charset='UTF-8'):
                pass

            # Include the favicon
            with tag(
                    'link',
                    rel='shortcut icon',
                    type='image/png',
                    href='/static/images/mappin.png'
            ):
                pass

            # Include jQuery
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js'):
                pass

            # Include W3 CSS Stylesheet
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/w3.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/slickgrid-w3.css'):
                pass

            # Include SlickGrid CSS
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/css/smoothness/jquery-ui-1.11.3.custom.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/slick.grid.css'):
                pass

            # Include SlickGrid dependencies
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery-ui-1.11.3.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery.event.drag-2.3.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery.event.drop-2.3.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.core.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.grid.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.editors.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.formatters.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.dataview.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.checkboxselectcolumn.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.autotooltips.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellrangedecorator.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellrangeselector.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellcopymanager.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellselectionmodel.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.rowselectionmodel.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/controls/slick.columnpicker.js'):
                pass

        with tag('body', id='body'):
            # Give the page a title
            with tag('title'):
                text("Mappin: A Map-Based Inventory System")

            with tag('script', type='module', src='/load_mappin_settings.js'):
                pass

    return HttpResponse(indent(doc.getvalue()), content_type='text/html')

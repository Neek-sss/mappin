from django.contrib.auth import authenticate, login, logout
from django.http import QueryDict
from django.shortcuts import HttpResponse, redirect
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from yattag import Doc, indent


@csrf_protect
@ensure_csrf_cookie
def login_interface(request):
    """
    The client login page.  If the client is logged in, it is redirected to the
    interface page, otherwise the login script is run on the client.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    # Send the user to the interface if logged in, otherwise log them in
    if request.user.is_authenticated:
        return redirect('/interface')
    else:

        # Get the document's constructors
        doc, tag, text = Doc().tagtext()

        # Create an HTML document
        doc.asis('<!DOCTYPE html>')
        with tag('html', lang='en'):

            # Create the HTML header
            with tag('head'):
                with tag('meta', charset='UTF-8'):
                    pass

                # Include the favicon
                with tag(
                    'link',
                    rel='shortcut icon',
                    type='image/png',
                        href='/static/images/mappin.png'
                ):
                    pass

                # Include jQuery
                with tag('script', type='text/javascript',
                         src='/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js'):
                    pass

                # Include W3 CSS Stylesheet
                with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                         href='/static/w3.css'):
                    pass

            with tag('body', id='body'):

                # Give the page a title
                with tag('title'):
                    text("Mappin: A Map-Based Inventory System")

                with tag('script', type='module', src='/load_mappin_login.js'):
                    pass

        return HttpResponse(indent(doc.getvalue()), content_type='text/html')



@csrf_protect
def api_login(request):
    """
    Processes a login attempt.  Successful logins are redirected, unsuccessful
    logins are errors.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=401)

    # Automatically redirect the user if already logged in, otherwise attempt
    # a login
    if request.user.is_authenticated:
        ret = redirect('/interface', status=200)
    else:

        # Get the POST data
        post_data = QueryDict(request.body)

        # Make sure the login credentials are in the post
        if 'username' in post_data.keys() and 'password' in post_data.keys():

            # Attempt a login
            user = authenticate(
                request,
                username=post_data['username'],
                password=post_data['password']
            )

            # Redirect on success
            if user is not None:
                login(request, user)
                ret = redirect('/interface', status=200)

        else:
            ret = HttpResponse(status=400)

    return ret



@csrf_protect
def api_logout(request):
    """
    Logout a client and redirect to the login page.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    logout(request)
    return redirect('/mappin')

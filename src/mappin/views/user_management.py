from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.http import QueryDict
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect


# TODO: Protect these

@login_required(login_url='/mappin')
@permission_required("auth.add_user")
@csrf_protect
def new_user(request):
    """
    Creates a new user with the specified credentials

    Args:
        request (HttpRequest): The client's request
        slug (str): The data requested

    Returns:
        HttpResponse: The server's response
    """

    post_data = QueryDict(request.body)

    # Get the update that needs to occur
    username = post_data['username']
    password = post_data['password']

    User.objects.create_user(username=username, password=password)

    return HttpResponse(status=200)


@login_required(login_url='/mappin')
@csrf_protect
def change_password(request):
    """
    Loads the requested user data and returns it to the client.

    Args:
        request (HttpRequest): The client's request
        slug (str): The data requested

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=400)

    post_data = QueryDict(request.body)

    # Get the update that needs to occur
    username = ''
    if 'username' in post_data.keys():
        username = post_data['username']
    else:
        username = request.user.username
    password = post_data['old_password']
    new_password = post_data['new_password']

    change_user = authenticate(username=username, password=password)

    if change_user is not None:
        user = User.objects.get_by_natural_key(username)
        user.set_password(new_password)
        user.save()
        ret = HttpResponse(status=200)

    return ret


@login_required(login_url='/mappin')
@permission_required("auth.delete_user")
@csrf_protect
def delete_user(request):
    """
    Removes the current user from the database

    Args:
        request (HttpRequest): The client's request
        slug (str): The data requested

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=400)

    post_data = QueryDict(request.body)

    # Get the update that needs to occur
    username = ''
    if 'username' in post_data.keys():
        username = post_data['username']
    else:
        username = request.user.username
    password = post_data['old_password']
    new_password = post_data['new_password']

    change_user = authenticate(username=username, password=password)

    if change_user is not None:
        user = User.objects.get_by_natural_key(username)
        user.set_password(new_password)
        user.save()
        ret = HttpResponse(status=200)

    return ret

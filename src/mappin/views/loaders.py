import json

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.core import serializers
from django.http import QueryDict
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect

from mappin.models import (
    InventoryBuilding, InventoryBuildingHistory, InventoryBuildingImage,
    InventoryBuildingImageHistory, InventoryItem, InventoryItemHistory, InventoryItemImage, InventoryItemImageHistory,
)
from .helpers import json_response, permissions_for_user


@login_required(login_url='/mappin')
@csrf_protect
def client_data_load(request, slug):
    """
    Loads all associated data from the database as provies it to the client.

    Args:
        request (HttpRequest): The client's request
        slug (str): The data requested

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=400)

    # Only accept valid slugs
    if slug == 'image_building':

        # Send all the InventoryBuildingImage's to the client
        ret = json_response(
            serializers.serialize('json', InventoryBuildingImage.objects.order_by('path'))
        )

    elif slug == 'building':

        # Send all building data to the client
        buildings = InventoryBuilding.objects.order_by('name')

        # If there are no buildings currently (so it is a fresh server), make
        # a new one
        if len(buildings) is 0:
            InventoryBuilding(name='New Building').save()
            buildings = InventoryBuilding.objects.order_by('name')

        ret = json_response(serializers.serialize('json', buildings))

    elif slug == 'image_item':

        # Send all the InventoryItemImage's to the client
        ret = json_response(
            serializers.serialize('json', InventoryItemImage.objects.order_by('path'))
        )

    elif slug == 'item':

        # Send all the InventoryItem's to the client
        ret = json_response(
            serializers.serialize('json', InventoryItem.objects.order_by('name'))
        )
    elif slug == 'user':
        ret = user_loader(request)
    elif slug == 'permission':
        ret = permission_loader(request)

    return ret


@permission_required("auth.view_user")
def user_loader(request):
    # Send all the User's to the client
    users = serializers.serialize('json', User.objects.order_by('username'))

    json_users = json.loads(users)
    user_ret = []
    for user in json_users:
        user_ret.append({
            'pk': user["pk"],
            'model': user["model"],
            'fields': {
                'username': user["fields"]["username"],
                'is_superuser': user["fields"]["is_superuser"],
            }
        })

    return json_response(json.dumps(user_ret))


@permission_required("auth.view_permission")
def permission_loader(request):
    # Send all the Permission's to the client
    user_permissions = []
    for user in User.objects.order_by('username'):
        user_permissions.extend(permissions_for_user(user))

    return json_response(json.dumps(user_permissions))


@login_required(login_url='/mappin')
@csrf_protect
def client_history_load(request, slug):
    """
    A generic api for loading an object's history.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    # Make the request data searchable
    post_data = QueryDict(request.body)

    # Get the request time
    time = float(post_data['time'])

    # Load the history of objects that has happened since the last time the
    # client updated
    history = None
    if slug == 'image_building':
        history = InventoryBuildingImageHistory.objects.filter(time_of_change__gte=time).order_by('time_of_change')
    elif slug == 'building':
        history = InventoryBuildingHistory.objects.filter(time_of_change__gte=time).order_by('time_of_change')
    elif slug == 'image_item':
        history = InventoryItemImageHistory.objects.filter(time_of_change__gte=time).order_by('time_of_change')
    elif slug == 'item':
        history = InventoryItemHistory.objects.filter(time_of_change__gte=time).order_by('time_of_change')
    elif slug == 'user':
        # TODO: Actual histories
        # TODO: Make this with a security check
        history = []
    elif slug == 'permission':
        # TODO: Actual histories
        # TODO: Make this with a security check
        history = []

    if history is None:
        return HttpResponse(status='400')
    else:
        return json_response(serializers.serialize('json', history))

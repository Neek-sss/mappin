import json
from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Permission, User
from django.core import serializers
from django.http import QueryDict
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect

from mappin.models import (
    InventoryBuilding, InventoryBuildingHistory, InventoryBuildingImage, InventoryItem,
    InventoryItemHistory, InventoryItemImage,
)
from .helpers import json_response, permissions_for_user


def item_history(building, item, api_type, data):
    """
    Creates a database entry for the input item change.

    Args:
        building (InventoryBuilding): The item's building
        item (InventoryItem): The item that was changed
        api_type (str): The type of change that occurred
        data (str): The JSON string representing the change
    """

    InventoryItemHistory(
        building=building,
        history_object=item,
        time_of_change=datetime.now().timestamp(),
        type=api_type,
        data=data,
    ).save()


def building_history(building, api_type, data):
    """
    Creates a database entry for the input building change.

    Args:
        building (InventoryBuilding): The building that was changed
        api_type (str): The type of change that occurred
        data (str): The JSON string representing the change
    """

    InventoryBuildingHistory(
        history_object=building,
        time_of_change=datetime.now().timestamp(),
        type=api_type,
        data=data,
    ).save()


def permission(request, slug):
    current_permissions = permissions_for_user(request.user)

    ret = {
        'pk': 0,
        'model': 'mappin.permission',
        'fields': {
            'view': False,
            'add': False,
            'change': False,
            'delete': False,
        }
    }
    if slug == "item":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "inventoryitem":
                ret = permission
    elif slug == "image_item":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "inventoryitemimage":
                ret = permission
    elif slug == "building":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "inventorybuilding":
                ret = permission
    elif slug == "image_building":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "inventorybuildingimage":
                ret = permission
    elif slug == "user":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "user":
                ret = permission
    elif slug == "permission":
        for permission in current_permissions:
            if permission["fields"]["codename"] == "permission":
                ret = permission

    if request.user.is_superuser:
        ret["fields"]["view"] = True
        ret["fields"]["add"] = True
        ret["fields"]["change"] = True
        ret["fields"]["delete"] = True

    return json_response(json.dumps([ret]))


def new_object(request, slug, post_data):
    """
    Makes a brand new database object given client post data.

    Args:
        slug (str): The client request slug
        post_data (dict): The client's request data

    Returns:
        str: A JSON string of the item that was added
    """

    output_data = ''

    # It's different for each item type
    if slug == 'image_building':

        # Images are added through the upload_image api
        pass

    elif slug == 'building':
        if request.user.has_perm("mappin.add_inventorybuilding"):
            # Save the building [Need Name]
            name = post_data['name']
            database_item = InventoryBuilding(name=name)
            database_item.save()

            # Get the JSON string
            output_data = serializers.serialize('json', [database_item])

            # Save the history event
            building_history(database_item, 'add', output_data)

    elif slug == 'image_item':

        # Images are added through the upload_image api
        pass

    elif slug == 'item':
        if request.user.has_perm("mappin.add_inventoryitem"):
            # Load the correct building
            building = InventoryBuilding.objects.filter(id=post_data['building']).first()

            # Save the item [Need Name]
            name = post_data['name']
            database_item = InventoryItem(
                building=building,
                name=name,
            )
            database_item.save()

        # Get the JSON string
        output_data = serializers.serialize('json', [database_item])

        # Save the history event
        item_history(building, database_item, 'add', output_data)

    return output_data


def get_object(slug, post_data):
    """
    Retrieves the different data types and returns them.

    Args:
        slug (str): The data type to retrieve
        post_data (dict): The client's requesting POST information

    Returns:
        obj: The requested object, or None if invalid
    """

    ret = None

    if slug == 'image_building':

        # Can't update image data like other data, so no point in getting it
        # here
        pass

    elif slug == 'building':

        # Get the building
        ret = InventoryBuilding.objects.filter(id=post_data['id']).first()

    elif slug == 'image_item':

        # Can't update image data like other data, so no point in getting it
        # here
        pass

    elif slug == 'item':

        # Get the item's building
        building = InventoryBuilding.objects.filter(
            id=post_data['building']
        ).first()

        # Get the item
        ret = InventoryItem.objects.filter(
            id=post_data['id'],
            building=building
        ).first()

    return ret


def update_object(request, slug, object, post_data):
    """
    Updates an database object's data with the requested information and saves
    it.

    Args:
        slug (str): The data type
        object (obj): The object to update
        post_data (dict): The client's POST data
    """

    if (
            slug == "permission" and request.user.has_perm("auth.change_permission")
    ) or (
            slug == "user" and request.user.has_perm("auth.change_user")
    ) or (
            slug == "item" and request.user.has_perm("mappin.change_inventoryitem")
    ) or (
            slug == "building" and request.user.has_perm("mappin.change_inventorybuilding")
    ) or (
            slug == "image_item" and request.user.has_perm("mappin.change_inventoryitemimage")
    ) or (
            slug == "image_building" and request.user.has_perm("mappin.change_inventorybuildingimage")
    ):
        ret = HttpResponse(status=400)

        if slug == "permission":
            column = post_data['column']
            data = post_data['data']

            # Booleans are not processed properly, so fix that
            if data in ["False", "false", "FALSE"]:
                data = False
            elif data in ["True", "true", "TRUE"]:
                data = True
            elif column in ['image'] and data is '':
                data = None

            user_id = post_data['user_id']
            action_id = post_data['action_id']

            user = User.objects.filter(id=user_id).first()
            permission = Permission.objects.filter(id=action_id).first()

            if data:
                user.user_permissions.add(permission)
            else:
                user.user_permissions.remove(permission)

            user.save()

            ret_data = json.dumps({column: data})

            ret = ret_data
        else:
            # Get the update that needs to occur
            column = post_data['column']
            data = post_data['data']

            # Booleans are not processed properly, so fix that
            if data in ["False", "false", "FALSE"]:
                data = False
            elif data in ["True", "true", "TRUE"]:
                data = True
            elif column in ['image'] and data is '':
                data = None

            # Performing the update can only really work in a generic way if the object
            # is serialized, then updated in string form, and then deserialized
            json_object = json.loads(serializers.serialize('json', [object]))[0]
            json_object['fields'][column] = data

            ret_data = ''
            for database_object in serializers.deserialize('json', json.dumps([json_object])):
                database_object.save()

                ret_data = json.dumps({column: data})

                # Save the history event
                if slug == 'item':
                    database_item = InventoryItem.objects.filter(id=json_object['pk']).first()
                    item_history(
                        InventoryBuilding.objects.filter(id=json_object['fields']['building']).first(),
                        database_item,
                        'update',
                        ret_data
                    )
                elif slug == 'building':
                    database_item = InventoryBuilding.objects.filter(id=json_object['pk']).first()
                    building_history(database_item, 'update', ret_data)

            ret = ret_data

        return ret
    else:
        return HttpResponse(status='304')


def delete_object(request, slug, post_data):
    """
    Permanently deletes an object from the database.

    Args:
        slug (str): The type of object to delete
        post_data (dict): The client's POST data
    """

    if slug == 'image_building':

        # Remove the specified building image
        InventoryBuildingImage.objects.filter(id=post_data['id']).first().delete()

    elif slug == 'building':

        # Remove the specified building
        InventoryBuilding.objects.filter(id=post_data['id']).first().delete()

    elif slug == 'image_item':

        # Remove the specified item image
        InventoryItemImage.objects.filter(id=post_data['id']).first().delete()

    elif slug == 'item':

        # Remove the specified item
        building = InventoryBuilding.objects.filter(id=post_data['building']).first()
        InventoryItem.objects.filter(id=post_data['id'], building=building).first().delete()


@login_required(login_url='/mappin')
@csrf_protect
def server_api(request, slug):
    """
    A generic connecting point between the client and server, this allows the
    client to add, update, and delete database objects.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=404)

    # Make the request data searchable
    post_data = QueryDict(request.body)

    # Get the request type
    type = post_data['type']
    output_data = ''

    # Perform the proper request
    if type == 'add':
        output_data = new_object(request, slug, post_data)
    elif type == 'update':
        object = get_object(slug, post_data)
        output_data = update_object(request, slug, object, post_data)
    elif type == 'delete':
        delete_object(request, slug, post_data)

    return json_response(output_data)

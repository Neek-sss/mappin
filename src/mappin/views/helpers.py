import json
from datetime import datetime
from pathlib import Path

from django.contrib.auth.models import Permission
from django.core import serializers
from django.shortcuts import HttpResponse


def load_file(file_path):
    """
    Loads a file into memory

    Args:
        file_path (str): The relative file path to load

    Returns:
        str: The file's contents
    """

    cwd = Path(__file__).parent
    full_file_path = (cwd / '..' / file_path).resolve()
    return open(full_file_path, mode='rb').read()


def load_py(file_path):
    """
    Provides a Python script file.

    Args:
        file_path (str): The relative file path to load

    Returns:
        HttpResponse: The response file
    """

    cwd = Path(__file__).parent
    full_file_path = (cwd / file_path).resolve()
    return HttpResponse(
        load_file(file_path),
        content_type='text/python',
        status=200
    )


def load_js(file_path):
    """
    Provides a Javascript script file.

    Args:
        file_path (str): The relative file path to load

    Returns:
        HttpResponse: The response file
    """

    cwd = Path(__file__).parent
    full_file_path = (cwd / file_path).resolve()
    return HttpResponse(
        load_file(file_path),
        content_type='application/javascript',
        status=200
    )


def load_wasm(file_path):
    """
    Provides a WASM script file.

    Args:
        file_path (str): The relative file path to load

    Returns:
        HttpResponse: The response file
    """

    return HttpResponse(
        load_file(file_path),
        content_type='application/wasm',
        status=200
    )


def json_response(data):
    """
    Creates a generic JSON HTTP response with the input JSON data.

    Args:
        data (str): The data to put in the response

    Returns:
        HttpResponse: The completed JSON HTTP response
    """

    # Create the response
    response = HttpResponse(
        data,
        content_type='text/json',
        status=200,
    )

    # Add a timestamp
    response.set_cookie('timestamp', datetime.now().timestamp())

    return response


def permissions_for_user(user):
    permission = serializers.serialize('json', Permission.objects.order_by('name'))
    json_permissions = json.loads(permission)

    permission_index = {}
    for permission in json_permissions:
        permission_pk = permission["pk"]
        permission_codename = permission["fields"]["codename"]
        split_string = permission_codename.split("_")
        action = split_string[0]
        model = split_string[1]
        permission_index[permission_pk] = [action, model]

    model_mapping = {
        # "contenttype": "Content Type",
        # "group": "Group",
        "inventorybuilding": "Building",
        # "inventorybuildinghistory": "Building History",
        "inventorybuildingimage": "Building Image",
        # "inventorybuildingimagehistory": "Building Image History",
        "inventoryitem": "Item",
        # "inventoryitemhistory": "Item History",
        "inventoryitemimage": "Item Image",
        # "inventoryitemimagehistory": "Item Image History",
        # "logentry": "Log Entry",
        "permission": "Permission",
        # "session": "Session",
        "user": "User",
    }

    permissions_creator = {}
    for permission in json_permissions:
        permission_pk = permission["pk"]
        permission_codename = permission["fields"]["codename"]
        split_string = permission_codename.split("_")
        action = split_string[0]
        model = split_string[1]
        if model in model_mapping.keys():
            if model not in permissions_creator.keys():
                permissions_creator[model] = {
                    'pk': permission_pk,
                    'model': 'mappin.permission',
                    'fields': {
                        'name': model_mapping[model],
                        'codename': model,
                        'view': False,
                        'add': False,
                        'change': False,
                        'delete': False,
                    }
                }
            permissions_creator[model]['fields'][action + "_id"] = permission_pk

    user_json = json.loads(serializers.serialize('json', [user]))[0]
    user_pk = user_json["pk"]
    current_user_permissions = user_json["fields"]["user_permissions"]

    current_permissions_creator = json.loads(json.dumps(permissions_creator))
    for permission in current_permissions_creator.values():
        permission["pk"] = int(str(user_pk) + str(permission["pk"]))
        permission["fields"]["user_id"] = user_pk

    for user_permission in current_user_permissions:
        user_permission_data = permission_index[user_permission]
        user_permission_action = user_permission_data[0]
        user_permission_model = user_permission_data[1]

        current_permissions_creator[user_permission_model]['fields'][user_permission_action] = True

    user_permissions = []
    user_permissions.extend(current_permissions_creator.values())

    return user_permissions

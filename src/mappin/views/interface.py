# Disable CSSUtils' default warning logging (it was getting annoying)
import logging

from cssutils import css, log
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from yattag import Doc, indent

log.setLevel(logging.CRITICAL)


@csrf_protect
@ensure_csrf_cookie
@login_required(login_url='/mappin')
def interface(request):
    """
    Provides the main program interface to the client.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    # Create the CSS Sheet
    sheet = css.CSSStyleSheet()

    # TODO: Use cssutils color rules and parenting

    # Interface header Style
    interface_header_style = css.CSSStyleDeclaration()
    interface_header_style['z-index'] = '80'
    interface_header_rule = css.CSSStyleRule(selectorText=u'.interface-header', style=interface_header_style)
    sheet.add(interface_header_rule)

    # Building name style
    building_name_style = css.CSSStyleDeclaration()
    building_name_style['cursor'] = 'text'
    building_name_rule = css.CSSStyleRule(selectorText=u'.building-name',
                                          style=building_name_style)
    sheet.add(building_name_rule)

    # Panel style
    panel_style = css.CSSStyleDeclaration()
    panel_style['overflow-y'] = 'auto'
    panel_rule = css.CSSStyleRule(selectorText=u'.panel', style=panel_style)
    sheet.add(panel_rule)

    # Image manager background style
    image_manager_background_style = css.CSSStyleDeclaration()
    image_manager_background_style['position'] = 'fixed'
    image_manager_background_style['width'] = '100%'
    image_manager_background_style['height'] = '100%'
    image_manager_background_style['top'] = '0px'
    image_manager_background_style['bottom'] = '0px'
    image_manager_background_style['left'] = '0px'
    image_manager_background_style['right'] = '0px'
    image_manager_background_style['z-index'] = 101
    image_manager_background_style['cursor'] = 'pointer'
    image_manager_background_style['background-color'] = 'rgba(0, 0, 0, 0.5)'
    image_manager_background_rule = css.CSSStyleRule(selectorText=u'.image-manager-background',
                                                     style=image_manager_background_style)
    sheet.add(image_manager_background_rule)

    # Image manager foreground style
    image_manager_foreground_style = css.CSSStyleDeclaration()
    image_manager_foreground_style['position'] = 'fixed'
    image_manager_foreground_style['width'] = '50%'
    image_manager_foreground_style['height'] = '50%'
    image_manager_foreground_style['transform'] = 'translate(50%, 50%)'
    image_manager_foreground_style['cursor'] = 'auto'
    image_manager_foreground_style['overflow-y'] = 'hidden'
    image_manager_foreground_style['background-color'] = 'rgba(255, 255, 255, 1.0)'
    image_manager_foreground_rule = css.CSSStyleRule(selectorText=u'.image-manager-foreground',
                                                     style=image_manager_foreground_style)
    sheet.add(image_manager_foreground_rule)

    # Image upload form style
    image_upload_form_style = css.CSSStyleDeclaration()
    image_upload_form_style['height'] = '10%'
    image_upload_form_rule = css.CSSStyleRule(selectorText=u'.image-upload-form', style=image_upload_form_style)
    sheet.add(image_upload_form_rule)

    # Image collection style
    image_collection_style = css.CSSStyleDeclaration()
    image_collection_style['display'] = 'flex'
    image_collection_style['flex-wrap'] = 'wrap'
    image_collection_style['width'] = '100%'
    image_collection_style['height'] = '70%'
    image_collection_style['flex-grow'] = 1
    image_collection_style['overflow-y'] = 'auto'
    image_collection_style['background-color'] = 'rgba(255, 255, 255, 1.0)'
    image_collection_rule = css.CSSStyleRule(selectorText=u'.image-collection', style=image_collection_style)
    sheet.add(image_collection_rule)

    # Image in collection style
    image_in_collection_style = css.CSSStyleDeclaration()
    image_in_collection_style['min-width'] = '25%'
    image_in_collection_style['max-width'] = '25%'
    image_in_collection_style['flex'] = '25%'
    image_in_collection_rule = css.CSSStyleRule(selectorText=u'.image-in-collection', style=image_in_collection_style)
    sheet.add(image_in_collection_rule)

    # Get the document's constructors
    doc, tag, text = Doc().tagtext()

    # Create the HTML document
    doc.asis('<!DOCTYPE html>')
    with tag('html', lang='en'):

        # Create the header
        with tag('head'):
            with tag('meta', charset='UTF-8'):
                pass

            # Include the favicon
            with tag('link', rel='shortcut icon', type='image/png', href='/static/images/mappin.png'):
                pass

            # Include SlickGrid CSS
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/css/smoothness/jquery-ui-1.11.3.custom.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/SlickGrid-2.4.3/slick.grid.css'):
                pass
            # with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
            #          href='/static/SlickGrid-2.4.3/slick-default-theme.css'):
            #     pass
            # with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
            #          href='/static/SlickGrid-2.4.3/plugins-gdoc-style.css'):
            #     pass

            # Include OpenLayers CSS
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/OpenLayers v5.3.0/src/ol.css'):
                pass

            # Include W3 CSS Stylesheet
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/w3.css'):
                pass
            with tag('link', type='text/css', rel='stylesheet', charset='utf-8', media='screen',
                     href='/static/slickgrid-w3.css'):
                pass

            # Include custom CSS Stylesheet
            with tag('style'):
                text(str(sheet.cssText, 'UTF-8'))

            # Include SlickGrid dependencies
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery-ui-1.11.3.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery.event.drag-2.3.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/lib/jquery.event.drop-2.3.0.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.core.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.grid.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.editors.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.formatters.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/slick.dataview.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.checkboxselectcolumn.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.autotooltips.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellrangedecorator.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellrangeselector.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellcopymanager.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.cellselectionmodel.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/plugins/slick.rowselectionmodel.js'):
                pass
            with tag('script', type='text/javascript',
                     src='/static/SlickGrid-2.4.3/controls/slick.columnpicker.js'):
                pass

            # Include OpenLayers dependencies
            with tag('script', type='text/javascript',
                     src='/static/OpenLayers v5.3.0/ol.js'):
                pass

        with tag('body', id='body'):

            # Give the page a title
            with tag('title'):
                text("Mappin: A Map-Based Inventory System")

            with tag('script', type='module', src='/load_mappin_interface.js'):
                pass

            with tag('script', type='module', src='/report.js'):
                pass

    # Return the final HTML Document
    return HttpResponse(indent(doc.getvalue()), content_type='text/html')

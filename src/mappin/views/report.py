import json
import subprocess
from datetime import datetime
from os import listdir, mkdir, path
from shutil import copyfile, rmtree

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import QueryDict
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect

from mappin.admin import InventoryItemResource


@login_required(login_url='/mappin')
@csrf_protect
def generate_report(request):
    """
    Creates a report when requested.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    ret = HttpResponse(status=500)

    project_path = path.dirname(path.realpath(__file__)) + "/../../.."
    module_dir = project_path + "/src/reporting_modules"

    report_dir = project_path + "/report"
    if not path.exists(settings.MEDIA_ROOT):
        mkdir(settings.MEDIA_ROOT)
    if not path.exists(report_dir):
        mkdir(report_dir)

    # Make the request data searchable
    post_data = QueryDict(request.body)

    # Save the csv data
    with open(report_dir + "/data.csv", 'w') as destination:
        data = InventoryItemResource().export()
        destination.write(data.csv)

    modules = json.loads(post_data['selected'])
    with open(report_dir + "/in.cbmd", "w") as destination:
        destination.write("---\n")
        destination.write("title: Report\n")
        destination.write("date: " + str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + "\n")
        destination.write("geometry: \"left=3cm,right=3cm,top=1cm,bottom=1cm\"\n")
        destination.write("---\n\n")

        for i in range(0, len(modules)):
            module_name = modules[i]
            if module_name + ".py.part" in listdir(module_dir):
                with open(module_dir + "/imports.py.part", "r") as header, \
                        open(module_dir + "/module_start.py.part", "r") as module_start, \
                        open(module_dir + "/module_end.py.part", "r") as module_end, \
                        open(module_dir + "/" + module_name + ".py.part", "r") as source:
                    header_string = header.readlines()
                    module_start_string = module_start.readlines()
                    module_end_string = module_end.readlines()

                    module_string = source.readlines()
                    destination.write("```{.python .cb.run session=python" + str(i) + " show=stdout:raw}\n")
                    destination.writelines(header_string)
                    destination.writelines(module_start_string)
                    destination.writelines(module_string)
                    destination.writelines(module_end_string)
                    destination.write("```\n")
            elif module_name + ".r.part" in listdir(module_dir):
                with open(module_dir + "/imports.r.part", "r") as header, \
                        open(module_dir + "/module_start.r.part", "r") as module_start, \
                        open(module_dir + "/module_end.r.part", "r") as module_end, \
                        open(module_dir + "/" + module_name + ".r.part", "r") as source:
                    header_string = header.readlines()
                    module_start_string = module_start.readlines()
                    module_end_string = module_end.readlines()

                    module_string = source.readlines()
                    destination.write("```{.R .cb.run session=R" + str(i) + " show=stdout:raw}\n")
                    destination.writelines(header_string)
                    destination.writelines(module_start_string)
                    destination.writelines(module_string)
                    destination.writelines(module_end_string)
                    destination.write("```\n")

    out_code = subprocess.Popen(
        ["codebraid", "pandoc", "--from", "markdown", "in.cbmd", "--out", "out.pdf", "--overwrite"],
        cwd=report_dir).wait()

    if out_code is 0:
        copyfile(report_dir + "/out.pdf", settings.MEDIA_ROOT + "/out.pdf")
        rmtree(report_dir)
        ret = HttpResponse(status=200)

    return ret

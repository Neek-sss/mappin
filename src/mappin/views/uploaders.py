import random
import string
from os import mkdir, path

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.shortcuts import HttpResponse
from django.views.decorators.csrf import csrf_protect
from PIL import Image

from mappin.models import InventoryBuildingImage, InventoryItemImage
from .helpers import json_response


@login_required(login_url='/mappin')
@csrf_protect
def upload_image(request, slug):
    """
    Allows the client to upload images to the server and retrieve them later for
    use in the map panel.

    Args:
        request (HttpRequest): The client's request

    Returns:
        HttpResponse: The server's response
    """

    if (
            slug == "image_building" and request.user.has_perm("mappin.add_inventorybuildingimage")
    ) or (
            slug == "image_item" and request.user.has_perm("mappin.add_inventoryitemimage")
    ):
        # Only accept valid image types
        if slug in ['image_building', 'image_item']:

            print(request.FILES)

            # Make the media folder, if it doesn't exist yet
            if not path.exists(settings.MEDIA_ROOT):
                mkdir(settings.MEDIA_ROOT)

            # Generate a random, unique filename
            filename = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)]) + \
                       path.splitext(request.FILES['file'].name)[1]
            while path.exists(filename):
                filename = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)]) + \
                           path.splitext(request.FILES['file'].name)[1]

            # Write the client's image to the file
            with open(settings.MEDIA_ROOT + filename, 'wb+') as destination:
                for chunk in request.FILES['file'].chunks():
                    destination.write(chunk)

            # Read the saved image's data
            image = Image.open(settings.MEDIA_ROOT + filename)

            # Create a database image entry with the appropriate data
            image_object = None
            if slug == 'image_building':
                image_object = InventoryBuildingImage(path=settings.MEDIA_URL + filename, width=image.width,
                                                      height=image.height)
                image_object.save()
            elif slug == 'image_item':
                image_object = InventoryItemImage(path=settings.MEDIA_URL + filename, width=image.width,
                                                  height=image.height)
                image_object.save()

            return json_response(serializers.serialize('json', [image_object]))
        else:
            return HttpResponse(status='400')
    else:
        return HttpResponse(status='304')

"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

urlpatterns = [
    # Include these links in the root index
    path('', views.login_interface),

    # Login page files
    path('load_mappin_login.js', lambda _: views.load_js('frontend/load_mappin_login.js')),
    path('mappin_login.js', lambda _: views.load_js('../mappin-login/pkg/mappin_login.js')),
    path('mappin_login_bg.wasm', lambda _: views.load_wasm('../mappin-login/pkg/mappin_login_bg.wasm')),
    path('api_login', views.api_login),
    path('api_logout', views.api_logout),

    # The primary interface page
    path('interface', views.interface),
    path('load_mappin_interface.js', lambda _: views.load_js('frontend/load_mappin_interface.js')),
    path('mappin_interface.js', lambda _: views.load_js('../mappin-interface/pkg/mappin_interface.js')),
    path('mappin_interface_bg.wasm', lambda _: views.load_wasm('../mappin-interface/pkg/mappin_interface_bg.wasm')),
    path('report.js', lambda _: views.load_js('frontend/report.js')),

    # Settings page
    path('settings', views.settings_interface),
    path('load_mappin_settings.js', lambda _: views.load_js('frontend/load_mappin_settings.js')),
    path('mappin_settings.js', lambda _: views.load_js('../mappin-settings/pkg/mappin_settings.js')),
    path('mappin_settings_bg.wasm', lambda _: views.load_wasm('../mappin-settings/pkg/mappin_settings_bg.wasm')),
    path('report.js', lambda _: views.load_js('frontend/report.js')),

    # API
    path('permission/<slug:slug>/', views.permission),
    path('load/<slug:slug>/', views.client_data_load),
    path('api/<slug:slug>/', views.server_api),
    path('history/<slug:slug>/', views.client_history_load),
    path('upload_image/<slug:slug>/', views.upload_image),

    # User Management
    path('new_user/', views.new_user),
    path('change_password/', views.change_password),

    # Report
    path('generate_report/', views.generate_report),
]

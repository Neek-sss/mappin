from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export import resources

from .models import (
    InventoryBuilding, InventoryBuildingHistory, InventoryBuildingImage, InventoryBuildingImageHistory,
    InventoryItem, InventoryItemHistory, InventoryItemImage, InventoryItemImageHistory,
)


# Allows for managing any database data with the URL /admin
@admin.register(InventoryItem)
class InventoryItemAdmin(ImportExportModelAdmin):
    pass

class InventoryItemResource(resources.ModelResource):
    class Meta:
        model = InventoryItem

class InventoryItemResource(resources.ModelResource):
    class Meta:
        model = InventoryItem


@admin.register(InventoryBuilding)
class InventoryBuildingAdmin(ImportExportModelAdmin):
    pass


admin.site.register(InventoryItemHistory)
admin.site.register(InventoryItemImage)
admin.site.register(InventoryItemImageHistory)
admin.site.register(InventoryBuildingHistory)
admin.site.register(InventoryBuildingImage)
admin.site.register(InventoryBuildingImageHistory)

use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};

use js_sys::{Array as JsArray, Function as JsFunction, Object as JsObject, Reflect as JsReflect};
use wasm_bindgen::JsValue;
use web_sys::HtmlElement;

use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    panel::{PanelManager, PanelType},
    server_api::ServerAPI,
};

use crate::{
    data_connector::DataConnector,
    slick_grid::{CheckboxSelectColumn, Grid, RowSelectionModel},
};

/// A macro very similar to `set_on_x`, but used for setting functions in the SlickGrid spreadsheet
#[macro_export]
macro_rules! subscribe_x {
    ($grid:expr, $subscriber:expr, $func:expr, $cleanup:expr) => {
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();

        *g.borrow_mut() = Some(wasm_bindgen::closure::Closure::wrap(Box::new(
            move |event: JsValue, args: JsValue| {
                if $cleanup {
                    let _ = f.borrow_mut().take();
                }
                if let Err(error) = $func(&event, &args) {
                    web_sys::console::error_1(&error);
                }
            },
        )
            as Box<dyn FnMut(JsValue, JsValue)>));
        let subscriber = js_sys::Reflect::get(&$grid.into(), &$subscriber.into())?;
        let subscribe =
            js_sys::Function::from(js_sys::Reflect::get(&subscriber, &"subscribe".into())?);
        subscribe.call1(&subscriber, g.borrow().as_ref().unwrap().as_ref())?;
    };
}

pub fn on_x_get_id_column(args: &JsValue) -> Result<(u32, String), JsValue> {
    let grid = Grid::from(js_sys::Reflect::get(&args, &"grid".into())?);

    let item = js_sys::Reflect::get(&args, &"item".into())?;
    let id = DataType::from(js_sys::Reflect::get(&item, &"id".into())?)
        .as_f64()
        .unwrap_or_default() as u32;

    let column_name = if let Some(cell) = js_sys::Reflect::get(&args, &"cell".into())
        .ok()
        .and_then(|v| if v.is_undefined() { None } else { Some(v) })
    {
        let cell_number = cell.as_f64().ok_or(JsValue::from("Cell not a number"))? as usize;
        let column = grid
            .getColumns()
            .values()
            .into_iter()
            .nth(cell_number)
            .unwrap()?;
        js_sys::Reflect::get(&column, &"field".into())?
            .as_string()
            .ok_or(JsValue::from("Name not a string"))?
    } else {
        let column = js_sys::Reflect::get(&args, &"column".into())?;
        js_sys::Reflect::get(&column, &"field".into())?
            .as_string()
            .ok_or(JsValue::from("Name not a string"))?
    };

    Ok((id, column_name))
}

pub fn reset_spreadsheet_keeping_active_cell(grid: &Grid, id: u32) -> Result<(), JsValue> {
    let active_cell = grid.getActiveCell();
    let cell = js_sys::Reflect::get(&active_cell.clone(), &"cell".into())?
        .as_f64()
        .unwrap() as u32;

    grid.invalidate();

    let data_connector = grid.getData();
    let get_row_func =
        js_sys::Function::from(js_sys::Reflect::get(&data_connector, &"getRow".into())?);
    let row = get_row_func
        .call1(&data_connector, &id.into())?
        .as_f64()
        .unwrap() as u32;

    grid.setActiveCell(row, cell);

    Ok(())
}

/// Sets up the options that SlickGrid uses to create a spreadsheet
pub struct SpreadsheetOptions {
    /// Can edit the spreadsheet
    pub editable: bool,
    /// Can edit the spreadsheet
    pub auto_edit: bool,
    /// Can add rows (at the bottom)
    pub enable_add_row: bool,
    /// Can add rows (at the bottom)
    pub leave_space_for_new_rows: bool,
    /// Loading the spreadsheet prevents other things from loading (order
    /// is important so that there isn't a crash)
    pub async_editor_loading: bool,
    /// Arrow keys can be used to move between cells
    pub enable_cell_navigation: bool,
    /// Can drag-click to select a range of cells
    pub enable_cell_range_selection: bool,
    /// Can drag-click to select a range of cells
    pub multi_select: bool,
    /// Resize will update the spreadsheets size
    pub rerender_on_resize: bool,
    /// Column size will fit within the containers width
    pub force_fit_columns: bool,
    /// The height of the container is edited to the datas size
    pub auto_height: bool,
    /// Internally used to specify whether or not their should be a selection column
    pub selection_column: bool,
}

impl Default for SpreadsheetOptions {
    fn default() -> Self {
        Self {
            // Can edit the spreadsheet
            editable: false,
            auto_edit: false,
            // Can add rows (at the bottom)
            enable_add_row: false,
            leave_space_for_new_rows: false,
            // Loading the spreadsheet prevents other things from loading (order
            // is important so that there isnt a crash)
            async_editor_loading: false,
            // Arrow keys can be used to move between cells
            enable_cell_navigation: true,
            // Can drag-click to select a range of cells
            enable_cell_range_selection: true,
            multi_select: true,
            // Resize will update the spreadsheets size
            rerender_on_resize: true,
            // Column size will fit within the containers width
            force_fit_columns: true,
            // The height of the container is edited to the datas size
            auto_height: true,
            selection_column: false,
        }
    }
}

impl SpreadsheetOptions {
    /// Turns the `SpreadsheetOptions` to a `JsObject`
    pub fn as_js_object(&self) -> Result<JsObject, JsValue> {
        let ret: JsValue = JsObject::new().into();
        JsReflect::set(&ret, &"editable".into(), &self.editable.into())?;
        JsReflect::set(&ret, &"autoEdit".into(), &self.auto_edit.into())?;
        JsReflect::set(&ret, &"enableAddRow".into(), &self.enable_add_row.into())?;
        JsReflect::set(
            &ret,
            &"leaveSpaceForNewRows".into(),
            &self.leave_space_for_new_rows.into(),
        )?;
        JsReflect::set(
            &ret,
            &"asyncEditorLoading".into(),
            &self.async_editor_loading.into(),
        )?;
        JsReflect::set(
            &ret,
            &"enableCellNavigation".into(),
            &self.enable_cell_navigation.into(),
        )?;
        JsReflect::set(
            &ret,
            &"enableCellRangeSelection".into(),
            &self.enable_cell_range_selection.into(),
        )?;
        JsReflect::set(&ret, &"multiSelect".into(), &self.multi_select.into())?;
        JsReflect::set(
            &ret,
            &"rerenderOnResize".into(),
            &self.rerender_on_resize.into(),
        )?;
        JsReflect::set(
            &ret,
            &"forceFitColumns".into(),
            &self.force_fit_columns.into(),
        )?;
        JsReflect::set(&ret, &"autoHeight".into(), &self.auto_height.into())?;
        Ok(ret.into())
    }
}

/// A single column in the spreadsheet
pub struct SpreadsheetColumn {
    /// The ID of the column
    pub id: String,
    /// The display name of the column
    pub name: String,
    /// The JS field containing the column data
    pub field: String,
    /// Is the field sortable
    pub sortable: bool,
    /// The optional editor that is used when the field needs to be... edited
    pub editor: Option<JsFunction>,
    /// The optional formatter to use in showing the column when not editing
    pub formatter: Option<JsFunction>,
}

impl From<SpreadsheetColumn> for JsObject {
    fn from(value: SpreadsheetColumn) -> JsObject {
        let ret = JsObject::new();
        JsReflect::set(&ret, &"id".into(), &value.id.into()).expect("Error setting id");
        JsReflect::set(&ret, &"name".into(), &value.name.into()).expect("Error setting name");
        JsReflect::set(&ret, &"field".into(), &value.field.into()).expect("Error setting field");
        JsReflect::set(&ret, &"sortable".into(), &value.sortable.into())
            .expect("Error setting sortable");
        if let Some(editor) = value.editor {
            JsReflect::set(&ret, &"editor".into(), &editor.into()).expect("Error setting editor");
        }
        if let Some(formatter) = value.formatter {
            JsReflect::set(&ret, &"formatter".into(), &formatter.into())
                .expect("Error setting formatter");
        }
        ret
    }
}

pub trait SpreadsheetBuilder {
    /// Get the header DOM elements
    fn get_header_elements(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<Vec<HtmlElement>, JsValue>;
    /// Get all the options used to create the spreadsheet
    fn get_options(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<SpreadsheetOptions, JsValue>;
    /// Retrieves all the columns that the spreadsheet uses
    fn get_columns(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<Vec<SpreadsheetColumn>, JsValue>;
    /// Gets the function that will filter the DatabaseObjects to the current view
    fn get_filter_func(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>;
    /// Get the APIType that this spreadsheet operates on
    fn get_api_type(&self) -> APIType;
    /// The function that controls how editing works
    fn on_cell_change(
        &self,
    ) -> Result<
        Box<dyn FnMut(u32, &str, DataType, &DatabaseObject, &mut ServerAPI) -> Result<(), JsValue>>,
        JsValue,
    >;
    /// The function that controls how new rows are added
    fn on_add_row(
        &self,
    ) -> Result<Box<dyn FnMut(&str, DataType, &mut ServerAPI) -> Result<u32, JsValue>>, JsValue>;
}

/// A basic spreadsheet for processing data
pub struct Spreadsheet {
    /// Notifies the spreadsheet that it needs an update
    pub needs_update: bool,
    /// The SlickGrid `Grid` object (that is the spreadsheet)
    pub grid: Grid,
    /// The `PanelType` identifying this spreadsheet
    pub panel_type: PanelType,
    /// The `APIType` that this spreadsheet works on
    pub api_type: APIType,
    /// The function to run when a cell is changed
    pub on_cell_change:
    Box<dyn FnMut(u32, &str, DataType, &DatabaseObject, &mut ServerAPI) -> Result<(), JsValue>>,
}

impl Spreadsheet {
    /// Creates the new spreadsheet
    pub fn new<B>(
        panel_type: PanelType,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
        builder: B,
    ) -> Result<Self, JsValue>
        where
            B: SpreadsheetBuilder,
    {
        // Set up header
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));
        let panel_header = document
            .get_element_by_id(&format!("{}_header", panel_type.get_id()))
            .ok_or("Header not found")?;
        for header_element in
            builder.get_header_elements(server_api_lock.clone(), panel_manager_lock.clone())?
            {
                panel_header.append_with_node_1(&header_element)?;
            }

        // Get the spreadsheet options
        let options = builder.get_options(server_api_lock.clone())?;

        let (selection_column, columns) = if options.selection_column {
            // Set up the columns and a selection column
            let selection_column = CheckboxSelectColumn::new();
            let columns = {
                let column_array = JsArray::new();
                column_array.push(&selection_column.getColumnDefinition());
                builder
                    .get_columns(server_api_lock.clone())?
                    .into_iter()
                    .fold(column_array, |array, column| {
                        array.push(&JsObject::from(column).into());
                        array
                    })
            };
            (Some(selection_column), columns)
        } else {
            let column_array = JsArray::new();
            let columns = builder
                .get_columns(server_api_lock.clone())?
                .into_iter()
                .fold(column_array, |array, column| {
                    array.push(&JsObject::from(column).into());
                    array
                });
            (None, columns)
        };

        // Create a DataConnector
        let data_connector = DataConnector::new(
            Some(builder.get_filter_func(server_api_lock.clone(), panel_manager_lock.clone())),
            "name".into(),
            None,
            server_api_lock.clone(),
            builder.get_api_type(),
        );

        // Create the spreadsheet
        let grid = Grid::new(
            format!("#{}_content", panel_type.get_id().replace(' ', "\\ ")),
            JsValue::from(data_connector).into(),
            columns,
            options.as_js_object()?,
        );

        let sort_func = move |_: &JsValue, args: &JsValue| {
            let ascending = js_sys::Reflect::get(args, &"sortAsc".into())?
                .as_bool()
                .unwrap_or(false);
            let column = js_sys::Reflect::get(args, &"sortCol".into())?;
            let column_name = js_sys::Reflect::get(&column, &"id".into())?
                .as_string()
                .unwrap_or_else(String::new);

            let grid = Grid::from(js_sys::Reflect::get(args, &"grid".into())?);
            let data_connector = grid.getData();

            let set_sort_column = js_sys::Function::from(js_sys::Reflect::get(
                &data_connector,
                &"set_sort_column".into(),
            )?);
            set_sort_column.call1(&data_connector, &column_name.into())?;

            let set_sort_ascending = js_sys::Function::from(js_sys::Reflect::get(
                &data_connector,
                &"set_sort_ascending".into(),
            )?);
            set_sort_ascending.call1(&data_connector, &ascending.into())?;

            grid.invalidate();

            Ok(())
        };
        subscribe_x!(&grid, "onSort", sort_func, false);

        // Allow the data to be selected
        if options.selection_column {
            grid.registerPlugin(selection_column.unwrap().clone());
        }
        let selection_model = {
            let selection_model_options = JsValue::from(js_sys::Object::new());
            js_sys::Reflect::set(
                &selection_model_options,
                &"selectActiveRow".into(),
                &false.into(),
            )?;
            RowSelectionModel::new(selection_model_options)
        };
        grid.setSelectionModel(selection_model.into());

        // Allow the InventorySpreadsheet/Archive to set up other spreadsheet stuff
        let mut on_cell_change = builder.on_cell_change()?;
        let server_api_lock_ref = server_api_lock.clone();
        let mut f = move |_: &JsValue, args: &JsValue| -> Result<(), JsValue> {
            let (id, column_name) = on_x_get_id_column(args)?;
            let data_object =
                DatabaseObject::from_js_object(&js_sys::Reflect::get(&args, &"item".into())?)?;
            let column_data = data_object.fields[&column_name].clone();

            on_cell_change(
                id,
                &column_name,
                column_data,
                &data_object,
                server_api_lock_ref
                    .clone()
                    .write()
                    .map_err(|_| JsValue::from("Could not lock server_api"))?
                    .as_mut()
                    .as_mut()
                    .unwrap(),
            )?;

            let grid = Grid::from(js_sys::Reflect::get(args, &"grid".into())?);
            let id = on_x_get_id_column(args)?.0;
            reset_spreadsheet_keeping_active_cell(&grid, id)?;

            Ok(())
        };
        subscribe_x!(grid.clone(), "onCellChange", f, false);

        let mut on_add_row = builder.on_add_row()?;
        let server_api_lock_ref = server_api_lock.clone();
        let mut f = move |_: &JsValue, args: &JsValue| -> Result<(), JsValue> {
            let (_, column_name) = on_x_get_id_column(args)?;
            let mut data_object = DatabaseObject::default();
            for entry in js_sys::Object::entries(&js_sys::Object::from(js_sys::Reflect::get(
                &args,
                &"item".into(),
            )?))
                .values()
                .into_iter()
                .filter_map(|a| a.ok())
                .map(|a| {
                    js_sys::Array::from(&a)
                        .values()
                        .into_iter()
                        .collect::<Vec<_>>()
                }) {
                let key = DataType::from(entry[0].clone()?).as_string().unwrap();
                let value = DataType::from(entry[1].clone()?);
                data_object.fields.insert(key, value);
            }

            let column_data = data_object.fields[&column_name].clone();

            let new_id = on_add_row(
                &column_name,
                column_data,
                server_api_lock_ref
                    .clone()
                    .write()
                    .map_err(|_| JsValue::from("Could not lock server_api"))?
                    .as_mut()
                    .as_mut()
                    .unwrap(),
            )?;

            let grid = Grid::from(js_sys::Reflect::get(args, &"grid".into())?);
            reset_spreadsheet_keeping_active_cell(&grid, new_id)?;

            Ok(())
        };
        subscribe_x!(grid.clone(), "onAddNewRow", f, false);

        // TODO: set_onresize for responsive SlickGrid here (will need height/width from panel)
        //
        //        {
        //            let window = web_sys::window().ok_or("Cannot get window")?;
        //            let document = web_sys::HtmlDocument::from(JsValue::from(
        //                window.document().ok_or("Cannot get document")?,
        //            ));
        //
        //            let grid_content = HtmlElement::from(JsValue::from(
        //                document
        //                    .get_element_by_id(&format!("{}_content", panel_type.get_id()))
        //                    .ok_or(format!("Content {} not found", panel_type.get_id()))?,
        //            ));
        //            let f = move |_event: JsValue| {
        //                let window = web_sys::window().ok_or("Cannot get window")?;
        //                let document = web_sys::HtmlDocument::from(JsValue::from(
        //                    window.document().ok_or("Cannot get document")?,
        //                ));
        //
        //                web_sys::console::log_1(&"Here".into());
        //
        //                Ok(())
        //            };
        //            set_on_x!(grid_content, set_onresize, f, false);
        //        }

        let mut ret = Self {
            needs_update: true,
            grid,
            panel_type,
            api_type: builder.get_api_type(),
            on_cell_change: builder.on_cell_change()?,
        };
        ret.update(server_api_lock.clone(), panel_manager_lock.clone())?;

        Ok(ret)
    }

    pub fn get_current_editor_data(
        &mut self,
    ) -> Result<Option<(u32, u32, DataType, bool, JsValue, JsValue)>, JsValue> {
        // Collect the current inventory_spreadsheet editor contents, if they exist
        // (otherwise it will be deleted)
        let spreadsheet_editor = self.grid.getCellEditor();
        let active_cell = self.grid.getActiveCell();
        let active_cell_node = self.grid.getActiveCellNode();

        // Current editor value is saved, unless it is a new row
        Ok(
            if !spreadsheet_editor.clone().is_null() && !active_cell.clone().is_null() {
                let is_value_changed = js_sys::Function::from(js_sys::Reflect::get(
                    &spreadsheet_editor,
                    &"isValueChanged".into(),
                )?);
                let validate = js_sys::Function::from(js_sys::Reflect::get(
                    &spreadsheet_editor,
                    &"validate".into(),
                )?);
                let serialize_value = js_sys::Function::from(js_sys::Reflect::get(
                    &spreadsheet_editor,
                    &"serializeValue".into(),
                )?);

                let active_cell_input = active_cell_node.first_element_child().unwrap();
                let selection_start =
                    js_sys::Reflect::get(&active_cell_input, &"selectionStart".into())?;
                let selection_end =
                    js_sys::Reflect::get(&active_cell_input, &"selectionEnd".into())?;

                let validatation_ret = validate.call0(&spreadsheet_editor)?;
                let is_valid =
                    DataType::from(js_sys::Reflect::get(&validatation_ret, &"valid".into())?)
                        .as_bool()
                        .map_err(|e| JsValue::from(e))?;
                let is_changed = DataType::from(is_value_changed.call0(&spreadsheet_editor)?)
                    .as_bool()
                    .map_err(|e| JsValue::from(e))?;

                if is_changed {
                    let value = DataType::from(serialize_value.call0(&spreadsheet_editor)?);

                    let row = js_sys::Reflect::get(&active_cell.clone(), &"row".into())?;

                    let data_connector = self.grid.getData();
                    let get_item_func = js_sys::Function::from(js_sys::Reflect::get(
                        &data_connector,
                        &"getItem".into(),
                    )?);
                    let item_js = get_item_func.call1(&data_connector, &row)?;

                    // Get the item id
                    if item_js.is_null() {
                        None
                    } else {
                        if let Some(item_id) =
                            DataType::from(js_sys::Reflect::get(&item_js, &"id".into())?)
                                .as_f64()
                                .ok()
                                .map(|f| f as u32)
                        {
                            let cell = js_sys::Reflect::get(&active_cell.clone(), &"cell".into())?
                                .as_f64()
                                .unwrap() as u32;

                            Some((
                                item_id,
                                cell,
                                value,
                                is_valid,
                                selection_start,
                                selection_end,
                            ))
                        } else {
                            None
                        }
                    }
                } else {
                    None
                }
            } else {
                None
            },
        )
    }

    pub fn save_current_cell_value_if_different(
        &mut self,
        item_id: u32,
        cell: u32,
        value: DataType,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<bool, JsValue> {
        let columns = self
            .grid
            .getColumns()
            .values()
            .into_iter()
            .map(|column| {
                DataType::from(js_sys::Reflect::get(&column.unwrap(), &"field".into()).unwrap())
                    .as_string()
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let database_object = server_api_lock
            .write()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_mut()
            .as_mut()
            .unwrap()
            .api_items
            .get_mut(&self.api_type)
            .ok_or(JsValue::from("Could not get api_type"))?
            .data
            .get_mut(&item_id)
            .unwrap()
            .clone();

        let old_value = database_object
            .fields
            .get(&columns[cell as usize])
            .cloned()
            .unwrap_or(DataType::NULL);

        if old_value == value {
            Ok(false)
        } else {
            (self.on_cell_change)(
                item_id,
                &columns[cell as usize],
                value.clone(),
                &database_object,
                server_api_lock
                    .write()
                    .map_err(|_| JsValue::from("Could not lock server_api"))?
                    .as_mut()
                    .as_mut()
                    .ok_or(JsValue::from("Could not lock server_api"))?,
            )?;
            Ok(true)
        }
    }

    /// Updates all the spreadsheet data to the current version
    pub fn update(
        &mut self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<(), JsValue> {
        if let Ok(panel_manager_unlocked) = panel_manager_lock.read() {
            let editor_value = self.get_current_editor_data()?;

            let is_different = if let Some((item_id, cell, value, is_valid, ..)) = &editor_value {
                if *is_valid {
                    self.save_current_cell_value_if_different(
                        *item_id,
                        *cell,
                        value.clone(),
                        server_api_lock.clone(),
                    )?
                } else {
                    false
                }
            } else {
                false
            };

            if self.needs_update
                && panel_manager_unlocked
                .as_ref()
                .as_ref()
                .unwrap()
                .active_panels
                .contains(&self.panel_type)
            {
                self.needs_update = false;

                self.grid.invalidate();

                if is_different {
                    if let Some((item_id, cell, .., selection_start, selection_end)) = editor_value
                    {
                        let data_connector = self.grid.getData();
                        let get_row_func = js_sys::Function::from(js_sys::Reflect::get(
                            &data_connector,
                            &"getRow".into(),
                        )?);
                        let row = get_row_func
                            .call1(&data_connector, &item_id.into())?
                            .as_f64()
                            .unwrap() as u32;

                        self.grid.setActiveCell(row, cell);
                        let spreadsheet_editor = self.grid.getActiveCellNode();
                        if let Some(editor_input) = spreadsheet_editor.first_element_child() {
                            let set_selection_range = js_sys::Function::from(js_sys::Reflect::get(
                                &editor_input,
                                &"setSelectionRange".into(),
                            )?);
                            set_selection_range.call2(
                                &editor_input,
                                &selection_start,
                                &selection_end,
                            )?;
                        }
                    }
                }
            }
        } else {
            web_sys::console::error_1(&JsValue::from("Could not lock panel_manager"))
        }

        Ok(())
    }
}

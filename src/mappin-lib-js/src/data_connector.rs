use std::sync::{Arc, RwLock};

use wasm_bindgen::prelude::*;

use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    server_api::ServerAPI,
};

/// A struct that allows JS functions (specifically SlickGrid functions) to access internal Rust data.
#[wasm_bindgen]
pub struct DataConnector {
    /// The filtering function that allows archive and spreadsheet to show different data
    pub(crate) filter_func: Option<Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>>,
    /// The column name to order by
    pub(crate) order_by: String,
    /// Whether or not to order ascending (default is ascending)
    pub(crate) order_ascending: Option<bool>,
    /// A permanent connection to the server_api
    pub(crate) server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    /// The APIType that this connector connects to
    pub(crate) api_type: APIType,
}

impl DataConnector {
    /// Creates a new `DataConnector` struct with the given options
    pub fn new(
        filter_func: Option<Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>>,
        order_by: String,
        order_ascending: Option<bool>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        api_type: APIType,
    ) -> Self {
        Self {
            filter_func,
            order_by,
            order_ascending,
            server_api_lock,
            api_type,
        }
    }
}

#[wasm_bindgen]
impl DataConnector {
    /// The number of data objects not filtered
    /// Note: A function required by SlickGrid
    #[wasm_bindgen(js_name = getLength)]
    pub fn get_length(&mut self) -> Result<u32, JsValue> {
        if let Ok(server_api_unlocked) = self.server_api_lock.read() {
            Ok(
                server_api_unlocked.as_ref().as_ref().unwrap().api_items[&self.api_type]
                    .get_num_data_object(&mut self.filter_func)? as u32,
            )
        } else {
            Err(JsValue::from("Could not lock server_api"))
        }
    }

    /// The item corresponding to the index as filtered and sorted
    /// Note: A function required by SlickGrid
    #[wasm_bindgen(js_name = getItem)]
    pub fn get_item(&mut self, index: u32) -> Result<JsValue, JsValue> {
        if let Ok(server_api_unlocked) = self.server_api_lock.read() {
            Ok(
                server_api_unlocked.as_ref().as_ref().unwrap().api_items[&self.api_type]
                    .get_data_object_ordered(
                        index,
                        &mut self.filter_func,
                        &self.order_by,
                        self.order_ascending,
                    )?
                    .and_then(|database_object| {
                        let mut local_object = database_object.clone();

                        // Convert the obtained date to something that can be used by SlickGrid
                        if let Some(field) = local_object.fields.get_mut("obtained".into()) {
                            let obtained = field.as_string().unwrap_or("".into());
                            let split_string = obtained.split('-').collect::<Vec<_>>();
                            if split_string.len() == 3
                                && split_string[0].len() == 4
                                && split_string[1].len() == 2
                                && split_string[2].len() == 2
                            {
                                let year = split_string[0].parse::<u32>();
                                let month = split_string[1].parse::<u8>();
                                let day = split_string[2].parse::<u8>();
                                if let (Ok(y), Ok(m), Ok(d)) = (year, month, day) {
                                    *field = DataType::S(format!("{}/{}/{}", m, d, y));
                                }
                            }
                        }

                        local_object
                            .as_js_object()
                            .ok()
                            .and_then(|js_object| Some(js_object.into()))
                    })
                    .unwrap_or(JsValue::NULL),
            )
        } else {
            Err(JsValue::from("Could not lock server_api"))
        }
    }

    #[wasm_bindgen(js_name = getRow)]
    pub fn get_row(&mut self, id: u32) -> Result<JsValue, JsValue> {
        if let Ok(server_api_unlocked) = self.server_api_lock.read() {
            Ok(
                server_api_unlocked.as_ref().as_ref().unwrap().api_items[&self.api_type]
                    .get_index_from_order(
                        id,
                        &mut self.filter_func,
                        &self.order_by,
                        self.order_ascending,
                    )?
                    .and_then(|row| Some(JsValue::from(row)))
                    .unwrap_or(JsValue::NULL),
            )
        } else {
            Err(JsValue::from("Could not lock server_api"))
        }
    }

    #[wasm_bindgen]
    pub fn set_sort_column(&mut self, column_name: String) {
        self.order_by = column_name;
    }

    #[wasm_bindgen]
    pub fn set_sort_ascending(&mut self, ascending: bool) {
        self.order_ascending = Some(ascending);
    }
}

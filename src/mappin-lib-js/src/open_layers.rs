use js_sys::Array as JsArray;
use wasm_bindgen::prelude::*;

pub fn fix_namespace() -> Result<(), JsValue> {
    // TODO: This is a temp fix because nested namespaces are bad
    let ol = js_sys::Reflect::get(&js_sys::global(), &"ol".into())?;

    {
        let proj = js_sys::Reflect::get(&ol, &"proj".into())?;
        let projection_class = js_sys::Reflect::get(&proj, &"Projection".into())?;
        js_sys::Reflect::set(&ol, &"Projection".into(), &projection_class)?;
    }

    {
        let interaction = js_sys::Reflect::get(&ol, &"interaction".into())?;
        let select_class = js_sys::Reflect::get(&interaction, &"Select".into())?;
        js_sys::Reflect::set(&ol, &"Select".into(), &select_class)?;
    }

    {
        let interaction = js_sys::Reflect::get(&ol, &"interaction".into())?;
        let modify_class = js_sys::Reflect::get(&interaction, &"Modify".into())?;
        js_sys::Reflect::set(&ol, &"Modify".into(), &modify_class)?;
    }

    {
        let source = js_sys::Reflect::get(&ol, &"source".into())?;
        let image_static_source = js_sys::Reflect::get(&source, &"ImageStatic".into())?;
        js_sys::Reflect::set(&ol, &"ImageStaticSource".into(), &image_static_source)?;
    }

    {
        let layer = js_sys::Reflect::get(&ol, &"layer".into())?;
        let image_layer = js_sys::Reflect::get(&layer, &"Image".into())?;
        js_sys::Reflect::set(&ol, &"ImageLayer".into(), &image_layer)?;
    }

    {
        let source = js_sys::Reflect::get(&ol, &"source".into())?;
        let vector_source = js_sys::Reflect::get(&source, &"Vector".into())?;
        js_sys::Reflect::set(&ol, &"VectorSource".into(), &vector_source)?;
    }

    {
        let layer = js_sys::Reflect::get(&ol, &"layer".into())?;
        let vector_layer = js_sys::Reflect::get(&layer, &"Vector".into())?;
        js_sys::Reflect::set(&ol, &"VectorLayer".into(), &vector_layer)?;
    }

    {
        let geom = js_sys::Reflect::get(&ol, &"geom".into())?;
        let point = js_sys::Reflect::get(&geom, &"Point".into())?;
        js_sys::Reflect::set(&ol, &"Point".into(), &point)?;
    }

    {
        let style = js_sys::Reflect::get(&ol, &"style".into())?;
        let style_style = js_sys::Reflect::get(&style, &"Style".into())?;
        js_sys::Reflect::set(&ol, &"Style".into(), &style_style)?;
    }

    {
        let style = js_sys::Reflect::get(&ol, &"style".into())?;
        let icon = js_sys::Reflect::get(&style, &"Icon".into())?;
        js_sys::Reflect::set(&ol, &"Icon".into(), &icon)?;
    }

    Ok(())
}

#[wasm_bindgen]
extern "C" {
    pub type Map;

    #[wasm_bindgen(constructor, js_class = Map, js_namespace = ol)]
    pub fn new(options: JsValue) -> Map;

    #[wasm_bindgen(method, js_class = Map)]
    pub fn addInteraction(this: &Map, selection: JsValue);

    #[wasm_bindgen(method, js_class = Map)]
    pub fn getLayers(this: &Map) -> Layers;

    #[wasm_bindgen(method, js_class = Map)]
    pub fn addLayer(this: &Map, layer: JsValue);

    #[wasm_bindgen(method, js_class = Map)]
    pub fn getView(this: &Map) -> View;

    #[wasm_bindgen(method, js_class = Map)]
    pub fn forEachFeatureAtPixel(this: &Map, pixel: JsValue, function: JsValue) -> JsValue;

    #[wasm_bindgen(method, js_class = Feature, js_namespace = ol)]
    pub fn on(this: &Map, name: &str, func: &JsValue);
}

#[wasm_bindgen]
extern "C" {
    pub type Layers;

    #[wasm_bindgen(method, js_class = Layers)]
    pub fn getLength(this: &Layers) -> u32;

    #[wasm_bindgen(method, js_class = Layers)]
    pub fn pop(this: &Layers);

    #[wasm_bindgen(method, js_class = Layers)]
    pub fn getArray(this: &Layers) -> JsArray;
}

#[wasm_bindgen]
extern "C" {
    pub type extent;

    #[wasm_bindgen(static_method_of = extent, js_namespace = ol)]
    pub fn getCenter(extent: JsArray) -> JsArray;
}

#[wasm_bindgen]
extern "C" {
    pub type Projection;

    #[wasm_bindgen(constructor, js_class = Projection, js_namespace = ol)]
    pub fn new(options: JsValue) -> Projection;
}

#[wasm_bindgen]
extern "C" {
    pub type View;

    #[wasm_bindgen(constructor, js_class = View, js_namespace = ol)]
    pub fn new(options: JsValue) -> View;

    #[wasm_bindgen(method, js_class = View)]
    pub fn calculateExtent(this: &View) -> JsArray;
}

#[wasm_bindgen]
extern "C" {
    pub type Select;

    #[wasm_bindgen(constructor, js_class = Select, js_namespace = ol)]
    pub fn new() -> Select;
}

#[wasm_bindgen]
extern "C" {
    pub type Modify;

    #[wasm_bindgen(constructor, js_class = Modify, js_namespace = ol)]
    pub fn new(options: JsValue) -> Modify;
}

#[wasm_bindgen]
extern "C" {
    pub type ImageStaticSource;

    #[wasm_bindgen(constructor, js_class = ImageStaticSource, js_namespace = ol)]
    pub fn new(options: JsValue) -> ImageStaticSource;
}

#[wasm_bindgen]
extern "C" {
    pub type ImageLayer;

    #[wasm_bindgen(constructor, js_class = ImageLayer, js_namespace = ol)]
    pub fn new(options: JsValue) -> ImageLayer;
}

#[wasm_bindgen]
extern "C" {
    pub type VectorSource;

    #[wasm_bindgen(constructor, js_class = VectorSource, js_namespace = ol)]
    pub fn new(options: JsValue) -> VectorSource;
}

#[wasm_bindgen]
extern "C" {
    pub type VectorLayer;

    #[wasm_bindgen(constructor, js_class = VectorLayer, js_namespace = ol)]
    pub fn new(options: JsValue) -> VectorLayer;
}

#[wasm_bindgen]
extern "C" {
    pub type Feature;

    #[wasm_bindgen(constructor, js_class = Feature, js_namespace = ol)]
    pub fn new(options: JsValue) -> Feature;

    #[wasm_bindgen(method, js_class = Feature, js_namespace = ol)]
    pub fn get(this: &Feature, name: &str) -> JsValue;

    #[wasm_bindgen(method, js_class = Feature, js_namespace = ol)]
    pub fn setId(this: &Feature, id: u32);

    #[wasm_bindgen(method, js_class = Feature, js_namespace = ol)]
    pub fn on(this: &Feature, name: &str, func: &JsValue);
}

#[wasm_bindgen]
extern "C" {
    pub type Point;

    #[wasm_bindgen(constructor, js_class = Point, js_namespace = ol)]
    pub fn new(options: js_sys::Array) -> Point;
}

#[wasm_bindgen]
extern "C" {
    pub type Style;

    #[wasm_bindgen(constructor, js_class = Style, js_namespace = ol)]
    pub fn new(options: JsValue) -> Style;
}

#[wasm_bindgen]
extern "C" {
    pub type Icon;

    #[wasm_bindgen(constructor, js_class = Icon, js_namespace = ol)]
    pub fn new(options: JsValue) -> Icon;
}

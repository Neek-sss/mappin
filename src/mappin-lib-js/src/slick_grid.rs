use js_sys::{Array as JsArray, Object as JsObject};
use wasm_bindgen::prelude::*;
use web_sys::HtmlElement;

#[wasm_bindgen]
extern "C" {
    pub type Grid;

    #[wasm_bindgen(constructor, js_class = Grid, js_namespace = Slick)]
    pub fn new(id: String, data_connector: JsObject, columns: JsArray, options: JsObject) -> Grid;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn invalidate(this: &Grid);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn updateRowCount(this: &Grid);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn render(this: &Grid);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn setSelectionModel(this: &Grid, model: JsValue);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn registerPlugin(this: &Grid, model: JsValue);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getSelectedRows(this: &Grid) -> JsArray;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn setSelectedRows(this: &Grid, array: JsArray);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getData(this: &Grid) -> JsValue;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getColumns(this: &Grid) -> JsArray;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getActiveCell(this: &Grid) -> JsValue;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getCellEditor(this: &Grid) -> JsValue;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn getActiveCellNode(this: &Grid) -> HtmlElement;

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn setActiveCell(this: &Grid, row: u32, cell: u32);

    #[wasm_bindgen(method, js_class = Grid)]
    pub fn resetActiveCell(this: &Grid);
}

#[wasm_bindgen]
extern "C" {
    pub type CheckboxSelectColumn;

    #[wasm_bindgen(constructor, js_class = CheckboxSelectColumn, js_namespace = Slick)]
    pub fn new() -> CheckboxSelectColumn;

    #[wasm_bindgen(method, js_class = CheckboxSelectColumn)]
    pub fn getColumnDefinition(this: &CheckboxSelectColumn) -> JsValue;
}

#[wasm_bindgen]
extern "C" {
    pub type RowSelectionModel;

    #[wasm_bindgen(constructor, js_class = RowSelectionModel, js_namespace = Slick)]
    pub fn new(options: JsValue) -> RowSelectionModel;
}

use std::{cell::RefCell, rc::Rc};

use maplit::hashmap;
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use mappin_lib::{
    ajax::{into_form_data, wasm_ajax_no_response},
    api_type::APIType,
    server_api::ServerAPI,
    set_on_x,
};

/// Sends the new user request to the server, checking the server's response and resetting input
/// data.
fn submit_new_user(event: JsValue) -> Result<(), JsValue> {
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the username
    let username_input = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("username_input")
            .ok_or("Username input not found")?,
    ));
    let username = js_sys::Reflect::get(&username_input.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Username not a string"))?;

    // Get the password
    let password_input_1 = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("password_input_1")
            .ok_or("Password input not found")?,
    ));
    let password_1 = js_sys::Reflect::get(&password_input_1.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Password not a string"))?;
    let password_input_2 = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("password_input_2")
            .ok_or("Password input not found")?,
    ));
    let password_2 = js_sys::Reflect::get(&password_input_2.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Password not a string"))?;

    // Make sure the passwords match
    if password_1 == password_2 {
        // Make the new user request
        let form_data =
            into_form_data(hashmap! {"username" => username.clone(), "password" => password_1});

        // Send the request to the server
        let response = wasm_ajax_no_response("/mappin/new_user/", &form_data)?;

        // Check the server response
        if response.status == 200 {
            reset_new_user_form("")?;
        } else {
            reset_new_user_form(&username)?;
            window.alert_with_message("Invalid new_user attempt!")?;
        }
    } else {
        reset_new_user_form(&username)?;
        window.alert_with_message("Passwords did not match!")?;
    }

    Ok(())
}

/// Resets all the user form data, keeping the username input that is passed as an argument.
pub fn reset_new_user_form(username: &str) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the new user form
    let new_user_form = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("new_user_form")
            .ok_or(JsValue::from("Could not find new_user form"))?,
    ));

    // Clear out the old inputs
    while new_user_form.child_element_count() > 0 {
        new_user_form.last_element_child().unwrap().remove();
    }

    // Make the username field
    let username_label = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    username_label.set_inner_text("Username:");
    username_label.set_class_name("w3-label");
    let username_input = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    username_input.set_id("username_input");
    username_input.set_class_name("w3-input");
    js_sys::Reflect::set(&username_input.clone(), &"value".into(), &username.into())?;
    username_label.append_with_node_1(&username_input)?;

    // Make the first password field
    let password_label_1 = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    password_label_1.set_inner_text("Password:");
    password_label_1.set_class_name("w3-label");
    let password_input_1 = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    password_input_1.set_id("password_input_1");
    password_input_1.set_class_name("w3-input");
    js_sys::Reflect::set(
        &password_input_1.clone(),
        &"type".into(),
        &"password".into(),
    )?;
    password_label_1.append_with_node_1(&password_input_1)?;

    // Make the second password field
    let password_label_2 = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    password_label_2.set_inner_text("Re-enter Password:");
    password_label_2.set_class_name("w3-label");
    let password_input_2 = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    password_input_2.set_id("password_input_2");
    password_input_2.set_class_name("w3-input");
    js_sys::Reflect::set(
        &password_input_2.clone(),
        &"type".into(),
        &"password".into(),
    )?;
    password_label_2.append_with_node_1(&password_input_2)?;

    // Make the submit button
    let submit_button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
    submit_button.set_inner_text("Submit");
    submit_button.set_id("submit_button");
    submit_button.set_class_name("w3-card w3-button");
    set_on_x!(submit_button, set_onclick, submit_new_user, false);

    // Add all the content to the form
    new_user_form.append_with_node_1(&username_label)?;
    new_user_form.append_with_node_1(&password_label_1)?;
    new_user_form.append_with_node_1(&password_label_2)?;
    new_user_form.append_with_node_1(&submit_button)?;

    Ok(())
}

#[derive(Default, Debug, Clone)]
pub struct NewUserManager {}

impl NewUserManager {
    pub fn new(panel_id: &str) -> Result<Self, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the header
        let panel_content = document
            .get_element_by_id(&format!("{}_content", panel_id))
            .ok_or("Content not found")?;

        // Create the form
        let form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
        form.set_id("new_user_form");
        set_on_x!(form, set_onsubmit, submit_new_user, false);
        panel_content.append_with_node_1(&form)?;
        reset_new_user_form("")?;

        Ok(Self::default())
    }

    pub fn check_permissions(server_api_lock: &ServerAPI) -> bool {
        let user_perm = server_api_lock.api_items[&APIType::User].permissions;
        user_perm.add
    }
}

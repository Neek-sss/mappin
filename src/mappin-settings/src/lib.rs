extern crate wee_alloc;

use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};

use wasm_bindgen::{JsCast, prelude::*};
use web_sys::{self, HtmlElement};

use mappin_lib::{
    ajax,
    api_type::APIType,
    panel::{PanelManager, PanelType},
    server_api::ServerAPI,
    set_on_x,
    timer::Timer,
};

use crate::{
    change_password::ChangePasswordManager, new_user_manager::NewUserManager,
    user_permissions::UserPermissionsManager,
};

pub mod change_password;
pub mod new_user_manager;
pub mod user_permissions;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

fn return_to_interface(_event: JsValue) -> Result<(), JsValue> {
    let location = js_sys::Reflect::get(&js_sys::global(), &"location".into())?;
    let replace = js_sys::Function::from(js_sys::Reflect::get(&location, &"replace".into())?);
    replace.call1(&location, &"interface".into())?;
    Ok(())
}

fn open_django_admin(_event: JsValue) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    window.open_with_url("/admin")?;
    Ok(())
}

/// Called when the wasm module is instantiated.  This is equivalent to a binary program's main
/// function.
#[wasm_bindgen(start)]
pub fn wasm_main() -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));
    let body = document.body().ok_or("Cannot get body")?;

    // Add the header to the page
    body.append_with_node_1(&settings_header()?.into())?;

    // Create all the empty locks
    let server_api_lock = Arc::new(RwLock::new(Box::new(None)));
    let panel_manager_lock = Arc::new(RwLock::new(Box::new(None)));
    let new_user_manager_lock = Arc::new(RwLock::new(Box::new(None)));
    let user_permissions_manager_lock = Arc::new(RwLock::new(Box::new(None)));
    let change_password_manager_lock = Arc::new(RwLock::new(Box::new(None)));

    // Load up the base data stuff
    *server_api_lock
        .write()
        .map_err(|_| "Cannot lock server_api")? =
        Box::new(Some(ServerAPI::new(&[APIType::User, APIType::Permission])?));

    // Create the panel system
    *panel_manager_lock
        .write()
        .map_err(|_| "Cannot lock panel_manager")? = Box::new(Some(PanelManager::new()?));

    let new_user_enabled = NewUserManager::check_permissions(
        server_api_lock
            .read()
            .map_err(|_| "Cannot lock server_api")?
            .as_ref()
            .as_ref()
            .unwrap(),
    );
    let user_permissions_enabled = UserPermissionsManager::check_permissions(
        server_api_lock
            .read()
            .map_err(|_| "Cannot lock server_api")?
            .as_ref()
            .as_ref()
            .unwrap(),
    );

    // Create the panel types
    let new_user_manager_panel_type = PanelType::NewUserManager("New User");
    let user_permissions_manager_panel_type = PanelType::UserPermissionsManager("User Permissions");
    let change_password_manager_panel_type = PanelType::ChangePasswordManager("Change Password");

    if let Ok(mut panel_manager_unlocked) = panel_manager_lock.write() {
        let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

        // Make the panels
        panel_manager.make_panel(&new_user_manager_panel_type, panel_manager_lock.clone())?;
        if user_permissions_enabled {
            panel_manager.make_panel(
                &user_permissions_manager_panel_type,
                panel_manager_lock.clone(),
            )?;
        }
        panel_manager.make_panel(
            &change_password_manager_panel_type,
            panel_manager_lock.clone(),
        )?;

        // Activate the panels
        if new_user_enabled {
            panel_manager.activate_panel(&new_user_manager_panel_type);
        }
        if user_permissions_enabled {
            panel_manager.activate_panel(&user_permissions_manager_panel_type);
        }
        panel_manager.activate_panel(&change_password_manager_panel_type);

        // Set the panels' size
        panel_manager.resize_panels()?;
    }

    // Create the user manager
    if new_user_enabled {
        *new_user_manager_lock
            .write()
            .map_err(|_| "Cannot lock new_user_manager")? = Box::new(Some(NewUserManager::new(
            new_user_manager_panel_type.get_id(),
        )?));
    }

    // Create the user permissions manager
    if user_permissions_enabled {
        *user_permissions_manager_lock
            .write()
            .map_err(|_| "Cannot lock user_permissions_manager")? =
            Box::new(Some(UserPermissionsManager::new(
                user_permissions_manager_panel_type.clone(),
                user_permissions_manager_lock.clone(),
                server_api_lock.clone(),
                panel_manager_lock.clone(),
            )?));
    }

    // Create the change password manager
    *change_password_manager_lock
        .write()
        .map_err(|_| "Cannot lock change_password_manager")? = Box::new(Some(
        ChangePasswordManager::new(change_password_manager_panel_type.get_id())?,
    ));

    // Continuously update data to/from the server
    let server_api_lock_ref = server_api_lock.clone();
    Timer::new(
        10_000.0,
        Box::new(move || {
            let mut server_api_unlocked = server_api_lock_ref
                .write()
                .map_err(|_| JsValue::from("Cannot get write lock on server_api"))?;
            let server_api = server_api_unlocked
                .as_mut()
                .as_mut()
                .unwrap();
            if let Err(e) = server_api.data_updates() {
                server_api.unsaved_changes = true;
                let window = web_sys::window().ok_or("Error opening window")?;
                let document = window.document().ok_or("Error opening document")?;
                let error_notification = web_sys::HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("error_notification")
                        .ok_or("Error getting error notification")?,
                ));
                error_notification.set_hidden(false);
            }

            Ok(())
        }),
        None,
    )
        .run()?;

    // Periodically update the user_permissions spreadsheet
    if user_permissions_enabled {
        let user_permissions_manager_spreadsheet_lock_ref = user_permissions_manager_lock
            .clone()
            .read()
            .map_err(|_| JsValue::from("Cannot get read lock on user_permissions_manager"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .spreadsheet
            .clone();
        let panel_manager_lock_ref = panel_manager_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        Timer::new(
            1_000.0,
            Box::new(move || {
                user_permissions_manager_spreadsheet_lock_ref
                    .write()
                    .map_err(|_| {
                        JsValue::from(
                            "Cannot get write lock on user_permissions_manager_spreadsheet",
                        )
                    })?
                    .as_mut()
                    .as_mut()
                    .and_then(|x| {
                        let ret = x
                            .update(server_api_lock_ref.clone(), panel_manager_lock_ref.clone())
                            .ok();
                        x.grid.resetActiveCell();
                        ret
                    })
                    .unwrap_or_default();

                Ok(())
            }),
            None,
        )
            .run()?;
    }

    Ok(())
}

/// Creates the page header containing the title and an option to return to the interface.
fn settings_header() -> Result<HtmlElement, JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Create the header
    let header = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    header.set_id("top_header");
    header.set_class_name("w3-row w3-container w3-display-container w3-bottombar");

    let user_dropdown_div = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_div.set_class_name("w3-dropdown-hover");
    user_dropdown_div.set_id("user_hover");
    header.append_with_node_1(&user_dropdown_div)?;

    let user_dropdown_button = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_button.set_inner_text("Menu");
    user_dropdown_button.set_class_name("w3-button");
    user_dropdown_button.set_id("user_hover");
    user_dropdown_div.append_with_node_1(&user_dropdown_button)?;

    let user_dropdown_content = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_content.set_class_name("w3-dropdown-content");
    user_dropdown_content.set_id("user_hover_button_dropdown");
    user_dropdown_div.append_with_node_1(&user_dropdown_content)?;

    {
        // Create the Interface button
        let interface_button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        interface_button.set_inner_text("Interface");
        interface_button.set_id("interface_button");
        interface_button.set_class_name("w3-button");
        set_on_x!(interface_button, set_onclick, return_to_interface, false);
        user_dropdown_content.append_with_node_1(&interface_button)?;

        // Create the "Django Admin" button
        let django_admin_button =
            HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        django_admin_button.set_inner_text("Open Django Admin");
        django_admin_button.set_id("django_button");
        django_admin_button.set_class_name("w3-button");
        set_on_x!(django_admin_button, set_onclick, open_django_admin, false);
        user_dropdown_content.append_with_node_1(&django_admin_button)?;

        // Create the logout button
        let element = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        element.set_inner_text("Logout");
        element.set_class_name("w3-button");
        element.set_id("logout_button");

        let f = move |_event| {
            if ajax::wasm_ajax("/mappin/api_logout", "").is_ok() {}
            let location = js_sys::Reflect::get(&js_sys::global(), &"location".into())?;
            let replace =
                js_sys::Function::from(js_sys::Reflect::get(&location, &"replace".into())?);
            replace.call1(&location, &"/".into())?;
            Ok(())
        };
        set_on_x!(element, set_onclick, f, false);

        user_dropdown_content.append_with_node_1(&element)?;
    }

    // Create the title
    let title = HtmlElement::from(JsValue::from(document.create_element("H1")?));
    title.set_id("title");
    title.set_inner_text("Settings");
    title.set_class_name("building-name w3-display-middle w3-content w3-xxlarge");
    header.append_with_node_1(&title)?;

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.set_inner_text("Error connecting to server!");
    element.set_id("error_notification");
    element.set_class_name("w3-display-topright w3-animation-fading w3-text-red");
    element.set_hidden(true);
    header.append_with_node_1(&element)?;

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.set_inner_text("Unsaved Changes");
    element.set_id("unsaved_changes_notification");
    element.set_class_name("w3-display-bottomright");
    element.set_hidden(true);
    header.append_with_node_1(&element)?;

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.append_with_str_1("Saving...")?;
    element.set_id("saving_notification");
    element.set_class_name("w3-display-bottomright w3-animation-fading");
    element.set_hidden(true);
    header.append_with_node_1(&element)?;

    Ok(header)
}

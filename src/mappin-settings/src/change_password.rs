use maplit::hashmap;
use mappin_lib::{
    ajax::{into_form_data, wasm_ajax_no_response},
    set_on_x,
};
use std::{cell::RefCell, rc::Rc};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

/// Submits the password request to the server, checking that the new password was entered the same
/// way twice.
fn submit_change_password(event: JsValue) -> Result<(), JsValue> {
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the old password
    let old_password_input = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("old_password_input")
            .ok_or("Old password input not found")?,
    ));
    let old_password = js_sys::Reflect::get(&old_password_input.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Username not a string"))?;

    // Get the new password (both inputs)
    let new_password_input_1 = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("new_password_input_1")
            .ok_or("New password input 1 not found")?,
    ));
    let new_password_1 = js_sys::Reflect::get(&new_password_input_1.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Password not a string"))?;
    let new_password_input_2 = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("new_password_input_2")
            .ok_or("New password input 2 not found")?,
    ));
    let new_password_2 = js_sys::Reflect::get(&new_password_input_2.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Password not a string"))?;

    // Make sure that the new passwords match
    if new_password_1 == new_password_2 {
        // Set up a password request
        let form_data = into_form_data(
            hashmap! {"old_password" => old_password.clone(), "new_password" => new_password_1},
        );

        // Send the password request to the server
        let response = wasm_ajax_no_response("/mappin/change_password/", &form_data)?;

        // Make sure that the password was successfully changed
        if response.status != 200 {
            window.alert_with_message("Invalid change_password attempt!")?;
        }
    } else {
        window.alert_with_message("Invalid change_password attempt!")?;
    }

    reset_change_password_form()?;

    Ok(())
}

/// Resets all the form content for changing passwords, making sure that changing passwords doesn't
/// leave the password fields filled between requests.
pub fn reset_change_password_form() -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the password form
    let change_password_form = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("change_password_form")
            .ok_or(JsValue::from("Could not find change_password form"))?,
    ));

    // Clear out the old form fields
    while change_password_form.child_element_count() > 0 {
        change_password_form.last_element_child().unwrap().remove();
    }

    // Create the old password field
    let old_password_label = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    old_password_label.set_inner_text("Old Password:");
    old_password_label.set_class_name("w3-label");
    let old_password_input = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    old_password_input.set_id("old_password_input");
    old_password_input.set_class_name("w3-input");
    js_sys::Reflect::set(
        &old_password_input.clone(),
        &"type".into(),
        &"password".into(),
    )?;
    old_password_label.append_with_node_1(&old_password_input)?;

    // Create the first new password field
    let new_password_label_1 = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    new_password_label_1.set_inner_text("New Password:");
    new_password_label_1.set_class_name("w3-label");
    let password_input_1 = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    password_input_1.set_id("new_password_input_1");
    password_input_1.set_class_name("w3-input");
    js_sys::Reflect::set(
        &password_input_1.clone(),
        &"type".into(),
        &"password".into(),
    )?;
    new_password_label_1.append_with_node_1(&password_input_1)?;

    // Create the second new password field
    let new_password_label_2 = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    new_password_label_2.set_inner_text("Repeat New Password:");
    new_password_label_2.set_class_name("w3-label");
    let password_input_2 = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    password_input_2.set_id("new_password_input_2");
    password_input_2.set_class_name("w3-input");
    js_sys::Reflect::set(
        &password_input_2.clone(),
        &"type".into(),
        &"password".into(),
    )?;
    new_password_label_2.append_with_node_1(&password_input_2)?;

    // Create the submit button
    let submit_button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
    submit_button.set_inner_text("Submit");
    submit_button.set_id("submit_button");
    submit_button.set_class_name("w3-card w3-button");
    set_on_x!(submit_button, set_onclick, submit_change_password, false);

    // Put all the new form data into the form
    change_password_form.append_with_node_1(&old_password_label)?;
    change_password_form.append_with_node_1(&new_password_label_1)?;
    change_password_form.append_with_node_1(&new_password_label_2)?;
    change_password_form.append_with_node_1(&submit_button)?;

    Ok(())
}

#[derive(Default, Debug, Clone)]
pub struct ChangePasswordManager {}

impl ChangePasswordManager {
    pub fn new(panel_id: &str) -> Result<Self, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the content
        let panel_content = document
            .get_element_by_id(&format!("{}_content", panel_id))
            .ok_or("Content not found")?;

        let form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
        form.set_id("change_password_form");
        set_on_x!(form, set_onsubmit, submit_change_password, false);
        panel_content.append_with_node_1(&form)?;
        reset_change_password_form()?;

        Ok(Self::default())
    }
}

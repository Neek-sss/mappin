use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};

use js_sys::Reflect as JsReflect;
use maplit::hashmap;
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    panel::{PanelManager, PanelType},
    server_api::ServerAPI,
    set_on_x,
};
use mappin_lib_js::{
    slick_grid::Grid,
    spreadsheet::{
        on_x_get_id_column, Spreadsheet, SpreadsheetBuilder, SpreadsheetColumn, SpreadsheetOptions,
    },
    subscribe_x,
};

#[derive(Default)]
pub struct UserPermissionsManager {
    pub current_user: u32,
    pub spreadsheet: Arc<RwLock<Box<Option<Spreadsheet>>>>,
}

fn toggle_users_list(
    user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    let current_user_dropdown = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("current_user_dropdown")
            .ok_or(JsValue::from("Could not find current user dropdown"))?,
    ));

    if current_user_dropdown.child_element_count() == 0 {
        let current_user = user_permissions_manager_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock user_permissions_manager"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .current_user;
        if let Ok(server_api_unlocked) = server_api_lock.read() {
            for (&id, user) in
                &server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::User].data
                {
                    if id != current_user {
                        let user_name = user
                            .fields
                            .get("username")
                            .ok_or("Username not found".into())
                            .and_then(|d| d.as_string())
                            .map_err(|e| JsValue::from(e))?;
                        let user_entry =
                            HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
                        user_entry.set_class_name("w3-button");
                        user_entry.set_inner_text(&user_name);
                        let user_permissions_manager_lock_ref = user_permissions_manager_lock.clone();
                        let server_api_lock_ref = server_api_lock.clone();
                        let f = move |_event| {
                            if let Ok(mut user_permissions_manager_unlocked) =
                            user_permissions_manager_lock_ref.write()
                            {
                                user_permissions_manager_unlocked
                                    .as_mut()
                                    .as_mut()
                                    .unwrap()
                                    .set_current_user(id, server_api_lock_ref.clone())?;
                            } else {
                                Err(JsValue::from(
                                    "Could not lock user_permissions_manager_lock",
                                ))?;
                            }
                            toggle_users_list(
                                user_permissions_manager_lock_ref.clone(),
                                server_api_lock_ref.clone(),
                            )?;
                            Ok(())
                        };
                        set_on_x!(user_entry, set_onclick, f, false);
                        current_user_dropdown.append_with_node_1(&user_entry.into())?;
                }
            }
        } else {
            Err(JsValue::from("Could not lock user_permissions_manager"))?;
        }
    } else {
        // If the listing is shown, empty it out and hide it
        while current_user_dropdown.child_element_count() > 0 {
            current_user_dropdown.last_element_child().unwrap().remove();
        }
    }

    Ok(())
}

impl UserPermissionsManager {
    pub fn new(
        panel_type: PanelType,
        user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<Self, JsValue> {
        let mut ret = Self::default();
        ret.create_header(
            panel_type.get_id(),
            user_permissions_manager_lock.clone(),
            server_api_lock.clone(),
        )?;
        ret.create_content(
            panel_type.clone(),
            user_permissions_manager_lock.clone(),
            server_api_lock.clone(),
            panel_manager_lock,
        )?;

        ret.set_current_user(
            *server_api_lock
                .read()
                .map_err(|_| JsValue::from("Could not lock server_api"))?
                .as_ref()
                .as_ref()
                .unwrap()
                .api_items[&APIType::User]
                .data
                .keys()
                .last()
                .unwrap(),
            server_api_lock.clone(),
        )?;

        Ok(ret)
    }

    pub fn check_permissions(server_api_lock: &ServerAPI) -> bool {
        let permission_perm = &server_api_lock.api_items[&APIType::Permission].permissions;
        let user_perm = server_api_lock.api_items[&APIType::User].permissions;
        permission_perm.view && user_perm.view
    }

    pub fn create_header(
        &self,
        panel_id: &str,
        user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the content
        let panel_header = document
            .get_element_by_id(&format!("{}_header", panel_id))
            .ok_or("Header not found")?;

        panel_header.set_class_name("w3-display-container w3-row-padding");

        let current_user_div = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        current_user_div.set_class_name("w3-dropdown-hover");
        panel_header.append_with_node_1(&current_user_div)?;

        let current_user_label =
            HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        current_user_label.set_class_name("w3-button");
        current_user_label.set_inner_text("Current User: ");
        let f =
            move |_event| toggle_users_list(user_permissions_manager_lock.clone(), server_api_lock);
        let f_ref = f.clone();
        set_on_x!(current_user_div, set_onpointerenter, f.clone(), false);
        set_on_x!(current_user_div, set_onpointerleave, f_ref.clone(), false);
        current_user_div.append_with_node_1(&current_user_label)?;

        let current_user = HtmlElement::from(JsValue::from(document.create_element("B")?));
        current_user.set_class_name("w3-content");
        current_user.set_id("current_user");
        current_user_label.append_with_node_1(&current_user)?;

        let current_user_dropdown =
            HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        current_user_dropdown.set_id("current_user_dropdown");
        current_user_dropdown.set_class_name("w3-dropdown-content");
        current_user_dropdown.set_attribute("style", "z-index:10")?;
        current_user_div.append_with_node_1(&current_user_dropdown)?;

        let superuser_notification =
            HtmlElement::from(JsValue::from(document.create_element("P")?));
        superuser_notification.set_id("superuser_notification");
        superuser_notification.set_inner_text("Superuser");
        superuser_notification.set_class_name("w3-display-bottomright w3-hide");
        panel_header.append_with_node_1(&superuser_notification)?;

        Ok(())
    }

    pub fn create_content(
        &mut self,
        panel_type: PanelType,
        user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<(), JsValue> {
        self.spreadsheet = Arc::new(RwLock::new(Box::new(Some(Spreadsheet::new(
            panel_type,
            server_api_lock,
            panel_manager_lock,
            UserPermissionsSpreadsheetBuilder::new(user_permissions_manager_lock),
        )?))));

        Ok(())
    }

    pub fn set_current_user(
        &mut self,
        id: u32,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the current user display
        let current_user_dom = HtmlElement::from(JsValue::from(
            document
                .get_element_by_id("current_user")
                .ok_or("Current user not found")?,
        ));

        // Find the superuser notification
        let superuser_notification = HtmlElement::from(JsValue::from(
            document
                .get_element_by_id("superuser_notification")
                .ok_or("Superuser notification not found")?,
        ));

        self.current_user = id;

        let server_api_unlocked = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?;
        let user_data =
            &server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::User].data[&id];

        let user_name = user_data
            .fields
            .get("username")
            .ok_or("Username not found".into())
            .and_then(|d| d.as_string())
            .map_err(|e| JsValue::from(e))?;
        current_user_dom.set_inner_text(&user_name);

        if user_data
            .fields
            .get("is_superuser")
            .ok_or("Is_superuser not found".into())
            .and_then(|d| d.as_bool())
            .map_err(|e| JsValue::from(e))?
        {
            superuser_notification.set_class_name("w3-display-bottomright w3-show");
        } else {
            superuser_notification.set_class_name("w3-display-bottomright w3-hide");
        }

        self.spreadsheet
            .write()
            .map_err(|_| JsValue::from("Could not lock user_permissions_manager_spreadsheet"))?
            .as_mut()
            .as_mut()
            .unwrap()
            .needs_update = true;

        Ok(())
    }
}

pub struct UserPermissionsSpreadsheetBuilder {
    user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
}

impl UserPermissionsSpreadsheetBuilder {
    pub fn new(
        user_permissions_manager_lock: Arc<RwLock<Box<Option<UserPermissionsManager>>>>,
    ) -> Self {
        Self {
            user_permissions_manager_lock,
        }
    }
}

impl SpreadsheetBuilder for UserPermissionsSpreadsheetBuilder {
    fn get_header_elements(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<Vec<HtmlElement>, JsValue> {
        Ok(Vec::new())
    }

    fn get_options(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<SpreadsheetOptions, JsValue> {
        let mut ret = SpreadsheetOptions::default();
        if server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::User]
            .permissions
            .change
        {
            ret.editable = true;
            ret.auto_edit = true;
        }
        Ok(ret)
    }

    fn get_columns(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<Vec<SpreadsheetColumn>, JsValue> {
        let editors = {
            let slick = JsReflect::get(&js_sys::global().into(), &"Slick".into())?;
            JsReflect::get(&slick, &"Editors".into())?
        };
        let formatters = {
            let slick = JsReflect::get(&js_sys::global().into(), &"Slick".into())?;
            JsReflect::get(&slick, &"Formatters".into())?
        };
        Ok(vec![
            SpreadsheetColumn {
                id: "name".into(),
                name: "Name".into(),
                field: "name".into(),
                sortable: true,
                editor: None,
                formatter: None,
            },
            SpreadsheetColumn {
                id: "view".into(),
                name: "View".into(),
                field: "view".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Checkbox".into())?.into()),
                formatter: Some(JsReflect::get(&formatters, &"Checkbox".into())?.into()),
            },
            SpreadsheetColumn {
                id: "add".into(),
                name: "Add".into(),
                field: "add".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Checkbox".into())?.into()),
                formatter: Some(JsReflect::get(&formatters, &"Checkbox".into())?.into()),
            },
            SpreadsheetColumn {
                id: "change".into(),
                name: "Change".into(),
                field: "change".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Checkbox".into())?.into()),
                formatter: Some(JsReflect::get(&formatters, &"Checkbox".into())?.into()),
            },
            // TODO: Allow deletions
            //            SpreadsheetColumn {
            //                id: "delete".into(),
            //                name: "Delete".into(),
            //                field: "delete".into(),
            //                sortable: true,
            //                editor: Some(JsReflect::get(&editors, &"Checkbox".into())?.into()),
            //                formatter: Some(JsReflect::get(&formatters, &"Checkbox".into())?.into()),
            //            },
        ])
    }

    fn get_filter_func(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>> {
        let user_permissions_manager_lock_ref = self.user_permissions_manager_lock.clone();
        Box::new(move |database_object: &DatabaseObject| {
            let f = || -> Result<_, JsValue> {
                let current_user = user_permissions_manager_lock_ref
                    .read()
                    .map_err(|_| JsValue::from("Error locking user_permissions_manager_lock"))?
                    .as_ref()
                    .as_ref()
                    .ok_or(JsValue::from("Error locking user_permissions_manager_lock"))?
                    .current_user;
                Ok(database_object.fields["user_id"].as_u64()? as u32 == current_user)
            };
            Ok(f().unwrap_or(false))
        })
    }

    fn get_api_type(&self) -> APIType {
        APIType::Permission
    }

    fn on_cell_change(
        &self,
    ) -> Result<
        Box<dyn FnMut(u32, &str, DataType, &DatabaseObject, &mut ServerAPI) -> Result<(), JsValue>>,
        JsValue,
    > {
        // When a cell is changed, update the data
        Ok(Box::new(
            move |permission_id: u32,
                  column_name: &str,
                  column_data: DataType,
                  database_object: &DatabaseObject,
                  server_api: &mut ServerAPI| {
                let action_id = database_object.fields[&format!("{}_id", column_name)].clone();
                let user_id = database_object.fields["user_id"].clone();

                server_api
                    .api_items
                    .get_mut(&APIType::Permission)
                    .unwrap()
                    .queue_update_skip_on_update(
                        permission_id,
                        &column_name,
                        column_data,
                        Some(
                            hashmap! {"action_id".into() => action_id, "user_id".into() => user_id},
                        ),
                    )?;
                server_api.mark_changed()?;

                Ok(())
            },
        ))
    }

    fn on_add_row(
        &self,
    ) -> Result<Box<dyn FnMut(&str, DataType, &mut ServerAPI) -> Result<u32, JsValue>>, JsValue>
    {
        Ok(Box::new(|_, _, _| Ok(0)))
    }
}

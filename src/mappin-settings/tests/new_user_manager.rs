use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;
use web_sys::HtmlElement;

use mappin_settings::new_user_manager::{NewUserManager, reset_new_user_form};

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn wasm_new_user_manager_test() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));
    let body = document.body().expect("Cannot get body");

    let password_form = HtmlElement::from(JsValue::from(
        document
            .create_element("FORM")
            .expect("Cannot create form element"),
    ));
    password_form.set_id("new_user_content");
    body.append_with_node_1(&password_form.into())
        .expect("Cannot append form to body");

    let cpm = NewUserManager::new("new_user").expect("Error creating new user manager");
}

#[wasm_bindgen_test]
fn wasm_reset_new_user_form_test() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));
    let body = document.body().expect("Cannot get body");

    let password_form = HtmlElement::from(JsValue::from(
        document
            .create_element("FORM")
            .expect("Cannot create form element"),
    ));
    password_form.set_id("new_user_form");
    body.append_with_node_1(&password_form.into())
        .expect("Cannot append form to body");

    reset_new_user_form("").expect("Error creating new user form content");
}

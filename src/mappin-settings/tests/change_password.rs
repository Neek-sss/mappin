use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;
use web_sys::HtmlElement;

use mappin_settings::change_password::{ChangePasswordManager, reset_change_password_form};

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn wasm_change_password_manager_test() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));
    let body = document.body().expect("Cannot get body");

    let password_form = HtmlElement::from(JsValue::from(
        document
            .create_element("FORM")
            .expect("Cannot create form element"),
    ));
    password_form.set_id("change_password_content");
    body.append_with_node_1(&password_form.into())
        .expect("Cannot append form to body");

    let cpm = ChangePasswordManager::new("change_password")
        .expect("Error creating change password manager");
}

#[wasm_bindgen_test]
fn wasm_reset_change_password_form_test() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));
    let body = document.body().expect("Cannot get body");

    let password_form = HtmlElement::from(JsValue::from(
        document
            .create_element("FORM")
            .expect("Cannot create form element"),
    ));
    password_form.set_id("change_password_form");
    body.append_with_node_1(&password_form.into())
        .expect("Cannot append form to body");

    reset_change_password_form().expect("Error creating password form content");
}

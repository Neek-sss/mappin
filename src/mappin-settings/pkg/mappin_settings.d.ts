/* tslint:disable */
/**
* Called when the wasm module is instantiated.  This is equivalent to a binary program\'s main
* function.
*/
export function wasm_main(): void;
/**
* A struct that allows JS functions (specifically SlickGrid functions) to access internal Rust data.
*/
export class DataConnector {
  free(): void;
/**
* The number of data objects not filtered
* Note: A function required by SlickGrid
* @returns {number} 
*/
  getLength(): number;
/**
* The item corresponding to the index as filtered and sorted
* Note: A function required by SlickGrid
* @param {number} index 
* @returns {any} 
*/
  getItem(index: number): any;
/**
* @param {number} id 
* @returns {any} 
*/
  getRow(id: number): any;
/**
* @param {string} column_name 
*/
  set_sort_column(column_name: string): void;
/**
* @param {boolean} ascending 
*/
  set_sort_ascending(ascending: boolean): void;
}

/**
* If `module_or_path` is {RequestInfo}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {RequestInfo | BufferSource | WebAssembly.Module} module_or_path
*
* @returns {Promise<any>}
*/
export default function init (module_or_path?: RequestInfo | BufferSource | WebAssembly.Module): Promise<any>;
        
import os

os.environ["DJANGO_SETTINGS_MODULE"] = "server.serve-settings"

import cherrypy
from django.conf import settings
from django.core.handlers.wsgi import WSGIHandler

import django


class DjangoApplication(object):
    HOST = "192.168.1.157"
    PORT = 8000

    def mount_static(self, url, root):
        """
        :param url: Relative url
        :param root: Path to static files root
        """
        config = {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': root,
            'tools.expires.on': True,
            'tools.expires.secs': 86400
        }
        cherrypy.tree.mount(None, url, {'/': config})

    def run(self):
        cherrypy.server.ssl_module = 'builtin'
        cherrypy.server.ssl_certificate = "cert.pem"
        cherrypy.server.ssl_private_key = "key.pem"
        # cherrypy.server.ssl_certificate_chain = "certchain.perm"

        cherrypy.config.update({
            'server.socket_host': self.HOST,
            'server.socket_port': self.PORT,
            'engine.autoreload_on': False,
            'log.screen': True
        })
        self.mount_static(settings.STATIC_URL, settings.STATIC_ROOT)
        self.mount_static(settings.MEDIA_URL, settings.MEDIA_ROOT)

        cherrypy.log("Loading and serving Django application")
        django.setup()
        cherrypy.tree.graft(WSGIHandler())
        cherrypy.engine.start()

        cherrypy.engine.block()


if __name__ == "__main__":
    DjangoApplication().run()

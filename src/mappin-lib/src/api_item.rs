use std::collections::HashMap;

use maplit::hashmap;
use wasm_bindgen::prelude::*;

use crate::{
    ajax::{into_form_data, parse_ajax_json, wasm_ajax},
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    database_objects::OrderedDataType,
};

/// The type of queue that is being processed.  Meant to help differentiate since all the queues
/// are just `HashMap`'s
#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum QueueType {
    /// Adds items to the system
    Add,
    /// Updates items already in the system
    Update,
    /// Removes an item from the system
    Delete,
}

/// The functions that are run whenever an `APIItem` performs a server I/O operation (send, update,
/// or delete).
#[derive(Default)]
pub struct APIFuncs {
    /// The operation for adding an item
    on_add: Option<Box<dyn FnMut(DatabaseObject, Option<DatabaseObject>) -> Result<(), JsValue>>>,
    /// The operation for updating an existing item
    on_update: Option<
        Box<dyn FnMut(DatabaseObject, String, Option<DataType>, DataType) -> Result<(), JsValue>>,
    >,
    /// The operation for deleting an existing item
    on_delete: Option<Box<dyn FnMut(DatabaseObject) -> Result<(), JsValue>>>,
}

/// The queues that store data updates to be sent to the server
#[derive(Clone, Default, Serialize, Deserialize)]
pub struct APIQueues {
    /// New `DataObject`'s to be added to the server
    pub(crate) add: HashMap<u32, String>,
    /// Updates to existing `DataObject`'s
    pub(crate) update: HashMap<u32, HashMap<String, String>>,
    /// `DataObject`'s to be removed
    pub(crate) delete: HashMap<u32, String>,
}

/// The permissions for this user in regards to this type of DatabaseObject
#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct APIPermission {
    pub view: bool,
    pub add: bool,
    pub change: bool,
    pub delete: bool,
}

/// Creates a central storage location that maintains consistency with the server and client as well
/// as provides the internal data for use in JS libraries.
#[derive(Default)]
pub struct APIItem {
    /// The type of Django model that this `APIItem` stores
    api_type: APIType,
    /// The functions that are run when the data changes
    funcs: APIFuncs,
    /// The internal storage for any data updates that occur and must be processed
    queues: APIQueues,
    /// The permissions that the user has in association with `DatabaseObject`'s of this type
    pub permissions: APIPermission,
    /// The internal storage for all of the `APIItem`'s `DatabaseObject`'s
    pub data: HashMap<u32, DatabaseObject>,
    /// The last time that the client/server update cycle was run (based on server time)
    timestamp: f64,
}

impl APIItem {
    /// Creates a new `APIItem` of the specified `APIType`.  The data is loaded from the server, and
    /// therefore may run into network errors if it cannot connect.
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::{api_item::{APIItem}, api_type::APIType};
    ///
    /// let api_item = APIItem::new(APIType::Building).expect("Error creating api item");
    /// ```
    pub fn new(api_type: APIType) -> Result<APIItem, JsValue> {
        // Get the user's permissions for this object
        let permission_response = wasm_ajax(
            &format!("/mappin/permission/{}/", api_type.as_str()),
            &into_form_data::<String, String>(HashMap::new()),
        )?;
        let permission = parse_ajax_json(&permission_response.data)
            .map_err(|e| JsValue::from(format!("{}", e)))?
            .into_iter()
            .last()
            .unwrap()
            .1;

        // Get the data from the server
        let (update_queues, parsed_objects, timestamp) = {
            if let Ok(response) = wasm_ajax(
                &format!("/mappin/load/{}/", api_type.as_str()),
                &into_form_data::<String, String>(HashMap::new()),
            ) {
                // Check the response is OK
                if response.status == 200 {
                    // Get the DatabaseObject's the server sent back
                    let parsed_objects = parse_ajax_json(&response.data).unwrap_or_default();
                    (
                        parsed_objects
                            .iter()
                            .map(|(&key, _)| (key, HashMap::new()))
                            .collect(),
                        parsed_objects,
                        response.timestamp,
                    )
                } else {
                    Default::default()
                }
            } else {
                Default::default()
            }
        };

        // Return a new Self with the loaded data and provided APIType
        Ok(Self {
            api_type,
            funcs: APIFuncs {
                on_add: None,
                on_update: None,
                on_delete: None,
            },
            data: parsed_objects,
            queues: APIQueues {
                add: Default::default(),
                update: update_queues,
                delete: Default::default(),
            },
            permissions: APIPermission {
                view: permission.fields["view"].as_bool().unwrap(),
                add: permission.fields["add"].as_bool().unwrap(),
                change: permission.fields["change"].as_bool().unwrap(),
                delete: permission.fields["delete"].as_bool().unwrap(),
            },
            timestamp,
        })
    }

    /// Sets the processing functions of the `APIItem` to the provided `Option`'s.  If set to
    /// `None`, a function will not be run (the default), otherwise the provided function will be
    /// run every time the data is updated from the server or data is input to the `APIItem` through
    /// a `queue_x` method.
    pub fn set_up_on_funcs(
        &mut self,
        on_add: Option<
            Box<dyn FnMut(DatabaseObject, Option<DatabaseObject>) -> Result<(), JsValue>>,
        >,
        on_update: Option<
            Box<
                dyn FnMut(
                    DatabaseObject,
                    String,
                    Option<DataType>,
                    DataType,
                ) -> Result<(), JsValue>,
            >,
        >,
        on_delete: Option<Box<dyn FnMut(DatabaseObject) -> Result<(), JsValue>>>,
    ) -> Result<(), JsValue> {
        self.funcs.on_add = on_add;
        self.funcs.on_update = on_update;
        self.funcs.on_delete = on_delete;
        Ok(())
    }

    /// Gets the number of `DatabaseObject`'s that pass the provided filter, if it is provided.
    pub fn get_num_data_object(
        &self,
        filter_field_func: &mut Option<Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>>,
    ) -> Result<usize, JsValue> {
        if let Some(filter_func) = filter_field_func {
            // Count the number of objects that pass the filter
            let mut count = 0;
            for object in self.data.values() {
                if filter_func(&object)? {
                    count += 1;
                }
            }

            Ok(count)
        } else {
            // If no filter is provided, then the total number of objects is returned
            Ok(self.data.len())
        }
    }

    /// Returns a `DatabaseObject` based on its index as filtered by `filter_field_func` and ordered
    /// on field `order_by`.  Ascending/Descending order is specified in `order_ascending` (defaults
    /// to ascending.
    pub fn get_data_object_ordered(
        &self,
        index: u32,
        mut filter_field_func: &mut Option<
            Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>,
        >,
        order_by: &str,
        order_ascending: Option<bool>,
    ) -> Result<Option<&DatabaseObject>, JsValue> {
        // Filter the DatabaseObject's down to only those that pass filter_field_func, and find
        // the order field.
        let mut cache = self
            .data
            .iter()
            .filter_map(|(key, object)| {
                // Get each object's order_by field and set that as the key
                let ret = (OrderedDataType::from(object.fields[order_by].clone()), *key);

                // Run the filter on each DatabaseObject
                if let Some(filter_func) = &mut filter_field_func {
                    // Check pass/fail
                    if let Ok(true) = filter_func(&object) {
                        Some(ret)
                    } else {
                        None
                    }
                } else {
                    // If there is no filter_field_func, then everything passes
                    Some(ret)
                }
            })
            .collect::<Vec<_>>();

        // Sort the DatabaseObject's
        cache.sort_by(|(key1, _), (key2, _)| key1.clone().cmp(&key2));

        // Get the Id that matches the final filtered/ordered data index
        let ordering = if let Some(false) = order_ascending {
            cache.into_iter().rev().nth(index as usize)
        } else {
            cache.into_iter().nth(index as usize)
        }
            .map(|(_, object_key)| object_key);

        // Return the DatabaseObject with that Id
        Ok(ordering.and_then(|object_key| Some(&self.data[&object_key])))
    }

    /// Reverses the function of `get_data_object_ordered`, allowing the user to get the unique id
    /// of an object when s/he only has the ordered position.
    pub fn get_index_from_order(
        &self,
        item_id: u32,
        mut filter_field_func: &mut Option<
            Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>>,
        >,
        order_by: &str,
        order_ascending: Option<bool>,
    ) -> Result<Option<u32>, JsValue> {
        // Filter the DatabaseObject's down to only those that pass filter_field_func, and find
        // the order field.
        let mut cache = self
            .data
            .iter()
            .filter_map(|(key, object)| {
                // Get each object's order_by field and set that as the key
                let ret = (OrderedDataType::from(object.fields[order_by].clone()), *key);

                // Run the filter on each DatabaseObject
                if let Some(filter_func) = &mut filter_field_func {
                    // Check pass/fail
                    if let Ok(true) = filter_func(&object) {
                        Some(ret)
                    } else {
                        None
                    }
                } else {
                    // If there is no filter_field_func, then everything passes
                    Some(ret)
                }
            })
            .collect::<Vec<_>>();

        // Sort the DatabaseObject's
        cache.sort_by(|(key1, _), (key2, _)| key1.clone().cmp(&key2));

        // Get the Id that matches the final filtered/ordered data index
        let position = if let Some(false) = order_ascending {
            cache.into_iter().rposition(|(_, id)| id == item_id)
        } else {
            cache.into_iter().position(|(_, id)| id == item_id)
        };

        // Return the DatabaseObject with that Id
        Ok(position.map(|i| i as u32))
    }

    /// Adds a `DatabaseObject` to the internal data storage and the internal server add queue of
    /// this `APIItem`.  Also runs self's `APIFuncs::on_add function`, if it exists.
    pub fn queue_add(
        &mut self,
        id: u32,
        name: &str,
        other_keys: Option<HashMap<String, DataType>>,
    ) -> Result<(), JsValue> {
        // Make the object
        let mut database_object = DatabaseObject::new(self.api_type.model(), id, &other_keys);
        database_object
            .fields
            .insert("name".into(), DataType::S(name.into()));

        // Put the object in internal storage
        let old_object = self
            .data
            .insert(database_object.id, database_object.clone());

        // Prepare the update queue to handle this object
        self.queues
            .update
            .insert(database_object.id, HashMap::new());

        // Prepare a server update
        let mut add_map: HashMap<String, DataType> = hashmap! {
            "id".into() => DataType::U(database_object.id.into()),
            "name".into() => DataType::S(name.into()),
            "type".into() => DataType::S("add".into()),
        };
        other_keys.map(|fields| add_map.extend(fields));

        // Queue the server update
        self.queues.add.insert(
            database_object.id,
            into_form_data::<String, DataType>(add_map),
        );

        if let Some(on_add) = &mut self.funcs.on_add {
            on_add(database_object, old_object)?;
        }

        Ok(())
    }

    /// Adds an existing `DatabaseObject` in the internal data storage and the internal server
    /// update queue of this `APIItem`.  Also runs self's `APIFuncs::on_update function`, if it
    /// exists.
    pub fn queue_update(
        &mut self,
        id: u32,
        column: &str,
        data: DataType,
        other_keys: Option<HashMap<String, DataType>>,
    ) -> Result<(), JsValue> {
        // Update the data
        if let Some(database_object) = self.data.get_mut(&id) {
            let old_data = database_object.fields.insert(column.into(), data.clone());

            // Prepare the server update
            let mut update_map: HashMap<String, DataType> = hashmap! {
                "id".into() => DataType::U(database_object.id.into()),
                "column".into() => DataType::S(column.into()),
                "data".into() => data.clone(),
                "type".into() => DataType::S("update".into()),
            };
            other_keys.map(|fields| update_map.extend(fields));

            // Queue the server update
            self.queues
                .update
                .get_mut(&database_object.id)
                .unwrap()
                .insert(
                    column.into(),
                    into_form_data::<String, DataType>(update_map),
                );

            if let Some(on_update) = &mut self.funcs.on_update {
                on_update(database_object.clone(), column.into(), old_data, data)?;
            }

            Ok(())
        } else {
            Err("Update key not found".into())
        }
    }

    /// Adds an existing `DatabaseObject` in the internal data storage and the internal server
    /// update queue of this `APIItem`.  this one ignores the `APIFuncs::on_update function`
    /// entirely.
    pub fn queue_update_skip_on_update(
        &mut self,
        id: u32,
        column: &str,
        data: DataType,
        other_keys: Option<HashMap<String, DataType>>,
    ) -> Result<(), JsValue> {
        // Update the data
        if let Some(database_object) = self.data.get_mut(&id) {
            let old_data = database_object.fields.insert(column.into(), data.clone());

            // Prepare the server update
            let mut update_map: HashMap<String, DataType> = hashmap! {
                "id".into() => DataType::U(database_object.id.into()),
                "column".into() => DataType::S(column.into()),
                "data".into() => data.clone(),
                "type".into() => DataType::S("update".into()),
            };
            other_keys.map(|fields| update_map.extend(fields));

            // Queue the server update
            self.queues
                .update
                .get_mut(&database_object.id)
                .unwrap()
                .insert(
                    column.into(),
                    into_form_data::<String, DataType>(update_map),
                );

            Ok(())
        } else {
            Err("Update key not found".into())
        }
    }

    /// Delete a `DatabaseObject` from the internal data storage.  Also runs self's
    /// `APIFuncs::on_add function`, if it exists.
    pub fn queue_delete(
        &mut self,
        id: u32,
        other_keys: Option<HashMap<String, DataType>>,
    ) -> Result<(), JsValue> {
        // Remove from internal storage
        if let Some(database_object) = self.data.remove(&id) {
            // Prepare the server update
            let mut delete_map: HashMap<String, DataType> = hashmap! {
                "id".into() => DataType::U(database_object.id.into()),
                "type".into() => DataType::S("delete".into()),
            };
            other_keys.map(|fields| delete_map.extend(fields));

            // Queue the server update
            self.queues.delete.insert(
                database_object.id,
                into_form_data::<String, DataType>(delete_map),
            );

            if let Some(on_delete) = &mut self.funcs.on_delete {
                on_delete(database_object)?;
            }

            Ok(())
        } else {
            Err("Delete key not found".into())
        }
    }

    /// Sends the specified queue to the server, updating the server to all operations performed
    /// on the client.
    pub fn send_queue(&mut self, queue: QueueType) -> Result<(), JsValue> {
        // The URL that corresponds to this APIType
        let url = format!("/mappin/api/{}/", self.api_type.as_str());

        match queue {
            QueueType::Add => {
                // Process each operation in add
                for (key, result_response) in self
                    .queues
                    .add
                    .clone()
                    .into_iter()
                    .map(|(key, value)| (key, wasm_ajax(&url, &value)))
                    {
                        match result_response {
                            Ok(response) => {
                                // Get the new object data (specifically the new Id)
                                let parsed_objects =
                                    parse_ajax_json(&response.data).map_err(|e| format!("{}", e))?;

                                for (new_key, parsed_object) in parsed_objects {
                                    // Replace the object's temporary key with the permanent key from
                                    // the server
                                    let old_object = self.data.remove(&key);
                                    self.data.insert(new_key, parsed_object.clone());

                                    // Replace the keys in update with the new key
                                    let mut update_queue = self.queues.update.remove(&key).unwrap();
                                    for update in update_queue.values_mut() {
                                        let replaced_string = update.replace(
                                            &format!("id={}&", key),
                                            &format!("id={}&", new_key),
                                        );
                                        update.drain(..);
                                        update.push_str(&replaced_string);
                                    }
                                    self.queues.update.insert(new_key, update_queue);

                                    // Replace the keys in delete with the new key
                                    if let Some(delete_queue) = self.queues.delete.remove(&key) {
                                        self.queues.delete.insert(new_key, delete_queue);
                                }

                                    // Run the on_add function, if it is set
                                    if let Some(on_func) = &mut self.funcs.on_add {
                                        on_func(parsed_object.clone(), old_object)?;
                                    }
                                }
                            }
                            Err(error) => web_sys::console::error_1(&error),
                        }
                    }
                self.queues.add.clear();
            }
            QueueType::Update => {
                // Process each operation in update
                for (key, columns) in
                    self.queues
                        .update
                        .clone()
                        .into_iter()
                        .map(|(key, column_map)| {
                            (
                                key,
                                column_map
                                    .into_iter()
                                    .map(|(column, value)| (column, wasm_ajax(&url, &value)))
                                    .collect::<HashMap<_, _>>(),
                            )
                        })
                    {
                        for (column_name, result_response) in columns {
                        match result_response {
                            Ok(response) => {
                                let json_value =
                                    serde_json::from_str::<serde_json::Value>(&response.data)
                                        .map_err(|_| "Error reading response data")?;
                                let (_, field_value) = json_value
                                    .as_object()
                                    .ok_or("Cannot convert JSON to object")?
                                    .into_iter()
                                    .last()
                                    .ok_or("JSON object empty")?;
                                let field: DataType = field_value.into();

                                let old_field = self.data[&key]
                                    .fields
                                    .get(&column_name)
                                    .cloned()
                                    .unwrap_or(DataType::NULL);

                                if field != old_field {
                                    // Run the on_update function, if it is set
                                    if let Some(on_func) = &mut self.funcs.on_update {
                                        on_func(
                                            self.data[&key].clone(),
                                            column_name.clone(),
                                            Some(old_field),
                                            field,
                                        )?;
                                    }
                                }
                            }
                            Err(error) => web_sys::console::error_1(&error),
                        }
                    }
                }
                self.queues.update.clear();
                for key in self.data.keys() {
                    self.queues.update.insert(*key, HashMap::new());
                }
            }
            QueueType::Delete => {
                // Process each operation in delete
                for (key, result_response) in self
                    .queues
                    .delete
                    .clone()
                    .into_iter()
                    .map(|(key, value)| (key, wasm_ajax(&url, &value)))
                    {
                        match result_response {
                            Ok(_response) => {
                                if let Some(data_object) = self.data.remove(&key) {
                                    // Run the on_delete function, if it is set
                                    if let Some(on_func) = &mut self.funcs.on_delete {
                                        on_func(data_object.clone())?;
                                }
                                }
                            }
                            Err(error) => web_sys::console::error_1(&error),
                        }
                    }
                self.queues.delete.clear();
            }
        }
        Ok(())
    }

    /// Loads any changes from the server that have occurred since the last update cycle.  These
    /// changes are then edited in their internal data storage and processed with the `on_x`
    /// functions set on `APIItem` creation.
    pub fn refresh_data_from_server(&mut self) -> Result<(), JsValue> {
        // Get any changes from the server
        let response = wasm_ajax(
            &format!("/mappin/history/{}/", self.api_type.as_str()),
            &into_form_data::<String, DataType>(
                hashmap! {"time".into() => DataType::F(self.timestamp)},
            ),
        )?;

        // Turn the JSON in History DatabaseObject's
        let parsed_responses = parse_ajax_json(&response.data).map_err(|e| format!("{}", e))?;

        // Process all the changes
        for history_object in parsed_responses.values() {
            if let (Some(history_type), Some(id), Some(json_string)) = (
                history_object
                    .fields
                    .get("type")
                    .and_then(|s| s.as_string().ok()),
                history_object
                    .fields
                    .get("history_object")
                    .and_then(|u| u.as_u64().ok()),
                history_object
                    .fields
                    .get("data")
                    .and_then(|s| s.as_string().ok()),
            ) {
                match history_type.as_str() {
                    "add" => {
                        // Recreate the DatabaseObject from the History data
                        let parsed_object =
                            parse_ajax_json(&json_string).map_err(|e| format!("{}", e))?;

                        // Get the old object, if it exists (it shouldn't)
                        let old_object = self
                            .data
                            .insert(id as u32, parsed_object[&(id as u32)].clone());

                        if let Some(old_object_inner) = &old_object {
                            let new_object = self.data.get_mut(&(id as u32)).unwrap();
                            for v in &old_object_inner.fields {
                                if let Some(string) = v.1.as_string().ok() {
                                    if string.as_str() != "" {
                                        new_object.fields.insert(v.0.clone(), v.1.clone());
                                    }
                                };
                            }
                        }

                        // Prepare the update queue for any updates to the object
                        self.queues
                            .update
                            .entry(id as u32)
                            .or_insert_with(HashMap::new);

                        // Run on_add, if it exists
                        if let Some(on_func) = &mut self.funcs.on_add {
                            on_func(parsed_object[&(id as u32)].clone(), old_object)?;
                        }
                    }
                    "update" => {
                        // Get the column and column value from the history
                        let edit_data = serde_json::from_str::<serde_json::Value>(&json_string)
                            .map_err(|_| "Error parsing JSON")?;
                        if let Some(data_object) = self.data.get_mut(&(id as u32)) {
                            for (column, data) in edit_data.as_object().unwrap() {
                                // Fix the frequent occurrence of the building id becoming a string,
                                // even though it is obviously an int??
                                let value = if column == "building" {
                                    let data_type = DataType::from(data);
                                    if data_type.as_u64().is_ok() {
                                        data_type
                                    } else {
                                        DataType::U(
                                            data_type
                                                .as_string()
                                                .map_err(|e| JsValue::from(e))?
                                                .parse()
                                                .map_err(|_| {
                                                    JsValue::from("Building id not a number")
                                                })?,
                                        )
                                    }
                                } else {
                                    DataType::from(data)
                                };

                                // Perform the update
                                let old_field = data_object.fields.insert(column.clone(), value);
                                let field = DataType::from(data);

                                // Run the on_update function, if it is set
                                if field != old_field.clone().unwrap_or(DataType::NULL) {
                                    if let Some(on_func) = &mut self.funcs.on_update {
                                        on_func(
                                            data_object.clone(),
                                            column.clone(),
                                            old_field,
                                            field,
                                        )?;
                                    }
                                }
                            }
                        }
                    }
                    "delete" => {
                        // Remove the object
                        if let Some(data_object) = self.data.remove(&(id as u32)) {
                            // Run the on_delete function, if it is set
                            if let Some(on_func) = &mut self.funcs.on_delete {
                                on_func(data_object.clone())?;
                            }
                        }
                    }
                    _ => (),
                }
            }
        }

        // Update the new timestamp
        self.timestamp = response.timestamp;

        Ok(())
    }
}

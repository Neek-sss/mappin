use wasm_bindgen::prelude::*;

/// Finds a cookie from the `Document`'s cookie store
///
/// Examples:
/// ```rust,should_panic
/// use mappin_lib::helpers::get_cookie;
///
/// let cookie_option = get_cookie("cookie_name").expect("Error getting cookie \"cookie_name\"");
/// let cookie = cookie_option.expect("Cookie \"cookie name\" is not set");
/// ```
pub fn get_cookie(id: &str) -> Result<Option<String>, JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    let cookies = document
        .cookie()?
        .chars()
        .filter(|&c| c != ' ')
        .collect::<String>();
    Ok(cookies
        .split(';')
        .filter_map(|cookie| {
            let id_str = format!("{}=", id);
            if cookie.starts_with(&id_str) {
                let cookie_data = cookie.split_at(id_str.len()).1.to_string();
                Some(cookie_data)
            } else {
                None
            }
        })
        .last())
}

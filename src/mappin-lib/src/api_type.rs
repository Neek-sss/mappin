/// The type of object that the data connects to.  This matches respective models in Django.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum APIType {
    /// Model: mappin.InventoryBuildingImage
    BuildingImage,
    /// Model: mappin.InventoryBuilding
    Building,
    /// Model: mappin.InventoryItemImage
    ItemImage,
    /// Model: mappin.InventoryItem
    Item,
    /// Model: django.contrib.auth.models.User
    User,
    /// Model: mappin.permission (not a real type, but an in-between for
    /// django.contrib.auth.models.Permission)
    Permission,
    /// Model does not match any known Django model
    UNKNOWN,
}

impl Default for APIType {
    fn default() -> Self {
        Self::UNKNOWN
    }
}

impl From<&str> for APIType {
    fn from(string: &str) -> Self {
        match string {
            "image_building" => Self::BuildingImage,
            "building" => Self::Building,
            "image_item" => Self::ItemImage,
            "item" => Self::Item,
            "user" => Self::User,
            "permission" => Self::Permission,
            _ => Self::UNKNOWN,
        }
    }
}

impl APIType {
    /// Get the model string that an APIType represents
    pub fn model(self) -> &'static str {
        match self {
            Self::Building => "mappin.inventorybuilding",
            Self::BuildingImage => "mappin.inventorybuildingimage",
            Self::Item => "mappin.inventoryitem",
            Self::ItemImage => "mappin.inventoryitemimage",
            Self::User => "auth.user",
            Self::Permission => "mappin.permission",
            Self::UNKNOWN => "",
        }
    }

    /// Turn an APIType into its matching internal string representation
    pub fn as_str(self) -> &'static str {
        match self {
            Self::BuildingImage => "image_building",
            Self::Building => "building",
            Self::ItemImage => "image_item",
            Self::Item => "item",
            Self::User => "user",
            Self::Permission => "permission",
            Self::UNKNOWN => "",
        }
    }
}

use std::collections::HashMap;

use wasm_bindgen::prelude::*;

use crate::{
    api_item::{APIItem, QueueType},
    api_type::APIType,
};

/// The core client/server connecting data structure
#[derive(Default)]
pub struct ServerAPI {
    /// All of the API's that this ServerAPI instance manages
    pub api_items: HashMap<APIType, APIItem>,
    /// Dictates when the server needs to be updated
    pub unsaved_changes: bool,
    /// The last time that a change was made to data in the ServerAPI
    pub last_change_time: f64,
}

impl ServerAPI {
    /// Creates a new server connection
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::{server_api::ServerAPI, api_type::APIType};
    ///
    /// let server_api = ServerAPI::new(&[APIType::Building]).expect("Error setting up server api");
    /// ```
    pub fn new(api_types: &[APIType]) -> Result<ServerAPI, JsValue> {
        let ret = Self {
            api_items: api_types
                .iter()
                .filter_map(|&api_type| {
                    APIItem::new(api_type)
                        .ok()
                        .map(|api_item| (api_type, api_item))
                })
                .collect(),
            unsaved_changes: false,
            last_change_time: js_sys::Date::now(),
        };
        Ok(ret)
    }

    /// Notifies the `ServerAPI` that the data has changed and shows a prompt to the user.  Requires
    /// that an element id'ed `unsaved_changes_notification` exists on the page.
    ///
    /// Examples:
    /// ```rust,should_panic
    /// # use mappin_lib::{server_api::ServerAPI, api_type::APIType};
    /// #
    /// # let mut server_api = ServerAPI::new(&[]).expect("Error setting up server api");
    /// server_api.mark_changed().expect("Error marking change");
    /// ```
    pub fn mark_changed(&mut self) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Error opening window")?;
        let document = window.document().ok_or("Error opening document")?;

        // Set the internal flag
        self.unsaved_changes = true;

        // Show the unsaved changes notifications
        let unsaved_changes_notification = web_sys::HtmlElement::from(JsValue::from(
            document
                .get_element_by_id("unsaved_changes_notification")
                .ok_or("Error getting unsaved_changes notification")?,
        ));
        unsaved_changes_notification.set_hidden(false);

        self.last_change_time = js_sys::Date::now();

        Ok(())
    }

    /// Updates the server with the client data, and the client with any server changes.  Requires
    /// that two elements exist on the page with id's `unsaved_changes_notification` and
    /// `saving_notification`.
    ///
    /// Examples:
    /// ```rust,should_panic
    /// # use mappin_lib::{server_api::ServerAPI, api_type::APIType};
    /// #
    /// # let mut server_api = ServerAPI::new(&[]).expect("Error setting up server api");
    /// server_api.data_updates().expect("Error updating data");
    pub fn data_updates(&mut self) -> Result<(), JsValue> {
        // Only run this section if there were changes (but not too recently, because current edits
        // may be interrupted by the data update routine)
        if js_sys::Date::now() - self.last_change_time > 1_000.0 {
            if self.unsaved_changes {
                let window = web_sys::window().ok_or("Error opening window")?;
                let document = window.document().ok_or("Error opening document")?;

                // Unset the internal flag
                self.unsaved_changes = false;

                // Hide the server error notification
                let error_notification = web_sys::HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("error_notification")
                        .ok_or("Error getting unsaved_changes notification")?,
                ));
                error_notification.set_hidden(true);

                // Hide the unsaved changes notifications
                let unsaved_changes_notification = web_sys::HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("unsaved_changes_notification")
                        .ok_or("Error getting unsaved_changes notification")?,
                ));
                unsaved_changes_notification.set_hidden(true);

                // Show the saving changes notifications
                let saving_notification = web_sys::HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("saving_notification")
                        .ok_or("Error getting saving notification")?,
                ));
                saving_notification.set_hidden(false);

                // Update the server to client changes
                for (_, api_item) in self.api_items.iter_mut() {
                    api_item.send_queue(QueueType::Add)?;
                    api_item.send_queue(QueueType::Update)?;
                    api_item.send_queue(QueueType::Delete)?;
                }

                // Hide the saving notification
                saving_notification.set_hidden(true);
            }

            // Update the client to any server updates
            for (_, api_item) in self.api_items.iter_mut() {
                api_item.refresh_data_from_server()?;
            }
        }

        Ok(())
    }
}

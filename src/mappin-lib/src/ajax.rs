use std::{
    collections::HashMap,
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
};

use serde_json;
use wasm_bindgen::prelude::*;
use web_sys;

use crate::{database_objects::DatabaseObject, helpers::get_cookie};

/// The response from the server whenever the client sends a request.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AJAXResponse {
    /// The HTTP status code
    pub status: u16,

    /// Any data that the server responded with (from the response text)
    pub data: String,

    /// The timestamp cookie that the server response set
    pub timestamp: f64,
}

/// Converts a hashmap of Rust values into an XHR form data string using the key/value's display trait
///
/// Example:
/// ```
/// # use std::collections::HashMap;
/// # use mappin_lib::ajax::into_form_data;
/// # use wasm_bindgen::JsValue;
/// #
/// let mut data = HashMap::new();
/// data.insert("first_arg", "3");
/// data.insert("second_arg", "Another Argument");
///
/// let ret = into_form_data(data);
///
/// assert!(ret.contains("first_arg=3&"), "First arg not written");
/// assert!(ret.contains("second_arg=Another Argument&"), "Second arg not written");
/// ```
pub fn into_form_data<K, V>(data: HashMap<K, V>) -> String
where
    K: Display,
    V: Display,
{
    let mut form_data = String::new();
    for (key, value) in data {
        form_data += &format!("{}={}&", key, value);
    }
    form_data
}

#[derive(Copy, Clone, Debug)]
pub struct AjaxJsonParseError {}

impl Error for AjaxJsonParseError {}

impl Display for AjaxJsonParseError {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "Error parsing AJAX JSON response")
    }
}

/// Converts a JSON formatted string into its respective DatabaseObject.
/// The JSON string must be formatted like:
/// [
///     {
///         id: [id],
///         model: [model],
///         fields: {
///             [field_name]: [field_value],
///             ...
///         }
///     }
/// ]
/// This is meant to deserialize the data field of `AJAXResponse`.
///
/// Example:
/// ```
/// # use std::{collections::HashMap, error::Error};
/// # use mappin_lib::{ajax::parse_ajax_json, database_objects::DataType};
/// #
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let mut data = r#"
/// [
///    {
///        "model": "mappin.testmodel",
///        "pk": 100,
///        "fields": {
///            "test_field_1": 1920,
///            "test_field_2": "Pizza"
///        }
///    }
/// ]
/// "#;
///
/// let ret = parse_ajax_json(data)?;
///
/// assert_eq!(ret.len(), 1);
/// assert_eq!(ret[&100].id, 100);
/// assert_eq!(ret[&100].model, "mappin.testmodel");
/// assert_eq!(ret[&100].fields.len(), 2);
/// assert_eq!(ret[&100].fields["test_field_1"], DataType::U(1920));
/// assert_eq!(ret[&100].fields["test_field_2"], DataType::S("Pizza".into()));
/// # Ok(())
/// # }
/// ```
pub fn parse_ajax_json(
    json_text: &str,
) -> Result<HashMap<u32, DatabaseObject>, AjaxJsonParseError> {
    // Turn the text into a JSON object
    if let Ok(json) = serde_json::from_str::<serde_json::Value>(&json_text) {
        // Iterate over each entry in the JSON
        let database_objects = json
            .as_array()
            .unwrap()
            .iter()
            .map(|item| {
                // Create a new DatabaseObject
                let mut obj = DatabaseObject::default();

                // Where to store the id
                let mut id = None;

                // Iterate over the object's entries
                for (name, value) in item.as_object().unwrap() {
                    match name.as_str() {
                        // The Django model
                        "model" => obj.model = value.as_str().unwrap().into(),
                        // The id/primary key of the object
                        "pk" => {
                            obj.id = value.as_u64().unwrap() as u32;
                            id = Some(value.as_u64().unwrap() as u32);
                        }
                        // All of the data fields in the object
                        "fields" => {
                            for (field_name, field_value) in value.as_object().unwrap() {
                                obj.fields.insert(field_name.into(), field_value.into());
                            }
                        }
                        _ => (),
                    }
                }
                (id.unwrap(), obj)
            })
            .collect::<HashMap<_, _>>();
        Ok(database_objects)
    } else {
        //        web_sys::console::error_1(&format!("{:#?}", json_text).into());
        Err(AjaxJsonParseError {})
    }
}

/// Sends and receives data from the server, returning an `AJAXResponse` containing the server's
/// response status and any data that the server responded with.
///
/// Examples:
/// ```rust,should_panic
/// use mappin_lib::ajax::wasm_ajax;
///
/// let form_data = "a=x&b=1";
/// let response = wasm_ajax("https://127.0.0.1:8080/a_site", form_data).expect("Error running ajax");
/// ```
pub fn wasm_ajax(url: &str, form_data: &str) -> Result<AJAXResponse, JsValue> {
    let csrf_token = get_cookie("csrftoken")?.unwrap_or(String::new());

    // Send the request
    let request = web_sys::XmlHttpRequest::new()?;
    request.open_with_async("POST", url, false)?;
    request.set_request_header("X-CSRFToken", &csrf_token)?;
    request.send_with_opt_str(Some(&form_data))?;

    // Prepare and return the response
    Ok(AJAXResponse {
        data: request.response_text()?.unwrap(),
        timestamp: get_cookie("timestamp")?
            .expect("Error getting new timestamp")
            .parse()
            .map_err(|_| "Error parsing float")?,
        status: request.status()?,
    })
}

/// Sends and receives data to the server, ignoring any response data or timestamp
/// (the status is still returned).
pub fn wasm_ajax_no_response(url: &str, form_data: &str) -> Result<AJAXResponse, JsValue> {
    let csrf_token = get_cookie("csrftoken")?.unwrap_or(String::new());

    // Send the request
    let request = web_sys::XmlHttpRequest::new()?;
    request.open_with_async("POST", url, false)?;
    request.set_request_header("X-CSRFToken", &csrf_token)?;
    request.send_with_opt_str(Some(&form_data))?;

    // Prepare and return the response
    Ok(AJAXResponse {
        status: request.status()?,
        data: "".into(),
        timestamp: 0.0,
    })
}

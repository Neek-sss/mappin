use std::{
    collections::HashMap,
    fmt::{Display, Formatter, Result as FmtResult},
};

use js_sys::{Object as JsObject, Reflect as JsReflect};
use serde::{Serialize, Serializer};
use serde_json;
use wasm_bindgen::prelude::*;

/// A generic container for the most basic types of data as collected from SerdeJSON and JS
#[derive(Debug, Clone, Deserialize, PartialEq)]
pub enum DataType {
    /// A String-like value (the assumed type if the data does not fit elsewhere)
    S(String),
    /// A boolean-like value
    B(bool),
    /// An integer-like value
    U(u64),
    /// A floating-number-like value
    F(f64),
    /// The JS Null
    NULL,
}

impl DataType {
    /// Get the string of a DataType::S
    ///
    /// Example:
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let string_data_type = DataType::S("A test string".into());
    /// assert_eq!(string_data_type.as_string(), Ok("A test string".into()));
    /// # Ok(())
    /// # }
    /// ```
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let null_data_type = DataType::NULL;
    /// assert!(null_data_type.as_string().is_err(), "NULL was not processed as NULL");
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn as_string(&self) -> Result<String, String> {
        if let Self::S(ret) = self {
            Ok(ret.clone())
        } else {
            Err(format!("{:?} is not a string", self))
        }
    }

    /// Get the bool of a DataType::B
    ///
    /// Example:
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let string_data_type = DataType::B(true);
    /// assert_eq!(string_data_type.as_bool(), Ok(true));
    /// # Ok(())
    /// # }
    /// ```
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let null_data_type = DataType::NULL;
    /// assert!(null_data_type.as_bool().is_err(), "NULL was not processed as NULL");
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn as_bool(&self) -> Result<bool, String> {
        if let Self::B(ret) = self {
            Ok(*ret)
        } else {
            Err(format!("{:?} is not a bool", self))
        }
    }

    /// Get the u64 of a DataType::U
    ///
    /// Example:
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let string_data_type = DataType::U(556);
    /// assert_eq!(string_data_type.as_u64(), Ok(556));
    /// # Ok(())
    /// # }
    /// ```
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let null_data_type = DataType::NULL;
    /// assert!(null_data_type.as_u64().is_err(), "NULL was not processed as NULL");
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn as_u64(&self) -> Result<u64, String> {
        if let Self::U(ret) = self {
            Ok(*ret)
        } else {
            Err(format!("{:?} is not a u64", self))
        }
    }

    /// Get the f64 of an DataType::f
    ///
    /// Example:
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let string_data_type = DataType::F(123.5);
    /// assert_eq!(string_data_type.as_f64(), Ok(123.5));
    /// # Ok(())
    /// # }
    /// ```
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::DataType;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let null_data_type = DataType::NULL;
    /// assert!(null_data_type.as_f64().is_err(), "NULL was not processed as NULL");
    /// #
    /// # Ok(())
    /// # }
    /// ```
    pub fn as_f64(&self) -> Result<f64, String> {
        if let Self::F(ret) = self {
            Ok(*ret)
        } else {
            Err(format!("{:?} is not an f64", self))
        }
    }
}

/// An in-between data type that can be ordered (necessary because floats have no inherent ordering)
#[derive(Debug, Clone, Eq, PartialOrd, Ord, PartialEq, Hash)]
pub enum OrderedDataType {
    /// A String-like value (the assumed type if the data does not fit elsewhere)
    S(String),
    /// A boolean-like value
    B(bool),
    /// An integer-like value
    U(u64),
    /// The JS Null
    NULL,
}

impl From<DataType> for OrderedDataType {
    fn from(other: DataType) -> Self {
        match other {
            DataType::S(s) => OrderedDataType::S(s),
            DataType::B(b) => OrderedDataType::B(b),
            DataType::U(u) => OrderedDataType::U(u),
            DataType::F(_) | DataType::NULL => OrderedDataType::NULL,
        }
    }
}

impl Serialize for DataType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Self::S(s) => serializer.serialize_str(s),
            Self::B(b) => serializer.serialize_bool(*b),
            Self::U(u) => serializer.serialize_u64(*u),
            Self::F(f) => serializer.serialize_f64(*f),
            Self::NULL => serializer.serialize_str(""),
        }
    }
}

impl From<&serde_json::Value> for DataType {
    fn from(value: &serde_json::Value) -> Self {
        if value.is_string() {
            Self::S(value.as_str().unwrap().into())
        } else if value.is_u64() {
            Self::U(value.as_u64().unwrap())
        } else if value.is_f64() {
            Self::F(value.as_f64().unwrap())
        } else if value.is_boolean() {
            Self::B(value.as_bool().unwrap())
        } else {
            Self::NULL
        }
    }
}

impl From<JsValue> for DataType {
    fn from(value: JsValue) -> Self {
        if let Some(float) = value.as_f64() {
            Self::F(float)
        } else if let Some(boolean) = value.as_bool() {
            Self::B(boolean)
        } else if let Some(string) = value.as_string() {
            Self::S(string)
        } else {
            Self::NULL
        }
    }
}

impl From<DataType> for JsValue {
    fn from(value: DataType) -> Self {
        match value {
            DataType::B(b) => b.into(),
            DataType::F(f) => f.into(),
            DataType::S(s) => s.into(),
            DataType::U(u) => (u as u32).into(),
            DataType::NULL => Self::NULL,
        }
    }
}

impl From<&DataType> for JsValue {
    fn from(value: &DataType) -> Self {
        match value {
            &DataType::B(b) => b.into(),
            &DataType::F(f) => f.into(),
            DataType::S(s) => s.into(),
            &DataType::U(u) => (u as u32).into(),
            &DataType::NULL => Self::NULL,
        }
    }
}

impl From<DataType> for String {
    fn from(value: DataType) -> Self {
        value.into()
    }
}

impl From<&DataType> for String {
    fn from(value: &DataType) -> Self {
        match value {
            &DataType::B(b) => format!("{}", b),
            &DataType::F(f) => format!("{}", f),
            DataType::S(s) => s.into(),
            &DataType::U(u) => format!("{}", u as u32),
            DataType::NULL => "".into(),
        }
    }
}

impl Display for DataType {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        write!(fmt, "{}", String::from(self))
    }
}

/// The core data type that is processed in the program.  This data is collected from the server
/// and processed through the JS libraries.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct DatabaseObject {
    /// The Django model represented by the `DatabaseObject`
    pub model: String,
    /// The unique ID of the `DatabaseObject`
    pub id: u32,
    /// A collection of fields that are contained within the `DatabaseObject` (anything other than
    /// the ID)
    pub fields: HashMap<String, DataType>,
}

impl From<DatabaseObject> for String {
    fn from(value: DatabaseObject) -> Self {
        serde_json::to_string(&value).unwrap_or("{}".into())
    }
}

impl From<&DatabaseObject> for String {
    fn from(value: &DatabaseObject) -> Self {
        serde_json::to_string(value).unwrap_or("{}".into())
    }
}

impl DatabaseObject {
    /// Creates a new `DatabaseObject` with the provided data
    ///
    /// Example:
    /// ```
    /// # use std::error::Error;
    /// # use mappin_lib::database_objects::{DatabaseObject, DataType};
    /// # use maplit::hashmap;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let model = "mappin.testmodel";
    /// let id = 55;
    /// let fields = hashmap!{
    ///     "test_field_1".into() => DataType::S("A test string".into()),
    ///     "test_field_2".into() => DataType::B(false),
    /// };
    /// let string_data_type = DatabaseObject::new(model, id, &Some(fields));
    /// # Ok(())
    /// # }
    /// ```
    pub fn new(
        model: &str,
        id: u32,
        field_option: &Option<HashMap<String, DataType>>,
    ) -> DatabaseObject {
        let fields = field_option.clone().unwrap_or_else(HashMap::new);
        DatabaseObject {
            model: model.into(),
            id,
            fields,
        }
    }

    /// Creates a `JsObject` from the `DatabaseObject`'s values
    ///
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::database_objects::DatabaseObject;
    ///
    /// let database_object = DatabaseObject::default();
    /// let js_object = database_object.as_js_object().expect("Error creating JsObject");
    /// ```
    pub fn as_js_object(&self) -> Result<JsObject, JsValue> {
        let ret: JsValue = JsObject::new().into();
        JsReflect::set(&ret, &"id".into(), &self.id.into())?;
        for (key, value) in &self.fields {
            JsReflect::set(&ret, &key.into(), &value.into())?;
        }
        Ok(ret.into())
    }

    pub fn from_js_object(value: &JsValue) -> Result<Self, JsValue> {
        let mut ret = Self::default();

        ret.id = DataType::from(JsReflect::get(value, &"id".into())?).as_f64()? as u32;
        ret.model = String::new();

        for key_field in js_sys::Object::entries(&js_sys::Object::from(value.clone()))
            .values()
            .into_iter()
            {
                let key_field_inner = js_sys::Array::from(&key_field?)
                    .values()
                    .into_iter()
                    .collect::<Vec<_>>();

                let key = DataType::from(key_field_inner[0].as_ref()?.clone()).as_string()?;
                let value = DataType::from(key_field_inner[1].as_ref()?.clone());

                if key != "id" {
                    ret.fields.insert(key, value);
                }
            }

        Ok(ret)
    }
}

use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};

use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::set_on_x;

/// A generic way to differentiate between the differentiate between the types of panel and their
/// names
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PanelType {
    InventorySpreadsheet(&'static str),
    Archive(&'static str),
    Map(&'static str),
    Report(&'static str),
    NewUserManager(&'static str),
    UserPermissionsManager(&'static str),
    ChangePasswordManager(&'static str),
}

impl PanelType {
    /// Retrieves the ID stored in the PanelType
    ///
    /// Examples:
    /// ```
    /// use mappin_lib::panel::PanelType;
    ///
    /// let pt = PanelType::Map("A Map");
    /// let id = pt.get_id();
    /// assert_eq!("A Map", id);
    /// ```
    pub fn get_id(&self) -> &'static str {
        match self {
            Self::InventorySpreadsheet(string)
            | Self::Archive(string)
            | Self::Map(string)
            | Self::Report(string)
            | Self::NewUserManager(string)
            | Self::UserPermissionsManager(string)
            | Self::ChangePasswordManager(string) => string,
        }
    }
}

/// Toggles if the icon menu is viewable or not, and fills it
pub fn toggle_icon_menu(
    panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    old_panel: &PanelType,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the icon menu
    let icon_menu = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id(&format!("{}_icon_menu", old_panel.get_id()))
            .ok_or(format!("Panel not found: {}", old_panel.get_id()))?,
    ));

    if icon_menu.child_element_count() == 0 {
        {
            // Show the icon menu and fill it
            if let Ok(panel_manager_unlocked) = panel_manager_lock.read() {
                for new_panel in panel_manager_unlocked
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .active_panels
                    .iter()
                    .chain(
                        panel_manager_unlocked
                            .as_ref()
                            .as_ref()
                            .unwrap()
                            .inactive_panels
                            .iter(),
                    )
                {
                    // Only create icons that are not the current panel
                    if new_panel != old_panel {
                        // Create the icon
                        let new_panel_icon =
                            HtmlElement::from(JsValue::from(document.create_element("IMG")?));
                        new_panel_icon.set_id(&format!(
                            "{}_to_{}",
                            old_panel.get_id(),
                            new_panel.get_id()
                        ));
                        new_panel_icon.set_class_name("w3-button");
                        new_panel_icon.set_attribute(
                            "src",
                            &format!(
                                "/static/images/{}.png",
                                new_panel.get_id().to_ascii_lowercase().replace(' ', "_")
                            ),
                        )?;
                        new_panel_icon.set_attribute(
                            "alt",
                            &format!("Switch {} to {}", old_panel.get_id(), new_panel.get_id()),
                        )?;

                        // Set the on_click function
                        let old_panel_ref = Rc::new(RefCell::new(old_panel.clone()));
                        let new_panel_ref = Rc::new(RefCell::new(new_panel.clone()));
                        let panel_manager_lock_ref = panel_manager_lock.clone();
                        let f = move |_event| {
                            if let Ok(mut panel_manager_unlocked) = panel_manager_lock_ref.write() {
                                panel_manager_unlocked
                                    .as_mut()
                                    .as_mut()
                                    .unwrap()
                                    .switch_panels(
                                        &old_panel_ref.borrow().clone(),
                                        &new_panel_ref.borrow().clone(),
                                    )
                            } else {
                                Err(JsValue::from("Could not lock panel_manager"))
                            }
                        };
                        set_on_x!(new_panel_icon, set_onclick, f, false);
                        icon_menu.append_with_node_1(&new_panel_icon)?;
                    }
                }
            } else {
                Err(JsValue::from("Could not lock panel_manager"))?
            }
        }
    } else {
        // Hide the icon menu and clear it
        while icon_menu.child_element_count() > 0 {
            icon_menu.last_element_child().unwrap().remove();
        }
    }
    Ok(())
}

/// The manager that handles different content panels
#[derive(Debug, Clone)]
pub struct PanelManager {
    /// The list of panels currently in use
    pub active_panels: Vec<PanelType>,
    /// The list of panels that are created, but not in use
    pub inactive_panels: Vec<PanelType>,
}

impl PanelManager {
    /// Creates a new `PanelManager`
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::panel::PanelManager;
    ///
    /// let panel_manager = PanelManager::new().expect("Error creating panel manager");
    /// ```
    pub fn new() -> Result<Self, JsValue> {
        let ret = Self {
            active_panels: Vec::new(),
            inactive_panels: Vec::new(),
        };
        ret.make_panel_container()?;
        Ok(ret)
    }

    /// Creates the initial panel container.  No need to call directly, as it is called in `new`;
    ///
    /// Examples:
    /// ```rust,should_panic
    /// # use mappin_lib::panel::PanelManager;
    /// #
    /// # let panel_manager = PanelManager::new().expect("Error creating panel manager");
    /// panel_manager.make_panel_container().expect("Error making panel container");
    /// ```
    pub fn make_panel_container(&self) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));
        let body = document.body().ok_or("Cannot get body")?;

        // Create the panel container
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id("panels");
        ret.set_class_name("w3-row");

        body.append_with_node_1(&ret)?;
        Ok(())
    }

    /// Create the panel's icon menu
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::panel::{PanelManager, PanelType};
    ///
    /// let panel_manager = PanelManager::new().expect("Error creating panel manager");
    /// let panel_type = PanelType::InventorySpreadsheet("spreadsheet_id");
    /// let icon_menu_element = panel_manager.icon_menu(&panel_type).expect("Error creating icon menu");
    /// ```
    pub fn icon_menu(&self, panel_type: &PanelType) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the icon menu
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id(&format!("{}_icon_menu", panel_type.get_id()));
        ret.set_class_name("w3-dropdown-content");

        Ok(ret)
    }

    /// Create the panel title
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use std::sync::{Arc, RwLock};
    /// use mappin_lib::panel::{PanelManager, PanelType};
    ///
    /// let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(PanelManager::new().expect("Error creating panel manager")))));
    /// let panel_manager_unlocked = panel_manager_lock.read().expect("Could not lock panel manager");
    /// let panel_manager = panel_manager_unlocked.as_ref().as_ref().unwrap();
    ///
    /// let panel_type = PanelType::InventorySpreadsheet("spreadsheet_id");
    /// let title_element = panel_manager.title(&panel_type, panel_manager_lock.clone()).expect("Error creating title");
    /// ```
    pub fn title(
        &self,
        panel_type: &PanelType,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Make the panel title
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id(&format!("{}_panel_title", panel_type.get_id()));
        ret.set_class_name("panel-title w3-bottombar");

        let panel_icon_div = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        panel_icon_div.set_id(&format!("{}_panel_icon_div", panel_type.get_id()));
        panel_icon_div.set_class_name("w3-dropdown-hover");

        panel_icon_div.append_with_node_1(&self.icon_menu(panel_type)?.into())?;

        // Make the panel's representative icon
        let icon = HtmlElement::from(JsValue::from(document.create_element("IMG")?));
        icon.set_id(&format!("{}_panel_icon", panel_type.get_id()));
        icon.set_class_name("w3-button");
        icon.set_attribute(
            "src",
            &format!(
                "/static/images/{}.png",
                panel_type.get_id().to_ascii_lowercase().replace(' ', "_")
            ),
        )?;
        icon.set_attribute("alt", &format!("{} icon", panel_type.get_id()))?;
        let panel_ref = Rc::new(RefCell::new(panel_type.clone()));

        // TODO: Make this run whenever the user hovers/leaves hover, instead of on load
        // When the icon is clicked, the icon menu is toggled
        let f =
            move |_event| toggle_icon_menu(panel_manager_lock.clone(), &panel_ref.borrow().clone());
        set_on_x!(icon, set_onload, f, false);

        panel_icon_div.append_with_node_1(&icon)?;
        ret.append_with_node_1(&panel_icon_div)?;

        // Create the title name element
        let title = HtmlElement::from(JsValue::from(document.create_element("H2")?));
        title.set_inner_text(panel_type.get_id());
        title.set_class_name("w3-content w3-xlarge");
        ret.append_with_node_1(&title)?;

        Ok(ret)
    }

    /// Create the panel header
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::panel::{PanelManager, PanelType};
    ///
    /// let panel_manager = PanelManager::new().expect("Error creating panel manager");
    /// let panel_type = PanelType::InventorySpreadsheet("spreadsheet_id");
    /// let header_element = panel_manager.header(&panel_type).expect("Error creating header");
    /// ```
    pub fn header(&self, panel_type: &PanelType) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the header
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id(&format!("{}_header", panel_type.get_id()));
        ret.set_class_name("w3-container w3-row-padding");

        Ok(ret)
    }

    /// Create the panel content element
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use mappin_lib::panel::{PanelManager, PanelType};
    ///
    /// let panel_manager = PanelManager::new().expect("Error creating panel manager");
    /// let panel_type = PanelType::InventorySpreadsheet("spreadsheet_id");
    /// let content_element = panel_manager.content(&panel_type).expect("Error creating content");
    /// ```
    pub fn content(&self, panel_type: &PanelType) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the element
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id(&format!("{}_content", panel_type.get_id()));
        ret.set_class_name("w3-border w3-display-container");

        Ok(ret)
    }

    /// Creates a panel with the specified panel type
    ///
    /// Examples:
    /// ```rust,should_panic
    /// use std::sync::{Arc, RwLock};
    /// use mappin_lib::panel::{PanelManager, PanelType};
    ///
    /// let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(PanelManager::new().expect("Error creating panel manager")))));
    /// let mut panel_manager_unlocked = panel_manager_lock.write().expect("Could not lock panel manager");
    /// let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();
    ///
    /// let panel_type = PanelType::InventorySpreadsheet("spreadsheet_id");
    /// panel_manager.make_panel(&panel_type, panel_manager_lock.clone()).expect("Error creating panel");
    /// ```
    pub fn make_panel(
        &mut self,
        panel_type: &PanelType,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the panel container
        let panel_container = document
            .get_element_by_id("panels")
            .ok_or("Panel container not found")?;

        // Create the panel
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id(panel_type.get_id());
        ret.set_class_name("panel w3-panel w3-hide");

        // Create the panel contents
        ret.append_with_node_1(&self.title(&panel_type, panel_manager_lock)?.into())?;
        ret.append_with_node_1(&self.header(&panel_type)?.into())?;
        ret.append_with_node_1(&self.content(&panel_type)?.into())?;

        panel_container.append_with_node_1(&ret)?;

        // Make sure the panel is inactive
        self.inactive_panels.push(panel_type.clone());

        Ok(())
    }

    /// Calculate the size of the panels based on the number of panels that are active
    pub fn resize_panels(&mut self) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // The size of the window
        let screen_width = window
            .inner_width()?
            .as_f64()
            .ok_or("Could not convert to f64")?;
        let screen_height = window
            .inner_height()?
            .as_f64()
            .ok_or("Could not convert to f64")?;

        // The calculated size of each panel
        let _panel_width = (screen_width / self.active_panels.len() as f64) - (screen_width * 0.01);
        let _panel_height = screen_height - (screen_height * 0.10);

        // Show all the active panels and set each panel's height and width
        if self.active_panels.len() == 1 {
            let panel = HtmlElement::from(JsValue::from(
                document
                    .get_element_by_id(self.active_panels[0].get_id())
                    .ok_or(format!(
                        "Panel not found: {}",
                        self.active_panels[0].get_id()
                    ))?,
            ));
            let mut class_name = panel.class_name();
            class_name = class_name.replace("w3-hide", "").trim().into();
            panel.set_class_name(&class_name);
        } else if self.active_panels.len() == 2 {
            for panel in &self.active_panels {
                let left_panel = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id(panel.get_id())
                        .ok_or(format!("Panel not found: {}", panel.get_id()))?,
                ));
                let mut class_name = left_panel.class_name();
                class_name = class_name.replace("w3-hide", "").trim().into();
                class_name.push_str(" w3-half");
                left_panel.set_class_name(&class_name);
            }
        } else if self.active_panels.len() == 3 {
            for panel in &self.active_panels {
                let left_panel = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id(panel.get_id())
                        .ok_or(format!("Panel not found: {}", panel.get_id()))?,
                ));
                let mut class_name = left_panel.class_name();
                class_name = class_name.replace("w3-hide", "").trim().into();
                class_name.push_str(" w3-third");
                left_panel.set_class_name(&class_name);
            }
        }

        // Hide all the inactive panels
        for panel_type in &self.inactive_panels {
            let panel = HtmlElement::from(JsValue::from(
                document
                    .get_element_by_id(panel_type.get_id())
                    .ok_or(format!("Panel not found: {}", panel_type.get_id()))?,
            ));

            let mut class_name = panel.class_name();
            class_name = class_name
                .replace("w3-display-left", "")
                .replace("w3-display-right", "")
                .replace("w3-half", "")
                .trim()
                .into();
            class_name.push_str(" w3-hide");
            panel.set_class_name(&class_name);
        }

        Ok(())
    }

    /// Activate a panel
    ///
    /// Examples:
    /// ```
    /// use mappin_lib::panel::{PanelType, PanelManager};
    ///
    /// let ugm = PanelType::UserPermissionsManager("UGM");
    /// let is = PanelType::InventorySpreadsheet("IS");
    /// let m = PanelType::Map("M");
    ///
    /// let mut panel_manager = PanelManager{active_panels: vec![], inactive_panels: vec![ugm, is]};
    ///
    /// panel_manager.activate_panel(&ugm);
    /// panel_manager.activate_panel(&ugm);
    /// assert!(panel_manager.active_panels.len() == 1 && panel_manager.active_panels.contains(&ugm), "Panel manager active not only ugm");
    /// assert!(panel_manager.inactive_panels.len() == 1 && panel_manager.inactive_panels.contains(&is), "Panel manager inactive not only is");
    ///
    /// panel_manager.activate_panel(&is);
    /// panel_manager.activate_panel(&m);
    /// assert!(panel_manager.active_panels.len() == 2 && panel_manager.active_panels.contains(&ugm) && panel_manager.active_panels.contains(&is), "Panel manager active not only is and ugm");
    /// assert!(panel_manager.inactive_panels.is_empty(), "Inactive panels not empty");
    /// ```
    pub fn activate_panel(&mut self, panel_type: &PanelType) {
        if let Some(index) = self
            .inactive_panels
            .iter()
            .position(|inactive_panel| inactive_panel == panel_type)
        {
            self.active_panels.push(self.inactive_panels.remove(index));
        }
    }

    /// Deactivate a panel
    ///
    /// Examples:
    /// ```
    /// use mappin_lib::panel::{PanelType, PanelManager};
    ///
    /// let ugm = PanelType::UserPermissionsManager("UGM");
    /// let is = PanelType::InventorySpreadsheet("IS");
    /// let m = PanelType::Map("M");
    ///
    /// let mut panel_manager = PanelManager{active_panels: vec![ugm, is], inactive_panels: vec![]};
    ///
    /// panel_manager.deactivate_panel(&ugm);
    /// panel_manager.deactivate_panel(&m);
    /// assert!(panel_manager.active_panels.len() == 1 && panel_manager.active_panels.contains(&is), "Panel manager active not only is");
    /// assert!(panel_manager.inactive_panels.len() == 1 && panel_manager.inactive_panels.contains(&ugm), "Panel manager inactive not only ugm");
    ///
    /// panel_manager.deactivate_panel(&is);
    /// assert!(panel_manager.active_panels.is_empty(), "Active panels not empty");
    /// assert!(panel_manager.inactive_panels.len() == 2 && panel_manager.inactive_panels.contains(&ugm) && panel_manager.inactive_panels.contains(&is), "Panel manager inactive not only is and ugm");
    /// ```
    pub fn deactivate_panel(&mut self, panel_type: &PanelType) {
        if let Some(index) = self
            .active_panels
            .iter()
            .position(|active_panel| active_panel == panel_type)
        {
            self.inactive_panels.push(self.active_panels.remove(index));
        }
    }

    /// Switch two panels active/deactive state
    ///
    /// Examples:
    /// ```rust,should_panic
    /// # use std::sync::{Arc, RwLock};
    /// # use mappin_lib::panel::{PanelManager, PanelType};
    /// #
    /// # let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(PanelManager::new().expect("Error creating panel manager")))));
    /// # let mut panel_manager_unlocked = panel_manager_lock.write().expect("Could not lock panel manager");
    /// # let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();
    ///
    /// let panel_1 = PanelType::InventorySpreadsheet("spreadsheet_1");
    /// let panel_2 = PanelType::InventorySpreadsheet("spreadsheet_2");
    /// panel_manager.make_panel(&panel_1, panel_manager_lock.clone()).expect("Error creating panel");
    /// panel_manager.make_panel(&panel_2, panel_manager_lock.clone()).expect("Error creating panel");
    ///
    /// panel_manager.activate_panel(&panel_1);
    /// panel_manager.switch_panels(&panel_1, &panel_2).expect("Error switching panels");
    ///
    /// assert!(panel_manager.inactive_panels.len() == 1 && panel_manager.inactive_panels.contains(&panel_1), "Panel manager active not only panel_1");
    /// assert!(panel_manager.inactive_panels.len() == 1 && panel_manager.inactive_panels.contains(&panel_2), "Panel manager inactive not only panel_2");
    /// ```
    pub fn switch_panels(
        &mut self,
        old_panel: &PanelType,
        new_panel: &PanelType,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // toggle_icon_menu(panel_manager_lock, old_panel)?;

        // Get the old/new panel elements
        let old_panel_element = HtmlElement::from(JsValue::from(
            document
                .get_element_by_id(old_panel.get_id())
                .ok_or(format!("Panel not found: {}", old_panel.get_id()))?,
        ));
        let new_panel_element = HtmlElement::from(JsValue::from(
            document
                .get_element_by_id(new_panel.get_id())
                .ok_or(format!("Panel not found: {}", new_panel.get_id()))?,
        ));

        // Switch the panel orders
        old_panel_element.before_with_node_1(&new_panel_element)?;

        // Set the active/deactive states
        if !self.active_panels.contains(new_panel) {
            self.deactivate_panel(old_panel);
            self.activate_panel(new_panel);

            self.resize_panels()?;
        }

        Ok(())
    }
}

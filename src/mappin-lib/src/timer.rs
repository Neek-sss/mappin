use js_sys::Function as JsFunction;
use wasm_bindgen::prelude::*;

/// Retrieve the current time
pub fn get_timestamp() -> Result<f64, JsValue> {
    let j_query = js_sys::Reflect::get(&js_sys::global(), &"jQuery".into())?;
    let now_func: JsFunction = js_sys::Reflect::get(&j_query.into(), &"now".into())?.into();
    let context = JsValue::NULL;
    Ok(now_func
        .call0(&context)?
        .into_serde()
        .map_err(|_| "Cannot convert into u32")?)
}

/// A single frame of the timed loop, that then sets up the next frame
pub fn frame_loop(
    last_timestamp: f64,
    wait_time: f64,
    mut frame_func: Box<dyn FnMut() -> Result<(), JsValue>>,
    end_case: Option<Box<dyn FnMut() -> Result<bool, JsValue>>>,
) {
    let window = web_sys::window().expect("No JS window");
    window
        .request_animation_frame(&JsFunction::from(Closure::once_into_js(Box::new(
            move || {
                let timestamp = get_timestamp().expect("Error retrieving timestamp");
                if timestamp - last_timestamp > wait_time {
                    if let Err(error) = frame_func() {
                        web_sys::console::error_1(&error);
                    }
                    if let Some(mut end_case_func) = end_case {
                        match end_case_func() {
                            Ok(should_end) => {
                                if !should_end {
                                    frame_loop(
                                        timestamp,
                                        wait_time,
                                        frame_func,
                                        Some(Box::new(end_case_func)),
                                    );
                                }
                            }
                            Err(error) => web_sys::console::error_1(&error),
                        }
                    } else {
                        frame_loop(timestamp, wait_time, frame_func, end_case);
                    }
                } else {
                    frame_loop(last_timestamp, wait_time, frame_func, end_case);
                }
            },
        ))))
        .expect("Error constructing animation frame");
}

/// A generic object that allows a timed repetition of a function
pub struct Timer {
    /// The amount of time to wait before rerunning the function
    wait_time: f64,
    /// The function to run
    frame_func: Box<dyn FnMut() -> Result<(), JsValue>>,
    end_case: Option<Box<dyn FnMut() -> Result<bool, JsValue>>>,
}

impl Timer {
    /// Creates a new timer
    pub fn new(
        wait_time: f64,
        frame_func: Box<dyn FnMut() -> Result<(), JsValue>>,
        end_case: Option<Box<dyn FnMut() -> Result<bool, JsValue>>>,
    ) -> Self {
        Self {
            wait_time,
            frame_func,
            end_case,
        }
    }

    /// Starts the timer
    pub fn run(self) -> Result<(), JsValue> {
        let starting_timestamp = get_timestamp()?;
        frame_loop(
            starting_timestamp,
            self.wait_time,
            self.frame_func,
            self.end_case,
        );
        Ok(())
    }
}

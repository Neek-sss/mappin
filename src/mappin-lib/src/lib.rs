#[macro_use]
extern crate serde_derive;
extern crate wee_alloc;

use std::fmt::Debug;

pub mod ajax;
pub mod api_item;
pub mod api_type;
pub mod database_objects;
pub mod helpers;
pub mod panel;
pub mod server_api;
pub mod timer;

/// Prints a Rust struct to the JS console using the struct's Debug trait.
///
/// Examples:
/// ```rust,should_panic
/// use mappin_lib::console_log;
///
/// let x = 100;
/// console_log(&x);
/// ```
pub fn console_log<T>(value: &T)
where
    T: Debug,
{
    web_sys::console::log_1(&format!("{:?}", value).into())
}

/// Sets a JS object's on_x function (like on_click).
///
/// Examples:
/// ```rust,should_panic
/// use std::{rc::Rc, cell::RefCell};
/// use mappin_lib::set_on_x;
/// use web_sys::HtmlElement;
/// use wasm_bindgen::{JsValue, closure::Closure, JsCast};
///
/// let window = web_sys::window().expect("Cannot get window");
/// let document = web_sys::HtmlDocument::from(JsValue::from(
///     window.document().expect("Cannot get document"),
/// ));
///
/// let element = HtmlElement::from(JsValue::from(document.create_element("BUTTON").expect("Cannot create div")));
/// let counter = Rc::new(RefCell::new(0));
/// let counter_ref = counter.clone();
/// let f = move |_event| {
///     *counter.borrow_mut() += 1;
///     web_sys::console::log_1(&"Hello, World!".into());
///     Ok(())
/// };
/// set_on_x!(element, set_onclick, f.clone(), *counter_ref.borrow() < 10);
/// ```
#[macro_export]
macro_rules! set_on_x {
    ($dom:expr, $set_on_x:ident, $func:expr, $cleanup:expr) => {
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();

        *g.borrow_mut() = Some(Closure::wrap(Box::new(move |event: JsValue| {
            if $cleanup {
                let _ = f.borrow_mut().take();
            }
            if let Err(error) = $func(event) {
                web_sys::console::error_1(&error);
            }
        }) as Box<dyn FnMut(JsValue)>));

        $dom.$set_on_x(Some(g.borrow().as_ref().unwrap().as_ref().unchecked_ref()));
    };
}

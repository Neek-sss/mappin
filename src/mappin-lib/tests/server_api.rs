use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;
use web_sys::HtmlElement;

use mappin_lib::server_api::ServerAPI;

wasm_bindgen_test_configure!(run_in_browser);

fn add_notification() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));
    let body = document.body().expect("Cannot get body");

    let notification = HtmlElement::from(JsValue::from(
        document.create_element("DIV").expect("Error creating div"),
    ));
    notification.set_id("error_notification");
    body.append_with_node_1(&notification.into())
        .expect("Error adding notification");

    let notification = HtmlElement::from(JsValue::from(
        document.create_element("DIV").expect("Error creating div"),
    ));
    notification.set_id("unsaved_changes_notification");
    body.append_with_node_1(&notification.into())
        .expect("Error adding notification");

    let notification = HtmlElement::from(JsValue::from(
        document.create_element("DIV").expect("Error creating div"),
    ));
    notification.set_id("saving_notification");
    body.append_with_node_1(&notification.into())
        .expect("Error adding notification");
}

#[wasm_bindgen_test]
fn wasm_mark_changed_test() {
    add_notification();

    let mut server_api = ServerAPI::new(&[]).expect("Error setting up server api");
    server_api.mark_changed().expect("Error marking change");
}

#[wasm_bindgen_test]
fn wasm_data_updates_test() {
    add_notification();

    let mut server_api = ServerAPI::new(&[]).expect("Error setting up server api");

    server_api.data_updates().expect("Error updating data");

    server_api.mark_changed().expect("Error marking change");
    server_api.data_updates().expect("Error updating data");
}

use std::sync::{Arc, RwLock};

use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;

use mappin_lib::panel::{PanelManager, PanelType};

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn wasm_new_panel_manager() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));

    let _panel_manager = PanelManager::new().expect("Error creating panel manager");

    let _panels = document
        .get_element_by_id("panels")
        .expect("Cannot find panels");
}

#[wasm_bindgen_test]
fn wasm_panel_functions() {
    let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(
        PanelManager::new().expect("Error creating panel manager"),
    ))));
    let panel_manager_unlocked = panel_manager_lock
        .read()
        .expect("Cannot lock panel manager");
    let panel_manager = panel_manager_unlocked.as_ref().as_ref().unwrap();

    let icon_menu = panel_manager
        .icon_menu(&PanelType::InventorySpreadsheet("sdsh"))
        .expect("Error creating icon menu");
    assert_eq!("sdsh_icon_menu", icon_menu.id());

    let header = panel_manager
        .header(&PanelType::NewUserManager("num"))
        .expect("Error creating header");
    assert_eq!("num_header", header.id());

    let content = panel_manager
        .content(&PanelType::Report("rep"))
        .expect("Error creating content");
    assert_eq!("rep_content", content.id());

    let title = panel_manager
        .title(&PanelType::Archive("arc"), panel_manager_lock.clone())
        .expect("Error creating title");
    assert_eq!("arc_panel_title", title.id());
}

#[wasm_bindgen_test]
fn wasm_make_panel() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));

    let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(
        PanelManager::new().expect("Error creating panel manager"),
    ))));
    let mut panel_manager_unlocked = panel_manager_lock
        .write()
        .expect("Cannot lock panel manager");
    let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

    panel_manager
        .make_panel(
            &PanelType::UserPermissionsManager("UPM"),
            panel_manager_lock.clone(),
        )
        .expect("Could not create panel");
    document
        .get_element_by_id("UPM")
        .expect("Cannot find panel");
    document
        .get_element_by_id("UPM_header")
        .expect("Cannot find header");
    document
        .get_element_by_id("UPM_content")
        .expect("Cannot find content");
}

#[wasm_bindgen_test]
fn wasm_resize_panels() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));

    let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(
        PanelManager::new().expect("Error creating panel manager"),
    ))));
    let mut panel_manager_unlocked = panel_manager_lock
        .write()
        .expect("Cannot lock panel manager");
    let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

    let ugm = PanelType::UserPermissionsManager("UPM");
    let is = PanelType::InventorySpreadsheet("IS");
    let m = PanelType::Map("M");

    panel_manager
        .make_panel(&ugm, panel_manager_lock.clone())
        .expect("Could not create panel");
    panel_manager
        .make_panel(&is, panel_manager_lock.clone())
        .expect("Could not create panel");
    panel_manager
        .make_panel(&m, panel_manager_lock.clone())
        .expect("Could not create panel");

    panel_manager.activate_panel(&is);
    panel_manager
        .resize_panels()
        .expect("Error resizing panels");

    assert!(
        document
            .get_element_by_id(ugm.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel not hidden"
    );
    assert!(
        document
            .get_element_by_id(m.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel not hidden"
    );
    assert!(
        !document
            .get_element_by_id(is.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel hidden"
    );

    panel_manager.activate_panel(&m);
    panel_manager
        .resize_panels()
        .expect("Error resizing panels");

    assert!(
        document
            .get_element_by_id(ugm.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel not hidden"
    );
    assert!(
        !document
            .get_element_by_id(m.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel hidden"
    );
    assert!(
        !document
            .get_element_by_id(is.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel hidden"
    );
}

#[wasm_bindgen_test]
fn wasm_switch_panels() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));

    let panel_manager_lock = Arc::new(RwLock::new(Box::new(Some(
        PanelManager::new().expect("Error creating panel manager"),
    ))));
    let mut panel_manager_unlocked = panel_manager_lock
        .write()
        .expect("Cannot lock panel manager");
    let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

    let ugm = PanelType::UserPermissionsManager("UPM");
    let is = PanelType::InventorySpreadsheet("IS");

    panel_manager
        .make_panel(&ugm, panel_manager_lock.clone())
        .expect("Could not create panel");
    panel_manager
        .make_panel(&is, panel_manager_lock.clone())
        .expect("Could not create panel");

    panel_manager.activate_panel(&is);

    panel_manager
        .switch_panels(&is, &ugm)
        .expect("Error switching panels");
    assert!(
        panel_manager.active_panels.len() == 1 && panel_manager.active_panels.contains(&ugm),
        "Panel manager active not only ugm"
    );
    assert!(
        panel_manager.inactive_panels.len() == 1 && panel_manager.inactive_panels.contains(&is),
        "Panel manager inactive not only is"
    );

    assert!(
        !document
            .get_element_by_id(ugm.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel hidden"
    );
    assert!(
        document
            .get_element_by_id(is.get_id())
            .expect("Could not get panel")
            .class_name()
            .contains("hide"),
        "Panel not hidden"
    );
}

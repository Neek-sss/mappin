use maplit::hashmap;
use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;

use mappin_lib::database_objects::{DatabaseObject, DataType};

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn default_database_object_as_js_object_test() {
    let database_object = DatabaseObject::default();
    let _js_object = database_object
        .as_js_object()
        .expect("Error creating object");
}

#[wasm_bindgen_test]
fn database_object_as_js_object_test() {
    let database_object = DatabaseObject {
        id: 25,
        model: "A Model".into(),
        fields: hashmap! {
            "field_1".into() => DataType::S("xyz".into()),
            "field_2".into() => DataType::F(123.456),
        },
    };
    let js_object = database_object
        .as_js_object()
        .expect("Error creating object");
    assert_eq!(
        Ok(JsValue::from(25)),
        js_sys::Reflect::get(&js_object, &"id".into())
    );
    assert_eq!(
        Ok(JsValue::from("xyz")),
        js_sys::Reflect::get(&js_object, &"field_1".into())
    );
    assert_eq!(
        Ok(JsValue::from(123.456)),
        js_sys::Reflect::get(&js_object, &"field_2".into())
    );
}

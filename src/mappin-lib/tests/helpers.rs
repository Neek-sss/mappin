use wasm_bindgen::JsValue;
use wasm_bindgen_test::*;

use mappin_lib::helpers::get_cookie;

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn wasm_get_cookie_test() {
    let window = web_sys::window().expect("Cannot get window");
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().expect("Cannot get document"),
    ));

    document.set_cookie("x=y;").expect("Cannot set cookie");
    document
        .set_cookie("rew=100_0_A;")
        .expect("Cannot set cookie");

    let x = get_cookie("x").expect("Error retrieving cookie");
    let rew = get_cookie("rew").expect("Error retrieving cookie");

    assert_eq!(x, Some(String::from("y")));
    assert_eq!(rew, Some(String::from("100_0_A")));
}

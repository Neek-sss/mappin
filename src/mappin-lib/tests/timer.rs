use wasm_bindgen_test::*;

use mappin_lib::timer::get_timestamp;

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn wasm_get_timestamp() {
    js_sys::eval(include_str!(
        "../../mappin/static/SlickGrid-2.4.3/lib/jquery-3.1.0.js"
    ))
        .expect("Cannot evaluate jQuery");

    let timestamps = (0..1000)
        .map(|_| get_timestamp().expect("Error getting timestamp"))
        .collect::<Vec<_>>();

    assert!(
        timestamps.iter().last().unwrap() - timestamps[0] > 0.0,
        "Invalid timestamp"
    );
}

use js_sys::Reflect as JsReflect;
use maplit::hashmap;
use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    panel::PanelManager,
    server_api::ServerAPI,
    set_on_x,
};
use mappin_lib_js::{
    slick_grid::Grid,
    spreadsheet::{Spreadsheet, SpreadsheetBuilder, SpreadsheetColumn, SpreadsheetOptions},
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::building::Building;

/// A dummy struct for building an Archive type spreadsheet
pub struct ArchiveBuilder {
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
}

impl ArchiveBuilder {
    pub fn new(
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    ) -> Self {
        Self {
            building_lock,
            archive_lock,
        }
    }
}

impl ArchiveBuilder {
    /// Builds a restore button that will put items from the archive into the current building
    pub fn get_restore_button(
        archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
    ) -> Result<HtmlElement, JsValue> {
        // Get the objects that create HtmlElements
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Make the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("restore_item");
        button.set_class_name("w3-card w3-button");
        button.set_inner_text("Restore");

        // Set up the click function
        let f = move |_event: JsValue| {
            if let Err(error) = Self::restore(archive_lock, server_api_lock, building_lock) {
                web_sys::console::error_1(&error);
            }
            Ok(())
        };
        set_on_x!(button, set_onclick, f.clone(), false);

        Ok(button)
    }

    /// Restores an item from the archive into the current building, updating the `Archive` and
    /// `InventorySpreadsheet`
    pub fn restore(
        spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
    ) -> Result<(), JsValue> {
        // Lock the archive
        let selected_rows = if let Ok(mut archive_unlocked) = spreadsheet_lock.write() {
            let archive = archive_unlocked.as_mut().as_mut().unwrap();

            let data_connector = archive.grid.getData();
            let get_item_func =
                js_sys::Function::from(js_sys::Reflect::get(&data_connector, &"getItem".into())?);

            let indices = archive
                .grid
                .getSelectedRows()
                .values()
                .into_iter()
                .filter_map(|value| value.ok())
                .collect::<Vec<_>>();

            indices
                .into_iter()
                .filter_map(|index| get_item_func.call1(&data_connector, &index).ok())
                .collect::<Vec<_>>()
        } else {
            Err(JsValue::from("Could not lock archive"))?
        };

        let current_building = building_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock building"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .current_building;

        // Get the DatabaseObjects to be restored (in JS form)
        for item_js in selected_rows {
            let f = || {
                if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                    let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

                    // Get the actual DatabaseObject
                    if let Some(item_id) =
                    DataType::from(js_sys::Reflect::get(&item_js, &"id".into())?)
                        .as_f64()
                        .ok()
                        .and_then(|f| Some(f as u32))
                    {
                        let item = server_api.api_items[&APIType::Item].data[&item_id].clone();

                        // Get the currently set building, since updates depend on that
                        if let Some(building_id) = item.fields.get("building") {
                            // Update the archive state of the object
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "archive",
                                    DataType::B(false),
                                    Some(hashmap! {"building".into() => building_id.clone()}),
                                )?;

                            // Update the building of the object
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "building",
                                    DataType::U(current_building as u64),
                                    Some(hashmap! {"building".into() => building_id.clone()}),
                                )?;

                            // Notify everyone of the changes
                            server_api.mark_changed()?;
                        }
                    }

                    Ok(())
                } else {
                    Err(JsValue::from("Could not lock server_api"))
                }
            };
            if let Err(error) = f() {
                web_sys::console::error_1(&error);
            }
        }

        if let Ok(mut archive_unlocked) = spreadsheet_lock.write() {
            let archive = archive_unlocked.as_mut().as_mut().unwrap();

            // Clear out the selection (since these objects are no longer in the archive)
            archive.grid.setSelectedRows(js_sys::Array::new());

            Ok(())
        } else {
            Err(JsValue::from("Could not lock archive"))?
        }
    }

    //    /// Creates a button for deleting the items in the archive.
    //    pub fn get_delete_button() -> Result<HtmlElement, JsValue> {
    //        let window = web_sys::window().ok_or("Cannot get window")?;
    //        let document = web_sys::HtmlDocument::from(JsValue::from(
    //            window.document().ok_or("Cannot get document")?,
    //        ));
    //
    //        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
    //        button.set_id("delete_item");
    //        button.set_class_name("w3-card w3-button");
    //        button.set_inner_text("Delete");
    //
    //        // TODO: Delete button function
    //        Ok(button)
    //    }
}

impl SpreadsheetBuilder for ArchiveBuilder {
    /// Get the header DOM elements
    fn get_header_elements(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        _panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<Vec<HtmlElement>, JsValue> {
        Ok(vec![
            Self::get_restore_button(
                self.archive_lock.clone(),
                server_api_lock.clone(),
                self.building_lock.clone(),
            )?,
            // TODO: Delete button
            //            Self::get_delete_button()?,
        ])
    }

    fn get_options(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<SpreadsheetOptions, JsValue> {
        Ok(SpreadsheetOptions {
            selection_column: true,
            ..Default::default()
        })
    }

    fn get_columns(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<Vec<SpreadsheetColumn>, JsValue> {
        let editors = {
            let slick = JsReflect::get(&js_sys::global().into(), &"Slick".into())?;
            JsReflect::get(&slick, &"Editors".into())?
        };
        Ok(vec![
            SpreadsheetColumn {
                id: "name".into(),
                name: "Name".into(),
                field: "name".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "manufacturer".into(),
                name: "Manufacturer".into(),
                field: "manufacturer".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "make".into(),
                name: "Make".into(),
                field: "make".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "model".into(),
                name: "Model".into(),
                field: "model".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "year".into(),
                name: "Year".into(),
                field: "year".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "serial_number".into(),
                name: "Serial Number".into(),
                field: "serial_number".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "price".into(),
                name: "Price".into(),
                field: "price".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Float".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "obtained".into(),
                name: "Obtained".into(),
                field: "obtained".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Date".into())?.into()),
                formatter: None,
            },
        ])
    }

    fn get_filter_func(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        _panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>> {
        // Create the filtering function
        let server_api_lock_ref = server_api_lock.clone();
        Box::new(move |database_object: &DatabaseObject| {
            // True if either the DatabaseObject or building are archived, otherwise false
            if let Ok(server_api_unlocked) = server_api_lock_ref.read() {
                if let Some(true) = database_object
                    .fields
                    .get("archive")
                    .and_then(|d| d.as_bool().ok())
                {
                    Ok(true)
                } else if let Some(building_id) = database_object
                    .fields
                    .get("building")
                    .and_then(|d| d.as_u64().ok())
                {
                    Ok(
                        server_api_unlocked.as_ref().as_ref().unwrap().api_items
                            [&APIType::Building]
                            .data[&(building_id as u32)]
                            .fields
                            .get("archive")
                            .and_then(|d| d.as_bool().ok())
                            .unwrap_or(false),
                    )
                } else {
                    Ok(false)
                }
            } else {
                Err(JsValue::from("Could not lock server_api"))
            }
        })
    }

    fn get_api_type(&self) -> APIType {
        APIType::Item
    }

    fn on_cell_change(
        &self,
    ) -> Result<
        Box<dyn FnMut(u32, &str, DataType, &DatabaseObject, &mut ServerAPI) -> Result<(), JsValue>>,
        JsValue,
    > {
        Ok(Box::new(move |_, _, _, _, _| Ok(())))
    }

    fn on_add_row(
        &self,
    ) -> Result<Box<dyn FnMut(&str, DataType, &mut ServerAPI) -> Result<u32, JsValue>>, JsValue>
    {
        Ok(Box::new(move |_, _, _| Ok(0)))
    }
}

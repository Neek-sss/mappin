#[macro_use]
extern crate lazy_static;
extern crate wee_alloc;

use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, Mutex, RwLock},
};

use wasm_bindgen::{JsCast, prelude::*};
use web_sys::{self, HtmlElement};

use mappin_lib::{
    ajax,
    api_type::APIType,
    database_objects::DataType,
    panel::{PanelManager, PanelType},
    server_api::ServerAPI,
    set_on_x,
    timer::Timer,
};
use mappin_lib_js::spreadsheet::Spreadsheet;

use crate::{
    archive::ArchiveBuilder, building::Building, image_manager::ImageManager,
    inventory_map::InventoryMap, inventory_spreadsheet::InventorySpreadsheetBuilder,
    report::Report, server_api::set_up_on_funcs,
};

mod archive;
mod building;
mod image_manager;
mod inventory_map;
mod inventory_spreadsheet;
mod server_api;
mod report;

// Prevent map drag bug
lazy_static! {
    pub static ref MAP_LOCK: Arc<Mutex<bool>> = Arc::new(Mutex::new(false));
}

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

/// Called when the wasm module is instantiated.  This is equivalent to a binary program's main
/// function.
#[wasm_bindgen(start)]
pub fn wasm_main() -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));
    let body = document.body().ok_or("Cannot get body")?;

    // Set up the header
    let header = interface_header()?;

    // Create the locks
    let server_api_lock = Arc::new(RwLock::new(Box::new(None)));
    let image_manager_lock = Arc::new(RwLock::new(Box::new(None)));
    let building_lock = Arc::new(RwLock::new(Box::new(None)));
    let panel_manager_lock = Arc::new(RwLock::new(Box::new(None)));
    let inventory_spreadsheet_lock = Arc::new(RwLock::new(Box::new(None)));
    let archive_lock = Arc::new(RwLock::new(Box::new(None)));
    let inventory_map_lock = Arc::new(RwLock::new(Box::new(None)));
    let report_lock = Arc::new(RwLock::new(Box::new(None)));

    // Load up the base data stuff
    *server_api_lock
        .write()
        .map_err(|_| "Cannot lock server_api")? = Box::new(Some({
        let mut s_api = ServerAPI::new(&[
            APIType::BuildingImage,
            APIType::Building,
            APIType::ItemImage,
            APIType::Item,
        ])?;
        set_up_on_funcs(
            &mut s_api,
            building_lock.clone(),
            inventory_spreadsheet_lock.clone(),
            archive_lock.clone(),
            inventory_map_lock.clone(),
        )?;
        s_api
    }));

    *image_manager_lock
        .write()
        .map_err(|_| "Cannot lock image_manager")? = Box::new(Some(ImageManager::new(
        server_api_lock.clone(),
        building_lock.clone(),
        inventory_spreadsheet_lock.clone(),
        inventory_map_lock.clone(),
        image_manager_lock.clone(),
    )?));

    *building_lock.write().map_err(|_| "Cannot lock building")? =
        Box::new(Some(Building::new(server_api_lock.clone())?));

    header.append_with_node_1(
        &building_lock
            .read()
            .map_err(|_| "Cannot get read lock on building")?
            .as_ref()
            .as_ref()
            .unwrap()
            .header(
                building_lock.clone(),
                server_api_lock.clone(),
                inventory_spreadsheet_lock.clone(),
                inventory_map_lock.clone(),
            )?
            .into(),
    )?;

    body.append_with_node_1(&header.into())?;

    // Create the panel system
    *panel_manager_lock
        .write()
        .map_err(|_| "Cannot lock panel_manager")? = Box::new(Some(PanelManager::new()?));

    let spreadsheet_panel_type = PanelType::InventorySpreadsheet("Spreadsheet");
    let map_panel_type = PanelType::Map("Map");
    let archive_panel_type = PanelType::Archive("Archive");
    let report_panel_type = PanelType::Report("Report");

    // Activate a couple of panels and set them up
    if let Ok(mut panel_manager_unlocked) = panel_manager_lock.write() {
        let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

        panel_manager.make_panel(&archive_panel_type, panel_manager_lock.clone())?;
        panel_manager.make_panel(&report_panel_type, panel_manager_lock.clone())?;

        panel_manager.activate_panel(&archive_panel_type);
        panel_manager.activate_panel(&report_panel_type);

        panel_manager.resize_panels()?;
    }

    *archive_lock.write().map_err(|_| "Cannot lock server_api")? =
        Box::new(Some(Spreadsheet::new(
            archive_panel_type.clone(),
            server_api_lock.clone(),
            panel_manager_lock.clone(),
            ArchiveBuilder::new(building_lock.clone(), archive_lock.clone()),
        )?));

    *report_lock
        .write()
        .map_err(|_| "Cannot lock panel_manager")? = Box::new(Some(Report::new(report_panel_type.get_id())?));

    // Activate the other two panels and set them up
    if let Ok(mut panel_manager_unlocked) = panel_manager_lock.write() {
        let panel_manager = panel_manager_unlocked.as_mut().as_mut().unwrap();

        panel_manager.make_panel(&spreadsheet_panel_type.clone(), panel_manager_lock.clone())?;
        panel_manager.make_panel(&map_panel_type, panel_manager_lock.clone())?;

        panel_manager.deactivate_panel(&archive_panel_type.clone());
        panel_manager.deactivate_panel(&report_panel_type);

        panel_manager.activate_panel(&spreadsheet_panel_type);
        panel_manager.activate_panel(&map_panel_type);

        panel_manager.resize_panels()?;
    }

    *inventory_spreadsheet_lock
        .write()
        .map_err(|_| "Cannot lock server_api")? = Box::new(Some(Spreadsheet::new(
        spreadsheet_panel_type.clone(),
        server_api_lock.clone(),
        panel_manager_lock.clone(),
        InventorySpreadsheetBuilder::new(
            building_lock.clone(),
            inventory_spreadsheet_lock.clone(),
            inventory_map_lock.clone(),
            image_manager_lock.clone(),
        ),
    )?));

    *inventory_map_lock
        .write()
        .map_err(|_| "Cannot lock server_api")? = Box::new(Some(InventoryMap::new(
        map_panel_type.get_id(),
        building_lock.clone(),
        server_api_lock.clone(),
        image_manager_lock.clone(),
        inventory_spreadsheet_lock.clone(),
        inventory_map_lock.clone(),
    )?));

    // Continuously update data to/from the server
    let server_api_lock_ref = server_api_lock.clone();
    Timer::new(
        10_000.0,
        Box::new(move || {
            // Fix the map drag bug
            if !crate::MAP_LOCK
                .try_lock()
                .ok()
                .and_then(|g| Some(*g))
                .unwrap_or(false)
            {
                let mut server_api_unlocked = server_api_lock_ref
                    .write()
                    .map_err(|_| JsValue::from("Cannot get write lock on server_api"))?;
                let server_api = server_api_unlocked
                    .as_mut()
                    .as_mut()
                    .unwrap();
                if let Err(e) = server_api.data_updates() {
                    server_api.unsaved_changes = true;
                    let window = web_sys::window().ok_or("Error opening window")?;
                    let document = window.document().ok_or("Error opening document")?;
                    let error_notification = web_sys::HtmlElement::from(JsValue::from(
                        document
                            .get_element_by_id("error_notification")
                            .ok_or("Error getting error notification")?,
                    ));
                    error_notification.set_hidden(false);
                }
            }

            Ok(())
        }),
        None,
    )
        .run()?;

    // Periodically update the inventory_spreadsheet
    let inventory_spreadsheet_lock_ref = inventory_spreadsheet_lock.clone();
    let panel_manager_lock_ref = panel_manager_lock.clone();
    let server_api_lock_ref = server_api_lock.clone();
    Timer::new(
        1_000.0,
        Box::new(move || {
            inventory_spreadsheet_lock_ref
                .write()
                .map_err(|_| JsValue::from("Cannot get write lock on inventory_spreadsheet"))?
                .as_mut()
                .as_mut()
                .unwrap()
                .update(server_api_lock_ref.clone(), panel_manager_lock_ref.clone())?;

            Ok(())
        }),
        None,
    )
        .run()?;

    // Update the archive continually
    let archive_ref = archive_lock.clone();
    let panel_manager_lock_ref = panel_manager_lock.clone();
    let server_api_lock_ref = server_api_lock.clone();
    Timer::new(
        1_000.0,
        Box::new(move || {
            archive_ref
                .write()
                .map_err(|_| JsValue::from("Cannot get write lock on archive"))?
                .as_mut()
                .as_mut()
                .unwrap()
                .update(server_api_lock_ref.clone(), panel_manager_lock_ref.clone())?;
            Ok(())
        }),
        None,
    )
        .run()?;

    // Update the map continually
    let inventory_map_lock_ref = inventory_map_lock.clone();
    let building_lock_ref = building_lock.clone();
    let server_api_lock_ref = server_api_lock.clone();
    Timer::new(
        1_000.0,
        Box::new(move || {
            inventory_map_lock_ref
                .write()
                .map_err(|_| JsValue::from("Cannot get write lock on inventory_map"))?
                .as_mut()
                .as_mut()
                .unwrap()
                .update(building_lock_ref.clone(), server_api_lock_ref.clone())?;
            Ok(())
        }),
        None,
    )
        .run()?;

    // Update the inventory_spreadsheet when the map changes view
    let last_extent = Rc::new(RefCell::new(if let Ok(inventory_map_unlocked) =
    inventory_map_lock.read()
    {
        inventory_map_unlocked
            .as_ref()
            .as_ref()
            .unwrap()
            .get_extent()
    } else {
        Err(JsValue::from("Could not lock inventory_map"))
    }?));
    let inventory_map_lock_ref = inventory_map_lock.clone();
    let inventory_spreadsheet_lock_ref = inventory_spreadsheet_lock.clone();
    Timer::new(
        1_000.0,
        Box::new(move || {
            let current_extent = inventory_map_lock_ref
                .read()
                .map_err(|_| JsValue::from("Cannot get read lock on inventory_map"))?
                .as_ref()
                .as_ref()
                .unwrap()
                .get_extent()?;
            if current_extent != *last_extent.borrow() {
                *last_extent.borrow_mut() = current_extent;
                inventory_spreadsheet_lock_ref
                    .write()
                    .map_err(|_| JsValue::from("Cannot get write lock on inventory_spreadsheet"))?
                    .as_mut()
                    .as_mut()
                    .unwrap()
                    .needs_update = true;
            }
            Ok(())
        }),
        None,
    )
        .run()?;

    Ok(())
}

/// Creates the mappin interface header DOM
pub fn interface_header() -> Result<HtmlElement, JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    let interface_header = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    interface_header.set_id("interface_header");
    interface_header
        .set_class_name("interface-header w3-row w3-container w3-display-container w3-bottombar");

    let user_dropdown_div = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_div.set_class_name("w3-dropdown-hover");
    user_dropdown_div.set_id("user_hover");
    interface_header.append_with_node_1(&user_dropdown_div)?;

    let user_dropdown_button = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_button.set_inner_text("Menu");
    user_dropdown_button.set_class_name("w3-button");
    user_dropdown_button.set_id("user_hover");
    user_dropdown_div.append_with_node_1(&user_dropdown_button)?;

    let user_dropdown_content = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
    user_dropdown_content.set_class_name("w3-dropdown-content");
    user_dropdown_content.set_id("user_hover_button_dropdown");
    user_dropdown_div.append_with_node_1(&user_dropdown_content)?;

    {
        let element = HtmlElement::from(JsValue::from(document.create_element("Button")?));
        element.set_inner_text("Settings");
        element.set_class_name("w3-button");
        element.set_id("settings_button");

        let f = move |_event| {
            let location = js_sys::Reflect::get(&js_sys::global(), &"location".into())?;
            let replace =
                js_sys::Function::from(js_sys::Reflect::get(&location, &"replace".into())?);
            replace.call1(&location, &"settings".into())?;
            Ok(())
        };
        set_on_x!(element, set_onclick, f, false);

        user_dropdown_content.append_with_node_1(&element)?;

        let element = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        element.set_inner_text("Logout");
        element.set_class_name("w3-button");
        element.set_id("logout_button");

        let f = move |_event| {
            if ajax::wasm_ajax("/mappin/api_logout", "").is_ok() {}
            let location = js_sys::Reflect::get(&js_sys::global(), &"location".into())?;
            let replace =
                js_sys::Function::from(js_sys::Reflect::get(&location, &"replace".into())?);
            replace.call1(&location, &"/".into())?;
            Ok(())
        };
        set_on_x!(element, set_onclick, f, false);

        user_dropdown_content.append_with_node_1(&element)?;
    }

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.set_inner_text("Error connecting to server!");
    element.set_id("error_notification");
    element.set_class_name("w3-display-topright w3-animation-fading w3-text-red");
    element.set_hidden(true);
    interface_header.append_with_node_1(&element)?;

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.set_inner_text("Unsaved Changes");
    element.set_id("unsaved_changes_notification");
    element.set_class_name("w3-display-bottomright");
    element.set_hidden(true);
    interface_header.append_with_node_1(&element)?;

    let element = HtmlElement::from(JsValue::from(document.create_element("P")?));
    element.set_inner_text("Saving...");
    element.set_id("saving_notification");
    element.set_class_name("w3-display-bottomright w3-animation-fading");
    element.set_hidden(true);
    interface_header.append_with_node_1(&element)?;

    Ok(interface_header)
}

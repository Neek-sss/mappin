use maplit::hashmap;
use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    server_api::ServerAPI,
};
use mappin_lib_js::{open_layers, spreadsheet::Spreadsheet};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::{building::Building, image_manager::ImageManager};

/// Exactly that same as the `set_on_x` macro, except that it operates on OpenLayer's objects,
/// so it only takes an args `JsValue` and connects to the feature's `on` function
#[macro_export]
macro_rules! openlayers_on {
    ($feature:expr, $name:expr, $func:expr, $cleanup:expr) => {
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();

        *g.borrow_mut() = Some(wasm_bindgen::closure::Closure::wrap(Box::new(
            move |args: JsValue| {
                if $cleanup {
                    let _ = f.borrow_mut().take();
                }
                if let Err(error) = $func(args) {
                    web_sys::console::error_1(&error);
                }
            },
        )
            as Box<dyn FnMut(JsValue)>));
        $feature.on($name, g.borrow().as_ref().unwrap().as_ref());
    };
}

/// Sets a JsObject's value to the closure specified
#[macro_export]
macro_rules! reflect_closure {
    ($object:expr, $name:expr, $func:expr, $cleanup:expr) => {
        let f = Rc::new(RefCell::new(None));
        let g = f.clone();

        *g.borrow_mut() = Some(wasm_bindgen::closure::Closure::wrap(Box::new(
            move |args: JsValue| {
                if $cleanup {
                    let _ = f.borrow_mut().take();
                }
                if let Err(error) = $func(args) {
                    web_sys::console::error_1(&error);
                }
            },
        )
            as Box<dyn FnMut(JsValue)>));
        js_sys::Reflect::set(
            &$object.into(),
            &$name.into(),
            g.borrow().as_ref().unwrap().as_ref(),
        )
        .expect("Error reflecting closure");
    };
}

/// The 2D map showing spacial location of `InventoryItems`
pub struct InventoryMap {
    /// Indicates when the items in the map should be updated
    pub item_update: bool,
    /// Indicates when the layers of the map should be recreated
    pub layer_update: bool,
    /// The OpenLayer's `Map` object
    pub open_layers_map: open_layers::Map,
}

impl InventoryMap {
    /// Creates the `InventoryMap`
    pub fn new(
        panel_id: &str,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<Self, JsValue> {
        // It's stupid, but we have to do this or else connecting to OpenLayers is very broken
        open_layers::fix_namespace()?;

        // The size of the map
        let image_extent = {
            let ret = js_sys::Array::new();
            ret.push(&0.into());
            ret.push(&0.into());
            ret.push(&1920.into());
            ret.push(&1080.into());
            ret
        };

        // The ?projection? of the map
        let projection = {
            let projection_options = JsValue::from(js_sys::Object::new());
            js_sys::Reflect::set(&projection_options, &"units".into(), &"pixels".into())?;
            js_sys::Reflect::set(&projection_options, &"extent".into(), &image_extent.clone())?;
            open_layers::Projection::new(projection_options.into())
        };

        // The client's view of the map
        let view = {
            let view_options = JsValue::from(js_sys::Object::new());
            js_sys::Reflect::set(&view_options, &"projection".into(), &projection.into())?;
            js_sys::Reflect::set(
                &view_options,
                &"center".into(),
                &open_layers::extent::getCenter(image_extent).into(),
            )?;
            js_sys::Reflect::set(&view_options, &"zoom".into(), &(-8).into())?;
            js_sys::Reflect::set(&view_options, &"maxZoom".into(), &8.into())?;
            open_layers::View::new(view_options)
        };

        // The entire map
        let map = {
            let map_options = JsValue::from(js_sys::Object::new());
            js_sys::Reflect::set(&map_options, &"layers".into(), &js_sys::Array::new().into())?;
            js_sys::Reflect::set(
                &map_options,
                &"target".into(),
                &format!("{}_content", panel_id).into(),
            )?;
            js_sys::Reflect::set(&map_options, &"view".into(), &view.into())?;
            open_layers::Map::new(map_options)
        };

        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the header
        let panel_content = HtmlElement::from(JsValue::from(
            document
                .get_element_by_id(&format!("{}_content", panel_id))
                .ok_or("Content not found")?,
        ));

        mappin_lib::set_on_x!(
            panel_content,
            set_onpointerdown,
            |_| {
                *crate::MAP_LOCK
                    .lock()
                    .map_err(|_| JsValue::from("Could not lock MAP_LOCK"))? = true;
                Ok(())
            },
            false
        );
        mappin_lib::set_on_x!(
            panel_content,
            set_onpointerup,
            |_| {
                *crate::MAP_LOCK
                    .lock()
                    .map_err(|_| JsValue::from("Could not lock MAP_LOCK"))? = false;
                Ok(())
            },
            false
        );

        let mut ret = Self {
            item_update: false,
            layer_update: false,
            open_layers_map: map,
        };

        // Create all the map panel's stuff
        ret.create_map_layers(building_lock.clone(), server_api_lock.clone())?;
        ret.create_map_background(building_lock.clone(), server_api_lock.clone())?;
        ret.create_map_items(building_lock.clone(), server_api_lock.clone())?;
        ret.create_header(
            panel_id,
            image_manager_lock.clone(),
            server_api_lock.clone(),
            building_lock.clone(),
            inventory_spreadsheet_lock.clone(),
            inventory_map_lock.clone(),
        )?;

        Ok(ret)
    }

    /// Creates the header DOM for the `InventoryMap`
    pub fn create_header(
        &mut self,
        panel_id: &str,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the header
        let panel_header = document
            .get_element_by_id(&format!("{}_header", panel_id))
            .ok_or("Header not found")?;

        let building_perm = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::Building]
            .permissions
            .clone();

        if building_perm.change {
            // Create the ImageManager button
            let image_manager_button = self.get_image_manager_button(
                image_manager_lock,
                server_api_lock,
                building_lock,
                inventory_spreadsheet_lock,
                inventory_map_lock,
            )?;
            panel_header.append_with_node_1(&image_manager_button)?;
        }

        // Create item identifier
        let item_identifier = HtmlElement::from(JsValue::from(document.create_element("P")?));
        item_identifier.set_id("item_identifier");
        item_identifier.set_class_name("w3-content w3-right");
        item_identifier.set_inner_text("No item found.");
        panel_header.append_with_node_1(&item_identifier)?;

        Ok(())
    }

    /// Retrieves the `ImageManager` button
    pub fn get_image_manager_button(
        &mut self,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        if let Ok(image_manager_unlocked) = image_manager_lock.read() {
            Ok(image_manager_unlocked
                .as_ref()
                .as_ref()
                .unwrap()
                .create_image_manager_button(
                    "Map Image Manager",
                    "inventory_map_image_manager",
                    APIType::BuildingImage,
                    image_manager_lock.clone(),
                    server_api_lock,
                    building_lock,
                    inventory_spreadsheet_lock,
                    inventory_map_lock,
                )?)
        } else {
            Err(JsValue::from("Could not lock image_manager"))
        }
    }

    /// Peridocally run to check whether or not the map needs to be updated
    pub fn update(
        &mut self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Fix the map drag bug
        if !crate::MAP_LOCK
            .try_lock()
            .ok()
            .and_then(|g| Some(*g))
            .unwrap_or(false)
        {
            if self.layer_update {
                self.layer_update = false;
                self.clear_map_background()?;
                self.create_map_background(building_lock.clone(), server_api_lock.clone())?;
                self.clear_map_items()?;
                self.create_map_items(building_lock.clone(), server_api_lock.clone())?;
            } else if self.item_update {
                self.item_update = false;
                self.clear_map_items()?;
                self.create_map_items(building_lock.clone(), server_api_lock.clone())?;
            }
        }
        Ok(())
    }

    /// Creates all map layers
    pub fn create_map_layers(
        &mut self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        let image_data = {
            if let (Ok(server_api_unlocked), Ok(building_unlocked)) =
            (server_api_lock.read(), building_lock.read())
            {
                let server_api = server_api_unlocked.as_ref().as_ref().unwrap();
                let building = building_unlocked.as_ref().as_ref().unwrap();

                // Get the current map image, or the default one if it is not set
                if let Some(image_id) = server_api.api_items[&APIType::Building].data
                    [&building.current_building]
                    .fields
                    .get("image")
                    .and_then(|d| d.as_u64().ok())
                {
                    server_api.api_items[&APIType::BuildingImage].data[&(image_id as u32)].clone()
                } else {
                    DatabaseObject::new(
                        "",
                        0,
                        &Some(hashmap! {
                            "path".into() => DataType::S("/static/images/default_map.png".into()),
                            "width".into() => DataType::U(1920),
                            "Height".into() => DataType::U(1080),
                        }),
                    )
                }
            } else {
                Err(JsValue::from("Could not lock server_api and building"))?
            }
        };

        // The image size
        let image_extent = {
            let ret = js_sys::Array::new();
            ret.push(&0.into());
            ret.push(&0.into());
            ret.push(&1920.into());
            ret.push(&1080.into());
            ret
        };

        // Still not sure what a projection is, but we need it...
        let projection = {
            let projection_options = JsValue::from(js_sys::Object::new());
            js_sys::Reflect::set(&projection_options, &"units".into(), &"pixels".into())?;
            js_sys::Reflect::set(&projection_options, &"extent".into(), &image_extent.clone())?;
            open_layers::Projection::new(projection_options)
        };

        if let Some(path) = image_data
            .fields
            .get("path")
            .and_then(|d| d.as_string().ok())
        {
            // The image to use
            let image_source = {
                let source_options = JsValue::from(js_sys::Object::new());
                js_sys::Reflect::set(&source_options, &"url".into(), &path.into())?;
                js_sys::Reflect::set(&source_options, &"projection".into(), &projection.into())?;
                js_sys::Reflect::set(&source_options, &"imageExtent".into(), &image_extent.into())?;
                open_layers::ImageStaticSource::new(source_options)
            };

            // The layer that uses the image
            let image_layer = {
                let layer_options = JsValue::from(js_sys::Object::new());
                js_sys::Reflect::set(&layer_options, &"source".into(), &image_source.into())?;
                open_layers::ImageLayer::new(layer_options)
            };

            // Set up the layer in the map as the background
            self.open_layers_map.addLayer(image_layer.into());

            // Set up a collection of points
            let vector_source = {
                let vector_source_options = JsValue::from(js_sys::Object::new());
                js_sys::Reflect::set(
                    &vector_source_options,
                    &"features".into(),
                    &js_sys::Array::new().into(),
                )?;
                open_layers::VectorSource::new(vector_source_options)
            };

            // Set up a layer that can take those points and translate them into tiny images on the
            // map.
            let vector_layer = {
                let vector_layer_options = JsValue::from(js_sys::Object::new());
                js_sys::Reflect::set(
                    &vector_layer_options,
                    &"source".into(),
                    &vector_source.clone(),
                )?;

                // TODO: Ewww... I should hide this
                {
                    let f = Rc::new(RefCell::new(None));
                    let g = f.clone();

                    *f.borrow_mut() = Some(Closure::wrap(Box::new(
                        move |feature: open_layers::Feature| {
                            let _ = g.clone();
                            Ok(feature.get("style"))
                        },
                    )
                        as Box<dyn FnMut(open_layers::Feature) -> Result<JsValue, JsValue>>));

                    js_sys::Reflect::set(
                        &vector_layer_options,
                        &"style".into(),
                        f.borrow().as_ref().unwrap().as_ref(),
                    )?;
                }

                open_layers::VectorLayer::new(vector_layer_options)
            };

            // Put the foreground in the map
            self.open_layers_map.addLayer(vector_layer.clone());

            let item_perm = server_api_lock
                .read()
                .map_err(|_| JsValue::from("Could not lock server_api"))?
                .as_ref()
                .as_ref()
                .unwrap()
                .api_items[&APIType::Item]
                .permissions
                .clone();

            // Allow the points in the vector layer's source to be edited (they can be dragged)
            if item_perm.change {
                let modify_interaction = {
                    let modify_interaction_options = JsValue::from(js_sys::Object::new());
                    js_sys::Reflect::set(
                        &modify_interaction_options,
                        &"source".into(),
                        &vector_source.into(),
                    )?;
                    open_layers::Modify::new(modify_interaction_options)
                };
                self.open_layers_map
                    .addInteraction(modify_interaction.into());
            }

            // Hovering data
            let open_layers_map_ref: open_layers::Map = self.open_layers_map.clone().into();
            let test_func = move |args| {
                let server_api_lock_ref = server_api_lock.clone();

                let f = Rc::new(RefCell::new(None));
                let g = f.clone();

                *f.borrow_mut() = Some(Closure::wrap(Box::new(
                    move |feature: open_layers::Feature| {
                        let _ = g.clone();

                        // Get the item ID
                        let get_id = js_sys::Function::from(js_sys::Reflect::get(
                            &feature,
                            &"getId".into(),
                        )?);
                        let id_js_value = get_id.call0(&feature)?;

                        // Make sure that we have an item and not the map layer
                        if id_js_value.as_f64().is_some() {
                            let window = web_sys::window().ok_or("Cannot get window")?;
                            let document = web_sys::HtmlDocument::from(JsValue::from(
                                window.document().ok_or("Cannot get document")?,
                            ));

                            // Find the item identifier
                            let item_identifier = HtmlElement::from(JsValue::from(
                                document
                                    .get_element_by_id("item_identifier")
                                    .ok_or("Header not found")?,
                            ));

                            // Get the item's name
                            let id = id_js_value.as_f64().ok_or("Id was not a number")? as u32;
                            let name = server_api_lock_ref
                                .read()
                                .map_err(|_| JsValue::from("Error locking server_api"))?
                                .as_ref()
                                .as_ref()
                                .unwrap()
                                .api_items[&APIType::Item]
                                .data[&id]
                                .fields["name"]
                                .as_string()?;

                            // Set item identifier to show the item's name
                            item_identifier.set_inner_text(&format!("Item: {}", name));
                        }

                        Ok(())
                    },
                )
                    as Box<dyn FnMut(open_layers::Feature) -> Result<(), JsValue>>));

                // Some borrow shenanigans
                let x = f.borrow();
                let js_func = x.as_ref().unwrap().as_ref();

                // Get all of the features that are at the mouse's location and run the hover
                // function on them.
                let pixel = js_sys::Reflect::get(&args, &"pixel".into())?;
                open_layers_map_ref.forEachFeatureAtPixel(pixel, js_func.into());

                Ok(())
            };

            // Check for hovering content whenever the mouse is moved
            openlayers_on!(&self.open_layers_map, "pointermove", test_func, false);
        }

        Ok(())
    }

    /// Creates the background for the map
    pub fn create_map_background(
        &mut self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Find the image layer
        self.open_layers_map
            .getLayers()
            .getArray()
            .for_each(&mut |layer, _index, _all_layers| {
                let f = || {
                    if js_sys::Reflect::get(&layer, &"type".into())?
                        .as_string()
                        .ok_or("Type not a string")?
                        == "IMAGE"
                    {
                        let image_data = {
                            if let (Ok(server_api_unlocked), Ok(building_unlocked)) =
                            (server_api_lock.read(), building_lock.read())
                            {
                                let server_api = server_api_unlocked.as_ref().as_ref().unwrap();
                                let building = building_unlocked.as_ref().as_ref().unwrap();

                                // Get the current map image, or the default one if it is not set
                                if let Some(image_id) = server_api.api_items[&APIType::Building].data
                                    [&building.current_building]
                                    .fields
                                    .get("image").and_then(|d| d.as_u64().ok())
                                {
                                    server_api.api_items[&APIType::BuildingImage].data[&(image_id as u32)].clone()
                                } else {
                                    DatabaseObject::new(
                                        "",
                                        0,
                                        &Some(hashmap! {
                            "path".into() => DataType::S("/static/images/default_map.png".into()),
                            "width".into() => DataType::U(1920),
                            "Height".into() => DataType::U(1080),
                        }),
                                    )
                                }
                            } else {
                                Err(JsValue::from("Could not lock server_api and building"))?
                            }
                        };

                        // The image size
                        let image_extent = {
                            let ret = js_sys::Array::new();
                            ret.push(&0.into());
                            ret.push(&0.into());
                            ret.push(&1920.into());
                            ret.push(&1080.into());
                            ret
                        };

                        // Still not sure what a projection is, but we need it...
                        let projection = {
                            let projection_options = JsValue::from(js_sys::Object::new());
                            js_sys::Reflect::set(&projection_options, &"units".into(), &"pixels".into())?;
                            js_sys::Reflect::set(&projection_options, &"extent".into(), &image_extent.clone())?;
                            open_layers::Projection::new(projection_options)
                        };

                        if let Some(path) = image_data.fields.get("path").and_then(|d| d.as_string().ok()) {
                            // The image to use
                            let image_source = {
                                let source_options = JsValue::from(js_sys::Object::new());
                                js_sys::Reflect::set(&source_options, &"url".into(), &path.into())?;
                                js_sys::Reflect::set(&source_options, &"projection".into(), &projection.into())?;
                                js_sys::Reflect::set(&source_options, &"imageExtent".into(), &image_extent.into())?;
                                open_layers::ImageStaticSource::new(source_options)
                            };

                            // Set the new image
                            let set_source = js_sys::Function::from(js_sys::Reflect::get(
                                &layer,
                                &"setSource".into(),
                            )?);
                            set_source.call1(&layer, &image_source)?;
                        }
                    }
                    Ok(())
                };
                if let Err(error) = f() {
                    web_sys::console::error_1(&error);
                }
            });
        Ok(())
    }

    /// Removes the map background
    pub fn clear_map_background(&mut self) -> Result<(), JsValue> {
        // Find the image layer
        self.open_layers_map
            .getLayers()
            .getArray()
            .for_each(&mut |layer, _index, _all_layers| {
                let f = || {
                    if js_sys::Reflect::get(&layer, &"type".into())?
                        .as_string()
                        .ok_or("Type not a string")?
                        == "IMAGE"
                    {
                        // The image size
                        let image_extent = {
                            let ret = js_sys::Array::new();
                            ret.push(&0.into());
                            ret.push(&0.into());
                            ret.push(&1920.into());
                            ret.push(&1080.into());
                            ret
                        };

                        // Still not sure what a projection is, but we need it...
                        let projection = {
                            let projection_options = JsValue::from(js_sys::Object::new());
                            js_sys::Reflect::set(
                                &projection_options,
                                &"units".into(),
                                &"pixels".into(),
                            )?;
                            js_sys::Reflect::set(
                                &projection_options,
                                &"extent".into(),
                                &image_extent.clone(),
                            )?;
                            open_layers::Projection::new(projection_options)
                        };

                        let image_source = {
                            // The image to use
                            let source_options = JsValue::from(js_sys::Object::new());
                            js_sys::Reflect::set(
                                &source_options,
                                &"url".into(),
                                &"/static/images/default_map.png".into(),
                            )?;
                            js_sys::Reflect::set(
                                &source_options,
                                &"projection".into(),
                                &projection.into(),
                            )?;
                            js_sys::Reflect::set(
                                &source_options,
                                &"imageExtent".into(),
                                &image_extent.into(),
                            )?;
                            open_layers::ImageStaticSource::new(source_options)
                        };

                        // Set the new image
                        let set_source = js_sys::Function::from(js_sys::Reflect::get(
                            &layer,
                            &"setSource".into(),
                        )?);
                        set_source.call1(&layer, &image_source)?;
                    }
                    Ok(())
                };
                if let Err(error) = f() {
                    web_sys::console::error_1(&error);
                }
            });
        Ok(())
    }

    /// Put all the items on the map
    pub fn create_map_items(
        &mut self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Find the vector layer
        self.open_layers_map
            .getLayers()
            .getArray()
            .for_each(&mut |layer, _index, _all_layers| {
                let mut f = || {
                    if js_sys::Reflect::get(&layer, &"type".into())?
                        .as_string()
                        .ok_or("Type not a string")?
                        == "VECTOR"
                    {
                        // Get the current building
                        let current_building = if let Ok(building_unlocked) = building_lock.read() {
                            building_unlocked
                                .as_ref()
                                .as_ref()
                                .unwrap()
                                .current_building
                        } else {
                            Err(JsValue::from("Could not lock building"))?
                        };

                        // Get the item data associated with the map
                        let item_data_vec = if let Ok(server_api_unlocked) = server_api_lock.read()
                        {
                            server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::Item]
                                .data
                                .values()
                                .map(|item| {
                                    (
                                        item.id,
                                        item.fields.get("on_map").cloned(),
                                        item.fields.get("archive").cloned(),
                                        item.fields.get("building").cloned(),
                                    )
                                })
                                .collect::<Vec<_>>()
                        } else {
                            Err(JsValue::from("Could not lock server_api"))?
                        };

                        // Add the item if it is on the map and not archived
                        for item_data in item_data_vec {
                            if let (Some(true), Some(false), Some(building_id)) = (
                                item_data.1.and_then(|d| d.as_bool().ok()),
                                item_data.2.and_then(|d| d.as_bool().ok()),
                                item_data.3.and_then(|d| d.as_u64().ok()),
                            ) {
                                if current_building == building_id as u32 {
                                    self.add_map_item(item_data.0, server_api_lock.clone())?;
                                }
                            }
                        }
                    }
                    Ok(())
                };
                if let Err(error) = f() {
                    web_sys::console::error_1(&error);
                }
            });
        Ok(())
    }

    /// Remove all items from the map
    pub fn clear_map_items(&mut self) -> Result<(), JsValue> {
        self.open_layers_map
            .getLayers()
            .getArray()
            .for_each(&mut |layer, _index, _all_layers| {
                let f = || {
                    if js_sys::Reflect::get(&layer, &"type".into())?
                        .as_string()
                        .ok_or("Type not a string")?
                        == "VECTOR"
                    {
                        let get_source = js_sys::Function::from(js_sys::Reflect::get(
                            &layer,
                            &"getSource".into(),
                        )?);
                        let source = get_source.call0(&layer)?;

                        let clear =
                            js_sys::Function::from(js_sys::Reflect::get(&source, &"clear".into())?);
                        clear.call0(&source)?;
                    }
                    Ok(())
                };
                if let Err(error) = f() {
                    web_sys::console::error_1(&error);
                }
            });
        Ok(())
    }

    /// Add a single item to the map
    pub fn add_map_item(
        &mut self,
        item_id: u32,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Get the x and y coordinates
        if let Ok(server_api_unlocked) = server_api_lock.read() {
            let item = &server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::Item]
                .data[&item_id];
            if let (Some(x_coord), Some(y_coord)) = (
                item.fields
                    .get("x_coord")
                    .and_then(|d| d.as_string().ok())
                    .and_then(|s| s.parse::<f64>().ok()),
                item.fields
                    .get("y_coord")
                    .and_then(|d| d.as_string().ok())
                    .and_then(|s| s.parse::<f64>().ok()),
            ) {
                // Get the item image or the default item image
                let image_id = item.fields.get("image").and_then(|d| {
                    if d.as_u64().is_ok() {
                        d.as_u64().ok()
                    } else if d.as_string().is_ok() {
                        d.as_string().ok().and_then(|d_s| d_s.parse().ok())
                    } else {
                        None
                    }
                });
                let image = if let Some(image_id) = image_id {
                    server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::ItemImage]
                        .data[&(image_id as u32)]
                        .clone()
                } else {
                    DatabaseObject::new(
                        "",
                        0,
                        &Some(hashmap! {
                            "path".into() => DataType::S("/static/images/default_item.png".into()),
                            "width".into() => DataType::U(256),
                            "height".into() => DataType::U(256),
                        }),
                    )
                };

                // Create the item icon
                let icon = {
                    let icon_options = JsValue::from(js_sys::Object::new());

                    let anchor = js_sys::Array::new();
                    anchor.push(&0.5.into());
                    anchor.push(&0.5.into());
                    js_sys::Reflect::set(&icon_options, &"anchor".into(), &anchor.into())?;

                    if let Some(path) = image.fields.get("path").and_then(|d| d.as_string().ok()) {
                        js_sys::Reflect::set(&icon_options, &"src".into(), &path.into())?;
                    }

                    if let Some(height) = image.fields.get("height").and_then(|d| d.as_u64().ok()) {
                        js_sys::Reflect::set(
                            &icon_options,
                            &"scale".into(),
                            &(1.0 / (height as f64 / 16.0)).into(),
                        )?;
                    }

                    open_layers::Icon::new(icon_options)
                };

                // Create a style that displays the item's icon
                let style = {
                    let style_options = JsValue::from(js_sys::Object::new());

                    js_sys::Reflect::set(&style_options, &"image".into(), &icon.into())?;

                    open_layers::Style::new(style_options)
                };

                // Create a feature to be placed on the map
                let feature = {
                    let feature_options = JsValue::from(js_sys::Object::new());

                    let point = {
                        let point_coords = js_sys::Array::new();
                        point_coords.push(&x_coord.into());
                        point_coords.push(&y_coord.into());
                        open_layers::Point::new(point_coords)
                    };
                    js_sys::Reflect::set(&feature_options, &"geometry".into(), &point.into())?;

                    js_sys::Reflect::set(&feature_options, &"style".into(), &style.into())?;

                    open_layers::Feature::new(feature_options)
                };

                // Set the feature's ID to be the item's ID
                feature.setId(item_id);

                let item_perm = server_api_lock
                    .read()
                    .map_err(|_| JsValue::from("Could not lock server_api"))?
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .api_items[&APIType::Item]
                    .permissions
                    .clone();

                // Make moving of the map items to change their saved coordinates
                if item_perm.change {
                    let server_api_lock_ref = server_api_lock.clone();
                    let f = move |args| {
                        let target = js_sys::Reflect::get(&args, &"target".into())?;

                        // Get the item ID
                        let get_id =
                            js_sys::Function::from(js_sys::Reflect::get(&target, &"getId".into())?);
                        let id = get_id
                            .call0(&target)?
                            .as_f64()
                            .ok_or("Id was not a number")? as u32;

                        // Get the x and y coordinates
                        let get_geometry = js_sys::Function::from(js_sys::Reflect::get(
                            &target,
                            &"getGeometry".into(),
                        )?);
                        let geometry = get_geometry.call0(&target)?;
                        let get_coordinates = js_sys::Function::from(js_sys::Reflect::get(
                            &geometry,
                            &"getCoordinates".into(),
                        )?);
                        let coordinates = js_sys::Array::from(&get_coordinates.call0(&geometry)?)
                            .values()
                            .into_iter()
                            .map(|value| {
                                Ok(value?
                                    .as_f64()
                                    .ok_or(JsValue::from("Coordinate not a number"))?)
                            })
                            .collect::<Vec<Result<f64, JsValue>>>();
                        let x_coord = coordinates[0].clone()?;
                        let y_coord = coordinates[1].clone()?;

                        // Update the saved data
                        if let Ok(mut server_api_unlocked) = server_api_lock_ref.write() {
                            let server_api = server_api_unlocked.as_mut().as_mut().unwrap();
                            if let Some(building) = server_api.api_items[&APIType::Item].data[&id]
                                .fields
                                .get("building")
                                .cloned()
                            {
                                server_api
                                    .api_items
                                    .get_mut(&APIType::Item)
                                    .unwrap()
                                    .queue_update(
                                        id,
                                        "x_coord",
                                        DataType::S(format!("{}", x_coord)),
                                        Some(hashmap!("building".into() => building.clone())),
                                    )?;
                                server_api
                                    .api_items
                                    .get_mut(&APIType::Item)
                                    .unwrap()
                                    .queue_update(
                                        id,
                                        "y_coord",
                                        DataType::S(format!("{}", y_coord)),
                                        Some(hashmap!("building".into() => building.clone())),
                                    )?;
                                server_api.mark_changed()?;
                                Ok(())
                            } else {
                                Err(JsValue::from("Building not set"))
                            }
                        } else {
                            Err(JsValue::from("Could not lock server_api"))
                        }
                    };
                    openlayers_on!(feature, "change", f, false);
                }

                // Add the feature to the map
                self.open_layers_map.getLayers().getArray().for_each(
                    &mut |layer, _index, _all_layers| {
                        let f = || {
                            if js_sys::Reflect::get(&layer, &"type".into())?
                                .as_string()
                                .ok_or("Type not a string")?
                                == "VECTOR"
                            {
                                let get_source = js_sys::Function::from(js_sys::Reflect::get(
                                    &layer,
                                    &"getSource".into(),
                                )?);
                                let source = get_source.call0(&layer)?;

                                let add_feature = js_sys::Function::from(js_sys::Reflect::get(
                                    &source,
                                    &"addFeature".into(),
                                )?);

                                add_feature.call1(&source, &feature.clone())?;
                            }
                            Ok(())
                        };
                        if let Err(error) = f() {
                            web_sys::console::error_1(&error);
                        }
                    },
                );
            }
            Ok(())
        } else {
            Err(JsValue::from("Could not lock server_api"))
        }
    }

    /// Get the current map view that the client sees in x/y coordinates
    /// [x_min, y_min, x_max, y_max]
    pub fn get_extent(&self) -> Result<[f64; 4], JsValue> {
        // Get the coodinates
        let extent_array = self.open_layers_map.getView().calculateExtent();

        // Convert to a Vec
        let mut extent_vec = Vec::new();
        extent_array.for_each(&mut |extent_coordinate, _index, _array| {
            extent_vec.push(
                DataType::from(extent_coordinate)
                    .as_f64()
                    .map_err(|e| JsValue::from(e)),
            )
        });

        // Convert to a Slice
        Ok([
            extent_vec[0].clone()?,
            extent_vec[1].clone()?,
            extent_vec[2].clone()?,
            extent_vec[3].clone()?,
        ])
    }
}

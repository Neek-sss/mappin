use js_sys::Reflect as JsReflect;
use mappin_lib::{
    api_item::QueueType, api_type::APIType, database_objects::DataType, server_api::ServerAPI,
    set_on_x,
};
use mappin_lib_js::spreadsheet::Spreadsheet;
use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::inventory_map::InventoryMap;

pub struct Building {
    pub current_building: u32,
}

/// Activates the building name editor
fn building_name_edit_on(
    _event: JsValue,
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the DOM elements
    let building_name_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name")
            .ok_or("Building name not found")?,
    ));
    let building_name_form_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name_form")
            .ok_or("Building name form not found")?,
    ));
    let building_name_input_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name_input")
            .ok_or("Building name input not found")?,
    ));

    // Get the current building name
    let building_name = {
        if let (Ok(server_api_unlocked), Ok(building_unlocked)) =
        (server_api_lock.read(), building_lock.read())
        {
            let server_api = server_api_unlocked.as_ref().as_ref().unwrap();
            let building = building_unlocked.as_ref().as_ref().unwrap();
            server_api.api_items[&APIType::Building].data[&building.current_building].fields["name"]
                .as_string()?
        } else {
            Err(JsValue::from("Could not get locks"))?
        }
    };

    // Set the editor's starting data to be the building name
    JsReflect::set(
        &building_name_input_element.clone().into(),
        &"value".into(),
        &building_name.into(),
    )?;

    // Replace the building name title with the building name editor
    building_name_element.set_hidden(true);
    building_name_form_element.set_hidden(false);
    building_name_input_element.focus()?;

    Ok(())
}

/// Closes the building_name editor and saves the changes
fn building_name_edit_off(
    event: JsValue,
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
) -> Result<(), JsValue> {
    // Prevent the page from reloading if enter was pressed
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the elements
    let building_name_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name")
            .ok_or("Building name not found")?,
    ));
    let building_name_form_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name_form")
            .ok_or("Building name form not found")?,
    ));
    let building_name_input_element = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("building_name_input")
            .ok_or("Building name input not found")?,
    ));

    // Process the new name
    let new_name = JsReflect::get(&building_name_input_element.clone().into(), &"value".into())?
        .as_string()
        .unwrap();
    {
        let current_building = building_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock building"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .current_building;

        if let Ok(mut server_api_unlocked) = server_api_lock.write() {
            let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

            // Only change things if the old and new name are different
            let old_building_name =
                server_api.api_items[&APIType::Building].data[&current_building].fields["name"]
                    .as_string()?;

            if new_name != old_building_name && new_name != "" {
                // Change the building name in the title and in the server_api data
                building_name_element.set_inner_text(&new_name);
                server_api
                    .api_items
                    .get_mut(&APIType::Building)
                    .unwrap()
                    .queue_update(current_building, "name", DataType::S(new_name), None)?;
                server_api.mark_changed()?;
            }
        }
    }

    // Clear out the editor
    JsReflect::set(
        &building_name_input_element.clone().into(),
        &"value".into(),
        &"".into(),
    )?;

    // Hide the editor and show the building name
    building_name_element.set_hidden(false);
    building_name_form_element.set_hidden(true);

    Ok(())
}

/// Creates a new, empty building with default values
fn new_building(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    // Figure out a unique name
    let new_name = {
        let mut unique = false;
        let mut number = 0;
        let mut new_name = String::new();
        while !unique {
            unique = true;

            // Generate a name
            number += 1;
            new_name = format!("New Building {}", number);

            // Check it is unique
            if let Ok(server_api_unlocked) = server_api_lock.read() {
                unique = !server_api_unlocked.as_ref().as_ref().unwrap().api_items
                    [&APIType::Building]
                    .data
                    .values()
                    .any(|building| {
                        building
                            .fields
                            .get("name")
                            .and_then(|d| d.as_string().ok())
                            .map(|name| new_name == name)
                            .unwrap_or(false)
                    });
            } else {
                Err(JsValue::from("Could not lock server_api"))?
            }
        }
        new_name
    };

    // Generate a random temporary Id
    let new_id = (js_sys::Math::random() * 10_000.0) as u32;

    // Update the server (Set some default data too)
    if let Ok(mut server_api_unlocked) = server_api_lock.write() {
        let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

        server_api
            .api_items
            .get_mut(&APIType::Building)
            .unwrap()
            .queue_add(new_id, &new_name, None)?;
        server_api
            .api_items
            .get_mut(&APIType::Building)
            .unwrap()
            .queue_update(new_id, "archive", DataType::B(false), None)?;
        server_api
            .api_items
            .get_mut(&APIType::Building)
            .unwrap()
            .queue_update(new_id, "image", DataType::NULL, None)?;
        server_api.mark_changed()?;
    } else {
        Err(JsValue::from("Could not lock server_api"))?
    }

    // Change to the newly created building (since the user will probably want to play with his/her
    // new building.
    building_lock
        .write()
        .map_err(|_| JsValue::from("Could not lock building"))?
        .as_mut()
        .as_mut()
        .unwrap()
        .change_building(
            new_id,
            server_api_lock.clone(),
            inventory_spreadsheet_lock,
            inventory_map_lock,
        )?;

    // Force the building to at least be added to the server before we leave
    server_api_lock
        .write()
        .map_err(|_| JsValue::from("Could not lock server_api"))?
        .as_mut()
        .as_mut()
        .unwrap()
        .api_items
        .get_mut(&APIType::Building)
        .unwrap()
        .send_queue(QueueType::Add)?;

    Ok(())
}

/// Toggles the menu to change building.
fn change_building_toggle(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the listing of buildings
    let change_building_listing = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id(&"change_building_listing")
            .ok_or("Change building listing not found")?,
    ));

    if change_building_listing.child_element_count() == 0 {
        // If the listing is hidden, unhide and fill it
        if let (Ok(server_api_unlocked), Ok(building_unlocked)) =
        (server_api_lock.read(), building_lock.read())
        {
            for building in server_api_unlocked.as_ref().as_ref().unwrap().api_items
                [&APIType::Building]
                .data
                .values()
                {
                    // Only list buildings that are not the current building
                    if building.id
                        != building_unlocked
                        .as_ref()
                        .as_ref()
                        .unwrap()
                        .current_building
                    {
                        // Make sure that the buiding is not archived either
                        if let (Some(false), Some(name)) = (
                            building
                                .fields
                                .get("archive")
                                .and_then(|d| d.as_bool().ok()),
                            building.fields.get("name").and_then(|d| d.as_string().ok()),
                        ) {
                            // Create the building switcher DOM element
                            let building_tag =
                                HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
                            building_tag.set_class_name("w3-button");
                            building_tag.set_id(&format!("change_to_building_{}", building.id));
                            building_tag.set_inner_text(&name);

                            // Create the building switcher function
                            let building_lock_ref = building_lock.clone();
                            let server_api_lock_ref = server_api_lock.clone();
                            let inventory_spreadsheet_lock_ref = inventory_spreadsheet_lock.clone();
                            let inventory_map_lock_ref = inventory_map_lock.clone();
                            let building_id_ref = building.id;
                            let f = move |_event: JsValue| {
                                let window = web_sys::window().ok_or("Cannot get window")?;
                                let document = web_sys::HtmlDocument::from(JsValue::from(
                                    window.document().ok_or("Cannot get document")?,
                                ));

                                // Hide the change_building listing (because it's annoying)
                                HtmlElement::from(JsValue::from(
                                    document
                                        .get_element_by_id(&"change_building")
                                        .ok_or("Change building button not found")?,
                                ))
                                    .click();

                                // Switch the buildings
                                if let Ok(mut building_unlocked) = building_lock_ref.write() {
                                    building_unlocked
                                        .as_mut()
                                        .as_mut()
                                        .unwrap()
                                        .change_building(
                                            building_id_ref,
                                            server_api_lock_ref.clone(),
                                            inventory_spreadsheet_lock_ref.clone(),
                                            inventory_map_lock_ref.clone(),
                                        )?;
                                } else {
                                    Err(JsValue::from("Could not lock building"))?
                                }

                                // Clear the listing
                                change_building_toggle(
                                    building_lock_ref,
                                    server_api_lock_ref,
                                    inventory_spreadsheet_lock_ref,
                                    inventory_map_lock_ref,
                                )?;

                                Ok(())
                            };
                            set_on_x!(building_tag, set_onclick, f.clone(), false);

                            change_building_listing.append_with_node_1(&building_tag)?;
                        }
                    }
                }
        } else {
            Err(JsValue::from("Could not lock server_api and building"))?
        }

        //        change_building_listing.set_hidden(false);
    } else {
        // If the listing is shown, empty it out and hide it
        while change_building_listing.child_element_count() > 0 {
            change_building_listing
                .last_element_child()
                .unwrap()
                .remove();
        }

        //        change_building_listing.set_hidden(true);
    }

    Ok(())
}

/// Puts the current building into the archive, causing all items it contains to be archived
fn archive_building(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    let current_building = building_lock
        .read()
        .map_err(|_| JsValue::from("Could not lock building"))?
        .as_ref()
        .as_ref()
        .unwrap()
        .current_building;

    let unarchived_building = if let Ok(mut server_api_unlocked) = server_api_lock.clone().write() {
        let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

        // Find an unarchived building
        let ret = server_api.api_items[&APIType::Building]
            .data
            .iter()
            .find_map(|(id, data)| {
                if *id == current_building {
                    None
                } else {
                    data.fields
                        .get("archive")
                        .and_then(|d| d.as_bool().ok())
                        .and_then(|archived| if !archived { Some(*id) } else { None })
                }
            })
            .ok_or("Cannot find unarchived building")?;

        // Update the current building to be archived
        server_api
            .api_items
            .get_mut(&APIType::Building)
            .unwrap()
            .queue_update(current_building, "archive", DataType::B(true), None)?;
        server_api.mark_changed()?;

        ret
    } else {
        Err(JsValue::from("Could not lock server_api and building"))?
    };

    // Switch to the unarchived building
    building_lock
        .write()
        .map_err(|_| JsValue::from("Could not lock building"))?
        .as_mut()
        .as_mut()
        .unwrap()
        .change_building(
            unarchived_building,
            server_api_lock.clone(),
            inventory_spreadsheet_lock,
            inventory_map_lock,
        )?;

    Ok(())
}

/// Toggles the menu to restore an archived building, thereby restoring its items
fn restore_building_toggle(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the listing of buildings to restore
    let restore_building_listing = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id(&"restore_building_listing")
            .ok_or("Change building listing not found")?,
    ));

    if restore_building_listing.child_element_count() == 0 {
        // If the listing is hidden, fill it and show it
        if let Ok(server_api_unlocked) = server_api_lock.read() {
            for building in server_api_unlocked.as_ref().as_ref().unwrap().api_items
                [&APIType::Building]
                .data
                .values()
                {
                    if let (Some(true), Some(name)) = (
                        building
                            .fields
                            .get("archive")
                            .and_then(|d| d.as_bool().ok()),
                        building.fields.get("name").and_then(|d| d.as_string().ok()),
                    ) {
                        // Create the restoration button
                        let building_tag =
                            HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
                        building_tag.set_class_name("w3-button");
                        building_tag.set_id(&format!("restore_to_building_{}", building.id));
                        building_tag.set_inner_text(&name);

                        // Attach the restoration function
                        let building_lock_ref = building_lock.clone();
                        let server_api_lock_ref = server_api_lock.clone();
                        let inventory_spreadsheet_lock_ref = inventory_spreadsheet_lock.clone();
                        let inventory_map_lock_ref = inventory_map_lock.clone();
                        let building_id_ref = building.id;
                        let f = move |_event: JsValue| {
                            if let Ok(mut server_api_unlocked) = server_api_lock_ref.write() {
                                let window = web_sys::window().ok_or("Cannot get window")?;
                                let document = web_sys::HtmlDocument::from(JsValue::from(
                                    window.document().ok_or("Cannot get document")?,
                                ));

                                // Hide the restore building listing
                                HtmlElement::from(JsValue::from(
                                    document
                                        .get_element_by_id(&"restore_building")
                                        .ok_or("Restore building button not found")?,
                                ))
                                    .click();

                                // Update the changes
                                server_api_unlocked
                                    .as_mut()
                                    .as_mut()
                                    .unwrap()
                                    .api_items
                                    .get_mut(&APIType::Building)
                                    .unwrap()
                                    .queue_update(
                                        building_id_ref,
                                        "archive",
                                        DataType::B(false),
                                        None,
                                    )?;
                                server_api_unlocked
                                    .as_mut()
                                    .as_mut()
                                    .unwrap()
                                    .mark_changed()?;
                            } else {
                                Err(JsValue::from("Could not lock server_api"))?
                            }

                            // Switch to the restored building
                            if let Ok(mut building_unlocked) = building_lock_ref.write() {
                                building_unlocked
                                    .as_mut()
                                    .as_mut()
                                    .unwrap()
                                    .change_building(
                                        building_id_ref,
                                        server_api_lock_ref.clone(),
                                        inventory_spreadsheet_lock_ref.clone(),
                                        inventory_map_lock_ref.clone(),
                                    )?;
                            } else {
                                Err(JsValue::from("Could not lock building"))?
                            }

                            restore_building_toggle(
                                building_lock_ref,
                                server_api_lock_ref,
                                inventory_spreadsheet_lock_ref,
                                inventory_map_lock_ref,
                            )?;

                            Ok(())
                        };
                        set_on_x!(building_tag, set_onclick, f.clone(), false);

                        restore_building_listing.append_with_node_1(&building_tag)?;
                    }
                }
        } else {
            Err(JsValue::from("Could not lock server_api and buiding"))?
        }

        //        restore_building_listing.set_hidden(false);
    } else {
        // If the listing is shown, hide it and clear it
        while restore_building_listing.child_element_count() > 0 {
            restore_building_listing
                .last_element_child()
                .unwrap()
                .remove();
        }

        //        restore_building_listing.set_hidden(true);
    }

    Ok(())
}

/// Lets you delete an entire building
fn delete_building_toggle(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the listing
    let delete_building_listing = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id(&"delete_building_listing")
            .ok_or("Change building listing not found")?,
    ));

    if delete_building_listing.child_element_count() == 0 {
        // If the listing is hidden, show and fill it
        if let (Ok(server_api_unlocked), Ok(building_unlocked)) =
        (server_api_lock.read(), building_lock.read())
        {
            for building in server_api_unlocked.as_ref().as_ref().unwrap().api_items
                [&APIType::Building]
                .data
                .values()
                {
                    if building.id
                        != building_unlocked
                        .as_ref()
                        .as_ref()
                        .unwrap()
                        .current_building
                    {
                        if let (Some(true), Some(name)) = (
                            building
                                .fields
                                .get("archive")
                                .and_then(|d| d.as_bool().ok()),
                            building.fields.get("name").and_then(|d| d.as_string().ok()),
                        ) {
                            // Create the entry to delete the building
                            let building_tag =
                                HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
                            building_tag.set_class_name("w3-button");
                            building_tag.set_id(&format!("delete_to_building_{}", building.id));
                            building_tag.set_inner_text(&name);

                            // TODO: Delete building function

                            delete_building_listing.append_with_node_1(&building_tag)?;
                        }
                    }
                }
        } else {
            Err(JsValue::from("Could not lock server_api and building"))?
        }

        //        delete_building_listing.set_hidden(false);
    } else {
        // If it's shown, hide and clear the listing
        while delete_building_listing.child_element_count() > 0 {
            delete_building_listing
                .last_element_child()
                .unwrap()
                .remove();
        }

        //        delete_building_listing.set_hidden(true);
    }

    Ok(())
}

impl Building {
    /// Create a new building object
    pub fn new(server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>) -> Result<Self, JsValue> {
        // Find a building that is not archived
        let unarchived_building = {
            if let Ok(server_api_unlocked) = server_api_lock.read() {
                server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::Building]
                    .data
                    .iter()
                    .find_map(|(id, data)| {
                        data.fields
                            .get("archive")
                            .and_then(|d| d.as_bool().ok())
                            .and_then(|archived| if !archived { Some(*id) } else { None })
                    })
                    .ok_or("Cannot find unarchived building")?
            } else {
                Err(JsValue::from("Could not lock server_api"))?
            }
        };

        // Use that unarchived building as the current building for the program
        Ok(Self {
            current_building: unarchived_building,
        })
    }

    /// Change the current building to another building
    pub fn change_building(
        &mut self,
        new_building_id: u32,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Only change if the new building is indeed a different building
        if self.current_building != new_building_id {
            if let Ok(server_api_unlocked) = server_api_lock.read() {
                if let Some(new_building) = server_api_unlocked.as_ref().as_ref().unwrap().api_items
                    [&APIType::Building]
                    .data
                    .get(&new_building_id)
                {
                    if let Some(new_name) = new_building
                        .fields
                        .get("name")
                        .and_then(|d| d.as_string().ok())
                    {
                        // Change the title to reflect the new building's name
                        HtmlElement::from(JsValue::from(
                            document
                                .get_element_by_id("building_name")
                                .ok_or("Building name not found")?,
                        ))
                            .set_inner_text(&new_name);

                        // Change the current_building id
                        self.current_building = new_building_id;

                        // Notify the inventory_spreadsheet and inventory_map of the changes
                        if let (
                            Ok(mut inventory_spreadsheet_unlocked),
                            Ok(mut inventory_map_unlocked),
                        ) = (
                            inventory_spreadsheet_lock.write(),
                            inventory_map_lock.write(),
                        ) {
                            inventory_spreadsheet_unlocked
                                .as_mut()
                                .as_mut()
                                .unwrap()
                                .needs_update = true;
                            inventory_map_unlocked
                                .as_mut()
                                .as_mut()
                                .unwrap()
                                .layer_update = true;
                        } else {
                            Err(JsValue::from(
                                "Could not lock inventory_spreadsheet and inventory_map",
                            ))?
                        }
                    }
                }
            } else {
                Err(JsValue::from("Could not lock server_api"))?
            }
        }
        Ok(())
    }

    /// Creates the building header with all the options for managing buildings
    pub fn header(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        let building_perm = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::Building]
            .permissions
            .clone();

        // Building header container
        let ret = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret.set_id("building_header_dropdown");
        ret.set_class_name("w3-dropdown-hover w3-display-middle");

        // Set up building name and editor
        let (building_name_dom, building_name_form) =
            self.building_name_editor(building_lock.clone(), server_api_lock.clone())?;
        ret.append_with_node_1(&building_name_dom)?;
        ret.append_with_node_1(&building_name_form)?;

        // Set up the dropdown container
        let ret_container = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        ret_container.set_id("building_header_dropdown_container");
        ret_container.set_class_name("w3-dropdown-content");
        ret.append_with_node_1(&ret_container)?;

        // Set up new building button
        if building_perm.add {
            let new_building_button = self.new_building_button(
                building_lock.clone(),
                server_api_lock.clone(),
                inventory_spreadsheet_lock.clone(),
                inventory_map_lock.clone(),
            )?;
            ret_container.append_with_node_1(&new_building_button)?;
        }

        // Set up change building button
        let change_building = self.change_building_button(
            building_lock.clone(),
            server_api_lock.clone(),
            inventory_spreadsheet_lock.clone(),
            inventory_map_lock.clone(),
        )?;
        ret_container.append_with_node_1(&change_building)?;

        // Set up archive button
        if building_perm.change {
            let archive_building_button = self.archive_building_button(
                building_lock.clone(),
                server_api_lock.clone(),
                inventory_spreadsheet_lock.clone(),
                inventory_map_lock.clone(),
            )?;
            ret_container.append_with_node_1(&archive_building_button)?;
        }

        // Set up restore building button
        if building_perm.change {
            let restore_building = self.restore_building_button(
                building_lock.clone(),
                server_api_lock.clone(),
                inventory_spreadsheet_lock.clone(),
                inventory_map_lock.clone(),
            )?;
            ret_container.append_with_node_1(&restore_building)?;
        }

        // Set up delete building button
        // TODO: Unhide delete building button
        if building_perm.change {
            let _delete_building =
                self.delete_building_button(building_lock.clone(), server_api_lock.clone())?;
            //        ret_container.append_with_node_1(&delete_building)?;
        }

        Ok(ret)
    }

    /// Creates the building name field and editor DOM elements
    pub fn building_name_editor(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(HtmlElement, HtmlElement), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        let building_perm = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::Building]
            .permissions
            .clone();

        // Create building name
        let building_name = {
            if let Ok(server_api_unlocked) = server_api_lock.write() {
                server_api_unlocked.as_ref().as_ref().unwrap().api_items[&APIType::Building].data
                    [&self.current_building]
                    .fields["name"]
                    .as_string()?
            } else {
                Err(JsValue::from("Could not lock server_api"))?
            }
        };
        let building_name_dom = HtmlElement::from(JsValue::from(document.create_element("H1")?));
        building_name_dom.set_id("building_name");
        building_name_dom.set_class_name("building-name w3-content w3-xxlarge w3-hover-shadow");
        building_name_dom.set_inner_text(&building_name);

        // Clicking activates editor
        if building_perm.change {
            let building_lock_ref1 = building_lock.clone();
            let server_api_lock_ref1 = server_api_lock.clone();
            let f1 =
                move |event| building_name_edit_on(event, building_lock_ref1, server_api_lock_ref1);
            set_on_x!(building_name_dom, set_onclick, f1.clone(), false);
        }

        // Create the building name editor form
        let building_name_form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
        building_name_form.set_id("building_name_form");
        building_name_form.set_hidden(true);
        let building_lock_ref2 = building_lock.clone();
        let server_api_lock_ref2 = server_api_lock.clone();
        // Submitting the form (pressing enter) will change the name
        let f2 =
            move |event| building_name_edit_off(event, building_lock_ref2, server_api_lock_ref2);
        set_on_x!(building_name_form, set_onsubmit, f2.clone(), false);

        // Create the text box for editing the building name
        let building_name_label =
            HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
        building_name_label.set_inner_text("Change building name:");
        let building_name_input =
            HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
        building_name_input.set_class_name("w3-input w3-display-middle");
        building_name_input.set_id("building_name_input");
        let building_lock_ref3 = building_lock.clone();
        let server_api_lock_ref3 = server_api_lock.clone();
        // Clicking outside the text box will submit the new name
        let f3 =
            move |event| building_name_edit_off(event, building_lock_ref3, server_api_lock_ref3);
        set_on_x!(building_name_input, set_onblur, f3.clone(), false);
        building_name_label.append_with_node_1(&building_name_input)?;

        building_name_form.append_with_node_1(&building_name_label)?;

        Ok((building_name_dom, building_name_form))
    }

    /// Creates the button for creating a new building
    pub fn new_building_button(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("new_building");
        button.set_class_name("w3-button");
        button.set_inner_text("New Building");

        // Assign the on_click button
        let building_lock_ref = building_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        let f = move |_event| {
            new_building(
                building_lock_ref,
                server_api_lock_ref,
                inventory_spreadsheet_lock,
                inventory_map_lock,
            )
        };
        set_on_x!(button, set_onclick, f.clone(), false);

        Ok(button)
    }

    /// Creates the change building button
    pub fn change_building_button(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        let container = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        container.set_class_name("w3-dropdown-hover");

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("change_building");
        button.set_class_name("w3-button");
        button.set_inner_text("Change Building");

        container.append_with_node_1(&button)?;

        // Create the container for the building listing
        let listing = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        listing.set_id("change_building_listing");
        listing.set_class_name("w3-dropdown-content w3-grey");
        listing.set_attribute("style", "transform:translate(130px,-40px);")?;

        container.append_with_node_1(&listing)?;

        // Hovering over the button fills the listing
        let building_lock_ref = building_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        let f = move |_event| {
            change_building_toggle(
                building_lock_ref,
                server_api_lock_ref,
                inventory_spreadsheet_lock,
                inventory_map_lock,
            )
        };
        let f_ref = f.clone();
        set_on_x!(container, set_onpointerenter, f.clone(), false);
        set_on_x!(container, set_onpointerleave, f_ref.clone(), false);

        Ok(container)
    }

    /// Create the archive building button
    pub fn archive_building_button(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("archive_building");
        button.set_class_name("w3-button");
        button.set_inner_text("Archive Building");

        // Assign the on_click function
        let building_lock_ref = building_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        let f = move |_event| {
            archive_building(
                building_lock_ref,
                server_api_lock_ref,
                inventory_spreadsheet_lock,
                inventory_map_lock,
            )
        };
        set_on_x!(button, set_onclick, f.clone(), false);

        Ok(button)
    }

    /// Create the restore building function
    pub fn restore_building_button(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        let container = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        container.set_class_name("w3-dropdown-hover");

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("restore_building");
        button.set_class_name("w3-button");
        button.set_inner_text("Restore Building");

        container.append_with_node_1(&button)?;

        // Create the building listing container
        let listing = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        listing.set_id("restore_building_listing");
        listing.set_class_name("w3-dropdown-content w3-grey");
        listing.set_attribute("style", "transform:translate(130px,-40px);")?;

        container.append_with_node_1(&listing)?;

        // Hovering over the button fills the listing
        let building_lock_ref = building_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        let f = move |_event| {
            restore_building_toggle(
                building_lock_ref,
                server_api_lock_ref,
                inventory_spreadsheet_lock,
                inventory_map_lock,
            )
        };
        let f_ref = f.clone();
        set_on_x!(container, set_onpointerenter, f.clone(), false);
        set_on_x!(container, set_onpointerleave, f_ref.clone(), false);

        Ok(container)
    }

    /// Create the delete building button
    pub fn delete_building_button(
        &self,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        let container = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        container.set_class_name("building-dropdown-container");

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("delete_building");
        button.set_class_name("w3-button");
        button.set_inner_text("Delete Building");

        // Assign the on_click function
        let building_lock_ref = building_lock.clone();
        let server_api_lock_ref = server_api_lock.clone();
        let f = move |_event| delete_building_toggle(building_lock_ref, server_api_lock_ref);
        set_on_x!(button, set_onclick, f.clone(), false);

        container.append_with_node_1(&button)?;

        // Create the building listing container
        let listing = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        listing.set_id("delete_building_listing");
        listing.set_class_name("building-dropdown w3-container");
        //        listing.set_hidden(true);

        container.append_with_node_1(&listing)?;

        Ok(container)
    }
}

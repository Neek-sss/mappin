use std::{cell::RefCell, collections::HashMap, rc::Rc};

use maplit::hashmap;
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use mappin_lib::{
    ajax::{into_form_data, wasm_ajax_no_response},
    database_objects::DataType, set_on_x,
};

fn submit_report(event: JsValue) -> Result<(), JsValue> {
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    let report_form = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("report_form")
            .ok_or(JsValue::from("Could not find report form"))?,
    ));

    let mut selected = Vec::new();
    let mut checkbox_option = report_form.first_element_child();
    while let Some(checkbox) = checkbox_option {
        checkbox_option = checkbox.next_element_sibling();
        if let Some(checkbox_input) = checkbox.last_element_child() {
            let checked = DataType::from(js_sys::Reflect::get(&checkbox_input.clone(), &"checked".into())?).as_bool()?;
            if checked {
                selected.push(checkbox_input.id().replace("_checkbox", ""));
            }
        }
    }

    // Set up a report request
    let form_data = into_form_data(
        hashmap! {
            "selected" => format!("{:?}", selected),
        },
    );

    // Send the report request to the server
    let response = wasm_ajax_no_response("/mappin/generate_report/", &form_data)?;

    // Make sure that the report was successfully generated
    if response.status == 200 {
        window.open_with_url("/media/out.pdf")?;
    } else {
        window.alert_with_message("Invalid report attempt!")?;
    }

    reset_report_form()?;

    Ok(())
}

pub fn reset_report_form() -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Find the password form
    let report_form = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("report_form")
            .ok_or(JsValue::from("Could not find report form"))?,
    ));

    // Clear out the old form fields
    while report_form.child_element_count() > 0 {
        report_form.last_element_child().unwrap().remove();
    }

    // Create the checkboxes
    let checkboxes = [
        "Cost Histogram",
        "Manufacturer Pie Chart",
        "Manufacturer Frequency",
        "Manufacturer Vs Price",
        "Money per Year",
        "Make Model Quantity",
        "Money per Building",
    ];
    for checkbox in &checkboxes {
        let checkbox_label = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
        checkbox_label.set_inner_text(&format!("{}:", checkbox));
        checkbox_label.set_class_name("w3-label");
        let checkbox_input = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
        checkbox_input.set_id(&format!("{}_checkbox", checkbox.replace(' ', "_").to_lowercase()));
        checkbox_input.set_attribute("type", "checkbox")?;
        checkbox_input.set_class_name("w3-input");
        checkbox_label.append_with_node_1(&checkbox_input)?;

        // Append the checkbox to the form
        report_form.append_with_node_1(&checkbox_label)?;
    }

    // Create the submit button
    let submit_button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
    submit_button.set_inner_text("Generate Report");
    submit_button.set_id("generate_report_button");
    submit_button.set_class_name("w3-card w3-button");
    set_on_x!(submit_button, set_onclick, submit_report, false);

    // Put all the submit button into the form
    report_form.append_with_node_1(&submit_button)?;

    Ok(())
}

#[derive(Default, Debug, Clone)]
pub struct Report {}

impl Report {
    pub fn new(panel_id: &str) -> Result<Self, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Find the content
        let panel_content = document
            .get_element_by_id(&format!("{}_content", panel_id))
            .ok_or("Content not found")?;

        let form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
        form.set_id("report_form");
        set_on_x!(form, set_onsubmit, submit_report, false);
        panel_content.append_with_node_1(&form)?;
        reset_report_form()?;

        Ok(Self::default())
    }
}

use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    server_api::ServerAPI,
};
use mappin_lib_js::spreadsheet::Spreadsheet;
use std::sync::{Arc, RwLock};
use wasm_bindgen::prelude::*;
use web_sys::HtmlElement;

use crate::{building::Building, inventory_map::InventoryMap};

/// The function that is run when an item is added to the data
fn add_item_func(
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    new_object: DatabaseObject,
    old_object_option: Option<DatabaseObject>,
) -> Result<(), JsValue> {
    // Get the spreadsheet and archive contexts
    if let (
        Ok(mut inventory_spreadsheet_unlocked),
        Ok(mut archive_unlocked),
        Ok(mut inventory_map_unlocked),
    ) = (
        inventory_spreadsheet_lock.write(),
        archive_lock.write(),
        inventory_map_lock.write(),
    ) {
        let inventory_spreadsheet = inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();
        let archive = archive_unlocked.as_mut().as_mut().unwrap();
        let inventory_map = inventory_map_unlocked.as_mut().as_mut().unwrap();

        // Remove the item from the archive or inventory spreadsheet, if it exists
        if let Some(old_object) = old_object_option {
            if old_object
                .fields
                .get("archive")
                .and_then(|d| d.as_bool().ok())
                .unwrap_or(false)
            {
                // Refresh the inventory spreadsheet
                inventory_spreadsheet.needs_update = true;
                // Refresh the map
                inventory_map.item_update = true;
            } else {
                // Refresh the archive
                archive.needs_update = true;
            }
        }

        if new_object
            .fields
            .get("archive")
            .and_then(|d| d.as_bool().ok())
            .unwrap_or(false)
        {
            // Refresh the inventory spreadsheet
            inventory_spreadsheet.needs_update = true;
            // Refresh the map
            inventory_map.item_update = true;
        } else {
            // Refresh the archive
            archive.needs_update = true;
        }

        Ok(())
    } else {
        Err("Cannot open inventory_spreadsheet, inventory_map, and archive".into())
    }
}

/// Runs when items need to be updated
fn update_item_func(
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    database_object: DatabaseObject,
    column_name: String,
    _old_value: Option<DataType>,
    new_value: DataType,
) -> Result<(), JsValue> {
    if let (
        Ok(mut inventory_spreadsheet_unlocked),
        Ok(mut archive_unlocked),
        Ok(mut inventory_map_unlocked),
    ) = (
        inventory_spreadsheet_lock.write(),
        archive_lock.write(),
        inventory_map_lock.write(),
    ) {
        let inventory_spreadsheet = inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();
        let archive = archive_unlocked.as_mut().as_mut().unwrap();
        let inventory_map = inventory_map_unlocked.as_mut().as_mut().unwrap();

        // Update the data, moving the item between the inventory spreadsheet and archive as needed
        if database_object
            .fields
            .get("archive")
            .and_then(|d| d.as_bool().ok())
            .unwrap_or(false)
        {
            if column_name == "archive" {
                if new_value.as_bool().unwrap_or(false) {
                    // Refresh the inventory spreadsheet
                    inventory_spreadsheet.needs_update = true;
                }
            }

            // Refresh the archive
            archive.needs_update = true;
            // Refresh the map
            inventory_map.item_update = true;
        } else {
            if column_name == "archive" {
                if new_value.as_bool().unwrap_or(false) {
                    // Refresh the archive
                    archive.needs_update = true;
                }
            }

            // Refresh the inventory spreadsheet
            inventory_spreadsheet.needs_update = true;
            // Refresh the map
            inventory_map.item_update = true;
        }
    }
    Ok(())
}

/// Runs when a new building is added
fn add_building_func(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    new_object: DatabaseObject,
    old_object_option: Option<DatabaseObject>,
) -> Result<(), JsValue> {
    if let Some(old_object_id) = old_object_option.and_then(|old_object| Some(old_object.id)) {
        if let Ok(mut building_unlocked) = building_lock.write() {
            let building = building_unlocked.as_mut().as_mut().unwrap();
            if old_object_id == building.current_building {
                building.current_building = new_object.id;
            }
            Ok(())
        } else {
            Err(JsValue::from("Could not lock building"))
        }
    } else {
        Ok(())
    }
}

/// Runs when a building is updated
fn update_building_func(
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    database_object: DatabaseObject,
    column_name: String,
    _old_value_option: Option<DataType>,
    new_value: DataType,
) -> Result<(), JsValue> {
    if let Ok(mut building_unlocked) = building_lock.write() {
        let building = building_unlocked.as_mut().as_mut().unwrap();
        if let Some(new_name) = new_value.as_string().ok() {
            if building.current_building == database_object.id && column_name == "name" {
                let window = web_sys::window().ok_or("Cannot get window")?;
                let document = web_sys::HtmlDocument::from(JsValue::from(
                    window.document().ok_or("Cannot get document")?,
                ));

                let building_name_element = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("building_name")
                        .ok_or("Building name not found")?,
                ));

                building_name_element.set_inner_text(&new_name);
            }
        }
        Ok(())
    } else {
        Err(JsValue::from("Could not lock building"))
    }
}

/// Sets up the functions that are run on data changes
pub fn set_up_on_funcs(
    server_api: &mut ServerAPI,
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    archive_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    let building_lock_ref1 = building_lock.clone();
    let building_lock_ref2 = building_lock.clone();
    server_api
        .api_items
        .get_mut(&APIType::Building)
        .unwrap()
        .set_up_on_funcs(
            Some(Box::new(move |x, y| {
                add_building_func(building_lock_ref1.clone(), x, y)
            })),
            Some(Box::new(move |w, x, y, z| {
                update_building_func(building_lock_ref2.clone(), w, x, y, z)
            })),
            None,
        )?;
    let inventory_spreadsheet_lock_ref1 = inventory_spreadsheet_lock.clone();
    let archive_lock_ref1 = archive_lock.clone();
    let inventory_map_lock_ref1 = inventory_map_lock.clone();
    let inventory_spreadsheet_lock_ref2 = inventory_spreadsheet_lock.clone();
    let archive_lock_ref2 = archive_lock.clone();
    let inventory_map_lock_ref2 = inventory_map_lock.clone();
    server_api
        .api_items
        .get_mut(&APIType::Item)
        .unwrap()
        .set_up_on_funcs(
            Some(Box::new(move |x, y| {
                add_item_func(
                    inventory_spreadsheet_lock_ref1.clone(),
                    archive_lock_ref1.clone(),
                    inventory_map_lock_ref1.clone(),
                    x,
                    y,
                )
            })),
            Some(Box::new(move |w, x, y, z| {
                update_item_func(
                    inventory_spreadsheet_lock_ref2.clone(),
                    archive_lock_ref2.clone(),
                    inventory_map_lock_ref2.clone(),
                    w,
                    x,
                    y,
                    z,
                )
            })),
            None,
        )?;
    Ok(())
}

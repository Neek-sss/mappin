use maplit::hashmap;
use mappin_lib::{
    ajax::parse_ajax_json, api_type::APIType, database_objects::DataType, helpers::get_cookie,
    server_api::ServerAPI, set_on_x,
};
use mappin_lib_js::spreadsheet::Spreadsheet;
use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::{building::Building, inventory_map::InventoryMap};

/// Changes the image of a `DatabaseObject` to the image_id input
pub fn switch_to_image(
    image_id: u32,
    api_type: APIType,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Figure out which DatabaseObject type is being changed
    match api_type {
        APIType::ItemImage => {
            // Make changes based on the selected spreadsheet rows
            // Get the selected items in JS
            let selected_rows = if let Ok(mut inventory_spreadsheet_unlocked) =
            inventory_spreadsheet_lock.write()
            {
                let inventory_spreadsheet =
                    inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();

                let data_connector = inventory_spreadsheet.grid.getData();
                let get_item_func = js_sys::Function::from(js_sys::Reflect::get(
                    &data_connector,
                    &"getItem".into(),
                )?);

                let indices = inventory_spreadsheet
                    .grid
                    .getSelectedRows()
                    .values()
                    .into_iter()
                    .filter_map(|value| value.ok())
                    .collect::<Vec<_>>();

                indices
                    .into_iter()
                    .filter_map(|index| get_item_func.call1(&data_connector, &index).ok())
                    .collect::<Vec<_>>()
            } else {
                Err(JsValue::from("Could not lock inventory_spreadsheet"))?
            };

            for item_js in selected_rows.into_iter().rev() {
                let f = || {
                    // Get the DatabaseObject's ID
                    if let Some(item_id) =
                    DataType::from(js_sys::Reflect::get(&item_js, &"id".into())?)
                        .as_f64()
                        .ok()
                        .map(|f| f as u32)
                    {
                        // More locks
                        if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                            let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

                            // Get the building id so that we can update the server
                            if let Some(building) = server_api.api_items[&APIType::Item].data
                                [&item_id]
                                .fields
                                .get("building")
                                .cloned()
                            {
                                // Change the image
                                server_api
                                    .api_items
                                    .get_mut(&APIType::Item)
                                    .unwrap()
                                    .queue_update(
                                        item_id,
                                        "image",
                                        DataType::U(image_id as u64),
                                        Some(hashmap! {"building".into() => building.clone()}),
                                    )?;
                                server_api.mark_changed()?;
                            }
                        } else {
                            Err(JsValue::from("Could not lock server_api"))?
                        }

                        if let Ok(mut inventory_map_unlocked) = inventory_map_lock.write() {
                            // Update the map
                            inventory_map_unlocked
                                .as_mut()
                                .as_mut()
                                .unwrap()
                                .item_update = true;
                        } else {
                            Err(JsValue::from("Could not lock inventory_map"))?
                        }
                    }
                    Ok(())
                };
                if let Err(error) = f() {
                    web_sys::console::error_1(&error);
                }
            }
        }
        APIType::BuildingImage => {
            // Get the current_building ID
            let current_building = if let Ok(building_unlocked) = building_lock.read() {
                building_unlocked
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .current_building
            } else {
                Err(JsValue::from("Could not lock building"))?
            };

            if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

                // Update the building
                server_api
                    .api_items
                    .get_mut(&APIType::Building)
                    .unwrap()
                    .queue_update(
                        current_building,
                        "image",
                        DataType::U(image_id as u64),
                        None,
                    )?;
                server_api.mark_changed()?;
            } else {
                Err(JsValue::from("Could not lock server_api"))?
            }

            if let Ok(mut inventory_map_unlocked) = inventory_map_lock.write() {
                // Update the map
                inventory_map_unlocked
                    .as_mut()
                    .as_mut()
                    .unwrap()
                    .layer_update = true;
            } else {
                Err(JsValue::from("Could not lock inventory_map"))?
            }
        }
        _ => (),
    }

    // Rehide the ImageManager DOM element
    let overlay_background = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("image_manager_background")
            .ok_or(JsValue::from("Could not find overlay background"))?,
    ));
    overlay_background.clone().set_hidden(true);

    // Clear out the input file field so that it is not stuck on the last file next time
    let image_upload_file_input =
        HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    image_upload_file_input.set_id("image_upload_file_input");
    image_upload_file_input.set_attribute("type", "file")?;
    image_upload_file_input.set_attribute("name", "file")?;
    let image_input_old = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("image_upload_file_input")
            .ok_or(JsValue::from("Could not find image upload input"))?,
    ));
    image_input_old.replace_with_with_node_1(&image_upload_file_input)?;

    Ok(())
}

/// Uploads an image to the server and adds the response data to the server_api data
pub fn image_upload(
    event: JsValue,
    server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
) -> Result<(), JsValue> {
    // Make sure that JS doesn't reload the page
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the file uploading field
    let file_input = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id(&"image_upload_file_input")
            .ok_or("Could not find file input")?,
    ));

    // Get all selected upload files (there is only one)
    let file_array = js_sys::Array::from(&js_sys::Reflect::get(&file_input, &"files".into())?);
    file_array.for_each(&mut |file, _index, _array| {
        let f = || {
            // Get the current APIType
            let api_type = if let Ok(image_manager_unlocked) = image_manager_lock.read() {
                image_manager_unlocked
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .current_api_type
            } else {
                Err(JsValue::from("Could not lock image_manager"))?
            };

            // Create a new JS FormData object
            let form_data = web_sys::FormData::new()?;
            let append =
                js_sys::Function::from(js_sys::Reflect::get(&form_data, &"append".into())?);
            append.call2(&form_data, &"file".into(), &file)?;

            // Make the url based on the APIType
            let url = format!("/mappin/upload_image/{}/", api_type.as_str());

            // Create a new server connection
            let xhr = web_sys::XmlHttpRequest::new()?;
            xhr.open_with_async("POST", &url, false)?;
            xhr.set_request_header(
                "X-CSRFToken",
                &get_cookie("csrftoken")?.ok_or(JsValue::from("CSRFToken not set"))?,
            )?;

            // Send the file to the server
            let send = js_sys::Function::from(js_sys::Reflect::get(&xhr, &"send".into())?);
            send.call1(&xhr, &form_data.into())?;

            // Process the response
            for (id, image_object) in parse_ajax_json(
                &xhr.response_text()?
                    .ok_or(JsValue::from("No server response text"))?,
            )
                .map_err(|e| format!("{}", e))?
                {
                let image_id = image_object.id;
                if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                    // Insert the new image DatabaseObject
                    match api_type {
                        APIType::ItemImage => {
                            server_api_unlocked
                                .as_mut()
                                .as_mut()
                                .unwrap()
                                .api_items
                                .get_mut(&APIType::ItemImage)
                                .unwrap()
                                .data
                                .insert(id, image_object);
                        }
                        APIType::BuildingImage => {
                            server_api_unlocked
                                .as_mut()
                                .as_mut()
                                .unwrap()
                                .api_items
                                .get_mut(&APIType::BuildingImage)
                                .unwrap()
                                .data
                                .insert(id, image_object);
                        }
                        _ => (),
                    }
                } else {
                    Err(JsValue::from("Could not lock server_api"))?
                }

                // Change over to that image
                switch_to_image(
                    image_id,
                    api_type,
                    server_api_lock.clone(),
                    building_lock.clone(),
                    inventory_spreadsheet_lock.clone(),
                    inventory_map_lock.clone(),
                )?;
            }

            Ok(())
        };
        if let Err(error) = f() {
            web_sys::console::error_1(&error);
        }
    });

    Ok(())
}

/// The base connection to the generic image management system
pub struct ImageManager {
    /// The current type of image being processed
    current_api_type: APIType,
}

impl ImageManager {
    /// Creates the `ImageManager`
    pub fn new(
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
    ) -> Result<Self, JsValue> {
        Self::create_overlay(
            server_api_lock,
            building_lock,
            inventory_spreadsheet_lock,
            inventory_map_lock,
            image_manager_lock,
        )?;
        Ok(Self {
            current_api_type: APIType::UNKNOWN,
        })
    }

    /// Makes the overlay structure for interacting with the `ImageManager`
    pub fn create_overlay(
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
    ) -> Result<(), JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));
        let body = document.body().ok_or("Cannot get body")?;

        // Create the dim background
        let overlay_background = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        overlay_background.set_id("image_manager_background");
        overlay_background.set_class_name("image-manager-background");
        overlay_background.set_hidden(true);

        // When clicking the background, the ImageManager is closed
        let f = move |_event| {
            let window = web_sys::window().ok_or("Cannot get window")?;
            let document = web_sys::HtmlDocument::from(JsValue::from(
                window.document().ok_or("Cannot get document")?,
            ));

            // Hide the image manager
            let overlay_background = HtmlElement::from(JsValue::from(
                document
                    .get_element_by_id("image_manager_background")
                    .ok_or(JsValue::from("Could not find overlay background"))?,
            ));
            overlay_background.set_hidden(true);

            Ok(())
        };
        set_on_x!(overlay_background, set_onclick, f, false);

        // Create the ImageManager window
        let overlay_foreground = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        overlay_foreground.set_id("image_manager_foreground");
        overlay_foreground.set_class_name("image-manager-foreground w3-container");

        // When clicking within the ImageManager window, it does not close
        let f = move |event| {
            let stop_propagation =
                js_sys::Function::from(js_sys::Reflect::get(&event, &"stopPropagation".into())?);
            stop_propagation.call0(&event)?;
            Ok(())
        };
        set_on_x!(overlay_foreground, set_onclick, f, false);

        // Create the form for uploading images
        let image_upload_form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
        image_upload_form.set_id("image_upload_form");
        image_upload_form.set_class_name("image-upload-form w3-hide");
        image_upload_form.set_attribute("method", "post")?;
        image_upload_form.set_attribute("enctype", "multipart/form-data")?;

        // Submitting the form uploads the image
        let f = move |event| {
            image_upload(
                event,
                server_api_lock.clone(),
                building_lock.clone(),
                inventory_spreadsheet_lock.clone(),
                inventory_map_lock.clone(),
                image_manager_lock.clone(),
            )?;
            Ok(())
        };
        set_on_x!(image_upload_form, set_onsubmit, f, false);

        // The button to be able to choose a file
        let image_upload_file_label =
            HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
        image_upload_file_label.set_inner_text("Choose image to upload:");
        let image_upload_file_input =
            HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
        image_upload_file_input.set_id("image_upload_file_input");
        image_upload_form.set_class_name("w3-input");
        image_upload_file_input.set_attribute("type", "file")?;
        image_upload_file_input.set_attribute("name", "file")?;

        image_upload_file_label.append_with_node_1(&image_upload_file_input)?;
        image_upload_form.append_with_node_1(&image_upload_file_label)?;

        // A submit button to start the file transfer
        let image_submit_button =
            HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
        image_submit_button.set_id("image_submit_button");
        image_submit_button.set_class_name("w3-card w3-button");
        image_submit_button.set_attribute("type", "submit")?;
        image_submit_button.set_attribute("value", "Upload File")?;

        image_upload_form.append_with_node_1(&image_submit_button)?;

        overlay_foreground.append_with_node_1(&image_upload_form)?;

        // A simple header to start the already uploaded image collection
        let image_collection_header =
            HtmlElement::from(JsValue::from(document.create_element("P")?));
        image_collection_header.set_inner_text("Available Images");

        overlay_foreground.append_with_node_1(&image_collection_header)?;

        // The collection of uploaded images
        let image_collection = HtmlElement::from(JsValue::from(document.create_element("DIV")?));
        image_collection.set_id("image_collection");
        image_collection.set_class_name("image-collection");

        overlay_foreground.append_with_node_1(&image_collection)?;

        overlay_background.append_with_node_1(&overlay_foreground)?;

        body.append_with_node_1(&overlay_background)?;

        Ok(())
    }

    /// Creates a button that can be used to activate the `ImageManager` with the appropriate
    /// `APIType`
    pub fn create_image_manager_button(
        &self,
        name: &str,
        id: &str,
        api_type: APIType,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id(id);
        button.set_class_name("w3-card w3-button");
        button.set_inner_text(name);

        // Create the on_click function
        let f = move |_event| {
            // Check that the image manager actually has something to edit
            let should_open_image_manager = if let APIType::ItemImage = api_type {
                if let Ok(inventory_spreadsheet_unlocked) = inventory_spreadsheet_lock.read() {
                    inventory_spreadsheet_unlocked
                        .as_ref()
                        .as_ref()
                        .unwrap()
                        .grid
                        .getSelectedRows()
                        .length()
                        > 0
                } else {
                    Err(JsValue::from(
                        "Could not get read lock on inventory_spreadsheet",
                    ))?
                }
            } else {
                true
            };

            if should_open_image_manager {
                // Set the new APIType
                if let Ok(mut image_manager_unlocked) = image_manager_lock.write() {
                    image_manager_unlocked
                        .as_mut()
                        .as_mut()
                        .unwrap()
                        .current_api_type = api_type;
                } else {
                    Err(JsValue::from("Could not lock image_manager"))?
                }

                let window = web_sys::window().ok_or("Cannot get window")?;
                let document = web_sys::HtmlDocument::from(JsValue::from(
                    window.document().ok_or("Cannot get document")?,
                ));

                // Show the ImageManager
                let overlay_background = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("image_manager_background")
                        .ok_or(JsValue::from("Could not find overlay background"))?,
                ));
                overlay_background.set_hidden(false);

                // Show or hide the image upload depending on permissions
                let image_perm = server_api_lock
                    .read()
                    .map_err(|_| JsValue::from("Could not lock server_api"))?
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .api_items[&api_type]
                    .permissions
                    .clone();

                let image_upload_form = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("image_upload_form")
                        .ok_or(JsValue::from("Could not find upload form"))?,
                ));
                if image_perm.add {
                    image_upload_form.set_class_name("image-upload-form w3-show");
                } else {
                    image_upload_form.set_class_name("image-upload-form w3-hide");
                }

                let image_collection = HtmlElement::from(JsValue::from(
                    document
                        .get_element_by_id("image_collection")
                        .ok_or(JsValue::from("Could not find image collection"))?,
                ));

                // Clear out any old images in the collection
                while image_collection.child_element_count() > 0 {
                    image_collection.last_element_child().unwrap().remove();
                }

                // Fill the image collection with already uploaded images
                if let Ok(server_api_unlocked) = server_api_lock.read() {
                    // Get the correct images for the APIType
                    let api_type_images = match api_type {
                        APIType::ItemImage => {
                            server_api_unlocked.as_ref().as_ref().unwrap().api_items
                                [&APIType::ItemImage]
                                .data
                                .values()
                        }
                        APIType::BuildingImage => {
                            server_api_unlocked.as_ref().as_ref().unwrap().api_items
                                [&APIType::BuildingImage]
                                .data
                                .values()
                        }
                        _ => Err(JsValue::from("Invalid image type"))?,
                    };

                    // Add all of the images
                    for image in api_type_images {
                        if let Some(path) =
                        image.fields.get("path").and_then(|d| d.as_string().ok())
                        {
                            // Create the image DOM element
                            let image_in_collection =
                                HtmlElement::from(JsValue::from(document.create_element("IMG")?));
                            image_in_collection.set_id(&format!("{}", image.id));
                            image_in_collection
                                .set_class_name("image-in-collection w3-card w3-button");
                            image_in_collection.set_attribute("src", &path)?;

                            // Set the on_click function to change to that image
                            let server_api_lock_ref = server_api_lock.clone();
                            let building_lock = building_lock.clone();
                            let inventory_spreadsheet_lock_ref = inventory_spreadsheet_lock.clone();
                            let inventory_map_lock_ref = inventory_map_lock.clone();
                            let f = move |event| {
                                // Get the selected image ID
                                let target = js_sys::Reflect::get(&event, &"target".into())?;
                                let id_data =
                                    DataType::from(js_sys::Reflect::get(&target, &"id".into())?);
                                if let Some(id) =
                                id_data.as_string().ok().and_then(|s| s.parse().ok())
                                {
                                    // Change to that image
                                    switch_to_image(
                                        id,
                                        api_type,
                                        server_api_lock_ref.clone(),
                                        building_lock.clone(),
                                        inventory_spreadsheet_lock_ref.clone(),
                                        inventory_map_lock_ref.clone(),
                                    )?;
                                }
                                Ok(())
                            };
                            set_on_x!(image_in_collection, set_onclick, f, false);

                            image_collection.append_with_node_1(&image_in_collection)?;
                        }
                    }
                } else {
                    Err(JsValue::from("Could not lock server_api"))?
                }
            }

            Ok(())
        };
        set_on_x!(button, set_onclick, f, false);

        Ok(button)
    }
}

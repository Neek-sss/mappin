use js_sys::Reflect as JsReflect;
use maplit::hashmap;
use mappin_lib::{
    api_type::APIType,
    database_objects::{DatabaseObject, DataType},
    panel::{PanelManager, PanelType},
    server_api::ServerAPI,
    set_on_x,
};
use mappin_lib_js::{
    slick_grid::Grid,
    spreadsheet::{
        on_x_get_id_column, Spreadsheet, SpreadsheetBuilder, SpreadsheetColumn, SpreadsheetOptions,
    },
    subscribe_x,
};
use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, RwLock},
};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::HtmlElement;

use crate::{building::Building, image_manager::ImageManager, inventory_map::InventoryMap};

/// A dummy struct for building `InventorySpreadsheet`'s
pub struct InventorySpreadsheetBuilder {
    building_lock: Arc<RwLock<Box<Option<Building>>>>,
    inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
    inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
}

impl InventorySpreadsheetBuilder {
    pub fn new(
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
    ) -> Self {
        Self {
            building_lock,
            inventory_spreadsheet_lock,
            inventory_map_lock,
            image_manager_lock,
        }
    }
}

impl InventorySpreadsheetBuilder {
    /// Toggles whether or not the map will display an item
    pub fn toggle_on_map(
        spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Get the selected items in JS
        let selected_rows = if let Ok(mut inventory_spreadsheet_unlocked) = spreadsheet_lock.write()
        {
            let inventory_spreadsheet = inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();

            let data_connector = inventory_spreadsheet.grid.getData();
            let get_item_func =
                js_sys::Function::from(js_sys::Reflect::get(&data_connector, &"getItem".into())?);

            let indices = inventory_spreadsheet
                .grid
                .getSelectedRows()
                .values()
                .into_iter()
                .filter_map(|value| value.ok())
                .collect::<Vec<_>>();

            indices
                .into_iter()
                .filter_map(|index| get_item_func.call1(&data_connector, &index).ok())
                .collect::<Vec<_>>()
        } else {
            Err(JsValue::from("Could not lock inventory_spreadsheet"))?
        };

        for item_js in selected_rows.into_iter().rev() {
            let f = || {
                if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                    let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

                    let item_id = DataType::from(js_sys::Reflect::get(&item_js, &"id".into())?)
                        .as_f64()? as u32;
                    let item = server_api.api_items[&APIType::Item].data[&item_id].clone();

                    if let Some(building) = item.fields.get("building") {
                        if item
                            .fields
                            .get("on_map")
                            .and_then(|d| d.as_bool().ok())
                            .unwrap_or(false)
                        {
                            // Remove the item from the map
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "on_map",
                                    DataType::B(false),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                        } else {
                            // Put the item on the map in a pseudo-random location so that
                            // multiple object placements will not overlap
                            let new_x_coord = js_sys::Math::random() * -200.0;
                            let new_y_coord = js_sys::Math::random() * 1080.0;
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "on_map",
                                    DataType::B(true),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "x_coord",
                                    DataType::S(format!("{}", new_x_coord)),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "y_coord",
                                    DataType::S(format!("{}", new_y_coord)),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                        }
                        server_api.mark_changed()?;
                    }
                    Ok(())
                } else {
                    Err(JsValue::from("Could not lock server_api and inventory_map"))
                }
            };
            if let Err(error) = f() {
                web_sys::console::error_1(&error);
            }
        }
        Ok(())
    }

    /// Generates the DOM button for toggling an item on the map
    pub fn get_toggle_on_map_button(
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("toggle_on_map");
        button.set_class_name("w3-card w3-button");
        button.set_inner_text("Toggle on map");

        // Set the on_click function
        let f = move |_event| {
            if let Err(error) = Self::toggle_on_map(inventory_spreadsheet_lock, server_api_lock) {
                web_sys::console::error_1(&error);
            }
            Ok(())
        };
        set_on_x!(button, set_onclick, f.clone(), false);

        Ok(button)
    }

    /// Requests the `ImageManager` button
    pub fn get_image_manager_button(
        image_manager_lock: Arc<RwLock<Box<Option<ImageManager>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        building_lock: Arc<RwLock<Box<Option<Building>>>>,
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        inventory_map_lock: Arc<RwLock<Box<Option<InventoryMap>>>>,
    ) -> Result<HtmlElement, JsValue> {
        if let Ok(image_manager_unlocked) = image_manager_lock.read() {
            Ok(image_manager_unlocked
                .as_ref()
                .as_ref()
                .unwrap()
                .create_image_manager_button(
                    "Item Image Manager",
                    "inventory_spredsheet_image_manager",
                    APIType::ItemImage,
                    image_manager_lock.clone(),
                    server_api_lock,
                    building_lock,
                    inventory_spreadsheet_lock,
                    inventory_map_lock,
                )?)
        } else {
            Err(JsValue::from("Could not lock image_manager"))
        }
    }

    /// Archives the selected items
    pub fn archive(
        spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<(), JsValue> {
        // Get the selected items in JS
        let selected_rows = if let Ok(mut inventory_spreadsheet_unlocked) = spreadsheet_lock.write()
        {
            let inventory_spreadsheet = inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();

            let data_connector = inventory_spreadsheet.grid.getData();
            let get_item_func =
                js_sys::Function::from(js_sys::Reflect::get(&data_connector, &"getItem".into())?);

            let indices = inventory_spreadsheet
                .grid
                .getSelectedRows()
                .values()
                .into_iter()
                .filter_map(|value| value.ok())
                .collect::<Vec<_>>();

            indices
                .into_iter()
                .filter_map(|index| get_item_func.call1(&data_connector, &index).ok())
                .collect::<Vec<_>>()
        } else {
            Err(JsValue::from("Could not lock inventory_spreadsheet"))?
        };

        for item_js in selected_rows.into_iter().rev() {
            let f = || {
                if let Ok(mut server_api_unlocked) = server_api_lock.write() {
                    let server_api = server_api_unlocked.as_mut().as_mut().unwrap();

                    // Get the actual DatabaseObject
                    if let Some(item_id) =
                    DataType::from(js_sys::Reflect::get(&item_js, &"id".into())?)
                        .as_f64()
                        .ok()
                        .map(|f| f as u32)
                    {
                        let item = server_api.api_items[&APIType::Item].data[&item_id].clone();

                        // Update the archive and on_map status
                        if let Some(building) = item.fields.get("building") {
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "archive",
                                    DataType::B(true),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                            server_api
                                .api_items
                                .get_mut(&APIType::Item)
                                .unwrap()
                                .queue_update(
                                    item_id,
                                    "on_map",
                                    DataType::B(false),
                                    Some(hashmap! {"building".into() => building.clone()}),
                                )?;
                            server_api.mark_changed()?;
                        }
                    }

                    Ok(())
                } else {
                    Err(JsValue::from("Could not lock server_api and inventory_map"))
                }
            };
            if let Err(error) = f() {
                web_sys::console::error_1(&error);
            }
        }

        if let Ok(mut inventory_spreadsheet_unlocked) = spreadsheet_lock.write() {
            let inventory_spreadsheet = inventory_spreadsheet_unlocked.as_mut().as_mut().unwrap();
            inventory_spreadsheet
                .grid
                .setSelectedRows(js_sys::Array::new());
            Ok(())
        } else {
            Err(JsValue::from("Could not lock inventory_spreadsheet"))?
        }
    }

    /// Generates the archive button
    pub fn get_archive_button(
        inventory_spreadsheet_lock: Arc<RwLock<Box<Option<Spreadsheet>>>>,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<HtmlElement, JsValue> {
        let window = web_sys::window().ok_or("Cannot get window")?;
        let document = web_sys::HtmlDocument::from(JsValue::from(
            window.document().ok_or("Cannot get document")?,
        ));

        // Create the button
        let button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
        button.set_id("archive_item");
        button.set_class_name("w3-card w3-button");
        button.set_inner_text("Archive");

        // Set the on_click function
        let f = move |_event| {
            if let Err(error) = Self::archive(inventory_spreadsheet_lock, server_api_lock) {
                web_sys::console::error_1(&error);
            }
            Ok(())
        };
        set_on_x!(button, set_onclick, f.clone(), false);

        Ok(button)
    }
}

impl SpreadsheetBuilder for InventorySpreadsheetBuilder {
    /// Get the header DOM elements
    fn get_header_elements(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        _panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Result<Vec<HtmlElement>, JsValue> {
        let mut header = Vec::new();

        let item_perm = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::Item]
            .permissions
            .clone();

        if item_perm.change {
            header.push(Self::get_toggle_on_map_button(
                self.inventory_spreadsheet_lock.clone(),
                server_api_lock.clone(),
            )?);
            header.push(Self::get_image_manager_button(
                self.image_manager_lock.clone(),
                server_api_lock.clone(),
                self.building_lock.clone(),
                self.inventory_spreadsheet_lock.clone(),
                self.inventory_map_lock.clone(),
            )?);
            header.push(Self::get_archive_button(
                self.inventory_spreadsheet_lock.clone(),
                server_api_lock.clone(),
            )?);
        }

        Ok(header)
    }

    /// Get the header DOM elements
    fn get_options(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<SpreadsheetOptions, JsValue> {
        let item_perm = server_api_lock
            .read()
            .map_err(|_| JsValue::from("Could not lock server_api"))?
            .as_ref()
            .as_ref()
            .unwrap()
            .api_items[&APIType::Item]
            .permissions
            .clone();

        let mut options = SpreadsheetOptions::default();

        if item_perm.change {
            // Can edit the spreadsheet
            options.editable = true;
            options.auto_edit = true;
            options.selection_column = true;
        }

        if item_perm.add {
            options.enable_add_row = true;
            options.leave_space_for_new_rows = true;
        }

        Ok(options)
    }

    fn get_columns(
        &self,
        server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
    ) -> Result<Vec<SpreadsheetColumn>, JsValue> {
        let editors = {
            let slick = JsReflect::get(&js_sys::global().into(), &"Slick".into())?;
            JsReflect::get(&slick, &"Editors".into())?
        };
        Ok(vec![
            SpreadsheetColumn {
                id: "name".into(),
                name: "Name".into(),
                field: "name".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "manufacturer".into(),
                name: "Manufacturer".into(),
                field: "manufacturer".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "make".into(),
                name: "Make".into(),
                field: "make".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "model".into(),
                name: "Model".into(),
                field: "model".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "year".into(),
                name: "Year".into(),
                field: "year".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "serial_number".into(),
                name: "Serial Number".into(),
                field: "serial_number".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Text".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "price".into(),
                name: "Price".into(),
                field: "price".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Float".into())?.into()),
                formatter: None,
            },
            SpreadsheetColumn {
                id: "obtained".into(),
                name: "Obtained".into(),
                field: "obtained".into(),
                sortable: true,
                editor: Some(JsReflect::get(&editors, &"Date".into())?.into()),
                formatter: None,
            },
        ])
    }

    fn get_filter_func(
        &self,
        _server_api_lock: Arc<RwLock<Box<Option<ServerAPI>>>>,
        panel_manager_lock: Arc<RwLock<Box<Option<PanelManager>>>>,
    ) -> Box<dyn FnMut(&DatabaseObject) -> Result<bool, JsValue>> {
        let building_lock_ref = self.building_lock.clone();
        let panel_manager_lock_ref = panel_manager_lock.clone();
        let inventory_map_lock_ref = self.inventory_map_lock.clone();
        Box::new(move |database_object: &DatabaseObject| {
            // Check if it's archived
            let archived = database_object
                .fields
                .get("archive")
                .and_then(|d| d.as_bool().ok())
                .unwrap_or(false);

            // Check if it is in the current building
            let in_current_building = if let Some(building_id) = database_object
                .fields
                .get("building")
                .and_then(|d| d.as_u64().ok())
            {
                if let Ok(building_unlocked) = building_lock_ref.read() {
                    building_id as u32
                        == building_unlocked
                            .as_ref()
                            .as_ref()
                            .unwrap()
                            .current_building
                } else {
                    Err(JsValue::from("Could not lock building"))?
                }
            } else {
                false
            };

            // See if the map is active
            let map_active = if let Ok(panel_manager_unlocked) = panel_manager_lock_ref.read() {
                panel_manager_unlocked
                    .as_ref()
                    .as_ref()
                    .unwrap()
                    .active_panels
                    .contains(&PanelType::Map("Map"))
            } else {
                Err(JsValue::from("Could not lock panel_manager"))?
            };

            // See what the current map extent is
            let current_extent = if let Ok(inventory_map_unlocked) = inventory_map_lock_ref.read() {
                if let Some(x) = inventory_map_unlocked.as_ref().as_ref() {
                    x.get_extent()?
                } else {
                    [0.0, 0.0, 1920.0, 1080.0]
                }
            } else {
                Err(JsValue::from("Could not lock inventory_map"))?
            };

            // Check if the map is zoomed out
            let zoomed_out = {
                current_extent[0] <= 0.0
                    && current_extent[1] <= 0.0
                    && current_extent[2] >= 1920.0
                    && current_extent[3] >= 1080.0
            };

            // Check if the item is on the map
            let on_map = database_object
                .fields
                .get("on_map")
                .and_then(|d| d.as_bool().ok())
                .unwrap_or(false);

            // Check if the item is within the current map view
            let in_current_extent = if let (Some(x_coord), Some(y_coord)) = (
                database_object
                    .fields
                    .get("x_coord")
                    .and_then(|d| d.as_string().ok())
                    .and_then(|s| s.parse::<f64>().ok()),
                database_object
                    .fields
                    .get("y_coord")
                    .and_then(|d| d.as_string().ok())
                    .and_then(|s| s.parse::<f64>().ok()),
            ) {
                x_coord > current_extent[0]
                    && x_coord < current_extent[2]
                    && y_coord > current_extent[1]
                    && y_coord < current_extent[3]
            } else {
                false
            };

            Ok(!archived
                && in_current_building
                && (!map_active || (zoomed_out || (on_map && in_current_extent))))
        })
    }

    fn get_api_type(&self) -> APIType {
        APIType::Item
    }

    fn on_cell_change(
        &self,
    ) -> Result<
        Box<dyn FnMut(u32, &str, DataType, &DatabaseObject, &mut ServerAPI) -> Result<(), JsValue>>,
        JsValue,
    > {
        // When a cell is changed, update the data
        Ok(Box::new(
            move |item_id: u32,
                  column_name: &str,
                  mut column_data: DataType,
                  database_object: &DatabaseObject,
                  server_api: &mut ServerAPI| {
                let building = database_object.fields["building"].clone();

                // Convert date to a format that can be used by Django
                if column_name == "obtained" {
                    let obtained = column_data.as_string().unwrap_or("".into());
                    let split_string = obtained.split('/').collect::<Vec<_>>();
                    if split_string.len() == 3
                        && (split_string[0].len() == 1 || split_string[0].len() == 2)
                        && (split_string[1].len() == 1 || split_string[1].len() == 2)
                        && split_string[2].len() == 4
                    {
                        let month = split_string[0].parse::<u8>();
                        let day = split_string[1].parse::<u8>();
                        let year = split_string[2].parse::<u32>();
                        if let (Ok(m), Ok(d), Ok(y)) = (month, day, year) {
                            column_data = DataType::S(format!("{:04}-{:02}-{:02}", y, m, d));
                        }
                    }
                }

                server_api
                    .api_items
                    .get_mut(&APIType::Item)
                    .unwrap()
                    .queue_update_skip_on_update(
                        item_id,
                        &column_name,
                        column_data,
                        Some(hashmap! {"building".into() => building}),
                    )?;
                server_api.mark_changed()?;

                Ok(())
            },
        ))
    }

    fn on_add_row(
        &self,
    ) -> Result<Box<dyn FnMut(&str, DataType, &mut ServerAPI) -> Result<u32, JsValue>>, JsValue>
    {
        // When an item is added, update the data
        let building_lock_ref = self.building_lock.clone();
        Ok(Box::new(
            move |column_name: &str, mut column_data: DataType, server_api: &mut ServerAPI| {
                // Convert date to a format that can be used by Django
                if column_name == "obtained" {
                    let obtained = column_data.as_string().unwrap_or("".into());
                    let split_string = obtained.split('/').collect::<Vec<_>>();
                    if split_string.len() == 3
                        && (split_string[0].len() == 1 || split_string[0].len() == 2)
                        && (split_string[1].len() == 1 || split_string[1].len() == 2)
                        && split_string[2].len() == 4
                    {
                        let month = split_string[0].parse::<u8>();
                        let day = split_string[1].parse::<u8>();
                        let year = split_string[2].parse::<u32>();
                        if let (Ok(m), Ok(d), Ok(y)) = (month, day, year) {
                            column_data = DataType::S(format!("{:04}-{:02}-{:02}", y, m, d));
                        }
                    }
                }

                let building = DataType::U(
                    building_lock_ref
                        .read()
                        .map_err(|_| JsValue::from("Could not lock building"))?
                        .as_ref()
                        .as_ref()
                        .unwrap()
                        .current_building as u64,
                );
                let new_id = (js_sys::Math::random() * 10_000.0) as u32;

                let item_name = if column_name == "name" {
                    column_data.as_string().unwrap_or(format!(
                        "New Item {}",
                        (js_sys::Math::random() * 10_000.0) as u32
                    ))
                } else {
                    format!("New Item {}", (js_sys::Math::random() * 10_000.0) as u32)
                };

                server_api
                    .api_items
                    .get_mut(&APIType::Item)
                    .unwrap()
                    .queue_add(
                        new_id,
                        &item_name,
                        Some(hashmap! {"building".into() => building.clone()}),
                    )?;
                if column_name != "name" {
                    server_api
                        .api_items
                        .get_mut(&APIType::Item)
                        .unwrap()
                        .queue_update(
                            new_id,
                            &column_name,
                            column_data,
                            Some(hashmap! {"building".into() => building}),
                        )?;
                }
                server_api.mark_changed()?;

                Ok(new_id)
            },
        ))
    }
}

/* tslint:disable */
export const memory: WebAssembly.Memory;
export function wasm_main(): void;
export function __wbg_dataconnector_free(a: number): void;
export function dataconnector_getLength(a: number): number;
export function dataconnector_getItem(a: number, b: number): number;
export function dataconnector_getRow(a: number, b: number): number;
export function dataconnector_set_sort_column(a: number, b: number, c: number): void;
export function dataconnector_set_sort_ascending(a: number, b: number): void;
export function __wbindgen_exn_store(a: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
export function __wbindgen_free(a: number, b: number): void;
export const __wbg_function_table: WebAssembly.Table;
export function __wbindgen_start(): void;

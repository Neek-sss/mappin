
let wasm;

const heap = new Array(32);

heap.fill(undefined);

heap.push(undefined, null, true, false);

let heap_next = heap.length;

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}
function __wbg_elem_binding0(arg0, arg1, arg2) {
    wasm.__wbg_function_table.get(178)(arg0, arg1, addHeapObject(arg2));
}

function getObject(idx) { return heap[idx]; }

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}
function __wbg_elem_binding1(arg0, arg1, arg2) {
    const ret = wasm.__wbg_function_table.get(185)(arg0, arg1, addHeapObject(arg2));
    return takeObject(ret);
}
function __wbg_elem_binding2(arg0, arg1, arg2, arg3) {
    wasm.__wbg_function_table.get(188)(arg0, arg1, addHeapObject(arg2), addHeapObject(arg3));
}
function __wbg_elem_binding3(arg0, arg1) {
    wasm.__wbg_function_table.get(215)(arg0, arg1);
}
function __wbg_elem_binding4(arg0, arg1, arg2) {
    wasm.__wbg_function_table.get(182)(arg0, arg1, addHeapObject(arg2));
}
function __wbg_elem_binding5(arg0, arg1, arg2) {
    wasm.__wbg_function_table.get(219)(arg0, arg1, addHeapObject(arg2));
}
function __wbg_elem_binding6(arg0, arg1, arg2, arg3, arg4) {
    wasm.__wbg_function_table.get(239)(arg0, arg1, addHeapObject(arg2), arg3, addHeapObject(arg4));
}
/**
* Called when the wasm module is instantiated.  This is equivalent to a binary program\'s main
* function.
*/
export function wasm_main() {
    wasm.wasm_main();
}

let WASM_VECTOR_LEN = 0;

let cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

let cachegetUint8Memory = null;
function getUint8Memory() {
    if (cachegetUint8Memory === null || cachegetUint8Memory.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory;
}

function passStringToWasm(arg) {

    let len = arg.length;
    let ptr = wasm.__wbindgen_malloc(len);

    const mem = getUint8Memory();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = wasm.__wbindgen_realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

function getStringFromWasm(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory().subarray(ptr, ptr + len));
}

let cachegetInt32Memory = null;
function getInt32Memory() {
    if (cachegetInt32Memory === null || cachegetInt32Memory.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory;
}

function handleError(e) {
    wasm.__wbindgen_exn_store(addHeapObject(e));
}

function isLikeNone(x) {
    return x === undefined || x === null;
}

function notDefined(what) { return () => { throw new Error(`${what} is not defined`); }; }

let cachegetUint32Memory = null;
function getUint32Memory() {
    if (cachegetUint32Memory === null || cachegetUint32Memory.buffer !== wasm.memory.buffer) {
        cachegetUint32Memory = new Uint32Array(wasm.memory.buffer);
    }
    return cachegetUint32Memory;
}

function debugString(val) {
    // primitive types
    const type = typeof val;
    if (type == 'number' || type == 'boolean' || val == null) {
        return  `${val}`;
    }
    if (type == 'string') {
        return `"${val}"`;
    }
    if (type == 'symbol') {
        const description = val.description;
        if (description == null) {
            return 'Symbol';
        } else {
            return `Symbol(${description})`;
        }
    }
    if (type == 'function') {
        const name = val.name;
        if (typeof name == 'string' && name.length > 0) {
            return `Function(${name})`;
        } else {
            return 'Function';
        }
    }
    // objects
    if (Array.isArray(val)) {
        const length = val.length;
        let debug = '[';
        if (length > 0) {
            debug += debugString(val[0]);
        }
        for(let i = 1; i < length; i++) {
            debug += ', ' + debugString(val[i]);
        }
        debug += ']';
        return debug;
    }
    // Test for built-in
    const builtInMatches = /\[object ([^\]]+)\]/.exec(toString.call(val));
    let className;
    if (builtInMatches.length > 1) {
        className = builtInMatches[1];
    } else {
        // Failed to match the standard '[object ClassName]'
        return toString.call(val);
    }
    if (className == 'Object') {
        // we're a user defined class or Object
        // JSON.stringify avoids problems with cycles, and is generally much
        // easier than looping through ownProperties of `val`.
        try {
            return 'Object(' + JSON.stringify(val) + ')';
        } catch (_) {
            return 'Object';
        }
    }
    // errors
    if (val instanceof Error) {
        return `${val.name}: ${val.message}\n${val.stack}`;
    }
    // TODO we could test for more things here, like `Set`s and `Map`s.
    return className;
}
/**
* A struct that allows JS functions (specifically SlickGrid functions) to access internal Rust data.
*/
export class DataConnector {

    static __wrap(ptr) {
        const obj = Object.create(DataConnector.prototype);
        obj.ptr = ptr;

        return obj;
    }

    free() {
        const ptr = this.ptr;
        this.ptr = 0;

        wasm.__wbg_dataconnector_free(ptr);
    }
    /**
    * The number of data objects not filtered
    * Note: A function required by SlickGrid
    * @returns {number}
    */
    getLength() {
        const ret = wasm.dataconnector_getLength(this.ptr);
        return ret >>> 0;
    }
    /**
    * The item corresponding to the index as filtered and sorted
    * Note: A function required by SlickGrid
    * @param {number} index
    * @returns {any}
    */
    getItem(index) {
        const ret = wasm.dataconnector_getItem(this.ptr, index);
        return takeObject(ret);
    }
    /**
    * @param {number} id
    * @returns {any}
    */
    getRow(id) {
        const ret = wasm.dataconnector_getRow(this.ptr, id);
        return takeObject(ret);
    }
    /**
    * @param {string} column_name
    */
    set_sort_column(column_name) {
        wasm.dataconnector_set_sort_column(this.ptr, passStringToWasm(column_name), WASM_VECTOR_LEN);
    }
    /**
    * @param {boolean} ascending
    */
    set_sort_ascending(ascending) {
        wasm.dataconnector_set_sort_ascending(this.ptr, ascending);
    }
}

function init(module) {
    if (typeof module === 'undefined') {
        module = import.meta.url.replace(/\.js$/, '_bg.wasm');
    }
    let result;
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbindgen_object_drop_ref = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbindgen_cb_drop = function(arg0) {
        const obj = takeObject(arg0).original;
        if (obj.cnt-- == 1) {
            obj.a = 0;
            return true;
        }
        const ret = false;
        return ret;
    };
    imports.wbg.__wbindgen_string_new = function(arg0, arg1) {
        const ret = getStringFromWasm(arg0, arg1);
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_object_clone_ref = function(arg0) {
        const ret = getObject(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_number_new = function(arg0) {
        const ret = arg0;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_is_undefined = function(arg0) {
        const ret = getObject(arg0) === undefined;
        return ret;
    };
    imports.wbg.__wbg_getColumns_7c2461a3fdad1189 = function(arg0) {
        const ret = getObject(arg0).getColumns();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_getActiveCell_6ec9066f5e519b69 = function(arg0) {
        const ret = getObject(arg0).getActiveCell();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_invalidate_4fed3363277e247c = function(arg0) {
        getObject(arg0).invalidate();
    };
    imports.wbg.__wbg_getData_a74083deb1741299 = function(arg0) {
        const ret = getObject(arg0).getData();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_setActiveCell_edfc28a66d0e8383 = function(arg0, arg1, arg2) {
        getObject(arg0).setActiveCell(arg1 >>> 0, arg2 >>> 0);
    };
    imports.wbg.__wbg_getCellEditor_bc2ee92da67f0ee0 = function(arg0) {
        const ret = getObject(arg0).getCellEditor();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_getActiveCellNode_e7dd7fbdb35de64b = function(arg0) {
        const ret = getObject(arg0).getActiveCellNode();
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_is_null = function(arg0) {
        const ret = getObject(arg0) === null;
        return ret;
    };
    imports.wbg.__wbg_new_c0087c07e003114d = function(arg0) {
        const ret = new ol.Map(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_addInteraction_cb3108c033f56838 = function(arg0, arg1) {
        getObject(arg0).addInteraction(takeObject(arg1));
    };
    imports.wbg.__wbg_getLayers_7b32aea972dc5199 = function(arg0) {
        const ret = getObject(arg0).getLayers();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_addLayer_a22db0c934982635 = function(arg0, arg1) {
        getObject(arg0).addLayer(takeObject(arg1));
    };
    imports.wbg.__wbg_getView_5499e1ed550228f1 = function(arg0) {
        const ret = getObject(arg0).getView();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_forEachFeatureAtPixel_11d8a773e09ee4df = function(arg0, arg1, arg2) {
        const ret = getObject(arg0).forEachFeatureAtPixel(takeObject(arg1), takeObject(arg2));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_on_7b1c288bc6ab8a6c = function(arg0, arg1, arg2, arg3) {
        getObject(arg0).on(getStringFromWasm(arg1, arg2), getObject(arg3));
    };
    imports.wbg.__wbg_getArray_2b51c96c5d8687d6 = function(arg0) {
        const ret = getObject(arg0).getArray();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_getCenter_eb473c640d814a40 = function(arg0) {
        const ret = ol.extent.getCenter(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_9475b76ece0cdc4c = function(arg0) {
        const ret = new ol.Projection(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_9d5fcbd15f26057f = function(arg0) {
        const ret = new ol.View(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_calculateExtent_1a1f34da1b90da66 = function(arg0) {
        const ret = getObject(arg0).calculateExtent();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_33e694ad3cdd96bc = function(arg0) {
        const ret = new ol.Modify(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_ecbba0d46d37413b = function(arg0) {
        const ret = new ol.ImageStaticSource(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_68494ef1833b51d7 = function(arg0) {
        const ret = new ol.ImageLayer(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_860d9b50cb4ae9d8 = function(arg0) {
        const ret = new ol.VectorSource(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_935403eca595aa9a = function(arg0) {
        const ret = new ol.VectorLayer(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_da7a066e236002a2 = function(arg0) {
        const ret = new ol.Feature(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_get_ca65adc8070b4c73 = function(arg0, arg1, arg2) {
        const ret = getObject(arg0).get(getStringFromWasm(arg1, arg2));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_setId_c3b61e707f9119ed = function(arg0, arg1) {
        getObject(arg0).setId(arg1 >>> 0);
    };
    imports.wbg.__wbg_new_ae0efb8d637b1a7a = function(arg0) {
        const ret = new ol.Point(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_26ca821a70ec311a = function(arg0) {
        const ret = new ol.Style(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_bf5234f82a55a94a = function(arg0) {
        const ret = new ol.Icon(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_dataconnector_new = function(arg0) {
        const ret = DataConnector.__wrap(arg0);
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_e2c9b950eff55c7d = function(arg0, arg1, arg2, arg3, arg4) {
        const v0 = getStringFromWasm(arg0, arg1).slice();
        wasm.__wbindgen_free(arg0, arg1 * 1);
        const ret = new Slick.Grid(v0, takeObject(arg2), takeObject(arg3), takeObject(arg4));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_setSelectionModel_2f50f918d434cdac = function(arg0, arg1) {
        getObject(arg0).setSelectionModel(takeObject(arg1));
    };
    imports.wbg.__wbg_registerPlugin_a80c3e07abf42036 = function(arg0, arg1) {
        getObject(arg0).registerPlugin(takeObject(arg1));
    };
    imports.wbg.__wbg_getSelectedRows_caa7d63546f0cd29 = function(arg0) {
        const ret = getObject(arg0).getSelectedRows();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_setSelectedRows_b040fc1e645988b8 = function(arg0, arg1) {
        getObject(arg0).setSelectedRows(takeObject(arg1));
    };
    imports.wbg.__wbg_new_f127ac540639e010 = function() {
        const ret = new Slick.CheckboxSelectColumn();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_getColumnDefinition_91290e931943c71e = function(arg0) {
        const ret = getObject(arg0).getColumnDefinition();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_82cec2d20e67c999 = function(arg0) {
        const ret = new Slick.RowSelectionModel(takeObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_json_serialize = function(arg0, arg1) {
        const obj = getObject(arg1);
        const ret = JSON.stringify(obj === undefined ? null : obj);
        const ret0 = passStringToWasm(ret);
        const ret1 = WASM_VECTOR_LEN;
        getInt32Memory()[arg0 / 4 + 0] = ret0;
        getInt32Memory()[arg0 / 4 + 1] = ret1;
    };
    imports.wbg.__widl_f_error_1_ = function(arg0) {
        console.error(getObject(arg0));
    };
    imports.wbg.__widl_instanceof_Window = function(arg0) {
        const ret = getObject(arg0) instanceof Window;
        return ret;
    };
    imports.wbg.__widl_f_create_element_Document = function(arg0, arg1, arg2) {
        try {
            const ret = getObject(arg0).createElement(getStringFromWasm(arg1, arg2));
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_get_element_by_id_Document = function(arg0, arg1, arg2) {
        const ret = getObject(arg0).getElementById(getStringFromWasm(arg1, arg2));
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_body_Document = function(arg0) {
        const ret = getObject(arg0).body;
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_set_attribute_Element = function(arg0, arg1, arg2, arg3, arg4) {
        try {
            getObject(arg0).setAttribute(getStringFromWasm(arg1, arg2), getStringFromWasm(arg3, arg4));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_id_Element = function(arg0, arg1) {
        const ret = getObject(arg1).id;
        const ret0 = passStringToWasm(ret);
        const ret1 = WASM_VECTOR_LEN;
        getInt32Memory()[arg0 / 4 + 0] = ret0;
        getInt32Memory()[arg0 / 4 + 1] = ret1;
    };
    imports.wbg.__widl_f_set_id_Element = function(arg0, arg1, arg2) {
        getObject(arg0).id = getStringFromWasm(arg1, arg2);
    };
    imports.wbg.__widl_f_class_name_Element = function(arg0, arg1) {
        const ret = getObject(arg1).className;
        const ret0 = passStringToWasm(ret);
        const ret1 = WASM_VECTOR_LEN;
        getInt32Memory()[arg0 / 4 + 0] = ret0;
        getInt32Memory()[arg0 / 4 + 1] = ret1;
    };
    imports.wbg.__widl_f_set_class_name_Element = function(arg0, arg1, arg2) {
        getObject(arg0).className = getStringFromWasm(arg1, arg2);
    };
    imports.wbg.__widl_f_before_with_node_1_Element = function(arg0, arg1) {
        try {
            getObject(arg0).before(getObject(arg1));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_remove_Element = function(arg0) {
        getObject(arg0).remove();
    };
    imports.wbg.__widl_f_replace_with_with_node_1_Element = function(arg0, arg1) {
        try {
            getObject(arg0).replaceWith(getObject(arg1));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_next_element_sibling_Element = function(arg0) {
        const ret = getObject(arg0).nextElementSibling;
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_append_with_node_1_Element = function(arg0, arg1) {
        try {
            getObject(arg0).append(getObject(arg1));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_first_element_child_Element = function(arg0) {
        const ret = getObject(arg0).firstElementChild;
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_last_element_child_Element = function(arg0) {
        const ret = getObject(arg0).lastElementChild;
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_child_element_count_Element = function(arg0) {
        const ret = getObject(arg0).childElementCount;
        return ret;
    };
    imports.wbg.__widl_f_new_FormData = function() {
        try {
            const ret = new FormData();
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_cookie_HTMLDocument = function(arg0, arg1) {
        try {
            const ret = getObject(arg1).cookie;
            const ret0 = passStringToWasm(ret);
            const ret1 = WASM_VECTOR_LEN;
            getInt32Memory()[arg0 / 4 + 0] = ret0;
            getInt32Memory()[arg0 / 4 + 1] = ret1;
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_click_HTMLElement = function(arg0) {
        getObject(arg0).click();
    };
    imports.wbg.__widl_f_focus_HTMLElement = function(arg0) {
        try {
            getObject(arg0).focus();
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_set_inner_text_HTMLElement = function(arg0, arg1, arg2) {
        getObject(arg0).innerText = getStringFromWasm(arg1, arg2);
    };
    imports.wbg.__widl_f_set_hidden_HTMLElement = function(arg0, arg1) {
        getObject(arg0).hidden = arg1 !== 0;
    };
    imports.wbg.__widl_f_set_onblur_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onblur = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onclick_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onclick = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onload_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onload = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onsubmit_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onsubmit = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onpointerdown_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onpointerdown = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onpointerup_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onpointerup = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onpointerenter_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onpointerenter = getObject(arg1);
    };
    imports.wbg.__widl_f_set_onpointerleave_HTMLElement = function(arg0, arg1) {
        getObject(arg0).onpointerleave = getObject(arg1);
    };
    imports.wbg.__widl_f_alert_with_message_Window = function(arg0, arg1, arg2) {
        try {
            getObject(arg0).alert(getStringFromWasm(arg1, arg2));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_open_with_url_Window = function(arg0, arg1, arg2) {
        try {
            const ret = getObject(arg0).open(getStringFromWasm(arg1, arg2));
            return isLikeNone(ret) ? 0 : addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_request_animation_frame_Window = function(arg0, arg1) {
        try {
            const ret = getObject(arg0).requestAnimationFrame(getObject(arg1));
            return ret;
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_document_Window = function(arg0) {
        const ret = getObject(arg0).document;
        return isLikeNone(ret) ? 0 : addHeapObject(ret);
    };
    imports.wbg.__widl_f_inner_width_Window = function(arg0) {
        try {
            const ret = getObject(arg0).innerWidth;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_inner_height_Window = function(arg0) {
        try {
            const ret = getObject(arg0).innerHeight;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_new_XMLHttpRequest = function() {
        try {
            const ret = new XMLHttpRequest();
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_open_with_async_XMLHttpRequest = function(arg0, arg1, arg2, arg3, arg4, arg5) {
        try {
            getObject(arg0).open(getStringFromWasm(arg1, arg2), getStringFromWasm(arg3, arg4), arg5 !== 0);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_send_with_opt_str_XMLHttpRequest = function(arg0, arg1, arg2) {
        try {
            getObject(arg0).send(arg1 === 0 ? undefined : getStringFromWasm(arg1, arg2));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_set_request_header_XMLHttpRequest = function(arg0, arg1, arg2, arg3, arg4) {
        try {
            getObject(arg0).setRequestHeader(getStringFromWasm(arg1, arg2), getStringFromWasm(arg3, arg4));
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_status_XMLHttpRequest = function(arg0) {
        try {
            const ret = getObject(arg0).status;
            return ret;
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__widl_f_response_text_XMLHttpRequest = function(arg0, arg1) {
        try {
            const ret = getObject(arg1).responseText;
            const ptr0 = isLikeNone(ret) ? 0 : passStringToWasm(ret);
            const len0 = WASM_VECTOR_LEN;
            const ret0 = ptr0;
            const ret1 = len0;
            getInt32Memory()[arg0 / 4 + 0] = ret0;
            getInt32Memory()[arg0 / 4 + 1] = ret1;
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_next_81628b1c8473d796 = function(arg0) {
        try {
            const ret = getObject(arg0).next();
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_done_cd1e7104a531367c = function(arg0) {
        const ret = getObject(arg0).done;
        return ret;
    };
    imports.wbg.__wbg_value_30dd3fdb46a8b5be = function(arg0) {
        const ret = getObject(arg0).value;
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_get_5776bf9cf68ec072 = function(arg0, arg1) {
        try {
            const ret = Reflect.get(getObject(arg0), getObject(arg1));
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_call_aa56d0132fec7569 = function(arg0, arg1) {
        try {
            const ret = getObject(arg0).call(getObject(arg1));
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_new_8caf63c832ef170c = function() {
        const ret = new Array();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_from_f808f222abb92c17 = function(arg0) {
        const ret = Array.from(getObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_forEach_e651488daf2c21cc = function(arg0, arg1, arg2) {
        const state0 = {a: arg1, b: arg2};
        const cb0 = (arg0, arg1, arg2) => {
            const a = state0.a;
            state0.a = 0;
            try {
                return __wbg_elem_binding6(a, state0.b, arg0, arg1, arg2);
            } finally {
                state0.a = a;
            }
        };
        try {
            getObject(arg0).forEach(cb0);
        } finally {
            state0.a = state0.b = 0;
        }
    };
    imports.wbg.__wbg_length_ed7702c9df8d8eba = function(arg0) {
        const ret = getObject(arg0).length;
        return ret;
    };
    imports.wbg.__wbg_push_3ed2be796ba59b8b = function(arg0, arg1) {
        const ret = getObject(arg0).push(getObject(arg1));
        return ret;
    };
    imports.wbg.__wbg_values_b6c56db88316fe68 = function(arg0) {
        const ret = getObject(arg0).values();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_newnoargs_0c3c518a7f7c56bf = function(arg0, arg1) {
        const ret = new Function(getStringFromWasm(arg0, arg1));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_call_34f87007c5d2a397 = function(arg0, arg1, arg2) {
        try {
            const ret = getObject(arg0).call(getObject(arg1), getObject(arg2));
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_call_c58528825706e9c2 = function(arg0, arg1, arg2, arg3) {
        try {
            const ret = getObject(arg0).call(getObject(arg1), getObject(arg2), getObject(arg3));
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_now_2a2b62626fe33646 = function() {
        const ret = Date.now();
        return ret;
    };
    imports.wbg.__wbg_entries_444b687e7077ae63 = function(arg0) {
        const ret = Object.entries(getObject(arg0));
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_new_de17f04ab3be4063 = function() {
        const ret = new Object();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_globalThis_4fa2faeae7a7a380 = function() {
        try {
            const ret = globalThis.globalThis;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_self_ed02073ec1d8fef4 = function() {
        try {
            const ret = self.self;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_window_356847be61f4a80f = function() {
        try {
            const ret = window.window;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_global_6580a67633b0dbc1 = function() {
        try {
            const ret = global.global;
            return addHeapObject(ret);
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_set_e11a72d9733dadef = function(arg0, arg1, arg2) {
        try {
            const ret = Reflect.set(getObject(arg0), getObject(arg1), getObject(arg2));
            return ret;
        } catch (e) {
            handleError(e)
        }
    };
    imports.wbg.__wbg_random_fa8400a56a74374c = typeof Math.random == 'function' ? Math.random : notDefined('Math.random');
    imports.wbg.__wbindgen_number_get = function(arg0, arg1) {
        const obj = getObject(arg0);
        if (typeof(obj) === 'number') return obj;
        getUint8Memory()[arg1] = 1;
        const ret = 0;
        return ret;
    };
    imports.wbg.__wbindgen_string_get = function(arg0, arg1) {
        const obj = getObject(arg0);
        if (typeof(obj) !== 'string') return 0;
        const ptr = passStringToWasm(obj);
        getUint32Memory()[arg1 / 4] = WASM_VECTOR_LEN;
        const ret = ptr;
        return ret;
    };
    imports.wbg.__wbindgen_boolean_get = function(arg0) {
        const v = getObject(arg0);
        const ret = typeof(v) === 'boolean' ? (v ? 1 : 0) : 2;
        return ret;
    };
    imports.wbg.__wbindgen_debug_string = function(arg0, arg1) {
        const ret = debugString(getObject(arg1));
        const ret0 = passStringToWasm(ret);
        const ret1 = WASM_VECTOR_LEN;
        getInt32Memory()[arg0 / 4 + 0] = ret0;
        getInt32Memory()[arg0 / 4 + 1] = ret1;
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm(arg0, arg1));
    };
    imports.wbg.__wbindgen_rethrow = function(arg0) {
        throw takeObject(arg0);
    };
    imports.wbg.__wbindgen_closure_wrapper611 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0) => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding5(a, state.b, arg0);
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(216)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper403 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0) => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding0(a, state.b, arg0);
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(179)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper405 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0) => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding4(a, state.b, arg0);
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(179)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper409 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0, arg1) => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding2(a, state.b, arg0, arg1);
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(179)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper407 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = (arg0) => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding1(a, state.b, arg0);
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(179)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };
    imports.wbg.__wbindgen_closure_wrapper609 = function(arg0, arg1, arg2) {
        const state = { a: arg0, b: arg1, cnt: 1 };
        const real = () => {
            state.cnt++;
            const a = state.a;
            state.a = 0;
            try {
                return __wbg_elem_binding3(a, state.b, );
            } finally {
                if (--state.cnt === 0) wasm.__wbg_function_table.get(216)(a, state.b);
                else state.a = a;
            }
        }
        ;
        real.original = state;
        const ret = real;
        return addHeapObject(ret);
    };

    if ((typeof URL === 'function' && module instanceof URL) || typeof module === 'string' || (typeof Request === 'function' && module instanceof Request)) {

        const response = fetch(module);
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            result = WebAssembly.instantiateStreaming(response, imports)
            .catch(e => {
                return response
                .then(r => {
                    if (r.headers.get('Content-Type') != 'application/wasm') {
                        console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);
                        return r.arrayBuffer();
                    } else {
                        throw e;
                    }
                })
                .then(bytes => WebAssembly.instantiate(bytes, imports));
            });
        } else {
            result = response
            .then(r => r.arrayBuffer())
            .then(bytes => WebAssembly.instantiate(bytes, imports));
        }
    } else {

        result = WebAssembly.instantiate(module, imports)
        .then(result => {
            if (result instanceof WebAssembly.Instance) {
                return { instance: result, module };
            } else {
                return result;
            }
        });
    }
    return result.then(({instance, module}) => {
        wasm = instance.exports;
        init.__wbindgen_wasm_module = module;
        wasm.__wbindgen_start();
        return wasm;
    });
}

export default init;


extern crate wee_alloc;

use std::{cell::RefCell, rc::Rc};

use maplit::hashmap;
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::{self, HtmlElement};

use mappin_lib::{
    ajax::{into_form_data, wasm_ajax_no_response},
    set_on_x,
};

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

/// Submits the login credentials to the server, checking the server's response.  Will redirect the
/// page if the the login was successful.
fn submit_login(event: JsValue) -> Result<(), JsValue> {
    let prevent_default =
        js_sys::Function::from(js_sys::Reflect::get(&event, &"preventDefault".into())?);
    prevent_default.call0(&event)?;

    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Get the input username
    let username_input = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("username_input")
            .ok_or("Username input not found")?,
    ));
    let username = js_sys::Reflect::get(&username_input.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Username not a string"))?;

    // Get the input password
    let password_input = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("password_input")
            .ok_or("Password input not found")?,
    ));
    let password = js_sys::Reflect::get(&password_input.into(), &"value".into())?
        .as_string()
        .ok_or(JsValue::from("Password not a string"))?;

    // Turn the input data into a form that the server can process
    let form_data =
        into_form_data(hashmap! {"username" => username.clone(), "password" => password});

    // Send the request to the server
    let response = wasm_ajax_no_response("/mappin/api_login", &form_data)?;

    if response.status == 200 {
        // Redirect the page on a successful login
        let location = js_sys::Reflect::get(&js_sys::global(), &"location".into())?;
        let assign = js_sys::Function::from(js_sys::Reflect::get(&location, &"assign".into())?);
        assign.call1(&location, &"/".into())?;
    } else {
        // Show an error on a failed login
        window.alert_with_message("Invalid login attempt!")?;
        reset_login_form(&username)?;
    }

    Ok(())
}

/// Clears out the form content and resets it with new inputs, carrying over the username if it was
/// set.
fn reset_login_form(username: &str) -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));

    // Set up the login form
    let login_form = HtmlElement::from(JsValue::from(
        document
            .get_element_by_id("login_form")
            .ok_or(JsValue::from("Could not find login form"))?,
    ));
    login_form.set_class_name("w3-display-middle");

    // Clear out the old login fields
    while login_form.child_element_count() > 0 {
        login_form.last_element_child().unwrap().remove();
    }

    // Set up the username input field
    let username_label = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    username_label.set_inner_text("Username:");
    username_label.set_class_name("w3-label");
    let username_input = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    username_input.set_id("username_input");
    username_input.set_class_name("w3-input");
    js_sys::Reflect::set(&username_input.clone(), &"value".into(), &username.into())?;
    username_label.append_with_node_1(&username_input)?;

    // Set up the password input field
    let password_label = HtmlElement::from(JsValue::from(document.create_element("LABEL")?));
    password_label.set_inner_text("Password:");
    password_label.set_class_name("w3-label");
    let password_input = HtmlElement::from(JsValue::from(document.create_element("INPUT")?));
    password_input.set_id("password_input");
    password_input.set_class_name("w3-input");
    js_sys::Reflect::set(&password_input.clone(), &"type".into(), &"password".into())?;
    password_label.append_with_node_1(&password_input)?;

    // Create the submit button
    let submit_button = HtmlElement::from(JsValue::from(document.create_element("BUTTON")?));
    submit_button.set_inner_text("Submit");
    submit_button.set_id("submit_button");
    submit_button.set_class_name("w3-card w3-button");
    set_on_x!(submit_button, set_onclick, submit_login, false);

    // Put the new input fields into the form
    login_form.append_with_node_1(&username_label)?;
    login_form.append_with_node_1(&password_label)?;
    login_form.append_with_node_1(&submit_button)?;

    Ok(())
}

/// Called when the wasm module is instantiated.  This is equivalent to a binary program's main
/// function.
#[wasm_bindgen(start)]
pub fn wasm_main() -> Result<(), JsValue> {
    let window = web_sys::window().ok_or("Cannot get window")?;
    let document = web_sys::HtmlDocument::from(JsValue::from(
        window.document().ok_or("Cannot get document")?,
    ));
    let body = document.body().ok_or("Cannot get body")?;

    let heading = HtmlElement::from(JsValue::from(document.create_element("H1")?));
    heading.set_inner_text("Login");
    heading.set_class_name("w3-display-topmiddle");
    body.append_with_node_1(&heading)?;

    // Create the login form
    let form = HtmlElement::from(JsValue::from(document.create_element("FORM")?));
    form.set_id("login_form");
    set_on_x!(form, set_onsubmit, submit_login, false);
    body.append_with_node_1(&form)?;
    reset_login_form("")?;

    Ok(())
}
